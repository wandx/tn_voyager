$(function () {
    // Initialize popup as usual
    $('.image-link').magnificPopup({
        type: 'image',
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below

        zoom: {
            enabled: true, // By default it's false, so don't forget to enable it

            duration: 300, // duration of the effect, in milliseconds
            easing: 'ease-in-out', // CSS transition easing function

            // The "opener" function should return the element from which popup will be zoomed in
            // and to which popup will be scaled down
            // By defailt it looks for an image tag:
            opener: function(openerElement) {
                // openerElement is the element on which popup was initialized, in this case its <a> tag
                // you don't need to add "opener" option if this code matches your needs, it's defailt one.
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }

        },
        image: {
            // options for image content type
            titleSrc: 'data-desc'
        },
        gallery: {
            // options for gallery
            enabled: false
        }


    }); // JavaScript Document


    $('.video-link').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below

        iframe:{
            markup: '<div class="mfp-iframe-scaler">'+
            '<div class="mfp-close"></div>'+
            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
            '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button

            patterns:{
                youtube: {
                    index: '/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

                    id: null, // String that splits URL in a two parts, second part should be %id%
                    // Or null - full URL will be returned
                    // Or a function that should return %id%, for example:
                    // id: function(url) { return 'parsed id'; }

                    src: '%id%?autoplay=1' // URL that will be set as a source for iframe.
                }
            },

            srcAction: 'iframe_src' // Templating object key. First part defines CSS selector, second attribute. "iframe_src" means: find "iframe" and set attribute "src".
        }


    }); // JavaScript Document
})

