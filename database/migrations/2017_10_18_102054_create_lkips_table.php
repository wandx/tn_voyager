<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLkipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lkips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('team_name');
            $table->string('team_leader');
            $table->date('team_leader_dob');
            $table->string('team_leader_address');
            $table->string('team_leader_phone');
            $table->string('team_leader_email')->unique();
            $table->string('team_leader_class');
            $table->string('team_leader_file');

            $table->string('team_member');
            $table->date('team_member_dob');
            $table->string('team_member_address');
            $table->string('team_member_phone');
            $table->string('team_member_email')->unique();
            $table->string('team_member_class');
            $table->string('team_member_file');


            $table->string('average');
            $table->string('origin');
            $table->string('origin_address');
            $table->string('judul_makalah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lkips');
    }
}
