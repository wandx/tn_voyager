<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i=0;$i<10;$i++){
            (new \TCG\Voyager\Models\Category())->newQuery()->create([
                'name' => $name = $faker->words(2,true),
                'slug' => \Illuminate\Support\Str::slug($name)
            ]);

            (new \App\Models\Tag())->newQuery()->create([
                'name' => $name = $faker->words(2,true),
                'slug' => \Illuminate\Support\Str::slug($name)
            ]);
        }
    }
}
