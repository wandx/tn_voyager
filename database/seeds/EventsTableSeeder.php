<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('events')->delete();
        
        \DB::table('events')->insert(array (
            0 => 
            array (
                'id' => 6,
                'title' => 'Event 1',
                'start_at' => '2017-09-26',
                'end_at' => '2017-09-29',
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 15:16:00',
                'desc' => 'akan diselenggarakan di gedung serbaguna',
                'color' => '#ff0055',
            ),
            1 => 
            array (
                'id' => 7,
                'title' => 'Event 2',
                'start_at' => '2017-09-30',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 15:05:28',
                'desc' => 'haha',
                'color' => '#6171f2',
            ),
            2 => 
            array (
                'id' => 8,
                'title' => 'Event 3',
                'start_at' => '2017-10-01',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 15:06:08',
                'desc' => 'tes',
                'color' => '#923c3c',
            ),
            3 => 
            array (
                'id' => 9,
                'title' => 'Event 4',
                'start_at' => '2017-11-04',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 15:07:55',
                'desc' => 'jajl',
                'color' => '#19de28',
            ),
            4 => 
            array (
                'id' => 10,
                'title' => 'Event 5',
                'start_at' => '2017-10-04',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 12:00:49',
                'desc' => 'akan diselenggarakan di gedung serbaguna',
                'color' => NULL,
            ),
            5 => 
            array (
                'id' => 11,
                'title' => 'Event 6',
                'start_at' => '2017-10-05',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 12:00:49',
                'desc' => 'akan diselenggarakan di gedung serbaguna',
                'color' => NULL,
            ),
            6 => 
            array (
                'id' => 12,
                'title' => 'Event 7',
                'start_at' => '2017-10-06',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 12:00:49',
                'desc' => 'akan diselenggarakan di gedung serbaguna',
                'color' => NULL,
            ),
            7 => 
            array (
                'id' => 13,
                'title' => 'Event 8',
                'start_at' => '2017-10-07',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 12:00:49',
                'desc' => 'akan diselenggarakan di gedung serbaguna',
                'color' => NULL,
            ),
            8 => 
            array (
                'id' => 14,
                'title' => 'Event 9',
                'start_at' => '2017-10-08',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 12:00:49',
                'desc' => 'akan diselenggarakan di gedung serbaguna',
                'color' => NULL,
            ),
            9 => 
            array (
                'id' => 15,
                'title' => 'Event 10',
                'start_at' => '2017-10-09',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 12:00:49',
                'desc' => 'akan diselenggarakan di gedung serbaguna',
                'color' => NULL,
            ),
            10 => 
            array (
                'id' => 16,
                'title' => 'Event 11',
                'start_at' => '2017-10-10',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 12:00:49',
                'desc' => 'akan diselenggarakan di gedung serbaguna',
                'color' => NULL,
            ),
            11 => 
            array (
                'id' => 17,
                'title' => 'Event 12',
                'start_at' => '2017-10-11',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 12:00:49',
                'desc' => 'akan diselenggarakan di gedung serbaguna',
                'color' => NULL,
            ),
            12 => 
            array (
                'id' => 18,
                'title' => 'Event 13',
                'start_at' => '2017-10-12',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 12:00:49',
                'desc' => 'akan diselenggarakan di gedung serbaguna',
                'color' => NULL,
            ),
            13 => 
            array (
                'id' => 19,
                'title' => 'Event 14',
                'start_at' => '2017-10-13',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 12:00:49',
                'desc' => 'akan diselenggarakan di gedung serbaguna',
                'color' => NULL,
            ),
            14 => 
            array (
                'id' => 20,
                'title' => 'Event 15',
                'start_at' => '2017-10-14',
                'end_at' => NULL,
                'created_at' => '2017-09-28 12:00:49',
                'updated_at' => '2017-09-28 12:00:49',
                'desc' => 'akan diselenggarakan di gedung serbaguna',
                'color' => NULL,
            ),
        ));
        
        
    }
}