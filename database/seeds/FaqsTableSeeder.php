<?php

use Illuminate\Database\Seeder;

class FaqsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('faqs')->delete();
        
        \DB::table('faqs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'question' => 'hai saya mau tanya nih',
                'answer' => 'ya tanyaa aja mas',
            ),
            1 => 
            array (
                'id' => 2,
                'question' => 'ini pertanyaan?',
                'answer' => 'iya mas isi pertanyaan',
            ),
        ));
        
        
    }
}