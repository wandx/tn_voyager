<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menus')->delete();
        
        \DB::table('menus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'created_at' => '2017-09-25 11:20:39',
                'updated_at' => '2017-09-25 11:20:39',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'front',
                'created_at' => '2017-11-15 00:00:31',
                'updated_at' => '2017-11-28 10:16:26',
            ),
        ));
        
        
    }
}