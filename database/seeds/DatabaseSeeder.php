<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        \Illuminate\Database\Eloquent\Model::unguard();

//        $this->call(VoyagerDatabaseSeeder::class);
        $this->call(PagesTableSeeder::class);
//        $this->call(CategoriesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
//        $this->call(PrestasiSeeder::class);
        $this->call(MenuItemsTableSeeder::class);
        $this->call(MenusTableSeeder::class);
//        $this->call(PagesTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(DataRowsTableSeeder::class);
        $this->call(DataTypesTableSeeder::class);
        $this->call(TranslationsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
//        $this->call(PermissionGroupsTableSeeder::class);
        \Illuminate\Database\Eloquent\Model::reguard();
        DB::statement("SET FOREIGN_KEY_CHECKS = 1");
        $this->call(EventsTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(FilesTableSeeder::class);
        $this->call(GalleriesTableSeeder::class);
        $this->call(JadwalsTableSeeder::class);
        $this->call(LkipsTableSeeder::class);
        $this->call(MSIsTableSeeder::class);
        $this->call(MenuHistoriesTableSeeder::class);
        $this->call(PostCommentsTableSeeder::class);
        $this->call(PostTagTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(SlidersTableSeeder::class);
        $this->call(TagsTableSeeder::class);
    }
}
