<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdditionalDataType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data_types')->insert([
            array('id' => '1','name' => 'posts','slug' => 'posts','display_name_singular' => 'Post','display_name_plural' => 'Posts','icon' => 'voyager-news','model_name' => 'App\\Models\\Post','policy_name' => 'TCG\\Voyager\\Policies\\PostPolicy','controller' => '','description' => '','generate_permissions' => '1','server_side' => '0','created_at' => '2017-09-25 11:20:34','updated_at' => '2017-09-25 11:20:34'),
            array('id' => '2','name' => 'pages','slug' => 'pages','display_name_singular' => 'Page','display_name_plural' => 'Pages','icon' => 'voyager-file-text','model_name' => 'App\\Models\\Page','policy_name' => NULL,'controller' => '','description' => '','generate_permissions' => '1','server_side' => '0','created_at' => '2017-09-25 11:20:34','updated_at' => '2017-09-25 11:20:34'),
            array('id' => '3','name' => 'users','slug' => 'users','display_name_singular' => 'User','display_name_plural' => 'Users','icon' => 'voyager-person','model_name' => 'App\\Models\\User','policy_name' => 'TCG\\Voyager\\Policies\\UserPolicy','controller' => '','description' => '','generate_permissions' => '1','server_side' => '0','created_at' => '2017-09-25 11:20:35','updated_at' => '2017-09-25 11:20:35'),
            array('id' => '4','name' => 'categories','slug' => 'categories','display_name_singular' => 'Category','display_name_plural' => 'Categories','icon' => 'voyager-categories','model_name' => 'TCG\\Voyager\\Models\\Category','policy_name' => NULL,'controller' => '','description' => '','generate_permissions' => '1','server_side' => '0','created_at' => '2017-09-25 11:20:35','updated_at' => '2017-09-25 11:20:35'),
            array('id' => '5','name' => 'menus','slug' => 'menus','display_name_singular' => 'Menu','display_name_plural' => 'Menus','icon' => 'voyager-list','model_name' => 'TCG\\Voyager\\Models\\Menu','policy_name' => NULL,'controller' => '','description' => '','generate_permissions' => '1','server_side' => '0','created_at' => '2017-09-25 11:20:35','updated_at' => '2017-09-25 11:20:35'),
            array('id' => '6','name' => 'roles','slug' => 'roles','display_name_singular' => 'Role','display_name_plural' => 'Roles','icon' => 'voyager-lock','model_name' => 'TCG\\Voyager\\Models\\Role','policy_name' => NULL,'controller' => '','description' => '','generate_permissions' => '1','server_side' => '0','created_at' => '2017-09-25 11:20:35','updated_at' => '2017-09-25 11:20:35'),
            array('id' => '8','name' => 'galleries','slug' => 'galleries','display_name_singular' => 'Gallery','display_name_plural' => 'Galleries','icon' => 'voyager-photo','model_name' => 'App\\Models\\Gallery','policy_name' => NULL,'controller' => NULL,'description' => NULL,'generate_permissions' => '1','server_side' => '0','created_at' => '2017-09-25 11:38:29','updated_at' => '2017-09-26 16:06:50'),
            array('id' => '10','name' => 'achievements','slug' => 'achievements','display_name_singular' => 'Achievement','display_name_plural' => 'Achievements','icon' => 'voyager-trophy','model_name' => 'App\\Models\\Achievement','policy_name' => NULL,'controller' => NULL,'description' => NULL,'generate_permissions' => '1','server_side' => '0','created_at' => '2017-09-26 15:56:58','updated_at' => '2017-09-26 15:56:58'),
            array('id' => '12','name' => 'activities','slug' => 'activities','display_name_singular' => 'Activity','display_name_plural' => 'Activities','icon' => 'voyager-trophy','model_name' => 'App\\Models\\Activity','policy_name' => NULL,'controller' => NULL,'description' => NULL,'generate_permissions' => '1','server_side' => '0','created_at' => '2017-09-26 15:59:05','updated_at' => '2017-09-26 15:59:05'),
            array('id' => '13','name' => 'faqs','slug' => 'faqs','display_name_singular' => 'Faq','display_name_plural' => 'Faqs','icon' => 'voyager-megaphone','model_name' => 'App\\Models\\Faq','policy_name' => NULL,'controller' => NULL,'description' => NULL,'generate_permissions' => '1','server_side' => '0','created_at' => '2017-09-26 16:00:06','updated_at' => '2017-09-26 16:00:06'),
            array('id' => '14','name' => 'files','slug' => 'files','display_name_singular' => 'File','display_name_plural' => 'Files','icon' => 'voyager-harddrive','model_name' => 'App\\Models\\File','policy_name' => NULL,'controller' => NULL,'description' => NULL,'generate_permissions' => '1','server_side' => '0','created_at' => '2017-09-26 16:01:20','updated_at' => '2017-09-26 16:01:20'),
            array('id' => '15','name' => 'sliders','slug' => 'sliders','display_name_singular' => 'Slider','display_name_plural' => 'Sliders','icon' => 'voyager-photos','model_name' => 'App\\Models\\Slider','policy_name' => NULL,'controller' => NULL,'description' => NULL,'generate_permissions' => '1','server_side' => '0','created_at' => '2017-09-26 16:04:00','updated_at' => '2017-09-26 16:04:00'),
            array('id' => '16','name' => 'tags','slug' => 'tags','display_name_singular' => 'Tag','display_name_plural' => 'Tags','icon' => 'voyager-tag','model_name' => 'App\\Models\\Tag','policy_name' => NULL,'controller' => NULL,'description' => NULL,'generate_permissions' => '1','server_side' => '0','created_at' => '2017-09-26 16:04:52','updated_at' => '2017-09-26 16:04:52')
        ]);
    }
}
