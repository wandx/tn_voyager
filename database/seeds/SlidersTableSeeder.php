<?php

use Illuminate\Database\Seeder;

class SlidersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sliders')->delete();
        
        \DB::table('sliders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'SMA Taruna Nusantara',
                'subtitle' => 'Satu Nusa - Satu Bangsa - Satu Bahasa',
                'image' => 'sliders/October2017/C_nU-AOU0AA7xtC1.jpg',
                'url' => 'http://203.130.238.189:6080/blog/msi-and-lkip-2017',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'SMA Taruna Nusantara',
                'subtitle' => 'Satu Nusa - Satu Bangsa - Satu Bahasa',
                'image' => 'sliders/October2017/sma taruna nusantara.jpg',
                'url' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'SMA Taruna Nusantara',
                'subtitle' => 'Satu Nusa - Satu Bangsa - Satu Bahasa',
                'image' => 'sliders/October2017/11270215-911115162265195-3348623862972551528-o-56c937a8927a61211255d11b.jpg',
                'url' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'SMA Taruna Nusantara',
                'subtitle' => 'Satu Nusa - Satu Bangsa - Satu Bahasa',
                'image' => 'sliders/October2017/2684cce25d8a7738b39e32b8359e23ab.jpg',
                'url' => NULL,
            ),
        ));
        
        
    }
}