<?php

use Illuminate\Database\Seeder;

class PrestasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<10;$i++){
            factory(\App\Models\Achievement::class)->create();
        }
        for($i=0;$i<10;$i++){
            factory(\App\Models\Activity::class)->create();
        }

    }
}
