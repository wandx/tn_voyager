<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2017-09-26 20:28:06',
                'updated_at' => '2017-09-26 20:28:06',
                'permission_group_id' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2017-09-26 20:28:06',
                'updated_at' => '2017-09-26 20:28:06',
                'permission_group_id' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2017-09-26 20:28:06',
                'updated_at' => '2017-09-26 20:28:06',
                'permission_group_id' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'browse_pages',
                'table_name' => 'pages',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'read_pages',
                'table_name' => 'pages',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'edit_pages',
                'table_name' => 'pages',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'add_pages',
                'table_name' => 'pages',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'delete_pages',
                'table_name' => 'pages',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'browse_posts',
                'table_name' => 'posts',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'read_posts',
                'table_name' => 'posts',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'edit_posts',
                'table_name' => 'posts',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'add_posts',
                'table_name' => 'posts',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'key' => 'delete_posts',
                'table_name' => 'posts',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'key' => 'browse_categories',
                'table_name' => 'categories',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'key' => 'read_categories',
                'table_name' => 'categories',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'key' => 'edit_categories',
                'table_name' => 'categories',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'key' => 'add_categories',
                'table_name' => 'categories',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'key' => 'delete_categories',
                'table_name' => 'categories',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2017-09-26 20:28:07',
                'updated_at' => '2017-09-26 20:28:07',
                'permission_group_id' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'key' => 'browse_achievements',
                'table_name' => 'achievements',
                'created_at' => '2017-09-26 21:00:20',
                'updated_at' => '2017-09-26 21:00:20',
                'permission_group_id' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'key' => 'read_achievements',
                'table_name' => 'achievements',
                'created_at' => '2017-09-26 21:00:20',
                'updated_at' => '2017-09-26 21:00:20',
                'permission_group_id' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'key' => 'edit_achievements',
                'table_name' => 'achievements',
                'created_at' => '2017-09-26 21:00:20',
                'updated_at' => '2017-09-26 21:00:20',
                'permission_group_id' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'key' => 'add_achievements',
                'table_name' => 'achievements',
                'created_at' => '2017-09-26 21:00:20',
                'updated_at' => '2017-09-26 21:00:20',
                'permission_group_id' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'key' => 'delete_achievements',
                'table_name' => 'achievements',
                'created_at' => '2017-09-26 21:00:20',
                'updated_at' => '2017-09-26 21:00:20',
                'permission_group_id' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'key' => 'browse_activities',
                'table_name' => 'activities',
                'created_at' => '2017-09-26 21:00:54',
                'updated_at' => '2017-09-26 21:00:54',
                'permission_group_id' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'key' => 'read_activities',
                'table_name' => 'activities',
                'created_at' => '2017-09-26 21:00:54',
                'updated_at' => '2017-09-26 21:00:54',
                'permission_group_id' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'key' => 'edit_activities',
                'table_name' => 'activities',
                'created_at' => '2017-09-26 21:00:54',
                'updated_at' => '2017-09-26 21:00:54',
                'permission_group_id' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'key' => 'add_activities',
                'table_name' => 'activities',
                'created_at' => '2017-09-26 21:00:54',
                'updated_at' => '2017-09-26 21:00:54',
                'permission_group_id' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'key' => 'delete_activities',
                'table_name' => 'activities',
                'created_at' => '2017-09-26 21:00:54',
                'updated_at' => '2017-09-26 21:00:54',
                'permission_group_id' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'key' => 'browse_faqs',
                'table_name' => 'faqs',
                'created_at' => '2017-09-26 21:02:31',
                'updated_at' => '2017-09-26 21:02:31',
                'permission_group_id' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'key' => 'read_faqs',
                'table_name' => 'faqs',
                'created_at' => '2017-09-26 21:02:31',
                'updated_at' => '2017-09-26 21:02:31',
                'permission_group_id' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'key' => 'edit_faqs',
                'table_name' => 'faqs',
                'created_at' => '2017-09-26 21:02:31',
                'updated_at' => '2017-09-26 21:02:31',
                'permission_group_id' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'key' => 'add_faqs',
                'table_name' => 'faqs',
                'created_at' => '2017-09-26 21:02:31',
                'updated_at' => '2017-09-26 21:02:31',
                'permission_group_id' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'key' => 'delete_faqs',
                'table_name' => 'faqs',
                'created_at' => '2017-09-26 21:02:31',
                'updated_at' => '2017-09-26 21:02:31',
                'permission_group_id' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'key' => 'browse_files',
                'table_name' => 'files',
                'created_at' => '2017-09-26 21:02:40',
                'updated_at' => '2017-09-26 21:02:40',
                'permission_group_id' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'key' => 'read_files',
                'table_name' => 'files',
                'created_at' => '2017-09-26 21:02:40',
                'updated_at' => '2017-09-26 21:02:40',
                'permission_group_id' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'key' => 'edit_files',
                'table_name' => 'files',
                'created_at' => '2017-09-26 21:02:40',
                'updated_at' => '2017-09-26 21:02:40',
                'permission_group_id' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'key' => 'add_files',
                'table_name' => 'files',
                'created_at' => '2017-09-26 21:02:40',
                'updated_at' => '2017-09-26 21:02:40',
                'permission_group_id' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'key' => 'delete_files',
                'table_name' => 'files',
                'created_at' => '2017-09-26 21:02:40',
                'updated_at' => '2017-09-26 21:02:40',
                'permission_group_id' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'key' => 'browse_galleries',
                'table_name' => 'galleries',
                'created_at' => '2017-09-26 21:02:48',
                'updated_at' => '2017-09-26 21:02:48',
                'permission_group_id' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'key' => 'read_galleries',
                'table_name' => 'galleries',
                'created_at' => '2017-09-26 21:02:48',
                'updated_at' => '2017-09-26 21:02:48',
                'permission_group_id' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'key' => 'edit_galleries',
                'table_name' => 'galleries',
                'created_at' => '2017-09-26 21:02:48',
                'updated_at' => '2017-09-26 21:02:48',
                'permission_group_id' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'key' => 'add_galleries',
                'table_name' => 'galleries',
                'created_at' => '2017-09-26 21:02:48',
                'updated_at' => '2017-09-26 21:02:48',
                'permission_group_id' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'key' => 'delete_galleries',
                'table_name' => 'galleries',
                'created_at' => '2017-09-26 21:02:48',
                'updated_at' => '2017-09-26 21:02:48',
                'permission_group_id' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'key' => 'browse_sliders',
                'table_name' => 'sliders',
                'created_at' => '2017-09-26 21:03:41',
                'updated_at' => '2017-09-26 21:03:41',
                'permission_group_id' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'key' => 'read_sliders',
                'table_name' => 'sliders',
                'created_at' => '2017-09-26 21:03:41',
                'updated_at' => '2017-09-26 21:03:41',
                'permission_group_id' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'key' => 'edit_sliders',
                'table_name' => 'sliders',
                'created_at' => '2017-09-26 21:03:41',
                'updated_at' => '2017-09-26 21:03:41',
                'permission_group_id' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'key' => 'add_sliders',
                'table_name' => 'sliders',
                'created_at' => '2017-09-26 21:03:41',
                'updated_at' => '2017-09-26 21:03:41',
                'permission_group_id' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'key' => 'delete_sliders',
                'table_name' => 'sliders',
                'created_at' => '2017-09-26 21:03:41',
                'updated_at' => '2017-09-26 21:03:41',
                'permission_group_id' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'key' => 'browse_tags',
                'table_name' => 'tags',
                'created_at' => '2017-09-26 21:03:49',
                'updated_at' => '2017-09-26 21:03:49',
                'permission_group_id' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'key' => 'read_tags',
                'table_name' => 'tags',
                'created_at' => '2017-09-26 21:03:49',
                'updated_at' => '2017-09-26 21:03:49',
                'permission_group_id' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'key' => 'edit_tags',
                'table_name' => 'tags',
                'created_at' => '2017-09-26 21:03:49',
                'updated_at' => '2017-09-26 21:03:49',
                'permission_group_id' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'key' => 'add_tags',
                'table_name' => 'tags',
                'created_at' => '2017-09-26 21:03:49',
                'updated_at' => '2017-09-26 21:03:49',
                'permission_group_id' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'key' => 'delete_tags',
                'table_name' => 'tags',
                'created_at' => '2017-09-26 21:03:49',
                'updated_at' => '2017-09-26 21:03:49',
                'permission_group_id' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'key' => 'browse_events',
                'table_name' => 'events',
                'created_at' => '2017-09-28 10:29:51',
                'updated_at' => '2017-09-28 10:29:51',
                'permission_group_id' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'key' => 'read_events',
                'table_name' => 'events',
                'created_at' => '2017-09-28 10:29:51',
                'updated_at' => '2017-09-28 10:29:51',
                'permission_group_id' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'key' => 'edit_events',
                'table_name' => 'events',
                'created_at' => '2017-09-28 10:29:51',
                'updated_at' => '2017-09-28 10:29:51',
                'permission_group_id' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'key' => 'add_events',
                'table_name' => 'events',
                'created_at' => '2017-09-28 10:29:51',
                'updated_at' => '2017-09-28 10:29:51',
                'permission_group_id' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'key' => 'delete_events',
                'table_name' => 'events',
                'created_at' => '2017-09-28 10:29:51',
                'updated_at' => '2017-09-28 10:29:51',
                'permission_group_id' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'key' => 'browse_jadwals',
                'table_name' => 'jadwals',
                'created_at' => '2017-09-28 13:33:16',
                'updated_at' => '2017-09-28 13:33:16',
                'permission_group_id' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'key' => 'read_jadwals',
                'table_name' => 'jadwals',
                'created_at' => '2017-09-28 13:33:16',
                'updated_at' => '2017-09-28 13:33:16',
                'permission_group_id' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'key' => 'edit_jadwals',
                'table_name' => 'jadwals',
                'created_at' => '2017-09-28 13:33:16',
                'updated_at' => '2017-09-28 13:33:16',
                'permission_group_id' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'key' => 'add_jadwals',
                'table_name' => 'jadwals',
                'created_at' => '2017-09-28 13:33:16',
                'updated_at' => '2017-09-28 13:33:16',
                'permission_group_id' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'key' => 'delete_jadwals',
                'table_name' => 'jadwals',
                'created_at' => '2017-09-28 13:33:16',
                'updated_at' => '2017-09-28 13:33:16',
                'permission_group_id' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'key' => 'browse_contacts',
                'table_name' => 'contacts',
                'created_at' => '2017-10-05 17:34:08',
                'updated_at' => '2017-10-05 17:34:08',
                'permission_group_id' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'key' => 'read_contacts',
                'table_name' => 'contacts',
                'created_at' => '2017-10-05 17:34:08',
                'updated_at' => '2017-10-05 17:34:08',
                'permission_group_id' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'key' => 'edit_contacts',
                'table_name' => 'contacts',
                'created_at' => '2017-10-05 17:34:08',
                'updated_at' => '2017-10-05 17:34:08',
                'permission_group_id' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'key' => 'add_contacts',
                'table_name' => 'contacts',
                'created_at' => '2017-10-05 17:34:08',
                'updated_at' => '2017-10-05 17:34:08',
                'permission_group_id' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'key' => 'delete_contacts',
                'table_name' => 'contacts',
                'created_at' => '2017-10-05 17:34:08',
                'updated_at' => '2017-10-05 17:34:08',
                'permission_group_id' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'key' => 'browse_m_s_i_descs',
                'table_name' => 'm_s_i_descs',
                'created_at' => '2017-10-17 18:10:35',
                'updated_at' => '2017-10-17 18:10:35',
                'permission_group_id' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'key' => 'read_m_s_i_descs',
                'table_name' => 'm_s_i_descs',
                'created_at' => '2017-10-17 18:10:35',
                'updated_at' => '2017-10-17 18:10:35',
                'permission_group_id' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'key' => 'edit_m_s_i_descs',
                'table_name' => 'm_s_i_descs',
                'created_at' => '2017-10-17 18:10:35',
                'updated_at' => '2017-10-17 18:10:35',
                'permission_group_id' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'key' => 'add_m_s_i_descs',
                'table_name' => 'm_s_i_descs',
                'created_at' => '2017-10-17 18:10:35',
                'updated_at' => '2017-10-17 18:10:35',
                'permission_group_id' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'key' => 'delete_m_s_i_descs',
                'table_name' => 'm_s_i_descs',
                'created_at' => '2017-10-17 18:10:35',
                'updated_at' => '2017-10-17 18:10:35',
                'permission_group_id' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'key' => 'browse_m_s_is',
                'table_name' => 'm_s_is',
                'created_at' => '2017-10-17 18:28:08',
                'updated_at' => '2017-10-17 18:28:08',
                'permission_group_id' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'key' => 'read_m_s_is',
                'table_name' => 'm_s_is',
                'created_at' => '2017-10-17 18:28:08',
                'updated_at' => '2017-10-17 18:28:08',
                'permission_group_id' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'key' => 'edit_m_s_is',
                'table_name' => 'm_s_is',
                'created_at' => '2017-10-17 18:28:08',
                'updated_at' => '2017-10-17 18:28:08',
                'permission_group_id' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'key' => 'add_m_s_is',
                'table_name' => 'm_s_is',
                'created_at' => '2017-10-17 18:28:08',
                'updated_at' => '2017-10-17 18:28:08',
                'permission_group_id' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'key' => 'delete_m_s_is',
                'table_name' => 'm_s_is',
                'created_at' => '2017-10-17 18:28:08',
                'updated_at' => '2017-10-17 18:28:08',
                'permission_group_id' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'key' => 'browse_lkip_descs',
                'table_name' => 'lkip_descs',
                'created_at' => '2017-10-18 12:24:39',
                'updated_at' => '2017-10-18 12:24:39',
                'permission_group_id' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'key' => 'read_lkip_descs',
                'table_name' => 'lkip_descs',
                'created_at' => '2017-10-18 12:24:39',
                'updated_at' => '2017-10-18 12:24:39',
                'permission_group_id' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'key' => 'edit_lkip_descs',
                'table_name' => 'lkip_descs',
                'created_at' => '2017-10-18 12:24:39',
                'updated_at' => '2017-10-18 12:24:39',
                'permission_group_id' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'key' => 'add_lkip_descs',
                'table_name' => 'lkip_descs',
                'created_at' => '2017-10-18 12:24:39',
                'updated_at' => '2017-10-18 12:24:39',
                'permission_group_id' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'key' => 'delete_lkip_descs',
                'table_name' => 'lkip_descs',
                'created_at' => '2017-10-18 12:24:39',
                'updated_at' => '2017-10-18 12:24:39',
                'permission_group_id' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'key' => 'browse_lkips',
                'table_name' => 'lkips',
                'created_at' => '2017-10-18 12:25:43',
                'updated_at' => '2017-10-18 12:25:43',
                'permission_group_id' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'key' => 'read_lkips',
                'table_name' => 'lkips',
                'created_at' => '2017-10-18 12:25:43',
                'updated_at' => '2017-10-18 12:25:43',
                'permission_group_id' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'key' => 'edit_lkips',
                'table_name' => 'lkips',
                'created_at' => '2017-10-18 12:25:43',
                'updated_at' => '2017-10-18 12:25:43',
                'permission_group_id' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'key' => 'add_lkips',
                'table_name' => 'lkips',
                'created_at' => '2017-10-18 12:25:43',
                'updated_at' => '2017-10-18 12:25:43',
                'permission_group_id' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'key' => 'delete_lkips',
                'table_name' => 'lkips',
                'created_at' => '2017-10-18 12:25:43',
                'updated_at' => '2017-10-18 12:25:43',
                'permission_group_id' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'key' => 'browse_additional_menus',
                'table_name' => 'additional_menus',
                'created_at' => '2017-11-13 15:18:14',
                'updated_at' => '2017-11-13 15:18:14',
                'permission_group_id' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'key' => 'read_additional_menus',
                'table_name' => 'additional_menus',
                'created_at' => '2017-11-13 15:18:14',
                'updated_at' => '2017-11-13 15:18:14',
                'permission_group_id' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'key' => 'edit_additional_menus',
                'table_name' => 'additional_menus',
                'created_at' => '2017-11-13 15:18:14',
                'updated_at' => '2017-11-13 15:18:14',
                'permission_group_id' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'key' => 'add_additional_menus',
                'table_name' => 'additional_menus',
                'created_at' => '2017-11-13 15:18:14',
                'updated_at' => '2017-11-13 15:18:14',
                'permission_group_id' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'key' => 'delete_additional_menus',
                'table_name' => 'additional_menus',
                'created_at' => '2017-11-13 15:18:14',
                'updated_at' => '2017-11-13 15:18:14',
                'permission_group_id' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'key' => 'browse_menu_histories',
                'table_name' => 'menu_histories',
                'created_at' => '2018-01-29 10:25:32',
                'updated_at' => '2018-01-29 10:25:32',
                'permission_group_id' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'key' => 'read_menu_histories',
                'table_name' => 'menu_histories',
                'created_at' => '2018-01-29 10:25:32',
                'updated_at' => '2018-01-29 10:25:32',
                'permission_group_id' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'key' => 'edit_menu_histories',
                'table_name' => 'menu_histories',
                'created_at' => '2018-01-29 10:25:32',
                'updated_at' => '2018-01-29 10:25:32',
                'permission_group_id' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'key' => 'add_menu_histories',
                'table_name' => 'menu_histories',
                'created_at' => '2018-01-29 10:25:32',
                'updated_at' => '2018-01-29 10:25:32',
                'permission_group_id' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'key' => 'delete_menu_histories',
                'table_name' => 'menu_histories',
                'created_at' => '2018-01-29 10:25:32',
                'updated_at' => '2018-01-29 10:25:32',
                'permission_group_id' => NULL,
            ),
        ));
        
        
    }
}