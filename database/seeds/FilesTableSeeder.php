<?php

use Illuminate\Database\Seeder;

class FilesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('files')->delete();
        
        \DB::table('files')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'ewftgewrtgew ge',
                'description' => 'wrgwrg wrgwrgwr',
                'file' => '[{"download_link":"files\\/September2017\\/tkFF7y1aa07B8XP2j18d.jpg","original_name":"post4.jpg"}]',
                'created_at' => '2017-09-27 00:09:25',
                'updated_at' => '2017-09-27 00:09:25',
                'user_id' => 1,
                'status' => 'published',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'etrhetrhet',
                'description' => 'hethetheth',
                'file' => '[{"download_link":"files\\/September2017\\/4KR9GyluzRtOoSXXC1fT.jpg","original_name":"post4.jpg"}]',
                'created_at' => '2017-09-27 00:10:13',
                'updated_at' => '2017-09-27 00:10:13',
                'user_id' => 1,
                'status' => 'published',
            ),
        ));
        
        
    }
}