<?php

use Illuminate\Database\Seeder;

class MSIsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('m_s_is')->delete();
        
        \DB::table('m_s_is')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'ito',
                'dob' => '1990-03-07',
                'address' => 'sajfkjs',
                'phone' => '768669787',
                'email' => 'sdsakj@asasf.com',
                'origin' => 'taruna nusantara',
                'origin_address' => 'sfafa',
                'class' => 'VII',
                'file' => 'msi/kp/ito_4ik0pp.jpg',
                'created_at' => '2017-12-03 10:18:10',
                'updated_at' => '2017-12-03 10:18:10',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'coba coba',
                'dob' => '2017-12-28',
                'address' => 'magelang',
                'phone' => '6768798921',
                'email' => 'adjkjd@dkajd.com',
                'origin' => 'hdkahdkad',
                'origin_address' => 'kadjkajdkaj',
                'class' => 'VIII',
                'file' => 'msi/kp/coba-coba_hI4PIA.jpg',
                'created_at' => '2017-12-28 08:11:45',
                'updated_at' => '2017-12-28 08:11:45',
            ),
        ));
        
        
    }
}