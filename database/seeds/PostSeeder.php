<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<10;$i++){
            factory(\App\Models\Post::class)->create()->each(function($model){
                try{
                    $model->tags()->attach(rand(1,10));
                    $model->tags()->attach(rand(1,10));
                    $model->tags()->attach(rand(1,10));
                    $model->tags()->attach(rand(1,10));
                    $model->tags()->attach(rand(1,10));
                    $model->tags()->attach(rand(1,10));
                }catch (Exception $e){}

            });
        }

    }
}
