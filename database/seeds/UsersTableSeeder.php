<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 1,
                'name' => 'wandy purnomo',
                'email' => 'wandx@sys.com',
                'avatar' => 'users/September2017/blog-img-3-cropped.jpg',
                'password' => '$2y$10$iK70XxF4RFh3OBFKkWegOOmBgzX16ZQbUhMnqx.RsBcPdEAL//4j2',
                'remember_token' => '7GdWs9qzc9D7zuz1iTHzLpBn4OylBJjFgjF4nSkStQihkiKg5EocnWGV9G15',
                'created_at' => '2017-09-26 20:28:37',
                'updated_at' => '2017-09-26 21:05:55',
            ),
            1 => 
            array (
                'id' => 2,
                'role_id' => 1,
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'avatar' => 'users/December2017/lambangtn-kecil.jpg',
                'password' => '$2y$10$xnt.Jxv4ngS5BhBNo3FUbuyZV6x5Ejo/GSID/uYOlG2UP9liCBBz6',
                'remember_token' => 'J41tytzGjRqEkGupKWhAKtEw0pRVqI4yrloEZz6yL4RjrH8ThphSkD2k929h',
                'created_at' => '2017-09-26 21:05:30',
                'updated_at' => '2017-12-07 19:12:40',
            ),
        ));
        
        
    }
}