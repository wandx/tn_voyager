<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdditionalMenu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
            array('id' => '1','name' => 'admin','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-25 11:20:39')
        ]);
    }
}
