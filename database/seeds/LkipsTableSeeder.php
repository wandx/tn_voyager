<?php

use Illuminate\Database\Seeder;

class LkipsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lkips')->delete();
        
        \DB::table('lkips')->insert(array (
            0 => 
            array (
                'id' => 1,
                'team_name' => 'fdsfsdf',
                'team_leader' => 'dsfsdf',
                'team_leader_dob' => '1990-03-07',
                'team_leader_address' => 'dsfsdf',
                'team_leader_phone' => '897987',
                'team_leader_email' => 'sfk@sda.cpm',
                'team_leader_class' => 'VII',
                'team_leader_file' => 'lkip/kp/dsfsdf_0mW5iS.jpg',
                'team_member' => 'dsfsdf',
                'team_member_dob' => '1990-03-07',
                'team_member_address' => 'djsljhdaj',
                'team_member_phone' => '87987897',
                'team_member_email' => 'sdhjskh@sd.com',
                'team_member_class' => 'vii',
                'team_member_file' => 'lkip/kp/asmfsanjkh_VKZmx2.jpg',
                'average' => '80',
                'origin' => 'SDSAKJDH',
                'origin_address' => 'SADASKJD',
                'judul_makalah' => 'dsvsdvsd',
                'created_at' => '2017-12-03 10:20:42',
                'updated_at' => '2017-12-03 10:20:42',
            ),
        ));
        
        
    }
}