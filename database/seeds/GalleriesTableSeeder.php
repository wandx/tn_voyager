<?php

use Illuminate\Database\Seeder;

class GalleriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('galleries')->delete();
        
        \DB::table('galleries')->insert(array (
            0 => 
            array (
                'id' => 2,
                'video' => 2,
                'path' => 'galleries/November2017/12.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => 'test',
                'created_at' => NULL,
                'updated_at' => '2018-01-10 11:15:10',
            ),
            1 => 
            array (
                'id' => 3,
                'video' => 2,
                'path' => 'galleries/November2017/38.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 4,
                'video' => 2,
                'path' => 'galleries/November2017/13.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => 'hakkke hokkya',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 5,
                'video' => 2,
                'path' => 'galleries/November2017/16143588_1323328907710483_3471904239378490781_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 6,
                'video' => 2,
                'path' => 'galleries/November2017/16179725_1330970280279679_3953621406391048242_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 7,
                'video' => 2,
                'path' => 'galleries/November2017/17311007_1374705662572807_568823797129732300_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 8,
                'video' => 2,
                'path' => 'galleries/November2017/17359210_1381127568597283_1193453595737597972_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 9,
                'video' => 2,
                'path' => 'galleries/November2017/17434728_1384118994964807_2483932956721083675_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 10,
                'video' => 2,
                'path' => 'galleries/November2017/17492843_1386496204727086_596976204568299151_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 11,
                'video' => 2,
                'path' => 'galleries/November2017/17966073_1411715285538511_5753786457210696328_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 12,
                'video' => 2,
                'path' => 'galleries/November2017/18055950_1419643111412395_7962590271452826124_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 13,
                'video' => 2,
                'path' => 'galleries/November2017/18121984_1424104874299552_6275282344198738600_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 14,
                'video' => 2,
                'path' => 'galleries/November2017/18010243_1424213804288659_4675116271879438865_n.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 15,
                'video' => 2,
                'path' => 'galleries/November2017/17991602_1425099694200070_1293698602339015353_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 16,
                'video' => 2,
                'path' => 'galleries/November2017/18192470_1428026557240717_7649852062005065494_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 17,
                'video' => 2,
                'path' => 'galleries/November2017/18320839_1431813616862011_523299012349537480_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 18,
                'video' => 2,
                'path' => 'galleries/November2017/18279074_1431818186861554_584577448736080406_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 19,
                'video' => 2,
                'path' => 'galleries/November2017/18422095_1437817629594943_7754908797891280720_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 20,
                'video' => 2,
                'path' => 'galleries/November2017/18320756_1437826776260695_4731469921185318740_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 21,
                'video' => 2,
                'path' => 'galleries/November2017/18623563_1449845598392146_7921493185043791638_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 22,
                'video' => 2,
                'path' => 'galleries/November2017/18891780_1467545796622126_3858518986532520391_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 23,
                'video' => 2,
                'path' => 'galleries/November2017/19023517_1472330642810308_6457897728385643473_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 24,
                'video' => 2,
                'path' => 'galleries/November2017/19055786_1472352709474768_6546936295647947174_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 25,
                'video' => 2,
                'path' => 'galleries/November2017/20045636_1509273522449353_6981115014040989605_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 26,
                'video' => 2,
                'path' => 'galleries/November2017/20247548_1515867651789940_2157941573008465413_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 27,
                'video' => 2,
                'path' => 'galleries/November2017/20369141_1521229391253766_3906441001688221568_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 28,
                'video' => 2,
                'path' => 'galleries/November2017/20451640_1524152130961492_7369326543193509718_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 29,
                'video' => 2,
                'path' => 'galleries/November2017/20545483_1527526310624074_3687600368564031500_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 30,
                'video' => 2,
                'path' => 'galleries/November2017/20507243_1527577200618985_6090969593453554013_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 31,
                'video' => 2,
                'path' => 'galleries/November2017/20818809_1536687983041240_2791727823844570850_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 32,
                'video' => 2,
                'path' => 'galleries/November2017/20746277_1538743869502318_3726060048495993932_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 33,
                'video' => 2,
                'path' => 'galleries/November2017/20861527_1541220119254693_5853279880500603981_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 34,
                'video' => 2,
                'path' => 'galleries/November2017/20861660_1541772955866076_7740393644726525036_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 35,
                'video' => 2,
                'path' => 'galleries/November2017/23668778_1623043324405705_1869275288000188210_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => '0',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 36,
                'video' => 2,
                'path' => 'galleries/November2017/23592006_1603189996408295_7733795921048135703_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => 'hakke hokkya',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 37,
                'video' => 2,
                'path' => 'galleries/November2017/23593669_1625214917521879_3668293554028934585_o.jpg',
                'video_url' => NULL,
                'image_url' => NULL,
                'caption' => 'zzx',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 38,
                'video' => 2,
                'path' => NULL,
                'video_url' => NULL,
                'image_url' => 'http://ksp.go.id/wp-content/uploads/2017/01/Pola-Pembelajaran-SMA-Taruna-Nusantara-Siap-Hadapi-Persaingan-Global-3.jpg',
                'caption' => 'kunjungan presiden RI Joko Widodo',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}