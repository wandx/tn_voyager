<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdditionalSetting extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            array('id' => '1','key' => 'site.title','display_name' => 'Site Title','value' => 'Taruna Nusantara','details' => '','type' => 'text','order' => '1','group' => 'Site'),
            array('id' => '2','key' => 'site.description','display_name' => 'Site Description','value' => 'Site Description','details' => '','type' => 'text','order' => '2','group' => 'Site'),
            array('id' => '3','key' => 'site.hotline','display_name' => 'Hotline','value' => '77089707','details' => '','type' => 'text','order' => '4','group' => 'Site'),
            array('id' => '4','key' => 'site.address','display_name' => 'Alamat','value' => 'fgewgweg, wergergewr, wergrwg','details' => '','type' => 'text','order' => '5','group' => 'Site'),
            array('id' => '5','key' => 'site.jam_kerja','display_name' => 'Jam Kerja','value' => 'Senin - Jumat 07:00AM - 04:00PM','details' => '','type' => 'text','order' => '6','group' => 'Site'),
            array('id' => '6','key' => 'site.logo','display_name' => 'Site Logo','value' => '','details' => '','type' => 'image','order' => '3','group' => 'Site'),
            array('id' => '7','key' => 'site.google_analytics_tracking_id','display_name' => 'Google Analytics Tracking ID','value' => '','details' => '','type' => 'text','order' => '4','group' => 'Site'),
            array('id' => '8','key' => 'admin.bg_image','display_name' => 'Admin Background Image','value' => '','details' => '','type' => 'image','order' => '5','group' => 'Admin'),
            array('id' => '9','key' => 'admin.title','display_name' => 'Admin Title','value' => 'TarunaNusantara','details' => '','type' => 'text','order' => '1','group' => 'Admin'),
            array('id' => '10','key' => 'admin.description','display_name' => 'Admin Description','value' => 'Selamat datang di halaman administrator tarunanusantara','details' => '','type' => 'text','order' => '2','group' => 'Admin'),
            array('id' => '11','key' => 'admin.loader','display_name' => 'Admin Loader','value' => '','details' => '','type' => 'image','order' => '3','group' => 'Admin'),
            array('id' => '12','key' => 'admin.icon_image','display_name' => 'Admin Icon Image','value' => '','details' => '','type' => 'image','order' => '4','group' => 'Admin'),
            array('id' => '13','key' => 'admin.google_analytics_client_id','display_name' => 'Google Analytics Client ID (used for admin dashboard)','value' => '','details' => '','type' => 'text','order' => '1','group' => 'Admin')
        ]);
    }
}
