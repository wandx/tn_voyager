<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdditionalMenuItem extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_items')->insert([
            array('id' => '1','menu_id' => '1','title' => 'Dashboard','url' => '','target' => '_self','icon_class' => 'voyager-boat','color' => NULL,'parent_id' => NULL,'order' => '1','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-25 11:20:39','route' => 'voyager.dashboard','parameters' => NULL),
            array('id' => '2','menu_id' => '1','title' => 'Media','url' => '','target' => '_self','icon_class' => 'voyager-images','color' => NULL,'parent_id' => NULL,'order' => '4','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-26 16:54:49','route' => 'voyager.media.index','parameters' => NULL),
            array('id' => '3','menu_id' => '1','title' => 'Posts','url' => '','target' => '_self','icon_class' => 'voyager-news','color' => NULL,'parent_id' => NULL,'order' => '5','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-26 16:54:49','route' => 'voyager.posts.index','parameters' => NULL),
            array('id' => '4','menu_id' => '1','title' => 'Users','url' => '','target' => '_self','icon_class' => 'voyager-person','color' => NULL,'parent_id' => NULL,'order' => '3','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-25 11:20:39','route' => 'voyager.users.index','parameters' => NULL),
            array('id' => '5','menu_id' => '1','title' => 'Categories','url' => '','target' => '_self','icon_class' => 'voyager-categories','color' => NULL,'parent_id' => NULL,'order' => '7','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-26 16:54:49','route' => 'voyager.categories.index','parameters' => NULL),
            array('id' => '6','menu_id' => '1','title' => 'Pages','url' => '','target' => '_self','icon_class' => 'voyager-file-text','color' => NULL,'parent_id' => NULL,'order' => '6','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-26 16:54:49','route' => 'voyager.pages.index','parameters' => NULL),
            array('id' => '7','menu_id' => '1','title' => 'Roles','url' => '','target' => '_self','icon_class' => 'voyager-lock','color' => NULL,'parent_id' => NULL,'order' => '2','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-25 11:20:39','route' => 'voyager.roles.index','parameters' => NULL),
            array('id' => '8','menu_id' => '1','title' => 'Tools','url' => '','target' => '_self','icon_class' => 'voyager-tools','color' => NULL,'parent_id' => NULL,'order' => '16','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-26 16:54:52','route' => NULL,'parameters' => NULL),
            array('id' => '9','menu_id' => '1','title' => 'Menu Builder','url' => '','target' => '_self','icon_class' => 'voyager-list','color' => NULL,'parent_id' => '8','order' => '1','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-26 16:54:49','route' => 'voyager.menus.index','parameters' => NULL),
            array('id' => '10','menu_id' => '1','title' => 'Database','url' => '','target' => '_self','icon_class' => 'voyager-data','color' => NULL,'parent_id' => '8','order' => '2','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-26 16:54:49','route' => 'voyager.database.index','parameters' => NULL),
            array('id' => '11','menu_id' => '1','title' => 'Compass','url' => '/backoffice/compass','target' => '_self','icon_class' => 'voyager-compass','color' => NULL,'parent_id' => '8','order' => '3','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-26 16:54:49','route' => NULL,'parameters' => NULL),
            array('id' => '12','menu_id' => '1','title' => 'Hooks','url' => '/backoffice/hooks','target' => '_self','icon_class' => 'voyager-hook','color' => NULL,'parent_id' => '8','order' => '4','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-26 16:54:49','route' => NULL,'parameters' => NULL),
            array('id' => '13','menu_id' => '1','title' => 'Settings','url' => '','target' => '_self','icon_class' => 'voyager-settings','color' => NULL,'parent_id' => NULL,'order' => '8','created_at' => '2017-09-25 11:20:39','updated_at' => '2017-09-26 16:54:49','route' => 'voyager.settings.index','parameters' => NULL),
            array('id' => '14','menu_id' => '1','title' => 'Tag','url' => '/backoffice/tags','target' => '_self','icon_class' => 'voyager-tag','color' => '#000000','parent_id' => NULL,'order' => '9','created_at' => '2017-09-26 16:50:58','updated_at' => '2017-09-26 16:54:49','route' => NULL,'parameters' => ''),
            array('id' => '15','menu_id' => '1','title' => 'Files','url' => '/backoffice/files','target' => '_self','icon_class' => 'voyager-harddrive','color' => '#000000','parent_id' => NULL,'order' => '10','created_at' => '2017-09-26 16:51:36','updated_at' => '2017-09-26 16:54:49','route' => NULL,'parameters' => ''),
            array('id' => '16','menu_id' => '1','title' => 'Slider','url' => '/backoffice/sliders','target' => '_self','icon_class' => 'voyager-images','color' => '#000000','parent_id' => NULL,'order' => '11','created_at' => '2017-09-26 16:51:57','updated_at' => '2017-09-26 16:54:49','route' => NULL,'parameters' => ''),
            array('id' => '17','menu_id' => '1','title' => 'Kegiatan','url' => '/backoffice/activities','target' => '_self','icon_class' => 'voyager-ship','color' => '#000000','parent_id' => NULL,'order' => '12','created_at' => '2017-09-26 16:52:34','updated_at' => '2017-09-26 16:54:52','route' => NULL,'parameters' => ''),
            array('id' => '18','menu_id' => '1','title' => 'Prestasi','url' => '/backoffice/achievements','target' => '_self','icon_class' => 'voyager-trophy','color' => '#000000','parent_id' => NULL,'order' => '13','created_at' => '2017-09-26 16:53:38','updated_at' => '2017-09-26 16:54:52','route' => NULL,'parameters' => ''),
            array('id' => '19','menu_id' => '1','title' => 'FAQ','url' => '/backoffice/faqs','target' => '_self','icon_class' => 'voyager-megaphone','color' => '#000000','parent_id' => NULL,'order' => '14','created_at' => '2017-09-26 16:54:06','updated_at' => '2017-09-26 16:54:52','route' => NULL,'parameters' => ''),
            array('id' => '20','menu_id' => '1','title' => 'Gallery','url' => '/backoffice/galleries','target' => '_self','icon_class' => 'voyager-images','color' => '#000000','parent_id' => NULL,'order' => '15','created_at' => '2017-09-26 16:54:36','updated_at' => '2017-09-26 16:54:52','route' => NULL,'parameters' => '')
        ]);
    }
}
