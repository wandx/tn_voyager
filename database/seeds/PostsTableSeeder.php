<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('posts')->delete();
        
        \DB::table('posts')->insert(array (
            0 => 
            array (
                'id' => 10,
                'author_id' => 1,
                'category_id' => 5,
                'title' => 'consectetur commodi iste quas',
                'seo_title' => NULL,
                'excerpt' => 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.',
                'body' => '<p>Cum suscipit eos nostrum. Aut voluptatem delectus ea eos nihil. Nam rerum in dignissimos qui laborum expedita aut. Sed et eveniet molestiae placeat mollitia. Dolores omnis consequuntur consequatur et libero et officiis qui. Eum eaque non molestiae rerum. Iste ut fugiat in nam ut labore deserunt. Dolor consectetur est occaecati mollitia et provident. Pariatur aut dolores soluta. Possimus laborum voluptatem suscipit modi odio fugit. Ipsam sunt exercitationem aut nulla vitae quia. Expedita vel atque vel et repellat. Rerum possimus explicabo est neque qui expedita nobis ut. Expedita est est corporis rem. Est tempora voluptatem blanditiis doloribus. Quia eos in ut explicabo quia. Aliquam et nobis eos corrupti. Sit aliquam distinctio tenetur facere necessitatibus et molestiae. Distinctio praesentium odio vel omnis odio laboriosam ut sit. Quaerat doloribus labore delectus laboriosam dicta officia aut voluptatem. A tempore beatae occaecati architecto. Saepe quo illo nemo molestiae tempora et quia ut. Consequuntur ut quo officia sed quis. Fugit similique vel non repudiandae earum expedita. Voluptates et unde est earum omnis et iure. Odio dolores tenetur nam. Ipsa fuga ut dignissimos ex aut quis dolorem. Voluptatem voluptas saepe et veniam voluptatem eveniet eos. Aliquid et eos et molestiae.</p>',
                'image' => 'posts/post3.jpg',
                'slug' => 'consectetur-commodi-iste-quas',
                'meta_description' => 'this be a meta descript',
                'meta_keywords' => 'keyword1, keyword2, keyword3',
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-09-26 20:28:11',
                'updated_at' => '2017-10-05 17:38:44',
                'image_url' => 'http://webneel.com/wallpaper/sites/default/files/images/04-2013/dreamy-beach-wallpaper.preview.jpg',
            ),
            1 => 
            array (
                'id' => 11,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'MSI & LKIP 2017',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>SALAM PELAJAR INDONESIA 🙌🏻</p>
<p>Kami Tim Humas Siswa SMA Taruna Nusantara, akan memberikan info ter-update mengenai pelaksanaan kegiatan-kegiatan terprogram sekolah maupun progam intern dari siswa kami yang akan terlaksanakan pada periode OSIS/MPK 2017.</p>
<p>A. MSI DAN LKIP 2017</p>
<p>SMA Taruna Nusantara Magelang mempersembahkan Lomba pengembangan akademik, kecerdasan dan kreativitas pelajar SMP/ Madrasah sederajat Tingkat Nasional MATEMATIKA, SAINS, dan INGGRIS (MSI) yang ke-14 dan LOMBA KARYA INOVASI PELAJAR (LKIP) yang ke-5.</p>
<p>Kalian semua pasti ingin tau lebih jauh kan, Apa Itu MSI dan LKIP?</p>
<p>1. Lomba MSI :<br /> MSI atau singkatan dari Matematika,Sains, dan Inggris. Merupakan ajang perlombaan di bidang mata pelajaran Matematika,Sains (IPA), dan Bahasa Inggris, yang menilai serta mengasah terhadap kemampuan dan kecerdasan akademik pelajar tingkat SMP/Madrasah sederajat yang diselenggarakan dan dilaksanakan setiap tahunnya, dan di tahun 2017 ini MSI akan terselenggarakan untuk yang ke-14 kali nya di Kampus SMA Taruna Nusantara.</p>
<p>2. Lomba LKIP :<br /> LKIP atau singkatan dari Lomba Karya Inovasi Pelajar, Merupakan ajang perlombaan di bidang mata pelajaran Prakarya dan Kewirausahaan yang menerapkan penilaian terhadap kreativitas dan keterampilan dalam berinovasi dan berkarya pelajar tingkat SMP/Madrasah sederajat yang diselenggarakan dan dilaksanakan setiap tahunnya, dan di tahun 2017 ini LKIP akan terselenggarakan untuk yang ke-5 kali nya di Kampus SMA Taruna Nusantara.<span id="more-10297"></span></p>
<p>B. CARA PENDAFTARAN :</p>
<p>Syarat Pendaftaran :<br /> 1. Masih aktif sebagai siswa SMP/ sederajat<br /> 2. Melampirkan buti keaktifan pelajar di formulir pendaftaran online (Kartu Pelajar / Surat Keterangan dari Sekolah)<br /> 3. Lomba MSI bersifat perorangan<br /> 4. LKIP bersifat tim (maksimal 2 orang)</p>
<p>Mendaftar secara ONLINE di Website MSI dan LKIP 2017 : msi-lkip.info</p>
<p>C. LOKASI : SMA Taruna Nusantara<br /> Jl. Raya Purworejo Km. 5<br /> Kec. Mertoyudan, Kab. Magelang<br /> Jawa Tengah. Kode Pos : 56172</p>
<p>D. KONTAK :</p>
<p>1. Kontak Pamong SMA Taruna Nusantara</p>
<p>Email : msismatn90@gmail.com</p>
<p>MSI : Bapak Ito &ndash; 089692922270<br /> Bapak Nikha &ndash; 087785580780</p>
<p>LKIP : Bapak Kuncoro &ndash; 081346459401</p>
<p>2. Kontak Panitia Siswa SMA Taruna Nusantara<br /> Offical Account LINE 1 : Rommyhero2<br /> Official Account LINE 2 : Adimasv</p>
<p>E. BIAYA PENDAFTARAN<br /> MSI : Rp 60.000,- / orang<br /> LKIP : Rp 100.000,- / orang</p>
<p>F. HADIAH</p>
<p>&ndash; 5 Finalis MSI mendapat kesempatan beasiswa dan bebas Tes Akademik masuk SMA TN<br /> &ndash; Juara 1 LKIP per bidang bebas Tes Akademik masuk SMA TN<br /> &ndash; Walupun bebas Tes Akademik, <strong>Tetap harus ikut tes Kesehatan, Psikologi dan Wawancara</strong></p>
<p>Juara I<br /> MSI (Piala Bergilir + Rp. 6.000.000,00)<br /> Juara II<br /> MSI (Piala + Rp. 5.000.000,00)<br /> Juara III<br /> MSI (Piala + Rp. 4.000.000,00)<br /> Juara Harapan I<br /> MSI (Piala + Rp. 3.000.000,00)<br /> Juara Harapan II<br /> MSI (Piala + Rp. 2.000.000,00)</p>
<p>Juara I<br /> LKIP (Piala Bergilir + Rp. 2.000.000,00)<br /> Juara II<br /> LKIP (Piala + Rp. 1.500.000,00)<br /> Juara III<br /> LKIP (Piala + Rp. 1.000.000,00)</p>
<p>Catatan Penting :<br /> 1. MSI dan LKIP 2017 dilaksanakan pada waktu yang sama yaitu pada tanggal 29 Oktober 2017<br /> 2. Pendaftaran telah dibuka mulai 17 Juli 2017 dan ditutup pada tanggal 23 Oktober 2017 (MSI), dan 24 September 2017 (LKIP)<br /> 3. MSI dan LKIP 2017 dilaksanakan di SMA Taruna Nusantara, berikut lokasi :<br /> Jl. Raya Purworejo KM. 5, Banyurojo, Mertoyudan, Banyurojo, Mertoyudan, Magelang, Jawa Tengah 56172 Telp (0293) 364195<br /> Google Map : https://goo.gl/maps/tnmHQgeKiw82</p>
<p>Berikut beberapa refrensi bagi kalian, yang ingin mengetahui lebih lanjut mengenai pelaksanaan MSI dan LKIP 2017.<br /> 1. Website : taruna-nusantara-mgl-sch.id<br /> 2. FB MSI LKIP : MSI &amp; LKIP SMA Taruna Nusantara<br /> 3. Instagram MSI LKIP : @msi_lkip.info<br /> 4. Twitter : @SMATN</p>',
                'image' => 'posts/October2017/DJ_WEUIVAAAl5y7.jpg large.jpg',
                'slug' => 'msi-and-lkip-2017',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-10-02 20:06:01',
                'updated_at' => '2017-10-02 20:19:32',
                'image_url' => NULL,
            ),
            2 => 
            array (
                'id' => 13,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Semarak Geografi 2017 tingkat Nasional di Universitas Negeri Yogyakarta',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>2 Tim berhasil meraih Juara 1 dan Juara 3 dalam Semarak Geografi 2017 tingkat Nasional di Universitas Negeri Yogyakarta.<br /> Mereka terdiri dari : Iknanda Januar Rizaldi, <a class="profileLink" href="https://www.facebook.com/zolasaputra.tn26?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=100004055297520&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Zola Saputra</a>, dan Jamal Habiburahman serta Bagas Patria Arikusuma, Adyan Pamungkas, Muhammad Rizki Arifudin dan Keshia Jenine Sahyasmita Setiabudi dibawah asuhan Bapak Rudi Adi Susanto.</p>',
                'image' => 'posts/November2017/23215445_1614047798638591_2521398736687641751_o.jpg',
                'slug' => 'semarak-geografi-2017-tingkat-nasional-di-universitas-negeri-yogyakarta',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 11:31:44',
                'updated_at' => '2017-11-09 11:32:52',
                'image_url' => NULL,
            ),
            3 => 
            array (
                'id' => 14,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Dua Siswa SMA Taruna Nusantara Berjaya di Ajang IMC Singapura 2017',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p>Irfan Urane Azis (kiri) dan Kinantan Arya Bagaspati menunjukkan medali dan penghargaan yang diperolehnya dalam ajang IMC di Singapura 2017.(Adi Daya Perdana/Radar Jogja)</p>
<p class="first-paragraph-bottom-zero">Nama Indonesia disebut dalam ajang International Mathematic Contest (IMC) di Singapura 2017. Siswa dari Indonesia mampu menunjukkan hasil terbaik pada ajang ini. Dua siswa SMA Taruna Nusantara Magelang Irfan Urane Azis dan Kinantan Arya Bagaspati, membawa pulang masing-masing medali Gold Prize. Bahkan Irfan juga dinobatkan sebagai Grand Champion Grade 10.</p>
<div class="paragraph-article-detail">
<div class="text-justify">
<p>IMC 2017 berlangsung di Singapura, Agustus ini. Sebanyak 14 negara dari belahan dunia mengikuti lomba matematika bergengsi itu. Di antaranya dari Tiongkok, Amerika, Korea, Inggris, Singapura, dan lainnya. Total peserta pun mencapai ribuan siswa dengan berbagai kelas.</p>
</div>
<div class="text-justify">
<p>Dua siswa SMA Taruna Nusantara (TN) pulang lomba dengan menggondol prestasi cukup membanggakan. Dua medali Gold Prize dalam kelas lomba siswa kelas X bisa dibawa pulang. Bahkan satu siswa mendapatkan Grand Champion Grade 10, penghargaan dengan nilai tertinggi di kelas perlombaannya.&nbsp;&ldquo;Senang banget dan bersyukur bisa berkontribusi terhadap negara,&rdquo; kata Irfan.</p>
</div>
</div>
<p>Di Singapura, ia bersaing dengan 1.000 siswa dari negara lain. Kelas lomba dibagi menjadi 5 grade, salah satunya grade (kelas) 10. Untuk mendapatkan medali itu, &nbsp;bukan merupakan perkara yang mudah.</p>
<p>Irfan harus mengerjakan 18 soal dalam waktu 1,5 jam. Ia pun harus mengatur waktu dengan baik untuk mengerjakan soal itu. Bekal disiplin yang diajarkan saat di SMA TN menjadikan ia mampu mengerjakan soal dengan baik.&nbsp;&ldquo;Sempat kesulitan mengerjakan 18 soal dalam waktu 1,5 jam. Tapi saat sekolah di TN, siswa terbiasa hidup teratur,&rdquo; katanya.</p>
<p>Salah satu soal yang keluar saat lomba, di antaranya, matematika tentang kombinatorik. Putra Kapolda Metro Jaya Irjen Idham Azis ini mengaku bisa mengatasi soal berbekal pengalaman saat di SMA TN. Saat di sekolah, teman-temannya berasal dari Sabang sampai Merauke dengan kepribadian yang berbeda-beda.</p>
<p>&ldquo;Itu kebawa sampai ke lomba bahwa di sini diajarkan untuk pantang menyerah. Dari teman siswa Sabang sampai Merauke, bisa selesaikan masalah beda-beda. Setiap masalah pasti ada solusinya. Termasuk mengatasi soal ketika lomba dengan tidak hanya tidur saja. Tetapi dengan bekerja lebih tekun,&rdquo; jelasnya.</p>
<p>Terlebih peserta lomba ada seribuan. Yang dari Indonesia terdapat 130 siswa, &nbsp;namun yang mendapatkan medali Gold Prize hanya 14 siswa. Sedangkan yang mendapatkan Grand Champion Grade 10 hanya satu orang.</p>
<p>&ldquo;Sebelum lomba, beberapa persiapan di antaranya belajar di asrama di ruang tersendiri. Dan dibimbing juga oleh guru matematika,&rdquo; kata siswa kelahiran 14 Juni 2001 ini.</p>
<p>Sebagian besar siswa memandang mata pelajaran matematika merupakan hal yang sulit. Namun, hal itu tidak berlaku baginyaa. Anak kedua dari empat bersaudara ini suka terhadap matematika sejak SD. Kemudian mendalami pelajarannya ketika memasuki SMP dan beberapa kali mengikuti les.</p>
<p>&ldquo;Awalnya mendalami pelajaran matematika, lalu mendapat suport dari keluarga dan sekolah. Bisa mengerjakan soal matematika merupakan kepuasaan tersendiri,&rdquo; katanya.</p>
<p>Selain Irfan, siswa SMA TN lainnya adalah Kinantan Arya Bagaspati, kelas 10, juga mendapatkan medali Gold Prize. Kinantan mengaku sudah mempersiapkan diri untuk mengikuti lomba itu. Ia merasa senang terhadap matematika sudah sejak SMP.<br /> Kala itu &nbsp;ia juga sering mengikuti lomba matematika dari berbagai tingkat. &nbsp;Mulai lomba lokal hingga lomba tingkat antarnegara. &nbsp;Siswa asal SMPN 2 Purwokerto ini mengaku hobi mengerjakan soal matematika.</p>
<p>&ldquo;Suka matematika itu awalnya sebagai hobi. Saya merasa puas bisa ngerjain soal dan ada tantangannya. Mengejar kepuasan batin,&rdquo; jelasnya.</p>
<p>Ditambah lagi, orang tuanya juga mendukung anaknya berkecimpung di mata pelajaran matematika. Bahkan, orang tuanya ikut mensuplai soal dan sang anak ikut bimbingan guru les.</p>
<p>Selain matematika, ia juga menyukai ilmu eksak lainnya. &nbsp;Seperti, kimia dan fisika. Bahkan, ia pun memiliki sikap terkait dengan ilmu-ilmu yang digeluti tersebut.&nbsp; &ldquo;Rumus itu dimengerti, tidak boleh dihafal,&rdquo; kata dia.</p>
<p>Dengan kesukaan terhadap matematika, itu mendorongnya mampu membawa pulang medali Gold Prize. Selain itu berbagai lomba yang diikuti sebelumnya membantu ia mengatasi soal-soal yang sulit.</p>
<p>&ldquo;Matematika itu seperti mata pelajaran Bahasa Indonesia. Banyak mata pelajaran yang menggunakan bahasa Indonesia. Begitu juga matematika menjadi pengantar untuk mata pelajaran yang lain,&rdquo; jelas putera pasangan Dinar dan Dewi Sekarsari ini.</p>
<p>Sementara itu, Kepala Humas SMA TN Magelang <abbr><span class="hover37">Cecep Iskandar</span></abbr> mengaku bangga dengan apa yang diraih siswanya. Setelah mendapatkan prestasi ini, terkonfirmasi &nbsp;bahwa sikap disiplin yang diajarkan sekolah berfungsi untuk membangun kepribadian pada diri siswa.</p>
<p>&ldquo;Dengan demikian, terbentuk jiwa pantang menyerah. Siswa &nbsp; bersungguh-sungguh dalam mengerjakan tugas yang diberikan,&rdquo; katanya.</p>
<p>SMA TN tidak hanya mendidik siswa dari sisi akademik semata. Tetapi juga mengembangkan potensi yang dimiliki siswa. Sekolah mempersilahkan potensi siswa dikembangkan, kemudian sekolah nanti bisa membantu.<strong>&nbsp;</strong></p>
<p><strong>(rj/ong/ong/JPR) ADI DAYA PERDANA, Mungkid &ndash; <a href="http://www.jawapos.com/radarjogja/read/2017/08/16/7999/dua-siswa-sma-taruna-nusantara-berjaya-di-ajang-imc-singapura-2017">http://www.jawapos.com/</a></strong></p>',
                'image' => 'posts/November2017/dua-siswa-sma-taruna-nusantara-berjaya-di-ajang-imc-singapura-2017_m_7999.jpeg',
                'slug' => 'dua-siswa-sma-taruna-nusantara-berjaya-di-ajang-imc-singapura-2017',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 11:47:03',
                'updated_at' => '2017-11-09 11:49:24',
                'image_url' => NULL,
            ),
            4 => 
            array (
                'id' => 15,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Kombes Pol. Jhonny Edison Isir (TN 1) Putra Papua Pertama dalam Sejarah Jadi Ajudan Presiden RI',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="the-content">
<p>Tribratanews.polri.go.id &ndash; Polda Papua Jayapura &ndash; Pertama dalam sejarah, putra asal Papua, kelahiran Jayapura 7 Juni 1975 dipercayakan (akan) menjabat sebagai ajudan Presiden RI Joko Widodo. Kabar ini sebenarnya kian santer diperbincangkan berbagai kalangan dan komunitas disejumlah grup media sosial beberapa waktu terakhir ini, bahwa suami dari Astrid Alice Parera yang sementara menjabat sebagai Dirkrimsus Polda Riau akan mencatatkan namanya dalam sejarah Polri.<span id="more-247207"></span></p>
<p>Tapi soal membuat sejarah, sebetulnya bukan pertama bagi pemilik nama lengkap Kombes Pol Jhonny Edison Isir SIK, MTcP. Ia pernah mencatatkan hal itu, ketika menyelesaikan pendidikan AKPOL pada 1996 dengan meraih penghargaan Adhi Makayasa, yang langsung disematkan oleh Presiden kedua RI, Suharto. Dialah putra Papua pertama yang mengukir namanya di Bumi Magelang, Jawa Tengah.</p>
<p>&nbsp;</p>
<p>Sebagai, putra dari anak polisi yang ditempa dengan kehidupan yang keras, boleh dik</p>
<p>ata jauh dari berkecukupan, ayahanda dari Victoria Hermione Isir dan Velove Malikha Isir pernah merasakan bagaimana menjual nasi kuning untuk membantu ekonomi ayah dan ibunya saat masa sekolah di SMP Negeri 6 Kota Jayapura.</p>
<p>Tentunya sikap displin dan tepat waktu, sudah menjadi hal yang biasa baginya yang tiap hari melihat kehidupan polisi di SPN Jayapura yang tak jauh dari lokasi rumahnya. Ia bisa sukses dikatakan terbilang sukses, bukan karena saat ini, tetapi telah dibentuk dan diasa semasa kecil, baik oleh orang tua dan lingkungannya.</p>
<p>Bahkan semasa menjadi Kapolres Jayawijaya (2013-2014), Jhonny Edison Isir yang pernah meraih Satya Lencana Seroja (1999), Satya Lencana 8 Tahun, Satya Lencana Dharma Nusa dan Satya Lencana 16 Tahun membuat berbagai gebrakan bagi anggota Polri dilingkungannya itu. Saat itu, Kapolri Jenderal Tito Karnavian masih menjabat sebagai Kapolda Papua.</p>
<p>Ketika akan berpisah dan dipromosi untuk menjadi Kapolres Manokwari (2014-2016), ratusan anggotanya di Mapolres Jayawijaya mengucurkan air mata dan mengantarkannya hingga naik pesawat terbang di Bandara Udara Wamena.</p>
<p>Dan tiga jabatan terakhir hingga mengantarkannya mendapat promosi menjadi ajudan Presiden Jokowi, Jhonny Edison Isir sempat menjabat sebagai Wadir Reskrimum Polda Banten (2016), Dosen Utama STIK PTIK (2016) dan Direktu Reskrimsus Polda Riau.</p>
<p>Kabar ini rupanya sampai di Papua. Kabid Humas Polda Papua Kombes Pol AM Kamal memberikan ancungan jempol. &ldquo;Anak Papua pertama jadi AFC Presiden, bintang menanti bahkan peluang menjadi Kapolri pada masanya,&rdquo; kata Kamal dalam grup WA Insan Pers Polda Papua.</p>
<p>Mantan Kapolda Papua yang kini menjabat sebagai Kapolda Sumatera Utara, Irjen Pol Drs Paulus Waterpauw juga memberikan apresiasi yang sangat tinggi bagi mantan anggotanya itu. &ldquo;Amin. Luar biasa, kami mewakili Polda Papua menghaturkan banyak terima kasih kepada bapak Presiden Jokowi dan Kapolri Jenderal M Tito Karnavian. Kami akan viralkan di medsos dan media lokal Papua serta media lainnya,&rdquo; kata suami dari Ramona.</p>
<p>Menurut jenderal bintang dua yang digadang-gadang jadi pemimpin Papua berikutnya ini, prestasi yang diraih oleh Jhonny Edison Isir patut dibanggakan karena bisa menjadi contoh dan teladan bagi anak-anak Papua lainnya untuk mencapai impiannya, dibidang apa saja, asalkan dengan tekun dan telaten menjalaninya, tak lupa disisipkan doa yang selalu dipanjatkan kepada Yang Maha Kuasa.</p>
<p>&ldquo;Karena dengan doa, semua cita, cinta dan impian bisa digapai, Prestasi Jhonny Edison Isir ini adalah contoh yang baik, bahwa putra Papua bisa menjabat dimana saja. Ini adalah sejarah bagi kita semua, bagi Indonesia dan ini seperti kado di HUT Kemerdekaan RI ke-72 oleh Presiden Jokowi,&rdquo; katanya dalam pesan singkat.</p>
<p>Sementara itu, secara terpisah Kombes Pol Jhonny Edison Isir yang dihubungi secara terpisah lewat media sosial meminta dukungan dari semua pihak, baik warga Jayapura dan Papua umumnya. &ldquo;Puji Tuhan, Alhamdulliah. Terima kasih banyak ya, dan mohon dukungan doa agar pelaksanaan tugas dapat menjadi amanah, amin,&rdquo; katanya menjawab pertanyaan awak media. (*)</p>
<p>Bangga AKU PAPUA</p>
<p>Penulis : Efer/Res Nabire<br /> Editor : Umi Fadilah<br /> Publish : Efer/Res Nabire<br /> <a href="http://tribratanews.polri.go.id/?p=247207">http://tribratanews.polri.go.id/?p=247207</a></p>
</div>',
                'image' => 'posts/November2017/IMG-20170816-WA0116.jpg',
            'slug' => 'kombes-pol-jhonny-edison-isir-(tn-1)-putra-papua-pertama-dalam-sejarah-jadi-ajudan-presiden-ri',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 11:52:22',
                'updated_at' => '2017-11-09 11:52:22',
                'image_url' => NULL,
            ),
            5 => 
            array (
                'id' => 16,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Irfan Urane Azis berhasil meraih predikat Grand Champion dan medali emas dalam Internasional Mathematic Contest (IMC) 2017',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="blog-post-thumb"><label>Irfan membawa tropi dan piagam bersama Kinantan Arya Bagaspati<br /> </label></div>
<h1 class="entry-title post-header-title">Putra Kapolda Jago Matematika, Raih Predikat Grand Champion Tingkat Dunia</h1>
<div class="entry-content">
<p><strong>Mertoyudan, (<a href="http://magelang.sorot.co" target="_blank" rel="noopener">magelang.sorot.co</a>)&ndash;</strong>Satu lagi prestasi kembali diraih siswa SMA Taruna Nusantara (TN) Magelang, Kabupaten Magelang. Kali ini berhasil meraih predikat Grand Champion dan medali emas dalam Internasional Mathematic Contest (IMC) 2017 yang dilangsungkan di Singapura pada awal Agustus 2017.</p>
<p>Dalam ajang yang diikuti lebih dari 1.000 pelajar perwakilan dari 14 negara tersebut, siswa yang dikirim yakni Irfan Urane Azis (16). Ia mampu meraih medali emas untuk Indonesia dalam IMC 2017. Bahkan dalam IMC ini, Irfan mampu memperoleh nilai tertinggi. Atas prestasi yang diraihnya, ia berhak Piala Grand Champion pada kontes Matematika tingkat dunia.</p>
<p>Putera kedua pasangan Irjen Pol Idham Azis (54) dan Fitri Handari (42) ini mengakui, kecintaannya terhadap pelajaran Matematika tumbuh sejak masih duduk di bangku sekolah dasar. Kemampuan soal Matematika terus diasah, bahkan saat masih duduk di SMPN 2 Depok pernah memenangi berbagai kontes Matematika tingkat dunia seperti di Korea, China, Malaysia hingga India.</p>
</div>
<p>&nbsp;</p>
<p>Malaysia hingga India.</p>
<blockquote>
<p><em>Saya suka Matematika karena ada tantangan tersendiri. Rasanya puas kalau bisa menyelesaikan soal yang rumit,&rdquo; tutur Irfan yang kini duduk di kelas XI IPA SMA TN itu, Selasa (15/8/2018). &nbsp;</em></p>
</blockquote>
<p>Setelah lulus dari SMPN 2 Depok, Irfan kemudian melanjutkan di SMA TN Magelang. Ia tinggal di asrama dengan jadwal padat, membuatnya disiplin termasuk dalam mengerjakan soal-soal. Kebiasaan disiplin inilah dibuktikan dalam IMC 2017, dia harus mengerjakan 18 soal Matematika dalam waktu 60 menit. Materi soal yang harus dikerjakan ini meliputi Geometri, Aljabar, Teori Bilangan, Kombinatorik, dan sebagainya.</p>
<blockquote>
<p><em>Untuk soal kombinatorik paling sulit, tapi paling sulit lagi mengatur waktunya karena harus selesai dalam 1 jam,&rdquo; tutur Irfan, remaja kelahiran Jakarta, 14 Juni 2001 itu. &nbsp;</em></p>
</blockquote>
<p>Dalam IMC tersebut, Irfan mengakui seluruh peserta merupakan lawan yang tangguh di bidang Matematika, terutama dari China, Filipina dan tuan rumah Singapura. Namun karena sudah terbiasa dengan kedisiplinan sehingga mengerjakan soal pun bisa tepat waktu.</p>
<blockquote>
<p><em>Kalau di sini bangun pagi, apel 5 kali, makan juga harus selesai tepat waktu. Jadi kebiasaan di sekolah membantu saya menyelesaikan masalah-masalah di luar, termasuk ketika ikut kontes ini,&rdquo; tuturnya seraya menyebut prestasi yang diraih atas dukungan kedua orang tuanya, keluarga dan sekolah. &nbsp;</em></p>
</blockquote>
<p>Nantinya setelah lulus dari SMA TN, Irfan ingin bercita-cita masuk di Akademi Kepolisian (Akpol) Semarang. Irfan ingin mengabdikan kepada negara sebagaimana ayahnya yang kini menjadi Kapolda Metro Jaya.</p>
<p>Sementara itu, Kepala Bagian Humas SMA Taruna Nusantara, <abbr><span class="hover37">Cecep Iskandar</span></abbr> mengatakan, kebanggaannya atas prestasi yang ditorehkan Irfan. Terlebih lagi prestasi yang diraih predikat Grand Champion merupakan prestasi tertinggi dalam kontes tingkat internasional yang diikuti siswa SMA Taruna Nusantara.</p>
<blockquote>
<p><em>Selama kami mengikuti kontes tingkat internasional, ini yang paling tinggi. Selebihnya kami dapat emas, perak, perunggu dan prestasi lainnya,&rdquo; tuturnya. &nbsp;</em></p>
</blockquote>
<p>Atas prestasi yang diraih tersebut sekolah akan akan memberikan penghargaan kepada Irfan dan siswa berprestasi lainnya menjelang peringatan HUT ke-72 Kemerdekaan RI.</p>
<p>Selasa, 15 Agustus 2017 12:59:00 WIB | oleh : eko-susanto | <span class="comments-count"><a href="http://magelang.sorot.co/berita-3135-putra-kapolda-jago-matematika-raih-predikat-grand-champion-tingkat-dunia.html#comments">0 komentar<br /> http://magelang.sorot.co/</a></span></p>',
                'image' => 'posts/November2017/3135_irfan_membawa_tropi_dan_piagam_20170815130304.jpg',
            'slug' => 'irfan-urane-azis-berhasil-meraih-predikat-grand-champion-dan-medali-emas-dalam-internasional-mathematic-contest-(imc)-2017',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 11:52:56',
                'updated_at' => '2017-11-09 11:52:56',
                'image_url' => NULL,
            ),
            6 => 
            array (
                'id' => 17,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Irfan Urane Azis, Siswa Taruna Nusantara Peraih “Grand Champion” Kontes Matematika Dunia',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<div class="photo__caption">Irfan Urane Azis, siswa SMA Taruna Nusantara Magelang yang meraih medali emas dan Grand Champion pada International Mathematic Contest (IMC) 2017, di Singapura, awal Agustus 2017. Irfan ditemui di SMA Taruna Nusantara, Senin (14/8/2017).<span class="photo__author author">(KOMPAS.com/Ika Fitriana)</span></div>
<div class="read__article mt2 clearfix js-tower-sticky-parent">
<div class="col-bs9-7">
<div class="read__content">
<p><strong>MAGELANG, KOMPAS.com</strong> &ndash; Seorang pelajar SMA <a class="inner-link" href="http://indeks.kompas.com/tag/Taruna-Nusantara" target="_blank" rel="noopener">Taruna Nusantara</a>, Kabupaten Magelang, Jawa Tengah, berhasil menorehkan prestasi tertinggi untuk Indonesia pada Internasional Mathematic Contest (IMC) 2017 di Singapura awal Agustus 2017.</p>
<p>Dia adalah Irfan Urane Azis (16) siswa kelas 11 sekolah tersebut. Irfan menggondol medali emas pada <a class="inner-link" href="http://indeks.kompas.com/tag/kontes" target="_blank" rel="noopener">kontes</a> yang diikuti oleh lebih dari 1.000 pelajar perwakilan dari 14 negara itu. Irfan bahkan menjadi satu-satunya peserta yang meraih nilai tertinggi dari seluruh peserta dan berhak menjadi Grand Champion pada kontes <a class="inner-link" href="http://indeks.kompas.com/tag/Matematika" target="_blank" rel="noopener">Matematika</a> tingkat dunia itu.</p>
<p>&ldquo;Saya bangga bisa mempersembahkan prestasi ini untuk keluarga, sekolah, dan Indonesia tentunya,&rdquo; ujar Irfan kepada <em>Kompas.com</em> ditemui di SMA Taruna Nusantara, Senin (14/8/2017).</p>
</div>
</div>
</div>
<p>&nbsp;</p>
<p>Remaja kelahiran Jakarta, 14 Juni 2001 ini menceritakan, pada kontes tersebut, setiap peserta harus bisa menjawab 18 soal Matematika dalam waktu 60 menit. Materi soal meliputi geometri, aljabar, teori bilangan, kombinatorik, dan sebagainya.</p>
<p>&ldquo;Soal kombinatorik paling sulit, tapi paling sulit lagi mengatur waktunya karena harus selesai dalam 1 jam,&rdquo; kisah putra kedua dari pasangan Irjen Pol Idham Azis (54) dan Fitri Handari (42) ini.</p>
<p>Akan tetapi, bekal kedisiplinan yang diajarkan di SMA Taruna Nusantara telah menjadikan Irfan pantang menyerah, sehingga ia mudah menaklukkan tantangan itu selama berkompetisi. Irfan mengakui, seluruh peserta adalah lawan yang tangguh di bidang Matematika, terutama dari China, Filipina, dan tuan rumah Singapura.</p>
<p>&ldquo;Di SMA Taruna Nusantara sudah biasa teratur, bangun pagi, apel 5 kali, makan juga harus selesai tepat waktu. Jadi kebiasaan di sekolah membantu saya menyelesaikan masalah-masalah di luar, bagaimana kerja keras, mencari solusi sendiri, termasuk ketika ikut kontes ini,&rdquo; katanya.</p>
<p>Irfan menyebutkan, kecintaannya pada pelajaran Matematika tumbuh sejak usia sekolah dasar. Kemampuannya terus diasah saat ia masuk di SMP Negeri 2 Depok. Saat itu Irfan bahkan sudah memenangi berbagai kontes Matematika tingkat dunia, antara lain di Malaysia, Korea, China hingga India.</p>
<p>&ldquo;Saya suka Matematika karena ada tantangan tersendiri. Rasanya puas kalau bisa menyelesaikan soal yang rumit,&rdquo; ucapnya.</p>
<p>Dia mengatakan, Irfan, prestasi-prestasi ini tidak mungkin diraih tanpa dukungan dari kedua orang tua, keluarga dan pihak sekolah.</p>
<p>Usai lulus dari SMA Taruna Nusantara, Irfan bercita-cita meneruskan pendidikan di Akademi Kepolisian (Akpol). Dia ingin mengabdi pada negara, seperti sang ayah yang kini menjabat sebagai Kepala Kepolisian Daerah (Kapolda) Metro Jaya Jakarta.</p>
<p>Kepala Bagian Humas SMA Taruna Nusantara <abbr><span id="__autoId2" class="hover37">Cecep Iskandar</span></abbr>, mengungkapkan kebanggaannya atas prestasi yang ditorehkan oleh Irfan. Apalagi, predikat Grand Champion adalah prestasi tertinggi dalam kontes tingkat internasional yang pernah diikuti oleh siswa SMA Taruna Nusantara.</p>
<p>&ldquo;Selama ikut kontes tingkat internasional, ini yang paling tinggi. Selebihnya kami dapat emas, perak, perunggu dan prestasi lainnya. Tentu ini membanggakan bagi kami semua,&rdquo; ucap Iskandar.</p>
<p>Dia mengatakan di sekolah Taruna Nusantara , disiplin adalah makanan sehari-hari para siswa. Sebab, dari disiplin akan membentuk dan membangun karakter siswa yang pantang menyerah, kerja keras dan sungguh-sungguh.</p>
<p>&ldquo;Jadi disiplin di sekolah diimplementasikan siswa ke luar. Kami tidak sekedar mendidik secara akademik tapi juga kepribadian. Kami fasilitasi siswa yang punya kemampuan atau minat masing-masing. Seperti bidang Matematika, kami sediakan guru, ruang kelas sendiri dan lainnya,&rdquo; paparnya.</p>
<p>Sebagai bentuk apresiasi, pihaknya akan memberikan penghargaan kepada Irfan dan siswa berprestasi lainnya menjelang peringatan HUT ke-72 Kemerdekaan RI pada malam 17 Agustus 2017 mendatang di sekolah setempat.</p>
<p>Kontributor Magelang, Ika Fitriana &ndash; <a href="http://regional.kompas.com/read/2017/08/15/09090031/irfan-siswa-taruna-nusantara-peraih-grand-champion-kontes-matematika-dunia">http://regional.kompas.com</a></p>',
                'image' => 'posts/November2017/3383761701.jpg',
                'slug' => 'irfan-urane-azis-siswa-taruna-nusantara-peraih-grand-champion-kontes-matematika-dunia',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 11:53:35',
                'updated_at' => '2017-11-09 11:53:35',
                'image_url' => NULL,
            ),
            7 => 
            array (
                'id' => 18,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Direktur BLU LPMUKP, Syarif Syahrial (TN 5), menyepakati mengelola dana LPMUKP Rp 13 miliar untuk permodalan 2.403 pelaku usaha mikro dan kecil sektor kelautan dan perikanan',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p>Syarif Syahrial (paling kiri)</p>
<div class="arial f12 pt5 grey2">Menteri KKP Susi Pudjiastuti menghadiri penandatanganan skema kredit khusus untuk nelayan bersama Badan Layanan Umum Lembaga Pengelola Modal Usaha Kelautan dan Perikanan (BLU LPMUKP) dengan KUD Mino Saroyo Cilacap, Koperasi Inka Bantul VII Projo Mino, Koperasi Wisata Mina Bahari 45 Bantul dan Pusat Penyuluh Mandiri Kelautan Perikanan (P2MKP) Bening Jati Anugerah Bogor. Penandatanganan di lakukan di di Pelabuhan Perikanan Samudera Cilacap, Senin (14/8/2017).</div>
<div class="side-article txt-article">
<p><strong>TRIBUNNEWS.COM, CILACAP &ndash;</strong> Setelah tertunda selama lebih dari 10 tahun, akhirnya skema kredit khusus untuk nelayan resmi diluncurkan.</p>
<p>Hal ini ditandai dengan kerjasama Badan Layanan Umum Lembaga Pengelola Modal Usaha Kelautan dan Perikanan (BLU LPMUKP) dengan KUD Mino Saroyo Cilacap, Koperasi Inka Bantul VII Projo Mino, Koperasi Wisata Mina Bahari 45 Bantul dan Pusat Penyuluh Mandiri Kelautan Perikanan (P2MKP) Bening Jati Anugerah Bogor.</p>
<p>Direktur BLU LPMUKP, Syarif Syahrial, menyatakan bahwa kerjasama ini menyepakati untuk mengelola dana bergulir LPMUKP sebesar Rp 13 miliar untuk memenuhi kebetulan permodalan sebanyak 2.403 pelaku usaha mikro dan kecil sektor kelautan dan perikanan.</p>
</div>
<p>&nbsp;</p>
<p>Syahrial berharap hal ini dapat berkontribusi pula terhadap penyerapan tenaga kerja serta menurunkan ketimpangan pendapatan antar masyarakat.</p>
<p>Tahun 2017 ini, pemerintah melalui menteri keuangan selaku bendahara umum negara telah mengalokasikan dana investasi pemerintah sebesar Rp 500 miliar untuk penguatan modal UMKM Kelautan dan Perikanan.</p>
<p>Penandatangan kerjasama ini dilaksanakan pada acara sinergi kementerian dalam mengangkat ekonomi rakyat melalui inklusi keuangan yang berlangsung di Pelabuhan Perikanan Samudera Cilacap.</p>
<p>Dalam kesempatan tersebut, Menteri Kelautan dan Perikanan Susi Pudjiastuti berpesan agar pinjaman lunak ini dapat dimanfaatkan oleh para pihak yang membutuhkan.</p>
<p>&ldquo;Harus dipastikan ketepatan sasarannya untuk membantu nelayan kecil&rdquo; imbuhnya.</p>
<p>Menteri Susi juga memberikan arahan agar BLU LPMUKP melakukan kerjasama dengan perbankan.</p>
<p>Kerjasama antara BLU LPMUKP dengan perbankan diharapkan mampu meningkatkan alokasi kredit kepada masyarakat kelautan dan perikanan.</p>
<p>Menutup arahan pada acara tersebut, Susi juga mengingatkan kalangan perbankan untuk jangan takut menyalurkan kreditnya kepada nelayan.</p>
<p>Potensi stok ikan yang semakin berlimpah menjadikan sektor ini semakin menarik untuk dapat dibiayai oleh perbankan nasional.</p>',
                'image' => 'posts/November2017/susi-puji_20170814_201623.jpg',
            'slug' => 'direktur-blu-lpmukp-syarif-syahrial-(tn-5)-menyepakati-mengelola-dana-lpmukp-rp-13-miliar-untuk-permodalan-2-403-pelaku-usaha-mikro-dan-kecil-sektor-kelautan-dan-perikanan',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 11:54:09',
                'updated_at' => '2017-11-09 11:54:09',
                'image_url' => NULL,
            ),
            8 => 
            array (
                'id' => 19,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Peresmian “The Yudhoyono Institute”, Agus Yudhoyono (TN 5) Sampaikan Amanah Jokowi',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<div class="photo__caption">Direktur Eksekutif The Yudhoyono Institute, Agus Harimurti Yudhoyono saat menyampaikan sambutnanya dalam acata peresmian The Yudhoyono Institute di Djjakarta Theater, Jakarta Pusat, Kamis (10/8/2017).<span class="photo__author author">(KOMPAS.com/Nabilla Tashandra)</span></div>
<div class="read__article mt2 clearfix js-tower-sticky-parent">
<div class="col-bs9-7">
<div class="read__content">
<p><strong>JAKARTA, KOMPAS.com</strong> &ndash; Direktur Eksekutif <a class="inner-link" href="http://indeks.kompas.com/tag/The-Yudhoyono-Institute" target="_blank" rel="noopener">The Yudhoyono Institute</a>, <a class="inner-link" href="http://indeks.kompas.com/tag/Agus-Harimurti-Yudhoyono" target="_blank" rel="noopener">Agus Harimurti Yudhoyono</a> ( <a class="inner-link" href="http://indeks.kompas.com/tag/AHY" target="_blank" rel="noopener">AHY</a>), meresmikan lembaga yang dipimpinnya, pada Kamis (10/8/2017) malam, di Djakarta Theatre, Jakarta.</p>
<p>Dalam pidatonya, putra sulung Presiden ke-6 RI Susilo Bambang Yudhoyono itu menyampaikan sejumlah harapan, terutama kepada generasi muda.</p>
<p><a class="inner-link" href="http://indeks.kompas.com/tag/Agus-Yudhoyono" target="_blank" rel="noopener">Agus Yudhoyono</a> juga menyampaikan amanah yang disampaikan Presiden Joko Widodo kepadanya saat bertemu di Istana Kepresidenan, pada Kamis siang.</p>
<p>Ia mengatakan, menjelang 100 tahun kemerdekaan Indonesia pada 2045, Indonesia akan menjadi negara yang semakin diperhitungkan.</p>
<p>&ldquo;Bumi akan semakin kompetitif dan kompetisi yang tidak dikelola dengan baik akan melahirkan sengketa dan konflik baru,&rdquo; kata Agus.</p>
</div>
</div>
</div>
<p>&nbsp;</p>
<p>Ia mengungkapkan pengalamannya berkunjung ke daerah-daerah beberapa bulan terakhir.</p>
<p>Menurut Agus Yudhoyono, sejumlah pernyataan mengenai patriotisme mengejutkannya.</p>
<p>Sebagian menilai, mengangkat senjata adalah bentuk patriotisme, dan kemudian mengkritik Agus karena meninggalkan karir militernya yang cemerlang.</p>
<p>Ada pula yang mengapresiasi keberaniannya meninggalkan zona nyaman.</p>
<p>&ldquo;Cerita tadi inspirasi bagi anak-anak muda bahwa dalam hidup kita tidak boleh takut berjuang, gagal, dan menyerah,&rdquo; kata Agus Yudhoyono.</p>
<p>Ia menekankan, generasi muda harus mempersiapkan diri menyambut transformasi bangsa, di tengah perubahan yang terjadi di era ini.</p>
<p><strong>Amanah Jokowi&nbsp;</strong></p>
<p>Dalam pidatonya, Agus juga menyinggung pertemuannya dengan Presiden Joko Widodo untuk meminta restu terkait peresmian The Yudhoyono Institute.</p>
<p>Agus mengatakan, ia mendapatkan amanah dari Jokowi.</p>
<p>&ldquo;Amanah Beliau kepada generasi muda, tentunya diharapkan menjadi tulang punggung perubahan dan kemajuan bangsa,&rdquo; kata dia.</p>
<p>Agus Yudhoyono juga menyinggung masa kepemimpinan SBY selama dua periode, di mana Indonesia masuk jajaran negara anggota G20.</p>
<p>Sejak itu, Indonesia menjadi negara yang diperhitungkan. Bahkan, Bank Dunia memproyeksikan Produk Domestik Bruto (PDB) Indonesia akan menempati peringkat empat dunia.</p>
<p>&ldquo;Insya Allah tahun 2045 masyarakat Indonesia makmur seperti negara maju,&rdquo; ujar dia.</p>
<p>Di akhir pidatonya, Agus menekankan soal pentingnya menghargai sejarah untuk menjaga bangsa tetap besar.</p>
<p>&ldquo;Bangsa yang besar adalah bangsa yang tidak pernah melupakan sejarahnya. Bangsa yang besar pasti berterima kasih dan mengapresiasi segala sesuatu yang telah diperjuangkan oleh para pemimpin terdahulunya,&rdquo; kata Agus Yudhoyono.</p>',
                'image' => 'posts/November2017/2063278172.jpg',
            'slug' => 'peresmian-the-yudhoyono-institute-agus-yudhoyono-(tn-5)-sampaikan-amanah-jokowi',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 11:54:51',
                'updated_at' => '2017-11-09 11:54:51',
                'image_url' => NULL,
            ),
            9 => 
            array (
                'id' => 20,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Teuku Faisal Fathani, Ph.D (TN 1) menerima anugerah Academic Leader pada Malam Apresiasi Hakteknas ke-22 di Universitas Negeri Makassar',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p>Teuku Faisal Fathani (tengah, memegang piala)</p>
<article class="membaca">Makassar (10/8) &ndash; Puncak peringatan Hari Kebangkitan Teknologi Nasional (Hakteknas) ke-22 yang dipusatkan di Makassar, Sulawesi Selatan berlangsung meriah. Seremoni tahunan yang turut dihadiri oleh Wakil Presiden Jusuf Kalla itu tidak hanya menampilkan karya-karya iptek melalui pameran &ldquo;<em>Ritech Expo</em>&ldquo;, &nbsp;tetapi juga memberikan apresiasi bagi akademisi, media, maupun masyarakat yang bersumbangsih pada kemajuan ilmu pengetahuan dan teknologi Tanah Air.Malam Apresiasi Hakteknas ke-22 digelar di Gedung Pinisi Universitas Negeri Makassar (UNM). Pada acara tersebut, Direktorat Jenderal Sumber Daya Iptek Dikti menominasikan dua Dosen yang dinilai telah mengabdikan diri, baik dalam mengajar maupun menghasilkan publikasi serta inovasi yang bermanfaat.
<p>Dua Dosen penerima anugerah <em>Academic Leader</em> itu adalah Teuku Faisal Fathani, Ph.D dari Universitas Gadjah Mada (UGM) dan Prof. Dr. dr. Mulyanto dari Universitas Mataram. Pemberian penghargaan dilakukan oleh Direktur Jenderal Sumber Daya Iptek Dikti, Ali Ghufron Mukti dengan disaksikan oleh seluruh jajaran pejabat Kementerian Riset, Teknologi, dan Pendidikan Tinggi (Kemristekdikti), serta tamu-tamu undangan.</p>
</article>
<p>&nbsp;</p>
<p>&ldquo;Penghargaan ini diberikan karena kedua Dosen ini telah berdedikasi tidak hanya dalam akademis, tetapi juga produktif dalam menghasilkan publikasi ilmiah dan kreatif berinovasi. Kami juga sengaja hanya memilih dua Dosen, karena untuk menjadi seorang dengan titel <em>academic leader</em> itu tidak sembarangan, melainkan harus punya peran nyata dalam kemajuan ilmu pengetahuan, teknologi, dan pendidikan tinggi,&rdquo; ucap Ghufron sebelum menyerahkan anugerah penghargaan kepada pemenang, Kamis (10/8).</p>
<p>Salah satu pemenang, Teuku Faisal Fathani, Ph.D merupakan penemu sistem peringatan bencana sedimen. Pria kelahiran Banda Aceh, 26 Mei 1975 ini sehari-hari berprofesi sebagai Dosen Departemen Teknik Sipil dan Lingkungan Fakultas Teknik UGM. Adapun inovasinya, yakni berupa varian alat deteksi dini GAMA-EWS dari lima generasi telah mendapatkan sebanyak lima hak paten (<em>granted</em>), bahkan digunakan di 28 Provinsi di Indonesia, serta di luar negeri, seperti Timor Leste, Myanmar, dan Sri Lanka.</p>
<p>Dalam hal publikasi, pria yang mendapatkan gelar Doktor dari Tokyo University itu sudah menghasilkan 10 publikasi ilmiah pada jurnal internasional bereputasi dan menjadi penulis 17 buah buku atau <em>book chapter</em> (sebagai penulis dan editor) yang sebagian besar diterbitkan oleh penerbit internasional bereputasi.</p>
<p>Kini, inovasi yang diciptakan Teuku Faisal tersebut sudah digunakan oleh beberapa perusahan besar di Indonesia, di antaranya PT Pertamina Geothermal Energy, PT Freeport Indonesia, United Mercury Group, dan PT Medco International. Oleh sebab itu, GAMA-EWS sudah diproduksi oleh Unit Usaha UGM dengan kapasitas 500 unit per tahun, melibatkan sekira 40 orang tenaga kerja terampil. Guna meningkatkan kapasitas produksi, ke depan pihaknya akan membangun Teaching Industry sehingga bisa memenuhi kebutuhan pasar, khususnya dalam bidang kebencanaan, baik domestik maupun pasar global.</p>
<p>Sementara pemenang selanjutnya, Prof. Dr. dr. Mulyanto merupakan Dosen di Fakultas Kedokteran Universitas Mataram. Dia berhasil menemukan reagensia diagnosis cepat beberapa penyakit, seperti reagensia anti-HIV dengan metode <em>imunochromatography</em>, reagensia <em>imunochromatography</em> malaria, deteksi virus hepatitis C, dan reagensia HBsAg. Tidak tanggung-tanggung, produk yang dihasilkan Mulyanto sudah dipasarkan di seluruh Indonesia melalui PT Hepatika, bahkan khusus reagensia untuk deteksi malaria sudah digunakan di Kansai Airport dan Osaka, Jepang.</p>
<p>Selama pengabdiannya, lulusan Doktor dari Universitas Airlangga ini telah terbukti memberikan kontribusi yang cukup besar terhadap ilmu immunologi melalui 24 <em>paper</em> yang diterbitkan di jurnal internasional bereputasi, yaitu dengan H-indeks Scopus = 15. Selain itu, Mulyanto pun banyak melakukan kerja sama penelitian dengan School of Medicine, Jichi Medical University di Jepang untuk menghasilkan inovasi di bidang kedokteran.</p>
<p>Semangat pria yang lahir di Cilacap, 25 Mei 1948 ini juga patut diacungi jempol. Pasalnya, meski akan memasuki usia pensiun tahun depan, Mulyanto masih sangat aktif melakukan penelitian untuk mengembangkan teknik diagnosis penyakit manusia. Hal ini sesuai dengan obsesinya, yakni menghasilkan teknologi diagnosis yang akurat, cepat, praktis, dan terjangkau oleh semua kalangan. (<em>ira)</em></p>
<p><a href="http://sumberdaya.ristekdikti.go.id/index.php/2017/08/12/academic-leader-sebuah-dedikasi-para-dosen-produktif-dan-kreatif/">http://sumberdaya.ristekdikti.go.id</a></p>',
                'image' => 'posts/November2017/IMG-20170811-WA0124.jpg',
            'slug' => 'teuku-faisal-fathani-ph-d-(tn-1)-menerima-anugerah-academic-leader-pada-malam-apresiasi-hakteknas-ke-22-di-universitas-negeri-makassar',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 11:55:25',
                'updated_at' => '2017-11-09 11:55:25',
                'image_url' => NULL,
            ),
            10 => 
            array (
                'id' => 21,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Pembukaan PORSISTARA',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p><span class=" _50f4">PORSISTARA atau pekan olahraga siswa SMA Taruna Nusantara merupakan kegiatan rutin tahunan siswa SMA Taruna Nusantara yang biasanya dilaksanakan setelah siswa melaksanakan ujian kenaikan kelas.setelah seminggu penuh siswa belajar keras dan mengarerjakan soal UKK,tentunya harus ada sesuatu hiburan.Hiburan ini dilakukan dengan cara pertandingan olahraga antar kelas. Porsistara ini diselenggarakan dalam rangka menggali dan mengembangkan potensi para siswa di bidang olahraga.Selain itu kegiatan ini dapat menambah rasa sportivitas dan kekeluargaan antar siswa.Disamping itu Porsistara ini juga dapat menjadi ajang seleksi untuk mengikuti lomba-lomba dengan tingkantan yang lebih tinggi,sehingga kita dapat memberikan karya yang terbaik bagi sekolah kita tercinta, SMA Taruna Nusantara.</span></p>',
                'image' => 'posts/November2017/20818809_1536687983041240_2791727823844570850_o1.jpg',
                'slug' => 'pembukaan-porsistara',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:00:08',
                'updated_at' => '2017-11-09 12:00:08',
                'image_url' => NULL,
            ),
            11 => 
            array (
                'id' => 22,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'SMA Taruna Nusantara Dominasi Paskibra Kabupaten Magelang',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="blog-post-thumb"><label>Kegiatan latihan Paskibraka</label></div>
<div class="post-info clearfix">Selasa, 08 Agustus 2017 09:28:00 WIB | oleh : eko-susanto | <span class="comments-count"><a href="http://magelang.sorot.co/berita-3090-sma-taruna-nusantara-dominasi-paskibraka-kabupaten-magelang.html#comments">0 komentar</a></span></div>
<div class="entry-content">
<p><strong>Mungkid,(magelang.sorot.co)&ndash;</strong>Pasukan Pengibar Bendera Pusaka (Paskibraka) Kabupaten Magelang berjumlah 30 pelajar. Dari 30 pelajar tersebut, enam diantaranya berasal dari siswa SMA Taruna Nusantara (TN) Magelang. Nantinya, mereka ini akan menjalani karantina selama dua hari pada 15-17 Agustus 2017.</p>
<p>Kepala Seksi Pembinaan dan Pengembangan Pemuda, Dinas Pariwisata, Kepemudaan dan Olahraga Kabupaten Magelang, Sriyono mengatakan, proses seleksi untuk lolos menjadi Paskibraka dilakukan dengan ketat. Mereka harus memenuhi persyaratan telah ditentukan antara lain administrasi, postur tubuh, kesehatan dan akademik.</p>
</div>
<p>&nbsp;</p>
<blockquote>
<p><em>Salah satunya tinggi badan perempuan 165 cm dan laki-laki 170 cm. Proses seleksi dilakukan sejak April dengan total pendaftar pelajar SMA/SMK dan MA se-Kabupaten Magelang ada 120 siswa. Kemudian dilakukan seleksi diambil 50 dan terakhir 30,&rdquo; kata Sriyono di sela-sela latihan di Lapangan drh. Soepardi, Mungkid, Selasa (08/8/2017).</em></p>
</blockquote>
<p>Dalam seleksi ini, katanya, setiap kabupaten/kota mengirimkan dua terdiri perempuan dan laki-laki untuk mengikuti seleksi menjadi Paskibraka di Provinsi Jawa Tengah. Kemudian, dari Kabupaten Magelang yang lolos menjadi Paskibraka Provinsi Jawa Tengah yakni Agnes Andrea Damayanti dari SMAN 1 Muntilan.</p>
<blockquote>
<p><em>Agnes telah kami antarkan untuk mengikuti latihan di Semarang sejak 4 Agustus lalu dan kami akan menjemput kembali pada 19 Agustus,&rdquo; katanya.</em></p>
</blockquote>
<p>Adapun total Paskibraka ada 30 pelajar terdiri 16 putra dan 14 putri. Mereka nanti bakal bertugas gabung dengan pasukan 8 dan pasukan 17, sedangkan 4 siswa menjadi cadangan. Sementara itu seorang menjadi Paskibraka Provinsi Jateng.</p>
<p>Mereka yang terpilih merupakan siswa kelas X dari utusan masing-masing sekolah. Kemudian untuk personil dari TNI ada 25 dan Polri ada 27.</p>
<blockquote>
<p><em>Dari 30 siswa ini, 6 siswa dari SMA TN, 4 siswa SMAN 1 Muntilan, 2 dari SMAN 1 Mungkid dan lainnya rata-rata satu sekolah satu siswa. Untuk pelatih dari Kodim 0705/Magelang dan Polres Magelang, sedang latihan dilakukan Senin-Kamis dan Sabtu,&rdquo; katanya. (eko susanto)</em></p>
</blockquote>
<p><a href="http://magelang.sorot.co/berita-3090-sma-taruna-nusantara-dominasi-paskibraka-kabupaten-magelang.html">http://magelang.sorot.co</a></p>',
                'image' => 'posts/November2017/3090_kegiatan_latihan_paskibraka_20170808093048.jpg',
                'slug' => 'sma-taruna-nusantara-dominasi-paskibra-kabupaten-magelang',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:00:41',
                'updated_at' => '2017-11-09 12:00:41',
                'image_url' => NULL,
            ),
            12 => 
            array (
                'id' => 23,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Irfan Urane Azis Raih Grand Champion di International Mathematics Contest di Singapura',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<div class="pic_artikel">Foto: Putra kedua Idham Azis (Ist)</div>
<p>&nbsp;</p>
<div data-sticky_parent="">
<div id="detikdetailtext" class="detail_text"><strong>Jakarta</strong> &ndash; Pelajar tim Indonesia meraih prestasi gemilang dalam International Mathematics Contest di Singapura (IMCS). Salah satu pelajar yang meraih penghargaan Grand Champion adalah putra Kapolda Metro Jaya Irjen Idham Azis, Irfan Urane Azis.
<p>Putra kedua Idham Azis ini adalah siswa kelas 10 SMA Taruna Nusantara, Magelang, Jawa Tengah. Ia ikut lomba IMCS bersama 129 delegasi pelajar dari berbagai sekolah di Indonesia.</p>
<p>Gelar Grand Champion ini sangat sulit diperoleh karena tidak semua peraih medali emas dan tidak semua negara bisa memperoleh gelar tersebut. Irfan tidak menyangka dan sangat bersyukur bisa meraih gelar Grand Champion.</p>
<p>IMCS tahun ini cukup menantang dan perlu berpikir cepat untuk bisa menyelesaikannya karena waktunya sangat terbatas. Irfan, yang bercita-cita menjadi polisi, terus berlatih untuk mempersiapkan diri mengikuti lomba tersebut.</p>
<p>&ldquo;Untuk persiapan lomba IMCS, selain mengikuti karantina, saya banyak melakukan latihan soal secara mandiri di asrama di Sekolah Taruna Nusantara. Saya sering mencari soal-soal yang mirip dengan IMCS yang ada di internet sebagai bahan latihan. Hal yang penting untuk sukses dalam matematika adalah usaha dan doa,&rdquo; kata Irfan, Senin (7/8/2017).</p>
</div>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Pada IMCS 2017 ini, tim Indonesia meraih 14 emas, 26 perak, 50 perunggu, dan 38 merit. Tapi hal yang sangat membanggakan di tahun ini adalah seorang siswa Indonesia memperoleh penghargaan Grand Champion, yakni Irfan.</p>
<p>&nbsp;</p>
<p>Total ada 129 pelajar Indonesia yang terdiri dari pelajar kelas 3 SD sampai kelas 11 SMA mengikuti IMSC, yang digelar pada 4-7 Agustus 2017, ini. Adapun total peserta pada lomba ini adalah 1.178 oran, yang berasal dari 11 negara, yaitu China, Malaysia, Hong Kong, Taiwan, Filipina, Indonesia, Korea, Thailand, Iran, Vietnam, dan Australia.</p>
<p>&nbsp;</p>
<p>Para peserta IMCS dari Indonesia merupakan hasil seleksi dari sekitar 250 ribu siswa yang mengikuti Kompetisi Matematika Nalaria Realistik (KMNR) dan para siswa pilihan yang mengikut Math In House Training (MIHT). Lomba IMCS 2017 merupakan tahun ke-10 yang diikuti Indonesia.</p>
<p>&nbsp;</p>
<p>Sebelum berangkat ke Singapura, para siswa dibina terlebih dahulu selama 1 minggu di Bogor. Para pembina berasal dari Tim Klinik Pendidikan MIPA. Di sana para siswa tidak hanya belajar matematika, tapi juga belajar bahasa Inggris serta mendapat pembinaan jasmani dan rohani.</p>
<p>&nbsp;</p>
<p>Ridwan Hasan Saputra, sebagai Team Leader Indonesia, mengatakan prestasi yang dicapai tim Indonesia tahun ini sangat membanggakan karena perolehan medali melebihi standar yang ditetapkan panitia, khususnya untuk pencapaian medali emas. Apalagi di tahun ini ada siswa Indonesia yang meraih Grand Champion yang patut disyukuri.</p>
<p>&nbsp;</p>
<p>&ldquo;Bagi saya, lomba matematika di luar negeri tujuan utamanya adalah memberikan pengalaman dan wawasan kepada anak-anak agar mereka bisa melihat dunia, banyak anak lain yang lebih pintar dan negara yang lebih maju. Namun tetap rendah hati dan saling menghargai. Sehingga mereka akan lebih semangat lagi belajar supaya Indonesia bisa menjadi bangsa yang besar. Lomba IMCS ini jadi momentum untuk meningkatkan ibadah para peserta karena selama karantina mereka semakin rajin beribadah. Medali hanyalah bonus,&rdquo; ujar Ridwan, yang juga Presiden Direktur Klinik Pendidikan MIPA.</p>
<p>&nbsp;</p>
<p>Sekilas mengenai Irfan, putra Idham Azis ini memang dikenal berprestasi di sekolahnya. Dia bahkan mendapatkan beasiswa dari TN. Putra pertama Idham juga berprestasi. Dia baru saja lulus Akpol 2017 dengan peringkat pertama.</p>
<p>&nbsp;</p>
<p>&ldquo;Iya betul, Irfan itu putranya Pak Kapolda. Dia dapat beasiswa dari TN, tapi diberikan ke siswa yang tidak mampu,&rdquo; ujar Kabid Humas Polda Metro Jaya Kombes Argo Yuwono.</p>
<p><strong>(mei/rvk) <a href="https://news.detik.com/berita/d-3588580/putra-kapolda-metro-raih-grand-champion-di-lomba-matematika?utm_source=twitter&amp;utm_campaign=detikcomsocmed&amp;utm_medium=btn&amp;utm_content=news">https://news.detik.com</a></strong></p>',
                'image' => 'posts/November2017/3adeb260-a332-4a39-a7cc-4dbcac0ec2ed_169.jpg',
                'slug' => 'irfan-urane-azis-raih-grand-champion-di-international-mathematics-contest-di-singapura',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:01:32',
                'updated_at' => '2017-11-09 12:01:32',
                'image_url' => NULL,
            ),
            13 => 
            array (
                'id' => 24,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Irfan Urane Azis bersama Tim Indonesia Raih Prestasi di Lomba Matematika di Singapura',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="wp-cap">
<div class="caption">Sebanyak 129 pelajar Indonesia yang terdiri dari pelajar kelas 3 SD sampai kelas 11 SMA IMCS pada 4-7 Agustus 2017.</div>
</div>
<div class="detail-berita">
<div class="content-detail">
<p>REPUBLIKA.CO.ID, JAKARTA &mdash; Tim Indonesia meraih prestasi gemilang dalam kompetisi matematika International Mathematics Contest di Singapura (IMCS) di Singapura. Sebanyak&nbsp; 129 pelajar Indonesia yang terdiri dari pelajar kelas 3 SD sampai kelas 11 SMA IMCS pada 4-7 Agustus 2017.</p>
<p>Hasil yang diperoleh dari IMCS tahun 2017 adalah 14 emas, 26 perak, 50 perunggu dan 38 merit.&nbsp; Hal yang sangat membanggakan di tahun ini adalah&nbsp; seorang siswa Indonesia memperoleh penghargaan Grand Champion. Peraih Grand Champion asal Indonesia atas nama Irfan Urane Azis yang merupakan siswa kelas 10 SMA Taruna Nusantara, Magelang, Jawa Tengah.&nbsp; Gelar ini sangat sulit diperoleh karena tidak semua peraih medali emas bisa memperoleh gelar ini dan tidak semua negara bisa memperoleh gelar ini.</p>
<p>Irfan Urane Azis Peraih Grand Champion mengatakan dirinya tidak menyangka dan sangat bersyukur bisa menjadi Grand Champion.&nbsp; IMCS tahun ini soalnya lumayan menantang dan perlu berpikir cepat untuk bisa menyelesaikannya karena waktunya sangat terbatas. Sehingga dirinya tidak berpikir sama sekali untuk mendapat grand champion.</p>
<p>&ldquo;Untuk persiapan lomba IMCS selain mengikuti karantina, saya banyak melakukan latihan soal secara mandiri di asrama di Sekolah Taruna Nusantara. Saya sering mencari soal-soal yang mirip dengan IMCS yang ada di internet sebagai bahan latihan. Hal yang penting untuk sukses dalam matematika adalah usaha dan doa,&rdquo; kata Irfan</p>
<p>&nbsp;</p>
<p>Total peserta pada lomba ini adalah 1178 peserta yang berasal dari&nbsp; 11 negara yaitu Cina, Malaysia, Hongkong, Taiwan, Filipina, Indonesia, Korea, Thailand, Iran, Vietnam dan Australia. Para peserta IMCS dari Indonesia merupakan hasil seleksi dari sekitar 250 ribu siswa yang mengikuti Kompetisi Matematika Nalaria Realistik&nbsp; (KMNR) dan para siswa pilihan yang mengikut Math In House Training (MIHT). Lomba IMCS tahun 2017 merupakan tahun ke-10 yang diikuti oleh Indonesia.</p>
<p>Sebelum berangkat ke Singapura, para siswa dibina terlebih dahulu selama 1 minggu di Bogor. Para pembinanya dari Tim Klinik Pendidikan MIPA. Pada pembinaan ini para siswa tidak hanya belajar matematika tetapi belajar Bahasa Inggris serta mendapat pembinaan Jasmani dan Ruhani. Bagi peserta yang beragama Islam dirutinkan untuk selalu melaksanakan sholat secara berjamaah, sholat tahajud, sholat dhuha dan tadarus Alquran.</p>
<p>Ridwan Hasan Saputra sebagai Team Leader Indonesia mengatakan bahwa prestasi tahun ini sangat membanggakan karena perolehan medali tim Indonesia melebih standart yang ditetapkan panitia khususnya untuk raihan medali emas. Apalagi di tahun ini ada siswa Indonesia yang meraih Grand Champion, hal ini merupakan anugerah yang harus di syukuri.</p>
<p>Ridwan yang juga Presiden Direktur Klinik Pendidikan MIPA mengatakan lomba matematika di luar negeri tujuan utamanya adalah memberikan pengalaman dan wawasan kepada anak-anak agar mereka bisa melihat dunia. &ldquo;Banyak anak lain yang lebih pintar dan negara yang lebih maju. Namun tetap rendah hati dan saling menghargai. Sehingga mereka akan lebih semangat lagi belajar supaya Indonesia bisa menjadi bangsa yang besar. Lomba IMCS ini jadi momentum untuk meningkatkan ibadah para peserta karena selama karantina mereka semakin rajin beribadah. Medali hanyalah Bonus,&rdquo; kata Ridwan.</p>
<p>Senin , 07 Agustus 2017, 07:39 WIB &ndash; Red: Dwi Murdaningsih &ndash; <a href="http://www.republika.co.id/berita/trendtek/fun-science-math/17/08/07/ouahtr368-tim-indonesia-raih-prestasi-di-lomba-matematika-di-singapura">http://www.republika.co.id/</a></p>
</div>
</div>',
                'image' => 'posts/November2017/sebanyak-129-pelajar-indonesia-yang-terdiri-dari-pelajar-_170807073859-678.jpg',
                'slug' => 'irfan-urane-azis-bersama-tim-indonesia-raih-prestasi-di-lomba-matematika-di-singapura',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:02:07',
                'updated_at' => '2017-11-09 12:02:07',
                'image_url' => NULL,
            ),
            14 => 
            array (
                'id' => 25,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Alumni SMA Taruna Nusantara Dominasi Lolos Penerimaan Taruna Taruni Akmil AAL AAU & Akpol',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p><strong>SHNet, JAKARTA</strong> &ndash; Total 590 Calon Prajurit Taruna dan Taruni (Capratar) Akademi TNI terdiri dari Akmil 385 orang (352 Taruna dan 33 Taruni), AAL 105 orang (94 Taruna dan 11 Taruni) dan AAU 100 orang (90 Taruna dan 10 Taruni), bertempat di Gedung Lily Rochli Akademi Militer, Magelang, Jawa Tengah, Kamis (3/8) tuntas mengikuti selesai tes penerimaan Capratar Akademi TNI.</p>
<p>&nbsp;</p>
<p>Dari hasil sidang Pantukhir itu, memutuskan yang dinyatakan lulus menjadi Capratar Akademi TNI dan siap mengikuti pendidikan pertama sebanyak 435 orang terdiri dari Akmil 230 orang (212 Taruna dan 18 Taruni), AAL 105 orang (94 Taruna dan 11 Taruni) dan AAU 100 orang (90 Taruna dan 10 Taruni).</p>
<p>&nbsp;</p>
<p>Pada siswa alumnus SMA Taruna Nusantara jelas paling bangga dengan hasil akhir ini. Pasalnya, SMA Taruna Nusantara ini jadi sekolah yang paling banyak menyumbang taruna taruni dengan total 98 orang.</p>
<p>&nbsp;</p>
<p>Sebanyak 53 orang lolos Akmil, 13 orang ke AAL, 10 orang ke AAU, dan Akpol dengan 22 orang.<span id="more-10196"></span></p>
<p>&nbsp;</p>
<p>Tak cuma itu. Lebih mencengangkan lagi karena Sekolah SMA Taruna Nusantara, angkatan 25 yg lulus pada 2017 ini adalah yang terbanyak menyumbang taruna taruni sejak berdirinya sejak sekolah ini berdiri pada 1990.</p>
<p>Kepala Humas SMA Taruna Nusantara, <abbr><span class="hover37">Cecep Iskandar</span></abbr> Salam perbincangan dengan <em>SHNet,</em> Jumat (4/8) siang mengatakan kalau ia secara pribadi maupun sekolah sangat bangga atas prestasi anak-anak lulusan tahun ini yang sekaligus mencatat rekor terbanyak.</p>
<p>&nbsp;</p>
<p>&ldquo;Jelas kami bangga karena pendidikan yang diterima selama sekolah tak sia-sia dan bisa menghasilkan sesuatu yang positif. Sekarang ini semua Capratar tenti sedang sibuk. Nantinya kalau waktu mereka sudah longgar kami akan bertemu dan tentu ada apresiasi atas prestasi ini,&rdquo; jelas Cecep.</p>
<p>&nbsp;</p>
<p>Menurutnya apresiasi itu di maksud agar bisa lebih memicu pada siswa yang saat ini masih di Bangku pendidikan SMA.</p>
<p>&nbsp;</p>
<p>Salah satu orang Tua murid siswa SMA Taruna Nusantara, Joel Wakanno, yang lulus sebagai Taruna terbaik Akmil, Gerard Wakanno pun menyatakan kagum terhadap sekolah terbaik di Indonesia di bawah naungan Departemen Pertahanan yang merupakan tempat putranya menuntut ilmu hingga dengan bekal itu bisa lolos sebagai Taruna terbaik Akmil tahun ini.</p>
<p>&nbsp;</p>
<p>&ldquo;Sebagai orang tua tentu saya bangga dan semoga kelas anak saya dan teman-temannya bisa menjadi Taruna dan Taruni yang berguna bagi keluarga, juga Bangsa dan negara,&rdquo; tutur Gerald yang bekerja di Thailand ini.</p>
<p><strong>(Nonnie Rering) <a href="http://sinarharapan.net/2017/08/sma-taruna-nusantara-dominasi-lolos-penerimaan-taruna-taruni-akmil-aal-aau-akpol/">http://sinarharapan.net/</a></strong></p>',
                'image' => 'posts/November2017/IMG_4547-640x360.jpg',
                'slug' => 'alumni-sma-taruna-nusantara-dominasi-lolos-penerimaan-taruna-taruni-akmil-aal-aau-and-akpol',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:03:11',
                'updated_at' => '2017-11-09 12:03:11',
                'image_url' => NULL,
            ),
            15 => 
            array (
                'id' => 26,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Ilham Urane Azis (TN 25) peringkat 1 dan Sulthan Ali Achmad Zulfikar (TN 25) peringkat 3 Calon Taruna Terbaik Akpol 2017 adalah TN 25',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="detikdetailtext" class="detail_text">
<p><strong>Jakarta</strong> &ndash; Akademi Kepolisian (Akpol) telah rampung merekrut calon taruna angkatan 2017. Sebanyak 282 calon taruna (catar) terpilih mendapat pendidikan taruna/taruni. Di antara ratusan catar itu beberapa yang dinilai berprestasi.</p>
<p>&ldquo;Saya memberikan hadiah kepada catar peraih nilai terbaik,&rdquo; kata Asisten bidang SDM Kapolri, Irjen Arief Sulistyanto kepada detikcom, Kamis (3/8/2017).<br /> <span id="more-10194"></span></p>
<p>Dari foto yang diberikan Arief pada detikcom, ada 5 catar yang berfoto dengan Arief. Empat catar memegang trofi berbentuk seorang taruna dan seorang memegang trofi besar berbentuk taruna dan sebuah bangunan.</p>
<p>&ldquo;&rdquo;Ada 5 penerima,&rdquo; ucap Arief.</p>
<p>Berikut para catar terpilih yang menerima trofi:</p>
<p>&bull; Peringkat 1 terbaik : Ilham Urane Azis dari Polda Metro Jaya<br /> &bull; Peringkat 2 terbaik : Arya Wisnu Dwi Saputra dari Polda Jateng<br /> &bull; Peringkat 3 terbaik : Sulthan Ali Achmad Zulfikar dari Polda Lampung<br /> &bull; Nilai tes kesamaptaan jasmani terbaik : Billy Pratama Putra dari Polda Kepri<br /> &bull; Nilai tes Bahasa Inggris terbaik : Alexander Timothy Simanjuntak dari Polda Metro Jaya</p>
</div>
<p><strong>Jumat 04 Agustus 2017, 09:53 WIB (aud/fjp) <a href="https://news.detik.com/berita/d-3585894/apresiasi-untuk-5-calon-taruna-terbaik-akpol-2017">Audrey Santoso &ndash; detikNews</a></strong></p>',
                'image' => 'posts/November2017/f0a0288a-9ee8-472c-8711-ba7e5c77e300.jpg',
            'slug' => 'ilham-urane-azis-(tn-25)-peringkat-1-dan-sulthan-ali-achmad-zulfikar-(tn-25)-peringkat-3-calon-taruna-terbaik-akpol-2017-adalah-tn-25',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:03:48',
                'updated_at' => '2017-11-09 12:03:48',
                'image_url' => NULL,
            ),
            16 => 
            array (
                'id' => 27,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Ilham Urane Aziz (TN 25) rank 1, Gholib Yudha Mawaridi (TN 25) rank 4, Muhammad Euro Belmiro Lamza (TN 25) rank 6 Tes Akademik Akpol',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="detikdetailtext" class="detail_text">
<p><strong>Jakarta</strong> &ndash; Ujian tahap tiga yakni tes akademik untuk calon taruna akademi kepolisian 2017 sudah selesai digelar. Ilham Urane Aziz, putra dari Kapolda Metro Jaya Irjen Idham Aziz menjadi yang terbaik di ujian ini.</p>
<p>Berdasarkan data resmi dari bagian SDM Polri, Ilham meraih nilai rata-rata 80,81 dari hasil tes potensi akademik dan bahasa Inggris. Remaja berusia 18 tahun itu menjadi peserta terbaik dari 331 peserta lainnya di tahap tes akademik yang dilangsungkan pada Sabtu (29/7) di Universitas Dian Nuswantoro (Udinus) Semarang.</p>
<p>Dari 10 peserta terbaik dalam tes akademik, tiga diantaranya adalah putra dari anggota kepolisian, empat lainnya dari ayahnya bekerja di swasta, seorang anak dari pegawai BUMN, dan dua anak lainnya ayahnya bekerja sebagai PNS.<br /> <span id="more-10192"></span></p>
<p><img class="" src="https://images.detik.com/community/media/visual/2017/07/30/22e7848d-2e74-4213-8f03-963c32db9f56.jpg?a=1" alt="Putra Kapolda Metro Raih Nilai Terbaik di Tes Akademik Akpol" width="802" height="394" /></p>
<p>Foto: 10 besar tes akademik Akpol 2017</p>
<p>Beda dengan tahapan tes sebelumnya, di tes akademik ini tidak ada pengguguran. Namun hasil tes akademik ini tetap penting karena nantinya akan diakumulasikan dengan tes-tes lain.</p>
</div>
<p>Pada tanggal 1 Agustus besok 331 calon taruna yang mengikuti seleksi akan menjalani tes pemeriksaan penampilan. Tes akan dilakukan dipimpin Wakapolri dengan anggota, Irwasum, Kalemdikpol, Ass SDM Kapolri, Kadiv Propam, dan Gubernur Akpol.<strong>Dijamin Transparan</strong></p>
<p>Asisten Kapolri Bidang SDM Irjen Arief memastikan tes akademik dan juga tes-tes lainnya dilakukan dengan transparansi penuh. Pada tes akademik misalnya, hasil sementara nilai yang diperoleh calon taruna bisa disimak secara realtime oleh para kerabat.</p>
<p>&nbsp;</p>
<p>&ldquo;Dilakukan secara transparan. Sesuai dengan misi untuk melakukan reformasi Polri,&rdquo; kata Arief di Udinus, Sabtu (30/7/2017) kemarin.</p>
<p>&nbsp;</p>
<p>Bahkan di tahap tes jasmani, ada seorang putra dari jenderal polisi bintang satu yang gugur. Si calon taruna gugur karena tidak berhasil melakukan pull up.</p>
<p>&nbsp;</p>
<p>&ldquo;Salah satu calon taruna yang gugur itu adalah teman saya sendiri. Satu kantor dengan saya. Bapaknya seorang brigadir jenderal,&rdquo; kata Arief, Jumat (30/7) kemarin.</p>
<p>&nbsp;</p>
<p><strong>Lingkungan Bikin Putra Polri Lebih Siap</strong></p>
<p>&nbsp;</p>
<p>Sekretaris Kompolnas Bekto Suprapto mengapresiasi reformasi yang dilakukan Arief. Sedangkan untuk putra dari anggota kepolisian yang lolos atau mendapatkan nilai tertinggi, kata Bekto, hal itu memang sangat mungkin terjadi.</p>
<p>&ldquo;Mengapa mereka bisa mendapatkan nilai yang lebih tinggi? Kecenderungannya karena mereka sudah disiapkan. Ayah mereka seorang polisi, alumni Akpol, mengetahui apa saja yang diperlukan untuk lolos. Karena Mereka sudah dilatih sejak lama. Dibiasakan sejak kecil,&rdquo; kata Bekto.<br /> <strong>(fjp/fjp) <a href="https://news.detik.com/berita/d-3579509/putra-kapolda-metro-raih-nilai-terbaik-di-tes-akademik-akpol">Fajar Pratama &ndash; detikNews </a></strong></p>',
                'image' => 'posts/November2017/db76b738-5495-45fe-8f7d-356d949bdfca_169.jpg',
            'slug' => 'ilham-urane-aziz-(tn-25)-rank-1-gholib-yudha-mawaridi-(tn-25)-rank-4-muhammad-euro-belmiro-lamza-(tn-25)-rank-6-tes-akademik-akpol',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:04:42',
                'updated_at' => '2017-11-09 12:04:42',
                'image_url' => NULL,
            ),
            17 => 
            array (
                'id' => 28,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Sulthan Ali Achmad Zulfikar (TN 25) peserta terbaik ketiga se-Indonesia calon taruna Akpol angkatan 2017',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p><strong>&nbsp;TRIBUNLAMPUNG.CO.ID,BANDAR LAMPUNG &ndash;</strong> Sultan Ali Achmad Zulfikar peserta terbaik ketiga se-Indonesia dalam proses rekrutmen calon taruna (catar) angkatan 2017, ternyata kerap menorehkan prestasi. Prestasi ini diukirnya sejak Sultan duduk di bangku sekolah dasar (SD) Rawa Laut, Bandar Lampung, hingga sampai tingkat sekolah menengah atas (SMA).</p>
<p>Hal tersebut disampaikan langsung&nbsp; kedua orangtuanya Dika Oriputra dan Euis Susanty saat ditemui Tribun di kediamannya, di Jalan Ikan Ekor Kuning, Kelurahan Pesawahan, Kecamatan <a title="" href="http://lampung.tribunnews.com/tag/telukbetung-selatan">Telukbetung Selatan</a>, Bandar Lampung, Sabtu, (05/08).</p>
<div id="div-Inside-MediumRectangle">&nbsp;</div>
<p>Sultan merupakan anak pertama dari dua bersaudara, kelahiran Jakarta, 5 Mei 1999. Sedangkan, adiknya. bernama Adinda Putri Azzahra, masih duduk dibangku kelas 6 SD.</p>
<p>&ldquo;Kebetulan dari&nbsp; awal masuk SD, kecerdasan Sultan sudah mulai terlihat dan selalu mendapat ranking di sekolahnya dan lulus pada tahun 2011,&rdquo; tutur Euis didampingi suaminya (Dika) saat diwawancarai Tribun di rumahnya.</p>
<p>Jenjang SMP Sultan berhasil lolos dan menajdi siswa di SMP N 2, Bandar Lampung dan lulus pada tahun 2014.</p>
<p>Menurut Euis, di tingkat SMP Sultan makin lebih banyak menorehkan prestasi, baik dalam aktifitas kegiatan belajarnya di sekolah, maupun kegiatan ekstra kuliner. Berbagai kegiatan lomba Sultan menjadi perwakilan utusan dari sekolahnya, bahkan sampai mewakili Provinsi Lampung.</p>
<p>&ldquo;Ketika itu, Sultan dinobatkan sebagai Ketua Forum Anak Provinsi Lampung,&rdquo; seingat Bunda sapaan-akrabnya Euis Susanty</p>
<p>Di tingkat SMA, Euis mengatakan, Sultan menempuh pendidikan di SMA Taruna Nusantara di Magelang, Jawa Tengah dan lulus pada tahun 2017.</p>
<p>&ldquo;Berbagai prestasi ditorehkan oleh Sultan, salah satunya Juara I Kesamaptaan Jasmani dan masih banyak piala dan penghargaan yang ditorehkan oleh Sultan,&rdquo; bilangnya</p>
<p>Sebelumnya, Sultan Ali Achmad Zulfikar menjadi peserta terbaik ketiga se-Indonesia dalam proses rekrutmen calon taruna (catar) angkatan 2017. Di antara ratusan catar itu Sultan dinilai berprestasi, sekaligus mengharumkan nama&nbsp;Polda Lampung.</p>
<p><a href="http://lampung.tribunnews.com/2017/08/05/sultan-ali-ternyata-terbiasa-berprestasi-sejak-bangku-sd">http://lampung.tribunnews.com/</a></p>',
                'image' => 'posts/November2017/ibunda-sultan-ali_20170805_173436.jpg',
            'slug' => 'sulthan-ali-achmad-zulfikar-(tn-25)-peserta-terbaik-ketiga-se-indonesia-calon-taruna-akpol-angkatan-2017',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:05:23',
                'updated_at' => '2017-11-09 12:05:23',
                'image_url' => NULL,
            ),
            18 => 
            array (
                'id' => 29,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Letda Bernardinus Yoga Kristian (TN 21) dan Ipda Ade Hertiawan Juniansyah (TN 21) dilantik dan diberikan penghargaan Adhi Makayasa oleh Presiden RI',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>Selamat kepada Letda <a class="profileLink" href="https://www.facebook.com/bernardinus.kristian?fref=mentions" target="_blank" rel="noopener">Bernardinus Yoga Kristian</a> (TN 21) dan Ipda <a class="profileLink" href="https://www.facebook.com/ade.h.juniansyah?fref=mentions" target="_blank" rel="noopener">Ade Hertiawan Juniansyah</a> (TN 21) yang hari ini dilantik dan diberikan penghargaan Adhi Makayasa oleh Presiden RI.</p>
<p>"Semoga Tuhan Memberkati Sumpah dan Janjimu"</p>
<p><a href="https://l.facebook.com/l.php?u=https%3A%2F%2Fgoo.gl%2F1Rbw4k&amp;h=ATPJ7UkH-EHZ7ZvcvaGGN2xwz7IsFNxocycMDk9lk_cWMgBgQYvmwPN9foJHtLZfLOPd8BnVmnZzI2UWFGr6BB_iU4lO9AUwHIPVfJ2rMY_Cg7Hkxhq8YnOpxJnt40fK6cEFS9V8qrHpVfSMipUDVv5zcRgcajfQ6pk_CFyvGcq6fFWj_9F09zV5ESd_KgPfpWiiFuz4rODqqz-I8ypmtM36s7sSG4gPZZ4q6r4L0NXhMrJhyX105QVKKuXG4jZrmxAzYwa8iYtB1ijAIE194-5GjUTcppdPIWnotbc" target="_blank" rel="nofollow noopener" data-ft="{&quot;tn&quot;:&quot;-U&quot;}" data-lynx-mode="async">https://goo.gl/1Rbw4k</a></p>
</div>',
                'image' => 'posts/November2017/2017-11-09_115258.jpg',
            'slug' => 'letda-bernardinus-yoga-kristian-(tn-21)-dan-ipda-ade-hertiawan-juniansyah-(tn-21)-dilantik-dan-diberikan-penghargaan-adhi-makayasa-oleh-presiden-ri',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:10:13',
                'updated_at' => '2017-11-09 12:10:13',
                'image_url' => NULL,
            ),
            19 => 
            array (
                'id' => 30,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Direktur Reskrimsus Polda Riau Kombes Johny Edison Isir (TN 1) Jadi Ajudan Pribadi Presiden RI',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<div class="post-thumb-desc">Direktur Reskrimsus Polda Riau, Kombes Johny Edizzon Isir (Foto: Dokumen GoRiau.com)</div>
<div class="post-content tabs">
<div id="page1" class="active"><strong>PEKANBARU &ndash; </strong> Direktur Reserse Kriminal Khusus Polda Riau, Kombes Johny Edison Isir dikabarkan lolos sebagai Aide De Camp (ADC) atau awam disebut sebagai ajudan pribadi. Tak tanggung-tanggung, informasinya ia jadi ADC orang nomor satu di Indonesia Presiden RI Joko Widodo (Jokowi).</div>
<div class="active">&nbsp;</div>
<div class="active">Informasi yang dirangkum <em>GoRiau.com</em>, Kombes Johny lolos menjadi ajudan pribadi Presiden RI setelah menjalani serangkaian tes khusus. Tentunya ini posisi yang menjanjikan, terutama bagi karir jebolan Akpol terbaik (Adhimakayasa) angkatan 96 tersebut.Edison Isir belum lama menjabat selaku Direktur Reserse Kriminal Khusus Polda Riau, menggantikan Kombes Rivai Sinambela. Kini pemegang penghargaan Satya Lencana Dharma Seroja tahun 2005 itu pun sudah punya tanggung jawab baru sebagai ajudan Presiden RI.Kapolda Riau Irjen Zulkarnain mengatakan, dirinya belum tahu soal kabar tersebut.</div>
<div class="active">Setahunya Isir baru akan mengikuti tes saja. Walau demikian, Jenderal bintang dua ini berharap Isir terpilih sebagai ADC.&rdquo;Semoga saja. Kalau untuk penggantinya tentu saja belum ada,&rdquo; jawabnya berbincang dengan <em>GoRiau.com</em>, Senin (24/7/2017) siang.Terpisah, Kombes Johny Edison Isir mengungkapkan, bahwa dirinya berharap agar posisi tersebut menjadi amanah. &ldquo;Semoga lancar dan menjadi amanah,&rdquo; ungkapnya kepada <em>GoRiau.com</em> melalui pesan Whatsapp.Selain Adhimakayasa 96, Kombes Johny juga punya sederet pengalaman terkait penanganan kasus-kasus yang menyangkut penyelundupan dan Korupsi. Ia pernah tergabung dalam Satgas BBM Ilegal Batam (2005) dan menjabat sebagai Kasatgas Anti Korupsi Polda Papua. <strong>***</strong></div>
</div>
<div class="active"><a href="https://www.goriau.com/berita/peristiwa/direktur-reskrimsus-polda-riau-kombes-johny-edison-isir-jadi-ajudan-pribadi-presiden-ri.html">https://www.goriau.com/</a></div>',
                'image' => 'posts/November2017/95c64fab1a372d85e55409udg-79713.jpg',
            'slug' => 'direktur-reskrimsus-polda-riau-kombes-johny-edison-isir-(tn-1)-jadi-ajudan-pribadi-presiden-ri',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:10:48',
                'updated_at' => '2017-11-09 12:10:48',
                'image_url' => NULL,
            ),
            20 => 
            array (
                'id' => 31,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Lulusan Terbaik AAU 2017 Letda Elektro Bernadinus Kristian (TN 21) Raih Adhi Makayasa',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="paragraph-article-detail">
<div class="row">
<div class=" deduktif-paragraph text-justify">
<p class="first-paragraph-bottom-zero">Yuniyanto tak pernah menaruh beban berat di atas pundak anaknya. Sebagai seorang petani, Yuni, sapaannya, hanya berharap anak keduanya, Bernadinus Yoga Kristian, diterima saat mendaftar di Akademi Angkatan Udara (AAU) empat tahun silam.</p>
</div>
</div>
<div class="text-justify">
<p>Rasa lelah masih begitu tampak pada wajah Yuni. Tampangnya pun tampak lusuh seolah baru bangun tidur. Pakaiannya yang dikenakan sangat sederhana, kaos polo motif garis dipadu celana tiga perempat. Di balik rasa lelah yang masih mendera, wajah bapak dua anak ini tak dapat menyembunyikan kebahagiaan. Senyum selalu mengembang.</p>
</div>
<div class="text-justify">
<p>&ldquo;Baru sampai rumah tadi pagi sekitar jam 04.00,&rdquo; ucap Yuni menceritakan bahwa dirinya baru saja pulang dari Jakarta saat ditemui di rumahnya, Dusun Kedon, Sumbermulyo, Bambanglipuro, Bantul kemarin (26/7).</p>
<p>Yuni ke ibu kota menghadiri upacara pelantikan taruna dan taruni akademi angkatan dan akademi kepolisian yang telah lulus pendidikan menjadi perwira pertama di halaman Istana Merdeka Selasa (25/7) lalu. Putra bungsunya, Bernadinus Yoga Kristian, turut dilantik oleh Presiden Joko Widodo. Bahkan, Yoga menjadi salah satu dari empat terbaik alias penerima Adhimakayasa dari masing-masing akademi.</p>
</div>
</div>
<p>Pria kelahiran 51 tahun lalu ini tidak datang sendiri. Melainkan bersama istri, anak sulung, dan cucunya.&ldquo;Berangkat malam Sabtu. Naik travel,&rdquo; lanjutnya.</p>
<p>Kecerahan raut muka Yuni sebenarnya akumulasi dari kebahagiaan atas kelulusan Yoga. Keluarga juga sudah mendengar bahwa Yoga bakal meraih Adhimakayasa sejak awal bulan ini. Persisnya saat acara wisuda sarjana terapan pertahanan di Gedung Sabang Merauke Kompleks AAU Bumi Maguwo. Namun, saat itu Yuni belum meyakini kepastiannya. Bagi Yuni, capaian ini di luar ekspektasi. Saat itu keluarga hanya berharap Yoga dapat diterima kala mendaftar di AAU pada 2013 lalu. Tak ada harapan lain. Sebab, AAU merupakan pilihan Yoga pribadi selepas lulus dari SMA Taruna Nusantara di Magelang.</p>
<p>Bahkan, Yoga sendiri yang mengambil, sekaligus mengisi formulir pendaftaran di SMA Taruna Nusantara. Terlebih, belum pernah ada keluarganya yang berkarir di dunia militer. Karena itulah keluarga sangat mendukung pilihan pendidikan pemuda kelahiran 1994 yang kini berpangkat letda elektro itu. &ldquo;Sing penting nek klebu melu dalan sing apik. Gur niku (Yang penting kalau diterima ikut jalan yang baik. Hanya itu, Red),&rdquo; ucapnya.</p>
<p>Selain bangga atas prestasi anaknya, Yuni menganggap penghargaan itu sebagai beban tersendiri. Minimal, Yoga harus sanggup mempertahankan kualitas diri. Sebab, Adhimakayasa bukanlah tujuan sekaligus akhir perjalanan. Sebaliknya, justru sebagai titik awal permulaan karirnya. &ldquo;Kudu tenanan. Ora oleh sembrono (Harus sungguh-sungguh. Tidak boleh main-main, Red),&rdquo; kata Yuni menirukan pesan khusus kepada anaknya usai penerimaan penghargaan.</p>
<p>Kendati anaknya bakal memiliki karir militer yang cemerlang, Yuni tetap tak jemawa. Tak lupa, Yuni mewanti-wanti anaknya agar tulus mengabdi kepada negara. Sekaligus siap ditempatkan di mana pun.</p>
<p>&ldquo;Ora duwe pengangkah nendi-nendi (Tidak punya niat macam-macam, Red),&rdquo; ungkap Yuni mengaku hingga sekarang belum mengetahui Yoga bakal berdinas di mana.</p>
<p>Sewaktu kecil, Yuni bercerita bahwa Yoga seperti anak seusianya. Bermain teman-teman sebayanya sepulang sekolah. Namun, menginjak SMP, Yuni mulai serius memberikan pengarahan. Itu ditandai dengan pembatasan jam bermain. Putra pasangan Yuniyanto-Chatharina Suharyanti ini hanya boleh bermain di luar rumah saat akhir pekan.<br /> &ldquo;Setelah sekolah les sampai sore. Anaknya juga mau,&rdquo; kenangnya.</p>
<p>Pembatasan jam bermain ini juga bertujuan untuk mendongkrak kompetensi Yoga. Sebab, sepanjang duduk di bangku SD prestasi akademik Yoga cukup mencolok. Tak pernah ke luar dari tiga terbaik. Begitu pula saat duduk di bangku SMP. &ldquo;Di SMA juga hampir sama,&rdquo; urainya.</p>
<p><strong>(rj/zam/ong/JPR)</strong></p>
<p><strong>ZAKKI MUBAROK, Bantul</strong></p>',
                'image' => 'posts/November2017/lulusan-terbaik-aau-2017-letda-elektro-bernadinus-kristian_m_3923.jpeg',
            'slug' => 'lulusan-terbaik-aau-2017-letda-elektro-bernadinus-kristian-(tn-21)-raih-adhi-makayasa',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:12:31',
                'updated_at' => '2017-11-09 12:12:31',
                'image_url' => NULL,
            ),
            21 => 
            array (
                'id' => 32,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Kinantan Arya Bagaspati raih medali perunggu di Olimpiade Matematika di Brazil',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p>Selamat untuk Tim Olimpiade Matematika Indonesia, khususnya Kinantan Arya Bagaspati (TN 27) yang meraih medali perunggu dalam ajang Olimpiade Matematika Internasional 2017 di Brazil.</p>',
                'image' => 'posts/November2017/20245905_1519512461425459_353414053412545242_n.jpg',
                'slug' => 'kinantan-arya-bagaspati-raih-medali-perunggu-di-olimpiade-matematika-di-brazil',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:13:36',
                'updated_at' => '2017-11-09 12:13:36',
                'image_url' => NULL,
            ),
            22 => 
            array (
                'id' => 33,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'SMA TN turut menyumbang 7 medali bagi Propinsi Jawa Tengah dalam OSN 2017 di Pekanbaru yang terdiri dari 1 Emas, 2 Perak dan 4 Perunggu',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="js_s" class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>SMA TN turut menyumbang 7 medali bagi Propinsi Jawa Tengah dalam OSN 2017 di Pekanbaru yang terdiri dari 1 Emas, 2 Perak dan 4 Perunggu :</p>
<p>Mereka adalah :<br /> 1. Muhammad Salman Al Farisi, Komputer - Emas<br /> 2. Ahmad Haulian Yoga Pratama, Komputer - Perak<br /> 3. Irfan Urane Azis, Matematika - Perak<br /> 4. Jamal Habibur Rahman, Geografi - Perunggu<br /> 5. Maulida Gadis Utami, Ekonomi - Perunggu<br /> 6. Lathif Syaifullah, Kimia - Perunggu<br /> 7. Riamizar Surya Baihaqi - Fisika - Perunggu</p>
<p>Rekapitulasi medali dapat di klik di <a href="https://l.facebook.com/l.php?u=https%3A%2F%2Fgoo.gl%2F2epkXC&amp;h=ATO3WH1aDAqdZSGezx7h9tI9bPBVcGfqKkw-RCUMeg-BGqgiwD0D-DJriC6XN0H3wtcNg5WgCOal2Wd9EmbZHOpOgCR80hKk3qw5dqcX80ai-BlpLmh_PS9Ew7Dbf6BwnI-TKswi8QYayKTTnPFZbyROhMiiNM44MAaNLNqdwTZ16m8uZ1BTo_ERApFr0SyZxd-faIgwdWgn_6V68JEfjG2R3LB_gBPoVc3j2fv66UyDxjUtpsVLts5Ao2TDh3fLuYK84boUtXfbw8UtMTO4JD5rAM_eU6A" target="_blank" rel="nofollow noopener" data-ft="{&quot;tn&quot;:&quot;-U&quot;}" data-lynx-mode="async">https://goo.gl/2epkXC</a></p>
</div>',
                'image' => 'posts/November2017/20258445_1514931618550210_1084510629793916378_n.jpg',
                'slug' => 'sma-tn-turut-menyumbang-7-medali-bagi-propinsi-jawa-tengah-dalam-osn-2017-di-pekanbaru-yang-terdiri-dari-1-emas-2-perak-dan-4-perunggu',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:14:50',
                'updated_at' => '2017-11-09 12:14:50',
                'image_url' => NULL,
            ),
            23 => 
            array (
                'id' => 34,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Berita Duka : Firdaus Hardhi Pranata (TN 8)',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="js_15" class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>Innalillahi wa inna illaihi rojiun.</p>
<p>Telah berpulang ke HadliratNya alumni SMA kami/kita, Firdaus Hardhi Pranata (TN 8), mohon doa semoga almarhum husnul khotimah dan keluarga yang ditinggalkan diberi kekuatan iman dan kesabaran menerima taqdirNya. Aamiin yra</p>
</div>',
                'image' => 'posts/November2017/20108244_1511946325515406_5587308976248361139_n.jpg',
            'slug' => 'berita-duka-firdaus-hardhi-pranata-(tn-8)',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:15:39',
                'updated_at' => '2017-11-09 12:15:39',
                'image_url' => NULL,
            ),
            24 => 
            array (
                'id' => 35,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Sermatutar Eka Indri Widarti (TN 21) Raih Anindya Wiratama',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="post-content">
<p>MAGELANG, KRJOGJA.com &ndash; <strong>Sermatutar Eka Indri Widarti (TN 21)</strong> terpilih sebagai taruni lulusan terbaik dan mendapatkan penghargaan Anindya Wiratama, yang diserahkan langsung Gubernur Akademi Militer (Akmil) Mayjen TNI Arif Rahman di Gedung Lily Rochli Akmil Magelang, Kamis (13/7/2017). Kegiatan di Gedung Lily Rochli ini juga dihadiri Wakil Gubernur Akmil Brigjen TNI Wisnoe PB maupun lainnya.</p>
<p>Informasi yang diperoleh KRJogja.com dari Penhumas Akmil, Kamis, menyebutkan pemberian penghargaan ini sesuai dengan keputusan Gubernur Akademi Militer Nomor Kep/26/IV/2016 tanggal 30 April 2016 tentang Petunjuk Pelaksanaan Evaluasi Hasil Belajar Sistem SKS Taruna Taruni Akademi Militer.</p>
<p>Peringkat pertama Sermatutar Eka Indri Widarti berhasil meraih penghargaan tertinggi berupa Anindya Wiratama, peringkat ke-2 diraih Sermatutar Rizky Puspita Rahayu, peringkat ke-3 diraih Sermatutar Anggi Anggraini, peringkat ke-4 diraih Sermatutar Feny Avisha dan peringat ke-5 diraih Sermatutar Rizka Ayu Rahma.</p>
<p>Gubernur Akmil atas nama Civitas Akademika Akademi Militer dan pribadi secara khusus mengucapan &ldquo;Selamat&rdquo; kepada lima taruni yang memperoleh nilai prestasi terbaik dari ketiga aspek secara akumulatif dari tingkat I (tahap-I/Diksarrit dan tahap-II/lanjutan), II, III serta IV, diberikan penghargaan setingkat dengan Adi Makayasa (penghargaan kepada Taruna) yaitu berupa medali Anindya Wiratama.</p>
<p>Gubernur Akmil berpesan agar para taruna dan taruni agar dapat mengembangkan semua bekal, potensi dan kapasitas diri yang telah diperoleh selama mengikuti proses pendidikan di Akademi Militer, untuk diaplikasikan di medan pengabdian yang sesungguhnya, yaitu satuan tempat mereka bertugas nantinya. (Tha)</p>
</div>
<p><a href="http://krjogja.com/web/news/read/38085/Sermatutar_Eka_Indri_Raih_Anindya_Wiratama">http://krjogja.com/web/news/read/38085/Sermatutar_Eka_Indri_Raih_Anindya_Wiratama</a></p>',
                'image' => 'posts/November2017/m-taruni1.jpg',
            'slug' => 'sermatutar-eka-indri-widarti-(tn-21)-raih-anindya-wiratama',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:16:19',
                'updated_at' => '2017-11-09 12:16:19',
                'image_url' => NULL,
            ),
            25 => 
            array (
                'id' => 36,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Menhan Minta Siswa SMA Taruna Nusantara Jauhi Tindak Kekerasan',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="entry-content">
<p><strong>Mertoyudan, (magelang.sorot.co)&ndash;</strong>Menteri Pertahanan RI, Ryamizard Ryacudu meminta kepada para siswa SMA Taruna Nusantara (TN) Magelang untuk menghindari kesombongan dan arogansi. Ia juga meminta agara siswa menjauhi tindak kekerasan sekecil mungkin.Hal itu terlebih keberadaan SMA TN telah mendapatkan pengakuan dari masyarakat luas, bukan karena menyandang nama besar, melainkan karena tradisi prestasi.</p>
<p>Keberadaan SMA TN, kata Menhan, memperoleh penghargaan di tingkat nasional sebagai sekolah pendidikan karakter bangsa dan sekolah rujukan nasional. Selain itu, memperoleh sertifikat ISO 9001-2015 yang menunjukkan SMA TN sudah terstandarisasi secara internasional. Prestasi ini merupakan modal dan sumber motivasi yang baik bagi SMA TN untuk terus meningkatkan diri dalam usianya yang menginjak ke-28.</p>
<p>&nbsp;</p>
<blockquote>
<p><em>Keberhasilan ini jangan membuat kalian sombong dan lupa diri yang hanya akan menjadi hambatan dalam pembangunan karakter yang sesungguhnya. Karena pada dasarnya upaya mempertahankan suatu prestasi akan lebih sulit dari pencapaian itu sendiri,&rdquo; kata Ryamizard di sela-sela pelantikan siswa baru SMA TN angkatan 28 di Balairung Pancasila, Sabtu (15/7/2017).</em></p>
</blockquote>
<p>Khusus untuk siswa baru kelas X angkatan 28 sebanyak 380 orang, Ryamizard menyampaikan ucapan selamat. Hal ini sungguh pantas disyukuri karena telah lolos dan diterima sebagai siswa SMA Taruna Nusantara.</p>
<blockquote>
<p><em>Kalian merupakan putra-putri terbaik yang dipilih dengan kriteria dan standar yang sangat tinggi dan para pendaftar yang berjumlah lebih dari 4.000 dari seluruh Indonesia,&rdquo; tegasnya.</em></p>
</blockquote>
<p>Selain itu, pihaknya meminta kepada para siswa baru untuk segera menyesuaikan diri dengan lingkungan pendidikan yang baru di sekolah berasrama.</p>
<blockquote>
<p><em>Tugas kalian di sini adalah belajar, belajar dan belajar serta berjuang keras untuk menimba ilmu, pengalaman dan keterampilan. Hilangkan rasa malas, asal-asalan, egois dan jangan cengeng,&rdquo; pintanya.</em></p>
</blockquote>
<p>Sementara itu, Kepala SMA Taruna Nusantara, <abbr><span class="hover10">Usdiyanto</span></abbr> menambahkan, tahun pelajaran 2017/2018 dimulai pada Senin 17 Juli 2017 dengan diikuti tiga angkatan yakni angkatan 26, 27 dan 28.</p>
<p>Untuk kelas X 380 siswa terdiri atas 260 siswa putra dan 120 siswa putri terbagi pemipanan IPA 316 siswa serta 64 siswa IPS. Kemudian untuk kelas XI terdiri dari 372 siswa terdiri siswa IPA 285 dan IPS ada 87 siswa. Sedangkan kelas XII terdiri dari 360 siswa untuk siswa IPA 302 siswa dan IPS 58 siswa.</p>
<blockquote>
<p><em>SMA Taruna Nusantara menggunakan perpaduan kurikulum 2013 Kementerian Pendidikan dan Kebudayaan serta kurikulum khusus SMA TN yang bertujuan mengembangkan potensi kepemimpinan siswa yang dikelola dalam bentuk sekolah berasrama penuh,&rdquo; katanya. (eko susanto)</em></p>
</blockquote>
<p><a href="http://magelang.sorot.co/berita-2950-link-.html">http://magelang.sorot.co/berita-2950-link-.html</a></p>
</div>',
                'image' => 'posts/November2017/19983900_1509273409116031_2042734872888197599_o.jpg',
                'slug' => 'menhan-minta-siswa-sma-taruna-nusantara-jauhi-tindak-kekerasan',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:17:16',
                'updated_at' => '2017-11-09 12:17:16',
                'image_url' => NULL,
            ),
            26 => 
            array (
                'id' => 37,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Berita Duka: Kolonel (Purn) Longinus Suyono',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p>Berita Duka, telah meninggal dunia dengan tenang, Bapak Kolonel (Purn) Longinus Suyono, Wakil Kepala Sekolah Kesiswaan 2006 &ndash; 2007</p>',
                'image' => 'posts/November2017/2017-11-09_120141.jpg',
            'slug' => 'berita-duka-kolonel-(purn)-longinus-suyono',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:18:49',
                'updated_at' => '2017-11-09 12:18:49',
                'image_url' => NULL,
            ),
            27 => 
            array (
                'id' => 38,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Ade Hertiawan Juniansyah (TN 21) raih Adhi Makayasa Akpol 2017',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="js_10" class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>Selamat kepada Ade Hertiawan Juniansyah (TN 21) yang menjadi lulusan terbaik Akademi Kepolisian 2017 dan meraih Adhi Makayasa.</p>
<p>\'Semoga Tuhan memberkati sumpah dan janjimu\'</p>
<p>@adeehj</p>
</div>',
                'image' => 'posts/November2017/19905024_1506162012760504_7800058277285839263_n.jpg',
            'slug' => 'ade-hertiawan-juniansyah-(tn-21)-raih-adhi-makayasa-akpol-2017',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:19:31',
                'updated_at' => '2017-11-09 12:19:31',
                'image_url' => NULL,
            ),
            28 => 
            array (
                'id' => 39,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Bernardinus Yoga Kristian (TN 21) raih Adhi Makayasa AAU 2017',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="js_13" class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>Selamat kepada <a class="profileLink" href="https://www.facebook.com/bernardinus.kristian?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=100000091524321&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Bernardinus Yoga Kristian</a> (TN 21) yang menjadi lulusan terbaik AAU 2017 dan meraih Adhi Makayasa.</p>
<p>\'Semoga Tuhan memberkati sumpah dan janjimu\'</p>
<p>Photo Credit: Ibu Susi Suryandari</p>
</div>',
                'image' => 'posts/November2017/19756601_1502282906481748_1128332099597825044_n.jpg',
            'slug' => 'bernardinus-yoga-kristian-(tn-21)-raih-adhi-makayasa-aau-2017',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:22:58',
                'updated_at' => '2017-11-09 12:22:58',
                'image_url' => NULL,
            ),
            29 => 
            array (
                'id' => 40,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Berita duka : Didit Kurniawan, ST (TN 8)',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="js_1h" class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>B e r i t a D u k a<br /> Kami segenap Keluarga Besar SMA Taruna Nusantara Magelang menyampaikan duka cita mendalam dan berbelasungkawa meninggalnya alumni sekolah kami angkatan 8 Didit Kurniawan, ST tadi pagi tgl. 6 Juli 2017.<br /> Semoga almarhum mendapatkan tempat terbaik disisiNya, diterima semua amal ibadahnya dan diampuni segala dosanya. Serta keluarga yang ditinggal diberi keikhlasan dan ketabahan menerima taqdirNya ini. Aamiin yra</p>
<p><a class="_58cn" href="https://www.facebook.com/hashtag/sangatbanyakkenanganbersamanya?source=feed_text&amp;story_id=1499853016724737" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">SangatBanyakKenanganBersamanya</span></span></a></p>
</div>',
                'image' => 'posts/November2017/19732154_1499853016724737_8896465859604327918_n.jpg',
            'slug' => 'berita-duka-didit-kurniawan-st-(tn-8)',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:28:43',
                'updated_at' => '2017-11-09 12:28:43',
                'image_url' => NULL,
            ),
            30 => 
            array (
                'id' => 41,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Teror ISIS di Kebayoran, Kapolsek Kebayoran Lama Kompol Ardi Rahanarto (TN 8): Jangan Takut dan Tetap Siaga!',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p><strong>Jakarta</strong> &ndash; Polisi mengimbau masyarakat tidak takut setelah terjadi teror dan pemasangan bendera ISIS di Polsek Kebayoran Lama. Masyarakat juga diminta tetap siaga dan meningkatkan kewaspadaan.</p>
<p>&nbsp;</p>
<p>&ldquo;Kalau wilayah Kebayoran Lama, kami mengimbau masyarakat, jangan takut, polisi juga jangan takut, kita tetap siaga, melaksanakan pelayanan seperti biasa. Hanya meningkatkan kewaspadaan. Silakan kalau ada informasi kejadian malam, silakan lapor ke kami. Kami akan ungkap secepatnya,&rdquo; ujar Kapolsek Kebayoran Lama Kompol Ardi Rahanarto, di kantornya, Jl Praja, Jakarta Selatan, Selasa (4/7/2017).</p>
<p>&nbsp;</p>
<p>Ardi mengatakan pihaknya terus meningkatkan pengamanan seusai insiden tersebut. Hal ini untuk menghindari teror serupa yang terjadi di wilayahnya.</p>
<p>&nbsp;</p>
<p>&ldquo;Untuk riilnya biasanya misalnya begini, kita biasa patroli sekian jam sekali. Itu kita perketat lagi. Untuk pasukan juga bukan cuma dari polsek, tapi dari polres dan bantuan polda juga mempertebal,&rdquo; katanya.</p>
<p>&nbsp;</p>
<p>Saat ini barang bukti yang telah diamankan pun tengah diperiksa tim Laboratorium Forensik Mabes Polri. Sejumlah saksi pun telah dimintai keterangan terkait dengan teror itu.</p>
<p>&nbsp;</p>
<p>&ldquo;Untuk masalah hasil penyelidikannya yang bisa sampaikan untuk berapa saksi dan olah TKP, barang bukti diperiksa langsung oleh Laboratorium Forensik Mabes,&rdquo; tutur Ardi.</p>
<p>&nbsp;</p>
<p>Sebelumnya, sebuah bendera ISIS dipasang di pagar Polsek Kebayoran Lama. Pasca-insiden tersebut, petugas langsung melakukan patroli dan memeriksa setiap sudut kantor untuk mencari bendera atau benda mencurigakan lainnya.</p>
<p>Orang yang memasang bendera ISIS di pagar Polsek Kebayoran Lama itu juga meninggalkan sebuah pesan ancaman. Pesan tersebut ditulis dalam karton kuning, yang salah satu isinya tentang Jakarta yang akan dibuat seperti Marawi, Filipina.<br /> <strong>(knv/dnu) <a href="https://news.detik.com/berita/3548134/teror-isis-di-kebayoran-kapolsek-jangan-takut-dan-tetap-siaga">news.detik.com</a></strong></p>',
                'image' => 'posts/November2017/b488710e-11a6-407d-9dde-d7a061946aff.jpg',
            'slug' => 'teror-isis-di-kebayoran-kapolsek-kebayoran-lama-kompol-ardi-rahanarto-(tn-8)-jangan-takut-dan-tetap-siaga!',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:29:41',
                'updated_at' => '2017-11-09 12:29:41',
                'image_url' => NULL,
            ),
            31 => 
            array (
                'id' => 42,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Berita duka : Kapten Laut (P) Ii Solihin (TN 14)',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="js_1j" class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>Telah gugur dalam tugas, Kapten Laut (P) <a class="profileLink" href="https://www.facebook.com/ii.solihin?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=1509192697&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Ii Solihin</a> (TN 14) dalam kecelakaan helikopter (Kopilot) Basarnas di Desa Canggal, Candiroto, Temanggung, Jawa Tengah tanggal 2 Juli 2017.</p>
<p>Semoga almarhum mendapatkan tempat terbaik dan semoga keluarga yang ditinggalkan diberi ketabahan dan kekuatan.</p>
<p><a href="https://kumparan.com/teuku-muhammad-valdy-arief/nama-nama-korban-tewas-heli-basarnas-yang-jatuh-di-temanggung?ref=rel" target="_blank" rel="nofollow noopener" data-ft="{&quot;tn&quot;:&quot;-U&quot;}" data-lynx-mode="async" data-lynx-uri="https://l.facebook.com/l.php?u=https%3A%2F%2Fkumparan.com%2Fteuku-muhammad-valdy-arief%2Fnama-nama-korban-tewas-heli-basarnas-yang-jatuh-di-temanggung%3Fref%3Drel&amp;h=ATPLNzWBeYB7WcNgeNLhy52prvfgkFEaxnvTbitPdHAGIe27_jk-zQLX0R72d5hmz1pEOizzJ07_NdBZMcNfS3FnbbB-49rL9a-mYtlWg_tD_az2QXDX4sjGhRKH03YntbvG3GgHF-akNXHjMBPexR4iH2-l6Izppr03GfvWXklvnsPVYZ_M5SielEw5TbctDHreuV8sNGFJMYfqHuXCQzyAdEuzhAiSpK2sR_19Luu_0pqrX-jpG_l18jXNJ_QwhyeeXPRLuVQTsn3ViM8b5up8h5NmPys">https://kumparan.com/teuku-muhammad-valdy-arief/nama-nama-korban-tewas-heli-basarnas-yang-jatuh-di-temanggung?ref=rel</a></p>
</div>',
                'image' => 'posts/November2017/2017-11-09_121348.jpg',
            'slug' => 'berita-duka-kapten-laut-(p)-ii-solihin-(tn-14)',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:31:25',
                'updated_at' => '2017-11-09 12:31:25',
                'image_url' => NULL,
            ),
            32 => 
            array (
                'id' => 43,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'AKBP I Ketut Agus Kusmayadi S.IK (TN 1) jabat Kapolres Tomohon',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>Selamat kepada AKBP I Ketut Agus Kusmayadi S.IK (TN 1) atas jabatan baru sebagai Kapolres Tomohon Polda Sulawesi Utara.</p>
<p>"Semoga Tuhan memberkati sumpah dan janjimu!"</p>
</div>',
                'image' => 'posts/November2017/18670874_1453671348009571_5571385698035126182_n.jpg',
            'slug' => 'akbp-i-ketut-agus-kusmayadi-s-ik-(tn-1)-jabat-kapolres-tomohon',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:33:49',
                'updated_at' => '2017-11-09 12:33:49',
                'image_url' => NULL,
            ),
            33 => 
            array (
                'id' => 44,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Letnan Satu (CZI) Hendrik Pardamean Hutagalung, S.S. T.Han (TN 15) raih Peringkat 1 Diklapa 1',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="js_d" class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>Selamat kepada Letnan Satu (CZI) <a class="profileLink" href="https://www.facebook.com/hendrik.p.hutagalung?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=1055410758&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Hendrik Pardamean Hutagalung</a>, S.S. T.Han (TN 15) yang meraih Peringkat 1 Kecangab Zeni, sekaligus meraih Sangkur Perak pada penutupan Pendidikan Lanjutan Perwira 1 Kecabangan TNI AD 2017.</p>
<p>Hendrik adalah peraih Adhi Makayasa 2011.</p>
<p>"Semoga Tuhan memberkati sumpah dan janjimu!"</p>
</div>',
                'image' => 'posts/November2017/18664350_1451449664898406_1871010703873571920_n.jpg',
            'slug' => 'letnan-satu-(czi)-hendrik-pardamean-hutagalung-s-s-t-han-(tn-15)-raih-peringkat-1-diklapa-1',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:34:29',
                'updated_at' => '2017-11-09 12:34:29',
                'image_url' => NULL,
            ),
            34 => 
            array (
                'id' => 45,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Dua tim SMA TN berhasil meraih juara II dan IV pada lomba Pre Event Olimpiade Telekomunikasi TELEMOTION',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="js_8" class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>Dua tim SMA TN berhasil meraih juara II dan IV pada lomba Pre Event Olimpiade Telekomunikasi TELEMOTION 2017 Connecting Ideas for Indonesia! Organized by Telecommunication Engineering ITB IMT "SIGNUM" ITB, 20 - 21 Mei 2017 di ITB</p>
<p>Juara II diraih oleh tim 1 yang beranggotakan :<br /> - Muhammad Salman Al-farisi, XI MIPA 1<br /> - Fakhru Adlan Ayub, X 12<br /> - Irfan Urane Azis, X 9</p>
<p>Juara IV diraih oleh tim 2 yang beranggotakan :<br /> - <a class="profileLink" href="https://www.facebook.com/yogahmad?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=100005905181917&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Ahmad Haulian Yoga Pratama</a> , XI MIPA 7<br /> - Riamizar Surya Baihaqi, XI MIPA 1<br /> - Achmad Naufal Fathurrahman, X 8</p>
</div>',
                'image' => 'posts/November2017/18623476_1450011621708877_4785650358126927156_o.jpg',
                'slug' => 'dua-tim-sma-tn-berhasil-meraih-juara-ii-dan-iv-pada-lomba-pre-event-olimpiade-telekomunikasi-telemotion',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:35:10',
                'updated_at' => '2017-11-09 12:35:10',
                'image_url' => NULL,
            ),
            35 => 
            array (
                'id' => 46,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Jamal Habiburrahman meraih medali perunggu Olimpiade Geografi dan Geosains',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p>Siswa Jamal Habiburrahman, Kelas XI MIPA 8 berhasil meraih medali perunggu pada Olimpiade Geografi dan Geosains (OGG) ITB 2017 yang diselenggarakan oleh Fakultas Ilmu Teknologi dan Kebumian ITB, 19 - 21 Mei 2017</p>',
                'image' => 'posts/November2017/18673246_1450025961707443_2781746556549890275_o.jpg',
                'slug' => 'jamal-habiburrahman-meraih-medali-perunggu-olimpiade-geografi-dan-geosains',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:36:01',
                'updated_at' => '2017-11-09 12:36:01',
                'image_url' => NULL,
            ),
            36 => 
            array (
                'id' => 47,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Juara 1 Nasional Lomba Debat Bahasa Indonesia',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="js_k" class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>"OSN" nya mapel Bahasa Indonesia dilibas Jawa Tengah</p>
<p>Tim Debat Bahasa Indonesia SMA Taruna Nusantara yang beranggotakan Azzahra Jelita Kusuma, Belly Ali Rakhamadansyah, Dania Emeralda Firdausi (SMAN 3 Semarang) berhasil meraih medali emas sekaligus Juara 1 Nasional dalam Lomba Debat Bahasa Indonesia (LDBI) Tahun 2017.<br /> Tidak mudah meraih Juara 1 dan mendapatkan emas.</p>
<p>Selasa, 16 Mei 2017<br /> 1. Jateng Vs DKI = Kalah dgn margin 1<br /> 2. Jateng Vs NTB = Menang dgn margin 1<br /> 3. Jateng Vs Riau = Kalah dgn margin 1</p>
<p>Rabu, 17 Mei 2017<br /> 4. Jateng Vs Sulawesi Tenggara = Menang<br /> 5. Jateng Vs Sumut = *silent round*</p>
<p>Dari kelima pertandingan tersebut, Tim Jawa Tengah masuk 16 besar dan berada di peringkat 12.<br /> Perjuangan tim pun masih berlanjut, yakni melawan Tim Riau yang sebelumnya berhasil mengalahkan Tim Jawa Tengah. Dan hasilnya, Jawa Tengah berhasil membalas kekalahannya. Yang lolos babak 8 besar :<br /> Jatim, Jabar, DKI Jakarta, Sulsel, Babel, Lampung, Jateng, dan Kepri</p>
<p>Semua tim itu ada di ranking 8 besar, kecuali Jateng.<br /> Jadi, ini saatnya membuktikan...! <br /> Pada perdelapan besar ini, kompetisi semakin memanas, yakni dengan Mosi:<br /> *Dewan ini akan memboikot Israel secara akademik*<br /> Jateng Vs Lampung</p>
<p>Hasilnya, Provinsi Lampung berhasil dikalahkan sehingga berhasil masuk semifinal dan bertemu dengan Jawa Barat yang berhasil mengalahkan DKI Jakarta (Peringkat teratas).</p>
<p>Semifinal pun berakhir dengan kemenangan mutlak antara Jawa Tengah vs Jawa Barat dengan skor 4:1. <br /> Akhirnya semangat melanjutkan tradisi prestasi semakin membara hingga masuk grand final mengalahkan Provinsi Jawa Timur dengan kemenangan telak 8:1 untuk Jawa Tengah. Terima kasih atas doa seluruh keluarga besar SMA Taruna Nusantara dan Abang kakak alumni Aufar Nugroho dan Safira Felicia TN 19 yang turut mendukung saya final. Bravo SMA TN!!! (ATL)</p>
</div>',
                'image' => 'posts/November2017/2017-11-09_122113.jpg',
                'slug' => 'juara-1-nasional-lomba-debat-bahasa-indonesia',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:38:54',
                'updated_at' => '2017-11-09 12:38:54',
                'image_url' => NULL,
            ),
            37 => 
            array (
                'id' => 48,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Semangat Kejuangan Berbuah Manis',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p>Selama tiga hari, Tim Broadcast SMA Taruna Nusantara berjuang dalam kompetisi Broadcaster Nasional di Universitas Muhamadiyah Yogyakarta (UMY). Peserta lomba ini tidak hanya diikuti oleh siswa DIY dan Jateng saja, melainkan dari Surabaya, Pontianak, dan kota-kota lainnya di Indonesia.</p>
<p>Tim Broadcast Taruna Nusantara yang dibina dan didampingi langsung oleh Bapak Pebri Dwi Lesmono, S.Pd ini menyisihkan puluhan kompetitor yang mempunyai kemampuan yang hampir merata, bahkan beberapa siswa berasal dari sekolah kejuruan yang mempunyai jurusan broadcast.</p>
<p>Akhirnya, dengan usaha keras dan daya juang tinggi, Tim Broadcast SMA TN mendapatkan :<br /> A. Kategori Radio Anouncer:<br /> Juara 3 Andi Aliya Adelina Priyadi (XI-MIPA 1)</p>
<p>B. Kategori Presenter<br /> Juara 1 Andi Aliya Adelina Priyadi (XI-MIPA 1)<br /> Juara 2 Hamzah Mustafa Amir (XI-MIPA 6)</p>
<p>C. Kategori Reporter<br /> Juara 3 Intan Aulia Retnoningrum (X-11)</p>
<p>Bravo SMA Taruna Nusantara! Kibarkan Tradisi Prestasi Terbaik untuk bangsa, negara, dan dunia&hellip; (ATL)</p>',
                'image' => 'posts/November2017/2017-11-09_122248.jpg',
                'slug' => 'semangat-kejuangan-berbuah-manis',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:40:09',
                'updated_at' => '2017-11-09 12:40:09',
                'image_url' => NULL,
            ),
            38 => 
            array (
                'id' => 49,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Kisah Anak SMA TN Magelang yang Bikin Listrik dari Tanah Liat',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p><span style="color: #000000;"><strong>Jatengkita.id, MAGELANG</strong>&ndash; Saat pengumuman kelulusan SMA kemarin, media sosial kita banyak dihiasi oleh berita konvoi kelulusan di berbagai kota. Konvoi yang diikuti dengan cora-coret itu jelas membuat banyak pihak prihatin.</span></p>
<p><span style="color: #000000;">Disaat yang sama seorang anak SMA, Bagas Pramana Putra berhasil membuat terobosan teknologi dengan mengubah tanah lihat menjadi listrik. Bahkan penemuan Bagas ini sudah diikutkan dalam sebuah kompetisi tingkat nasional yang diselenggarakan sebuah BUMN dan meraih juara kedua untuk kategori terobosan teknologi.</span></p>
<div id="attachment_512" class="wp-caption aligncenter" style="width: 1100px;"><img class="size-full wp-image-512" src="https://i2.wp.com/jatengkita.id/wp-content/uploads/2017/05/bagas3.jpg?resize=1090%2C613" alt="" width="625" height="351" data-attachment-id="512" data-permalink="http://jatengkita.id/2017/05/08/kisah-anak-sma-tn-magelang-yang-bikin-listrik-dari-tanah-liat/bagas3/" data-orig-file="https://i2.wp.com/jatengkita.id/wp-content/uploads/2017/05/bagas3.jpg?fit=1090%2C613" data-orig-size="1090,613" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="bagas3" data-image-description="" data-medium-file="https://i2.wp.com/jatengkita.id/wp-content/uploads/2017/05/bagas3.jpg?fit=300%2C300" data-large-file="https://i2.wp.com/jatengkita.id/wp-content/uploads/2017/05/bagas3.jpg?fit=1024%2C1024" />
<p class="wp-caption-text">Bagas raih juara berkat penemuan fenomenalnya.</p>
</div>
<p><span style="color: #000000;">Tidak main-main karena Bagas berhasil mengungguli orang-orang yang lebih tua, bahkan beberapa diantaranya adalah Profesor dan Doktor dari universitas terkemuka tanah air. Bagas yang berasal dari SMA Taruna Nusantara Magelang ini menamakan alatnya sebagai &ldquo;Genteng Triko&rdquo;.</span></p>
<p><span style="color: #000000;">Bagas yang segera naik ke kelas 12 SMA ini melakukan penelitian tanah liat sejak kelas 10 di Taruna Nusantara. Idenya mirip seperti Elon Musk yang mengubah energi cahaya menjadi listrik. Namun Bagas mengambil ide yang berbeda, mengubah energi panas menjadi listrik. Karena menurutnya di tanah air energi dalam bentuk panas sangat melimpah dan perlu diubah menjadi listrik, apalagi kebutuhan listrik di tanah air semakin besar.</span></p>
<p><span style="color: #000000;">Bagas sendiri punya cita-cita membuat sebuah pemukiman percontohan yang memanfaatkan alat temuannya sebagai sumber listrik. Menurut Bagas alat temuannya ini sangat murah dan mudah, karena bahan tersedia banyak di Indonesia, yaitu tanah liat.</span></p>
<p><span style="color: #000000;">Kendala yang dihadapi Bagas saat ini adalah ketersediaan bahan lain yang langka di tanah air, Magnesium. &ldquo;Bila alatnya nanti dipakai luas di masyarakat syarat selanjutnya adalah hutan yang harus dijaga. Menurutnya ketersediaan tanah liat juga dipengaruhi oleh eksistensi hutan, yang sayangnya sampai saat ini terus tergerus dan hilang,&rdquo;ujarnya, Senin (8/5/2017).</span></p>
<div id="attachment_513" class="wp-caption aligncenter" style="width: 1100px;"><img class="size-full wp-image-513" src="https://i2.wp.com/jatengkita.id/wp-content/uploads/2017/05/bagas4.jpg?resize=1090%2C613" alt="" width="625" height="351" data-attachment-id="513" data-permalink="http://jatengkita.id/2017/05/08/kisah-anak-sma-tn-magelang-yang-bikin-listrik-dari-tanah-liat/bagas4/" data-orig-file="https://i2.wp.com/jatengkita.id/wp-content/uploads/2017/05/bagas4.jpg?fit=1090%2C613" data-orig-size="1090,613" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="bagas4" data-image-description="" data-medium-file="https://i2.wp.com/jatengkita.id/wp-content/uploads/2017/05/bagas4.jpg?fit=300%2C300" data-large-file="https://i2.wp.com/jatengkita.id/wp-content/uploads/2017/05/bagas4.jpg?fit=1024%2C1024" />
<p class="wp-caption-text">Bgas memperagakan alatnya bekerja.</p>
</div>
<p><span style="color: #000000;">Guru pembimbing Bagas Amin Sukarjo berharap banyak penelitian Bagas bisa berlanjut dan ada bantuan untuk meneliti lebih lanjut. Karena target dari Bagas sendiri adalah alatnya segera memiliki fungsi menyimpan listrik yang dihasilkan oleh tanah liat tersebut.</span></p>
<p><span style="color: #000000;">&ldquo;Semoga satu talenta anak Indonesia dalam diri Bagas bisa dimaksimalkan oleh pemerintah dan semua pihak terkait di tanah air. Agar tidak terjadi seperti sebelumnya, potensi anak bangsa malah dilirik oleh negara tetangga,&rdquo;katanya. </span></p>',
                'image' => 'posts/November2017/bagas2.jpg',
                'slug' => 'kisah-anak-sma-tn-magelang-yang-bikin-listrik-dari-tanah-liat',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:41:33',
                'updated_at' => '2017-11-09 12:41:33',
                'image_url' => NULL,
            ),
            39 => 
            array (
                'id' => 50,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Berita duka : Wiriyadi Nusa Saputra, (TN 1)',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p><span id="fbPhotoSnowliftCaption" class="fbPhotosPhotoCaption" tabindex="0" data-ft="{&quot;tn&quot;:&quot;K&quot;}"><span class="hasCaption">Berita duka :<br /> <br /> Telah meninggal dunia, pada hari Jumat 20 Oktober 2017 di Sleman, <br /> Wiriyadi Nusa Saputra, (TN 1)<br /> <br /> Semoga almarhum diterima di sisi Tuhan YME dan keluarga yang ditinggalkan mendapat penghiburan, amin.</span></span></p>',
                'image' => 'posts/November2017/22555301_1597075757002462_3470086764395545504_n.jpg',
            'slug' => 'berita-duka-wiriyadi-nusa-saputra-(tn-1)',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:42:42',
                'updated_at' => '2017-11-09 12:42:42',
                'image_url' => NULL,
            ),
            40 => 
            array (
                'id' => 51,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'P A N D A T A R A – 2 0 1 7',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>SMA Taruna Nusantara Magelang menggelar PAMERAN SENI DAN BUDAYA NUSANTARA 2017 mulai tgl. 26 s.d 29 Oktober 2017 yang terbuka untuk masyarakat umum, remaja, pemuda dan pelajar agar semakin mengenal Seni dan Budaya Nusantara.</p>
<p>Selain sebagai hiburan menarik, juga merupakan media pembelajaran penting bagi peserta didik dan mahasiswa. Hadirilah gebyar budaya ini setiap hari, GRATIS, kecuali pada Jumat malam (ada penampilan ber<span class="text_exposed_show">bayar untuk artis HIVI dan MALIQ D&rsquo;ESSENTIALS). </span></p>
<p>Pameran Seni Budaya menampilkan 11 Anjungan Budaya Daerah Nusantara dan puluhan Warung Kuliner Nusantara yang sangat menarik dan menantang selera makan minum kita.</p>
<p>Ada pameran UMKM Kerajinan dan Makanan Khas, Pameran SMK-SMK dan Pameran Bonsai &ndash; Tanaman Hidroponik &ndash; Organik. Diwarnai atraksi macam-macam Seni dan Kirab Budaya.</p>
<p>Seluruh macam pameran yang bertempat di SMATN di atas BUKA setiap hari mulai tgl. 26 s.d 29 Oktober 2017 mulai pk. 08.00 s.d pk.17.00 (kecuali tgl. 26 Oktober dimulai setelah upacara Pembukaan pk.10.00 dan tgl. 29 Oktober diakhiri dalam upacara Penutupan pk.14.00)<br /><br /></p>
<p>&nbsp;</p>
<p><img class="alignnone size-medium" src="https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/22528493_1597368936973144_1369796673033926856_n.jpg?oh=705a06ae866b8211e238a64326aea607&amp;oe=5A6A856E" alt="" width="678" height="960" /></p>
<p><img class="alignnone size-medium" src="https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/22491596_1597368940306477_2197488006209266751_n.jpg?oh=c53fe492e0e3e303a8d9d590d02fcadb&amp;oe=5A8133E9" alt="" width="678" height="960" /></p>
<p><img class="alignnone " src="https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/22788973_1599449506765087_6480346588967441067_n.jpg?oh=1c4096f8b339c0662bc69adf1654fc49&amp;oe=5A821FBF" alt="" width="677" height="722" /></p>
<p><img class="alignnone " src="https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/22555078_1599449540098417_766573926580584189_n.jpg?oh=0aa20b0546ba69e4c2827b13364bcde1&amp;oe=5A6BC6F1" alt="" width="677" height="908" /></p>
<p><img class="alignnone " src="https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/22555268_1599449556765082_3363621415299004999_n.jpg?oh=5789110b2fe63baf1dfd605de62248ae&amp;oe=5A7165DA" alt="" width="674" height="591" /></p>
<p><img class="alignnone " src="https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/22730222_1599449576765080_7322796698027506090_n.jpg?oh=18cdb9b7305994e80ce6168005354eb5&amp;oe=5A7A6165" alt="" width="671" height="515" /></p>
<p>&nbsp;</p>',
                'image' => 'posts/November2017/22555087_1597368953639809_1676792846487138712_n.jpg',
                'slug' => 'p-a-n-d-a-t-a-r-a-2-0-1-7',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:43:49',
                'updated_at' => '2017-11-09 12:43:49',
                'image_url' => NULL,
            ),
            41 => 
            array (
                'id' => 52,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Bagas Pramana Putra Fadila dan Dimas Suprianto Juara 1 Inovator Inovasi Indonesia Expo (i3E) 2017',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>Siswa <a class="profileLink" href="https://www.facebook.com/bagas.putrafadila?fref=mentions" target="_blank" rel="noopener">Bagas Pramana Putra Fadila</a> dan <a class="profileLink" href="https://www.facebook.com/profile.php?id=100009355068624&amp;fref=mentions" target="_blank" rel="noopener">Dimas Suprianto</a> berhasil meraih Juara 1 kompetisi Inovasi Perguruan Tinggi dan Masyarakat Umum Inovator Inovasi Indonesia Expo (i3E) 2017.</p>
<p>Minggu, 22 Oktober 2017 di Exhibition Hall Grand City, Surabaya</p>
</div>',
                'image' => 'posts/November2017/22688335_1599514433425261_5804377838388184648_n.jpg',
            'slug' => 'bagas-pramana-putra-fadila-dan-dimas-suprianto-juara-1-inovator-inovasi-indonesia-expo-(i3e)-2017',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:45:20',
                'updated_at' => '2017-11-09 12:45:20',
                'image_url' => NULL,
            ),
            42 => 
            array (
                'id' => 53,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Berita duka : Agustinus Wahyudi (TN 1)',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="js_j" class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>Berita duka :</p>
<p>Telah meninggal dunia, pada hari Senin 23 Oktober 2017, Agustinus Wahyudi (TN 1)</p>
<p>Semoga almarhum diterima di sisi Tuhan YME dan keluarga yang ditinggalkan mendapat penghiburan, amin.</p>
</div>',
                'image' => 'posts/November2017/22780588_1600429796667058_1721999615837662831_n.jpg',
            'slug' => 'berita-duka-agustinus-wahyudi-(tn-1)',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:46:29',
                'updated_at' => '2017-11-09 12:46:29',
                'image_url' => NULL,
            ),
            43 => 
            array (
                'id' => 54,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Para pemenang di i3L : Indonesia International Institute for Life Sciences',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="js_c" class="_5pbx userContent _3576" data-ft="{&quot;tn&quot;:&quot;K&quot;}">
<p>Congratulations to Zola Saputra &amp; Salsabilla Tiaratama H Wiraksa students from SMA Taruna Nusantara Magelang on becoming the 1st winner in Life Sciences topic with Kadsi Food (Kabeh Dadi Siji) Food, as a correct solution to meet the needs of natural disaster victims.<br /> <a class="_58cn" href="https://www.facebook.com/hashtag/i3l?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">i3L</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/competition?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">competition</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/physics?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">physics</span></span></a></p>
<p>Congratulations to Faiq Haidar Hamid &amp; Ketut Shri Satya Yogananda, students from SMA Taruna Nusantara Magelang on becoming the 1st winner in Mathematics &amp; Computer Science topic with CRC &ldquo;Complex Ruler For Circle&rdquo;.<br /> #i3L #competition <a class="_58cn" href="https://www.facebook.com/hashtag/mathematics?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">mathematics</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/computerscience?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">computerscience</span></span></a> #physics</p>
<p>Congratulations to Muhammad Firman Nuruddin &amp; Afza Atsnaita Safina students from SMA Taruna Nusantara Magelang on becoming the 1st winner in Environmental Sciences topic with Leaf Solar Cell: The Potential of Leaf Waste as Dye-Sensitized Solar Cell.</p>
<p>#i3L #competition #physics <a class="_58cn" href="https://www.facebook.com/hashtag/environmentalsciences?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">environmentalsciences</span></span></a> #mathematics #computerscience <a class="_58cn" href="https://www.facebook.com/hashtag/lomba?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">lomba</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/event?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">event</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/icys?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">icys</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/cys?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">cys</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/centerforyoungscientist?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">centerforyoungscientist</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/lombapenelitibelia?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">lombapenelitibelia</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/like4like?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">like4like</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/l4l?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">l4l</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/igers?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">igers</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/highschool?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">highschool</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/jawatengah?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">jawatengah</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/likeforfollow?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">likeforfollow</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/indonesia?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">indonesia</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/apcys?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">apcys</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/serbia?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">serbia</span></span></a> <a class="_58cn" href="https://www.facebook.com/hashtag/thailand?source=feed_text&amp;story_id=1600643736645664" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx"><span class="_58cl _5afz">#</span><span class="_58cm">thailand</span></span></a> #biomedicine #BioTechnology #bioinformatics #foodscience #foodtechnology #entrepreneurship</p>
<p><a href="https://www.instagram.com/i3l_official/" target="_blank" rel="nofollow noopener" data-ft="{&quot;tn&quot;:&quot;-U&quot;}" data-lynx-mode="async" data-lynx-uri="https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.instagram.com%2Fi3l_official%2F&amp;h=ATMblSG9m7fx17A7unauyUANxSqoGQUWuHuLe-8s-POiFlkCKLS_W4MjqbL82BMJJcCxpuU-SgG6LoYermRLxlAdwLLfgZ2GLoJ7VM3TYeDnWmOj9zZMQruBMW1zjPy7SnMo8sOLjKZcebmZIYI4FkbjYNp1Y7ZG3rqO0hjAaiUn52xA2Oww0_5Tc8fOfhx043aP8g8L1oJbrfAH0LpUoUUBlAWE15vuxb4l76bgYMriqkbb1k472MPrqy80_bs5pH6qDPDEhBN3V-c6G2enOd6O6AnUV4o">https://www.instagram.com/i3l_official/</a><br /> i3L - BioMedicine, Pharmacy, BioInformatics, BioTechnology, FoodScience, FoodTechnology, &amp; Entrepreneurship</p>
</div>',
                'image' => 'posts/November2017/2017-11-09_123040.jpg',
                'slug' => 'para-pemenang-di-i3l-indonesia-international-institute-for-life-sciences',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:48:39',
                'updated_at' => '2017-11-09 12:48:39',
                'image_url' => NULL,
            ),
            44 => 
            array (
                'id' => 55,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Letkol (Inf) Rio Neswan (TN 5), Nakhoda Baru Satgas Pamtas RI-Malaysia',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="text-muted">BERTUGAS DI PERBATASAN: Komandan Satgas Pamtas 621/MTG, Letkol Inf Rio Neswan saat akan berpisah dengan istri dan anaknya dalam mengemban tugas di Nunukan. DOKUMEN PRIBADI UNTUK RADAR NUNUKAN</div>
<div>
<div id="bodytext">
<p><strong>PROKAL.CO</strong>, Baginya, tugas di perbatasan bukanlah kali pertama ia emban sejak lulus dari Akademi Militer (Akmil) pada 2000 silam. Sederet tugas operasi itu membawa ia tak asing lagi sejak memimpin Satgas Pamtas Yonif 621/ Manuntung di Nunukan. Bagaimana kisah Rio Neswan, berikut ulasannya.</p>
<p align="left"><strong>MEMIMPIN</strong> 350 prajurit kini menjadi tugas utama yang akan dilaksanakan Letnan Kolonel (Letkol) Inf Rio Neswan selama 9 bulan ke depan. Bertugas di perbatasan bukanlah menjadi kendala bagi dirinya, berkat pengalaman tugasnya di wilayah perbatasan, membuat pria lulusan Akademi Militer (Akmil) 2000 tidak merasa kesulitan menyesuaikan diri di manapun bertugas.</p>
<p align="left">Pria kelahiran, 11 Agustus 1978 Tajung Karang, Bandar Lampung ini awalnya tak pernah bercita-cita terjun ke dunia militer. Ia justru ingin mewujudkan keinginanya untuk dapat menjadi lulusan teknik informatika di Institut Teknologi Bandung (ITB), Jawa Barat.</p>
<p align="left">Namun semua itu berubah, saat dirinya masuk dan menjalani pendidikan menengah atas di SMA Taruna Nusantara, Magelang. Selama menjalani pendidikan di Magelang, dirinya berasa telah berada di lingkungan militer, sebab pendidikan yang diterapkan pada sekolah tersebut sudah menggunakan sistem militer.</p>
<p align="left">Orang tua dari Rio Neswan bahkan sempat kaget ketika mengetahui anaknya masuk pada sekolah tersebut. Keinginannya ingin menjadi seorang sarjana informatika seakan hilang dengan sendirinya.</p>
<p align="left">Usai lulus dari SMA Taruna Nusantara, ia kemudian mendaftarkan diri masuk Akademi Militer (Akmil) TNI AD. Usai lulus, sejumlah tugas pun menanti putra dari pasangan (alm) H. Ustamjam dan ibu Hj. Ruspayati itu.</p>
</div>
</div>
<p>&nbsp;</p>
<p align="left">Penempatan pertama pun menantinya, yakni Batalion Infantri 731 Kaberasi, Maluku Utara selama delapan tahun. Sejak bertugas di batalion, ia mengemban sejumlah jabatan yakni menjadi Komandan Peleton (Danton), Komandan Kompi (Danki) dan perwira seksi.</p>
<p align="left">Usai menjalankan tugas di batalion, Rio kemudian ditugaskan menuju Kodim 1504/Kota Ambon sebagai perwira seksi operasi selama setahun. Tuntas menjalankan tugas operasi di Ambon, Dansatgas Pamtas RI&ndash;Malaysia ini kemudian ditarik mengikuti pendidikan lanjutan perwira di Kota Bandung, Jawa Barat. Kemudian pindah ke Mabes TNI AD di Jakarta.</p>
<p align="left">Di Mabes TNI, ia lalu ditempatkan sebagai staf personel AD selama empat tahun. sejak di Mabes TNI, ia berkesempatan melanjutkan pendidikan umum sebagai sarjana dan master.</p>
<p align="left">Perjalanan pendidikan militer Rio tak sampai di situ, dia juga mendapat kesempatan meniti pendidikan di luar negeri. Di antaranya, pendidikan militer di Thailand terkait misi perdamaian dunia di bawah Perserikatan Bangsa-Bangsa (PBB) dan selanjutnya melanjutkan pendidikan Hawai, Amerika Serikat (AS) terkait manajemen krisis selama delapan minggu.</p>
<p align="left">&ldquo;Kemudian saya lanjutkan sekolah staf dan komandao di Riyadh, Saudi Arabia selama 11 bulan. Selesai sekolah di Riyadh, saya ditempatkan di Bandung, di Sekolah Staf dan Komandao AD,&rdquo; urai Rio kepada <em>Radar Nunukan</em>.</p>
<p align="left">Pasca melaksanakan tugas di Sekolah Staf dan Komando AD, ia kemudian ditarik mengisi jabatan di Korem 101/Antasari, Banjarmasin hingga Agustus 2016 lalu, dan dipercayakan memimpin Komandan Yonif 621/ Manuntung yang menjadi cikal bakal akhirnya menjadikan pengalaman pertamanya memimpin satuan bertugas di wilayah perbatasan Indonesia&ndash;Malaysia.</p>
<p align="left">Suami dari istri bernama Maulani yang bertugas sebagai Aparatur Sipil Negeri (ASN) di Lingkup Kementerian Dalam Negeri (Kemendagri) ini mengungkapkan, untuk keluarga sendiri bagi Rio tak pernah menjadi persoalan saat dirinya harus bertugas dan jauh dari rumah, karena faktor kebiasan dan pengertian.</p>
<p align="left">Harus dipahami bahwa sebagai prajurit TNI terkadang akan mendapatkan tugas di daerah operasi. Sehingga harus berpisah dengan keluarga untuk sementara waktu. &ldquo;Saya sudah sampaikan sebelumnya ketika bersama TNI pasti akan ada waktunya berpisah sementara waktu, istri dan anak pun mengerti akan hal tersebut,&rdquo; beber ayah dari Riyadh Maulana Neswan ini.</p>
<p align="left">Tugas sebagai penjaga perbatasan yang saat ini ia emban akan ia jalankan sebaik mungkin. Peran prajurit yang ada di tiap pos harus selalu solid untuk menjalankan visi dan misi yang telah dibawa. Sebagai satuan yang bertugas di perbatasan, tentu banyak hal yang harus diperhatikan. Termasuk kegiatan ilegal di jalur-jalur perbatasan.</p>
<p align="left">&ldquo;Tiap pasukan yang berada di pos harus selalu siap siaga, karena tugas di perbatasan yang utama adalah menjaga kedaulatan NKRI,&rdquo; ujarnya.</p>
<p align="left">Selain itu, predikat terbaik yang didapatkan Satgas Pamtas sebelumnya yakni 611/Awang Long menjadikan ia harus berkomitmen menjaga dan meningkatkan predikat terbaik tersebut. Karena penugasan sembilan bulan di wilayah perbatasan, ternyata mendapat penilaian tersendiri dari Mabes TNI AD kepada seluruh Satgas Pamtas yang mengemban tugas di perbatasan.</p>
<p align="left">Mabes TNI AD pun akan memberikan reward bagi satuan tugas yang berhasil keluar sebagai predikat terbaik dalam menjalankan misi, untuk membawa nama Indonesia dalam mengemban misi sebagai pasukan perdamaian dunia di bawah naungan PBB.</p>
<p align="left">Sementara, untuk prajuritnya sendiri, ia berkomitmen akan berusaha menjaga garis terdepan wilayah NKRI ini dari segala upaya penyelundupan, terorisme maupun percobaan mengancam kedaulatan Indonesia.</p>
<p align="left">&ldquo;Semoga kami dapat bertugas dengan baik dan mengharapkan dukungan dari seluruh elemen masyarakat di Kabupaten Nunukan ini,&rdquo; pungkas Rio Neswan. <strong>(***/eza)</strong></p>
<p><a href="http://kaltara.prokal.co/read/news/14666-pengalaman-pertama-memimpin-satuan-di-perbatasan.html">http://kaltara.prokal.co/</a></p>',
                'image' => 'posts/November2017/pengalaman-pertama-memimpin-satuan-di-perbatasan.jpg',
            'slug' => 'letkol-(inf)-rio-neswan-(tn-5)-nakhoda-baru-satgas-pamtas-ri-malaysia',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:49:16',
                'updated_at' => '2017-11-09 12:49:16',
                'image_url' => NULL,
            ),
            45 => 
            array (
                'id' => 56,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Pembukaan PANDATARA 2017 oleh Ketua MPR RI Zulkifli Hasan',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>&nbsp;.</p>',
                'image' => 'posts/November2017/22770854_1602575649785806_8915862703870481117_o1.jpg',
                'slug' => 'pembukaan-pandatara-2017-oleh-ketua-mpr-ri-zulkifli-hasan',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:50:29',
                'updated_at' => '2017-11-09 12:50:29',
                'image_url' => NULL,
            ),
            46 => 
            array (
                'id' => 57,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Ketua MPR RI Zulkifli Hasan Ingin Sistem Pendidikan Taruna Diterapkan di Berbagai Daerah Indonesia',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>Ketua MPR RI Zulkifli Hasan menyatakan bahwa Indonesia perlu membentuk sistem sekolah yang menanamkan nilai-nilai kebangsaan, perjuangan, dan kebudayaan di berbagai daerah. Dengan penerapan sistem tersebut, generasi penerus bangsa akan mampu menjaga merah putih agar tetap kokoh dan tidak dapat dikoyak oleh pengaruh asing.</p>
<p>&ldquo;Hari ini di negara kita banyak sekali salah paham. Perdebatan sering kali menjadi pemicu permusuhan. Bangsa ini menjadi rentan. Oleh karena itu, kita butuh generasi yang mengerti betul dan mengamalkan nilai-nilai luhur agar dapat menjadi penengah dari kekisruhan yang terjadi di tengah masyarakat,&rdquo; kata Zulkifli.</p>
<p>Hal tersebut ia sampaikan ketika membuka acara Pandatara (pameran seni dan budaya nusantara) yang diadakan oleh&nbsp; SMA Taruna Nusantara, Magelang, Jawa Tengah (26/09/17).&nbsp; Dalam acara tersebut, Zulkifli pun menyatakan bahwa sistem pendidikan yang ada di sekolah tersebut bisa diterapkan di daerah lain.<span id="more-10355"></span></p>
<p><img class="alignnone " src="http://assets.kompas.com/crop/0x0:780x390/780x390/data/photo/2017/10/26/259542368.jpg" alt="" width="787" height="403" /></p>
<p>Dengan memadukan aspek kebangsaan, kebudayaan, dan penekanan terhadap aspek akademik secara disiplin maka akan tercipta generasi penerus bangsa yang unggul dan dapat mengharumkan nama Indonesia di dunia internasional.</p>
<p>&ldquo;Hari ini jumlah penduduk Indonesia telah mencapai 261 juta jiwa. Dengan penduduk yang begitu banyak saja kita masih impor bahan pangan. Lalu bagaimana nanti ketika 50 tahun lagi bila penduduk mencapai 500 juta jiwa? Hanya ilmu dan teknologi yang mampu membuat kita menjadi bangsa yang mandiri,&rdquo; tambah Zulkifli.</p>
<p>Namun demikian, ilmu dan teknologi menjadi tidak ada harganya, menurut Zul, jika individu-individu yang bersangkutan tidak mengenal nilai-nilai luhur kebangsaan. Dalam beberapa Minggu terakhir misalnya, terdapat tiga kepala daerah tertangkap KPK. Hal itu menunjukkan bahwa&nbsp; uang telah menjadi indikator dari setiap hal di kehidupan kita dan menghapus seluruh nilai-nilai luhur.</p>
<p>Zul pun mengatakan bahwa setelah reformasi Indonesia mengalami banyak kemajuan dari sisi kesetaraan dan hak individu yang lebih luas. Akan tetapi, pengertian hak tersebut tidak bisa disalahartikan karena akan merugikan orang lain. Oleh sebab itu, kita tidak bisa menyamakan yang berbeda dan membedakan yang sama.</p>
<p>&ldquo;Saya berharap ke depannya kita bisa membuat sekolah-sekolah lain dengan sistem pendidikan seperti yang dimiliki oleh SMA Taruna Nusantara. Alangkah terangnya masa depan Indonesia jika sistem pendidikan seperti ini diterapkan di banyak daerah,&rdquo; tutup Zul.</p>
<p><a href="http://biz.kompas.com/read/2017/10/26/162213028/ketua-mpr-ri-ingin-sistem-pendidikan-taruna-diterapkan-di-berbagai-daerah">http://biz.kompas.com/</a></p>
<p>Berita terkait :<br /> http://www.mpr.go.id/posts/bertemu-siswa-taruna-nusantara-ketua-mpr-optimis-indonesia-punya-harapan<br /> http://www.antarajateng.com/detail/zulkifli-hasan-berharap-sma-tn-dikembangkan-daerah-lain.htmlhttps://radarsemarang.com/2017/10/26/sma-tn-gelar-pandatara/<br /> https://radarsemarang.com/2017/10/26/sma-tn-gelar-pandatara/<br /> http://www.kebumenekspres.com/2017/10/ketua-mpr-kagumi-pandatara-sma-taruna.html<br /> http://www.rmol.co/read/2017/10/26/312621/Tiga-Syarat-Negara-Maju-Versi-Zulkifli-Hasan-<br /> http://www.suaramerdeka.com/smcetak/detail/11063/Pameran-Seni-Budaya-SMA-Taruna-Nusantara<br /> http://news.akurat.co/id-80144-read-ketua-mpr-harap-sma-tn-dikembangkan-di-daerah-lain</p>',
                'image' => 'posts/November2017/719_bertemu-siswa-taruna-nusantara-ketua-mpr-optimis-indonesia-punya-harapan.jpeg',
                'slug' => 'ketua-mpr-ri-zulkifli-hasan-ingin-sistem-pendidikan-taruna-diterapkan-di-berbagai-daerah-indonesia',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:51:48',
                'updated_at' => '2017-11-09 12:51:48',
                'image_url' => NULL,
            ),
            47 => 
            array (
                'id' => 58,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Juara MSI & LKIP 2017',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>&nbsp;</p>
<header>
<h1 class="entry-title">&nbsp;</h1>
<header>
<h1 class="entry-title">Juara MSI 2017</h1>
</header>
<ol>
<li>Juara 1 PUTU TANISYA PUTRI W dari SMP N 3 DENPASAR, berhak atas Piala Tetap, Piala Bergilir Menteri Pertahanan RI dan uang pembinaan Rp. 6.000.000,00</li>
<li class="text_exposed_show">Juara 2 GEDE ARYANA SAPUTRA dari SMP N 3 DENPASAR, berhak atas Piala Tetap dan uang pembinaan Rp. 5.000.000,00</li>
<li class="text_exposed_show">Juara 3 LEONARDUS HANS S T dari SMP PL BINTANG LAUT SURAKARTA, berhak atas Piala Tetap dan uang pembinaan Rp. 4.000.000,00</li>
<li class="text_exposed_show">Juara Harapan 1 HAZRAT ARIDHO dari SMP ISLAM BAITUL IZZAH, berhak atas Piala Tetap dan uang pembinaan Rp. 3.000.000,00</li>
<li class="text_exposed_show">Juara Harapan 2 AZIZ SYAHARUDDIN dari SMP N 2 JOMBANG, berhak atas Piala Tetap dan uang pembinaan Rp. 2.000.000,00</li>
</ol>
<p>Kelima Finalis MSI 2017, yaitu Juara 1, Juara 2, Juara 3, Juara Harapan 1 dan Juara Harapan 2 dibebaskan dari Tes Akademik dan akan mendapatkan beasiswa penuh selama 3 tahun di SMA TN jika lolos Tes Psikologi, Tes Kesehatan dan Wawancara pada Penerimaan Siswa Baru T.P. 2018/2019</p>
<p>Selamat kepada para juara<br /> Sampai Jumpa pada MSI 2018</p>
<h1 class="entry-title"><br />Juara LKIP 2017</h1>
</header>
<p>ILMU PENGETAHUAN HAYATI</p>
<p>JUARA 1 : Muhammad Daffa P. P. dan Bangun Fariqoh S. (SMPN 1 Kebumen)<span class="text_exposed_show"><br /> JUARA 2 : Azzahra Fatinnuha A.P.P. (SMPN 1 Kota Kediri)<br /> JUARA 3 : Meisya N. dan Tamara Aliya Z. (SMP AL Azhar Syifa Budi Solo)<br /> JUARA HARAPAN 1 : Alfan A. dan Ahmad Mahat M. (SMPN 1 Wonosobo)<br /> JUARA HARAPAN 2 : Yoga D. dan Alma Ghina M. (SMPN 1 Jetis Ponorogo)</span></p>
<div class="text_exposed_show">
<p>ILMU PENGETAHUAN TEKNIK</p>
<p>JUARA 1 : Rizky Syafira P. dan Athaya Maharani P. G. (SMPN 2 Sokaraja)<br /> JUARA 2 : Galang Nurbudi U. dan Akmal Sulthon F. (SMPN 1 Jetis Ponorogo)<br /> JUARA 3 : I Made Dika D. dan I Gede Adhie K. (SMPN 3 Denpasar)<br /> JUARA HARAPAN 1 : Noor Izzani S. Z. J. dan Nida Muthia S. (SMPN 1 Mungkid)<br /> JUARA HARAPAN 2 : Ahmad Fariduddin A. dan Zaki Zaidan A. (MTsN 1 Kota Malang)</p>
<p>ILMU PENGETAHUAN SOSIAL KEMANUSIAAN</p>
<p>JUARA 1 : Wiwin A. dan R. Yodha Janardanu D. (SMPN 2 Selomerto) berhak membawa pulang piala bergilir Menteri Pertahanan RI<br /> JUARA 2 : Nisrina Himly H.H. dan Widya Aryana R. (SMPI-PK Muhammadiyah Delanggu)<br /> JUARA 3 : I Putu Mahendi K.M. dan Cok Istri Sitananda A. (SMPN 3 Denpasar)<br /> JUARA HARAPAN 1 : Putri Anggun R. dan Nabila P. (SMP Muhammadiyah 1 Pati)<br /> JUARA HARAPAN 2 : Ulayya Elma F. dan Edelweis Agatha Z. D. (SMPN 2 Temanggung)</p>
<p>Selamat untuk para pemenang<br /> Sampai jumpa pada LKIP 2018</p>
</div>',
                'image' => 'posts/November2017/23116709_1608325979210773_4188244931463706834_o.jpg',
                'slug' => 'juara-msi-and-lkip-2017',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:53:33',
                'updated_at' => '2017-11-09 12:53:33',
                'image_url' => NULL,
            ),
            48 => 
            array (
                'id' => 59,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Azzahra Jelita Kusuma Pamilih Juara Lomba Debat Tingkat Nasional',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p><strong>TRIBUNJOGJA.COM, MAGELANG</strong> &ndash; Tutur katanya lugas, namun tertata begitu rapi. Kesan cerdas begitu terpancar dari sosok bernama Azzahra Jelita Kusuma Pamilih, seorang siswi kelas XII SMA Taruna Nusantara (TN) Magelang.</p>
<div class="side-article txt-article">
<p>Rasanya pantas, kalau baru-baru ini, gadis asal Karanganyar tersebut, meraih predikat juara satu, dalam lomba debat Bahasa Indonesia tingkat nasional, yang diselenggarakan oleh Kementerian Pendidikan dan Kebudayaan (Kemendikbud) RI.</p>
<p>&ldquo;Awalnya ikut lomba di tingkat daerah, sampai akhirnya ditunjuk mewakili Provinsi Jawa Tengah untuk ikut di tingkat nasional,&rdquo; katanya, Selasa (31/10/2017).</p>
</div>
<p>&nbsp;</p>
<p>Tapi, siapa sangka, kalau minatnya di dunia debat, baru muncul saat mulai menempuh pendidikan di SMA TN.</p>
<p>Sebelumnya, minat remaja 16 tahun itu lebih condong ke mata pelajaran fisika.</p>
<p>Hal tersebut, di latar belakangi keinginannya, untuk mencoba hal baru.</p>
<p>&ldquo;Selain itu, iklim diskusi sangat terbangun di sini (SMA TN), lingkungan sangat mendukung. Terlebih, kami tidak sekadar dididik akademik semata, tapi juga dituntut bisa tampil di depan, harus bisa menyusun argumen,&rdquo; ucapnya.</p>
<p>Putri pasangan Bayu Tunggul Pamilih dan Irmawati Kusumastuti tersebut, menganggap keputusannya terjun ke dunia debat memang tepat.</p>
<p>Selain banyak pengalaman berharga yang didapat, wawasannya pun semakin luas.</p>
<p>&ldquo;Ya, kita dituntut up to date, harus tahu apa saja yang sedang trending di khalayak ramai. Jadi, otomatis harus sering-sering explore. Melalui debat, kita bisa melihat argumen seseorang dari banyak sisi, dari perspektif lain,&rdquo; ucapnya.</p>
<p>Selain menjadi andalan dalam setiap lomba debat, Azzahra rupanya juga memiliki kemampuan leadership yang mumpuni.</p>
<p>Buktinya, gadis kelahiran 16 Mei 2001 tersebut, saat ini menduduki posisi Ketua Umum III OSIS SMA TN.</p>
<p>&ldquo;Memang dasarnya suka berorganisasi, terutama sejak masuk SMA TN. Dengan berorganisasi, kita bisa memahami karakter banyak orang, harus menerapkan cara berbeda-beda, untuk merangkul satu sama lain,&rdquo; ungkap anak pertama dari dua bersaudara itu. <strong>(TRIBUNJOGJA.COM)</strong></p>
<p><a href="http://jogja.tribunnews.com/2017/10/31/azzahra-jelita-siswi-sma-tn-juara-lomba-debat-tingkat-nasional">http://jogja.tribunnews.com</a></p>',
                'image' => 'posts/November2017/azzahra-jelita_20171031_172135.jpg',
                'slug' => 'azzahra-jelita-kusuma-pamilih-juara-lomba-debat-tingkat-nasional',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:55:54',
                'updated_at' => '2017-11-09 12:55:54',
                'image_url' => NULL,
            ),
            49 => 
            array (
                'id' => 60,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Kompol Martua Raja Taripar Laut Silitonga Dapat Pin Emas dari Kapolri',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p><span class="jpnncom">jpnn.com</span>, <a href="http://www.jpnn.com/tag/jakarta">JAKARTA</a> &ndash; Kasat Resnarkoba Polresta Bandara Soetta <a href="http://www.jpnn.com/tag/kompol-martua">Kompol Martua</a> Silitonga mendapat penghargaan <a href="http://www.jpnn.com/tag/pin-emas-dari-kapolri">pin emas dari </a><a href="http://www.jpnn.com/tag/kapolri">Kapolri</a> Jenderal Tito Karnavian, Jumat (13/10) kemarin.</p>
<p>Kompol Martua mendapat penghargaan tersebut bersama dengan 43 anggota Polri dan dua anggota TNI yang berprestasi. &ldquo;Sangat membahagiakan, ada 43 polisi dan dua TNI mendapat penghargaan karena telah melaksanakan tugas dan mengukir prestasi melebihi panggilan tugasnya,&rdquo; ujar Tito di Rupatama Mabes Polri, Jakarta Selatan, Jumat (13/10).</p>
<p>Penerima penghargaan terbanyak berasal dari Polresta Bandara Soetta. Salah satunya <a href="http://www.jpnn.com/tag/kompol-martua">Kompol Martua</a>, lulusan Akpol 2004. Dia mengungkap kasus penyelundupan dan peredaran narkotika jaringan Nigeria-Benin-Jakarta dengan modus Swallow. Total barang bukti 139 kapsul berisi sabu 2.347 gram. Dua orang ditangkap, yaitu WNA Republik Benin dan Ghana. Satu di antaranya diambil tindakan tegas karena melawan petugas.<span id="more-10409"></span></p>
<p>Dalam kariernya di kepolisian, mantan Kasat Resnarkoba Polres Pelabuhan Tanjung Priok ini cukup gemilang. Sebut saja pada 2015, dia mengungkap jaringan narkoba internasional (Tiongkok, Kuala Lumpur dan Jakarta) dengan barang bukti 94 kg sabu dan 112.189 butir ekstasi.</p>
<p>Di tahun 2016, Martua juga mengungkap kasus sabu-sabu dalam mesin blower yang dikembangkan ke Ruko Arcadia Batu Ceper dan Provinsi Bali dengan barang bukti 72.343,8 gram. Dalam kasus ini, dia butuh waktu enam jam untuk membongkar mesinnya.</p>
<p>Di tahun yang sama, jebolan SMA Taruna Nusantara Magelang ini dianugerahi Tanda Kehormatan Satyalancana Operasi Kepolisian dari Presiden RI Joko Widodo karena telah mengungkap jaringan narkoba internasional Guangzhou Tiongkok, Hong Kong, Kuala Lumpur dan Jakarta.</p>
<p>Tahun 2016 pria kelahiran Sintang 4 Oktober 1982 ini mengungkap 61.551 butir ekstasi, 41.960 happy five dan 1.012 gram sabu. Martua juga berhasil mendapatkan penghargaan MURI atas rekor Pelantikan Duta Anti-Narkoba kepada Awak Pesawat Terbanyak pada tahun 2017.<strong> (adk/jpnn)</strong></p>
<p><a href="https://www.jpnn.com/news/kompol-martua-silitonga-dapat-pin-emas-dari-kapolri">https://www.jpnn.com</a></p>',
                'image' => 'posts/November2017/martua-silitonga-saat-menerima-pin-emas-dari-kapolri-tito-karnavian-foto-source-for-jpnncom.jpg',
                'slug' => 'kompol-martua-raja-taripar-laut-silitonga-dapat-pin-emas-dari-kapolri',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:57:10',
                'updated_at' => '2017-11-09 12:57:10',
                'image_url' => NULL,
            ),
            50 => 
            array (
                'id' => 61,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Juara 3 Umum Lomba Geological Events Magmadipa',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>Tim Geologi yang terdiri dari <a id="js_7f1" class="profileLink" href="https://www.facebook.com/Iknanda.J.R?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=100008405312924&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Iknanda Januar Rizaldi</a>, <a id="js_7gk" class="profileLink" href="https://www.facebook.com/zolasaputra.tn26?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=100004055297520&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Zola Saputra</a> dan <a id="js_7ij" class="profileLink" href="https://www.facebook.com/jamalhabibur?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=100003898578026&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Jamal Habiburrahman</a> Rahman berhasil meraih dapat juara 3 Umum di Universitas Diponegoro Semarang pada Lomba Geological Events Magmadipa</p>',
                'image' => 'posts/November2017/23157265_137717433617670_752664252500070123_o.jpg',
                'slug' => 'juara-3-umum-lomba-geological-events-magmadipa',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 12:58:41',
                'updated_at' => '2017-11-09 12:58:41',
                'image_url' => NULL,
            ),
            51 => 
            array (
                'id' => 62,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Ahmad Haulian Yoga Pratama Juara 1 dalam Lomba Informatics Olympiad 2017 di UNNES',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p><a id="js_1f3" class="profileLink" href="https://www.facebook.com/yogahmad?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=100005905181917&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Ahmad Haulian Yoga Pratama</a> siswa kelas XII MIPA 7 berhasil meraih Juara 1 dalam Lomba Informatics Olympiad 2017 di Universitas Negeri Semarang, Minggu 5 November 2017.</p>',
                'image' => 'posts/November2017/23155135_1612190988824272_3093001582604208085_o.jpg',
                'slug' => 'ahmad-haulian-yoga-pratama-juara-1-dalam-lomba-informatics-olympiad-2017-di-unnes',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 13:00:28',
                'updated_at' => '2017-11-09 13:00:28',
                'image_url' => NULL,
            ),
            52 => 
            array (
                'id' => 63,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Juara 1 dan Juara 3 dalam Semarak Geografi 2017 tingkat Nasional di Universitas Negeri Yogyakarta',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>2 Tim berhasil meraih Juara 1 dan Juara 3 dalam Semarak Geografi 2017 tingkat Nasional di Universitas Negeri Yogyakarta.<br /> Mereka terdiri dari : Iknanda Januar Rizaldi, <a class="profileLink" href="https://www.facebook.com/zolasaputra.tn26?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=100004055297520&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Zola Saputra</a>, dan Jamal Habiburahman serta Bagas Patria Arikusuma, Adyan Pamungkas, Muhammad Rizki Arifudin dan Keshia Jenine Sahyasmita Setiabudi dibawah asuhan Bapak Rudi Adi Susanto.</p>',
                'image' => 'posts/November2017/23316466_1614047798638591_2521398736687641751_n1.jpg',
                'slug' => 'juara-1-dan-juara-3-dalam-semarak-geografi-2017-tingkat-nasional-di-universitas-negeri-yogyakarta',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-09 13:03:33',
                'updated_at' => '2017-11-09 13:03:33',
                'image_url' => NULL,
            ),
            53 => 
            array (
                'id' => 64,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Juara 1 National Programing & Logic Competition',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Gabungan Tim Komputer dan Tim Matematika atas nama Muhamad Salman Al-Farisi,&nbsp;</span><a id="js_gg" class="profileLink" style="color: #365899; cursor: pointer; text-decoration: none; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;" href="https://www.facebook.com/yogahmad?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=100005905181917&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Ahmad Haulian Yoga Pratama</a><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">&nbsp;dan Kinantan Arya Bagaspati berhasil meraih juara 1 dalam even National Programing &amp; Logic Competition di Universitas Ciputra, di bawah asuhan Bp Ito Dwi Adha.</span></p>',
                'image' => 'posts/November2017/photo_2017-11-12_21-11-06.jpg',
                'slug' => 'juara-1-national-programing-and-logic-competition',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-13 08:46:40',
                'updated_at' => '2017-11-13 08:46:40',
                'image_url' => NULL,
            ),
            54 => 
            array (
                'id' => 65,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Kampanye Dialogis & Pemilihan Pengurus OSIS - MPK',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>&nbsp;Kampanye Dialogis &amp; Pemilihan Pengurus OSIS - MPK Masa Bakti 2017/2018</p>',
                'image' => 'posts/November2017/23668778_1623043324405705_1869275288000188210_o1.jpg',
                'slug' => 'kampanye-dialogis-and-pemilihan-pengurus-osis-mpk',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 1,
                'created_at' => '2017-11-16 13:22:43',
                'updated_at' => '2017-11-17 11:39:31',
                'image_url' => NULL,
            ),
            55 => 
            array (
                'id' => 66,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Sertijab Komandan Bataliyon Zeni Konstruksi 14/SWS Srengseng Sawah, Dari TN 4 ke TN 5',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p style="margin: 0px 0px 6px; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Serah Terima Jabatan Komandan Bataliyon Zeni Konstruksi 14/SWS Srengseng Sawah, Jagakarsa, Jakarta Selatan dari Letkol Fauzan Fadli, SE (TN 4) ke adik kelasnya Letkol Zaenal Arifin (TN 5).</p>
<p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Letkol Fauzan Fadli mendapat tugas baru sebagai Komandan Kodim 0707/Wonosobo.</p>
<p style="margin: 6px 0px 0px; display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">\'Semoga Tuhan memberkati sumpah dan janjimu\'</p>',
                'image' => 'posts/November2017/23592006_1603189996408295_7733795921048135703_o.jpg',
                'slug' => 'sertijab-komandan-bataliyon-zeni-konstruksi-14-sws-srengseng-sawah-dari-tn-4-ke-tn-5',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 1,
                'created_at' => '2017-11-17 11:33:30',
                'updated_at' => '2017-11-17 11:38:34',
                'image_url' => NULL,
            ),
            56 => 
            array (
                'id' => 67,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Tradisi Prestasi',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>Tradisi Prestasi</p>',
                'image' => 'posts/November2017/23593669_1625214917521879_3668293554028934585_o.jpg',
                'slug' => 'tradisi-prestasi',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-11-18 20:31:20',
                'updated_at' => '2017-11-18 20:31:20',
                'image_url' => NULL,
            ),
            57 => 
            array (
                'id' => 68,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Bakti Nusantara',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p style="margin: 0px 0px 6px; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Bangunan SMP Negeri Sekon di Timor Tengah Utara yang diinisiasi dan dikoordinasi oleh Ikatan Alumni SMA Taruna Nusantara mulai menampakkan wujudnya.</p>
<p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Bekerja sama dengan banyak pihak, termasuk masyarakat lokal, Satgas Perbatasan Yon 742/SWY, PGRI, juga Yayasan Matahati, rangkaian kegiatan yang terdiri dari Sehat Nusantara (layanan kesehatan dan operasi gratis), Bangun Nusantara (pembangunan sekolah), serta Inspirasi Nusantara (pelatihan guru dan peningkatan motivasi siswa melalui kartu pos alumni di luar negeri), akan terus berlangsung hingga nanti puncaknya pada 14 Desember 2017.</p>
<p style="margin: 6px 0px 0px; display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Ayo ikut berpartisipasi..!!</p>',
                'image' => 'posts/December2017/23735985_182549388967686_6050750743718977383_o.jpg',
                'slug' => 'bakti-nusantara',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-04 11:02:08',
                'updated_at' => '2017-12-04 11:02:08',
                'image_url' => NULL,
            ),
            58 => 
            array (
                'id' => 69,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Peringatan Hari Guru',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>&nbsp;Peringatan Hari Guru</p>
<p>&nbsp;</p>',
                'image' => 'posts/December2017/23847581_1632375403472497_8598533827480176844_o1.jpg',
                'slug' => 'peringatan-hari-guru',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-04 11:06:55',
                'updated_at' => '2017-12-04 11:06:55',
                'image_url' => NULL,
            ),
            59 => 
            array (
                'id' => 70,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Bakti Nusantara : Fisik Gedung SMP Negeri Sekon Sudah 80 Persen',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<div class="clearfix">Pose bersama ketua panitia kegiatan Nusantara daerah NTT, dr. Teguh Dwi Kuncoro bersama tim kerja pembangunan gedung SMP Negeri Sekon, Jumat (1/12/2017)</div>
<p><strong>KEFAMENANU &ndash;</strong>&nbsp;Ketua panitia bakti Nusantara 2017 daerah NTT dr. Teguh Dwi Nugroho, M.Biomed, SpB mengatakan pembangunan gedung SMP Negeri Sekon di desa Sekon, Kecamatan Insana, Kabupaten Timor Tengah Utara, Nusa Tenggara Timur (NTT) saat ini sudah memasuki dua setengah bulan pengerjaan dan fisik gedungnya sudah mencapai 80 persen.</p>
<p>&ldquo;Bangunan yang dimulai sejak 18 September 2017 dan peletakan batu pertama pada 16 September 2017 lalu, sejauh ini sudah 80 persen,&rdquo;tandasnya kepada wartawan, Jumat (01/12/2017) kemarin.</p>
<p>Pembangunan gedung tersebut meliputi tiga ruang kelas, tiga ruang guru, empat buah toilet, septik tank, tower air, tiang bendera, meja, kursi dan rak buku perspustakaan.</p>
<p>&ldquo;Terhitung Kamis (30/11/2017) bubungan pasang atas spandek sudah 90 persen, pasang atas rangka plafon 80 persen, pasang atas gipsum plafon 50 persen dan keramik 60 persen,&rdquo;imbuhnya.</p>
<p>Pembangunan gedung itu melibatkan seluruh masyarakat desa Sekon, 20 orang anggota Pamtas RI-RDTL Sektor Barat Yonif 742/SWY, 10 orang anggota Kodim 1618/TTU dan 40 pelajar SMP Negeri Sekon.</p>
<p>Diharapkan pembangunan gedung ini selesai tepat waktu sebelum diresmikan pada tanggal 14 Desember 2017 nanti.&nbsp;<strong>(Lius Salu) <a href="http://nusantaratimur.com/2017/12/02/fisik-gedung-smp-negeri-sekon-sudah-80-persen/">http://nusantaratimur.com</a></strong></p>',
                'image' => 'posts/December2017/IMG-20171201-WA0016-354hocquth91ac2d0wtrey.jpg',
                'slug' => 'bakti-nusantara-fisik-gedung-smp-negeri-sekon-sudah-80-persen',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-04 11:07:18',
                'updated_at' => '2017-12-04 11:08:20',
                'image_url' => NULL,
            ),
            60 => 
            array (
                'id' => 71,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Gunungan: dalam Filosofi dan Tradisi – Juara 1 Festival Film Pariwisata Kab. Magelang tahun 2017',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>Video : Gunungan: dalam Filosofi dan Tradisi &ndash; Juara 1 Festival Film Pariwisata Kab. Magelang tahun 2017</p>
<p>Sebuah film yang diproduksi dalam rangka Festival Film Pariwisata Kab. Magelang tahun 2017. Mendokumentasikan acara Festival Gunungan dengan tajuk : Ketep Summit Festival dengan sudut pandang yang berbeda. Melihat sebuah warisan dari segi filosofi yang mengakar menjadi tradisi, dan harus &ldquo;diuri-uri&rdquo; agar tak cepat mati dan dilupakan oleh anak cucu sendiri.</p>
<p>Selamat untuk tim film SMATN a.n. Nikolaus Johan Tirtono, Avanindra Ramadhantyo Utomo, Muhamad Jaya Hadi Kusuma, Pharaoudra Tiyarapwaza Prayitno, Dova Daffa Dhaifullah, Mutiara Widya Dewi Pribadi, Adira Larasati Indrawan, Abdurrahim Ahsan Mukhlisin, Muhammad Abiel Dewa Pratama, Muhammad Thariq Triezaputera dengan pembina Bp Muhammad Ibrahim, S.Pd.<br /><br /><br /></p>
<p><iframe src="https://www.youtube.com/embed/2WNg1c-FOX0?rel=0" width="560" height="315" frameborder="0" allowfullscreen=""></iframe></p>',
                'image' => NULL,
                'slug' => 'gunungan-dalam-filosofi-dan-tradisi-juara-1-festival-film-pariwisata-kab-magelang-tahun-2017',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-05 09:13:09',
                'updated_at' => '2017-12-05 09:13:09',
                'image_url' => NULL,
            ),
            61 => 
            array (
                'id' => 72,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Letkol (Inf) Mochammad Ghoffar Ngismangil (TN 4) jabat Komandan Kodim 1006/ Martapura, Kalimantan Selatan',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p style="margin: 0px 0px 6px; display: block; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Selamat kepada Letkol (Inf) Mochammad Ghoffar Ngismangil (TN 4) yang mendapat amanah sebagai Komandan Kodim 1006/ Martapura, Kalimantan Selatan</p>
<p style="margin: 6px 0px; display: block; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Di wilayah Kalimantan Selatan ada 2 Dandim dari TN 4 lainnya yaitu :<br />- Letkol (Arh) Samujiyo - Komandan Kodim 1004/Kotabaru.<br />- Letkol (Cpn) Sundoro Agung Nugroho - Komandan Kodim 1001/Amuntai.</p>
<div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">
<p style="margin: 0px 0px 6px; font-family: inherit;">\'Semoga Tuhan memberkati sumpah dan janjimu\'</p>
</div>',
                'image' => 'posts/December2017/24301095_10212315545096883_4974639189821137318_n.jpg',
            'slug' => 'letkol-(inf)-mochammad-ghoffar-ngismangil-(tn-4)-jabat-komandan-kodim-1006-martapura-kalimantan-selatan',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-07 16:32:06',
                'updated_at' => '2017-12-07 16:32:06',
                'image_url' => NULL,
            ),
            62 => 
            array (
                'id' => 73,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Luar Biasa Yang Dilakukan Alumni SMA Taruna Nusantara (Ikastara) di Kefamenanu',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p style="margin: 0px; padding: 0px 0px 25px; color: #323233; font-family: \'Open Sans\', arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;"><strong style="margin: 0px; padding: 0px;">POS KUPANG.COM | KEFAMENANU</strong>&nbsp;- Tentara Nasional Indonesia (TNI) yang tergabung dalam Ikatan Alumni SMA Taruna Nusantara (Ikastara) menggelar bhakti kedua di SMPN Sekon, Kecamatan Insana, Kabupaten TTU, Rabu (29/11/2017).</p>
<p style="margin: 0px; padding: 0px 0px 25px; color: #323233; font-family: \'Open Sans\', arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Kegiatan yang dilaksanakan adalah pemasangan keramik di setiap ruangan kelas.</p>
<p style="margin: 0px; padding: 0px 0px 25px; color: #323233; font-family: \'Open Sans\', arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Hal itu dikatakan Dandim 1618 TTU Letkol (Arm) Budi Wahyono kepada Pos Kupang, Rabu (29/11/2017).</p>
<p style="margin: 0px; padding: 0px 0px 25px; color: #323233; font-family: \'Open Sans\', arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Menurut Budi, kegiatan bhakti nusantara ini melibatkan anggota Makodim 1618 TTU, anggota&nbsp;<a style="margin: 0px; padding: 0px; outline: 0px; color: #016fba; text-decoration: none;" title="Satgas Pamtas" href="http://kupang.tribunnews.com/tag/satgas-pamtas">Satgas Pamtas</a>&nbsp;RI-RDTL Sektor Barat Yonif 742/SWY masyarakat dan pelajar SMPN Sekon.</p>
<p style="margin: 0px; padding: 0px 0px 25px; color: #323233; font-family: \'Open Sans\', arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Kegiatan yang dilaksanakan adalah pemasangan keramik di setiap ruang kelas.</p>
<p style="margin: 0px; padding: 0px 0px 25px; color: #323233; font-family: \'Open Sans\', arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Prosentase progres pekerjaan gedung sekolah ini terus bertambah sejak peletakan batu pertama oleh Bupati TTU, Raymundus Sau Fernandes, Sabtu (16/9/2017) lalu.</p>
<p style="margin: 0px; padding: 0px 0px 25px; color: #323233; font-family: \'Open Sans\', arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Hal ini bisa terjadi berkat kerja sama anggota Kodim 1618 TTU, anggota&nbsp;<a style="margin: 0px; padding: 0px; outline: 0px; color: #016fba; text-decoration: none;" title="Satgas Pamtas" href="http://kupang.tribunnews.com/tag/satgas-pamtas">Satgas Pamtas</a>&nbsp;RI-RDTL Sektor Barat, masyarakat dan pelajar.</p>
<p style="margin: 0px; padding: 0px 0px 25px; color: #323233; font-family: \'Open Sans\', arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Dana untuk pembangunan sekolah ini diperkirakan mencapai Rp 2 M yang dihimpun dari Alumni SMA Taruna Nusantara (Ikastara). (*)</p>',
                'image' => 'posts/December2017/tni-kerja_20171130_093349.jpg',
            'slug' => 'luar-biasa-yang-dilakukan-alumni-sma-taruna-nusantara-(ikastara)-di-kefamenanu',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-07 16:32:49',
                'updated_at' => '2017-12-07 16:32:49',
                'image_url' => NULL,
            ),
            63 => 
            array (
                'id' => 74,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Peringatan Maulid Nabi s.a.w di SMATN',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Mujahadah dalam rangka memperingati Maulid Nabi Muhammad saw. 1439 H oleh Keluarga Besar SMA Taruna Nusantara Magelang dengan menghadirkan KH. Afifuddin Ketua MUI Magelang sebagai pemberi tausiyah dan pencerahan khususnya kepada siswa SMATN. (30/1/2017).&nbsp;</span><br style="color: #1d2129; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Pembina Acara : Bapak Agus Budi, S.Ag</span></p>',
                'image' => 'posts/December2017/24312482_1644377065605664_2425160126472278698_n.jpg',
                'slug' => 'peringatan-maulid-nabi-s-a-w-di-smatn',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-07 16:33:41',
                'updated_at' => '2017-12-07 16:33:41',
                'image_url' => NULL,
            ),
            64 => 
            array (
                'id' => 75,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'BERITA DUKA: Peltu (Purn) Benediktus Ngadi',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Telah meninggal dunia Bp Peltu (Purn) Benediktus Ngadi - Mantan Pamong Administrasi pada hari ini, Rabu 6 Desember 2017.</p>
<p style="margin: 6px 0px; display: block; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Dimakamkan hari Kamis 7 Desember 2017 pukul 10.00</p>
<div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">
<p style="margin: 0px 0px 6px; font-family: inherit;">Keluarga besar SMA TN berduka cita yang mendalam. Semoga almarhum husnul khotimah, keluarga yang ditinggalkan diberikan ketabahan dan kekuatan iman. Aamiin</p>
</div>',
                'image' => 'posts/December2017/24879716_928922750613585_2296786924775897789_o.jpg',
            'slug' => 'berita-duka-peltu-(purn)-benediktus-ngadi',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-07 16:35:14',
                'updated_at' => '2017-12-07 16:35:14',
                'image_url' => NULL,
            ),
            65 => 
            array (
                'id' => 76,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Pangkostrad Letjen TNI Edy Rahmayadi melantik Letkol Inf Maycel Asmi (TN 2) sebagai Komandan Batalyon (Danyon) Mandala Yudha',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p><strong style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Jakarta</strong><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">&nbsp;- Pangkostrad Letjen TNI Edy Rahmayadi meresmikan Batalyon Mandala Yudha. Dia juga melantik Letkol Inf M Asmi sebagai Komandan Batalyon (Danyon) Mandala Yudha.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">"Keberadaan Batalyon Mandala Yudha ini merupakan jawaban dari pembentukan satuan siap gerak TNI, dalam menghadapi penugasan yang bersifat urgen dan dapat dikerahkan dalam waktu singkat meliputi proyeksi tugas Operasi Militer Perang (OMP) maupun Operasi Militer Selain Perang (OMSP), baik dalam skala nasional maupun internasional," ungkap Edy lewat keterangan tertulis, Rabu (6/12/2017).</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /></p>
<table class="pic_artikel_sisip_table" style="display: table; width: 540px; text-align: center; margin-bottom: 10px; color: #929292; font-size: 12px; line-height: 14.4px; table-layout: fixed; font-family: Helvetica, Arial; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" align="center">
<tbody>
<tr>
<td>
<div class="pic_artikel_sisip" style="margin-bottom: 25px; text-align: center; color: #929292; font-size: 12px; line-height: 14.4px; width: 534px;" align="center">
<div class="pic" style="position: relative; display: inline-block; width: 540px; max-width: 100%; text-align: center; background: #f9f9f9; padding-bottom: 10px;"><img style="vertical-align: middle; max-width: 100%; height: auto !important; overflow: hidden; margin-bottom: 5px; display: block; margin-left: auto; margin-right: auto;" src="https://akcdn.detik.net.id/community/media/visual/2017/12/06/f1c85149-6304-42ce-961f-7b26b8ff05e4_169.jpeg?w=620" alt="Letkol Inf M Asmi sebagai Komandan Batalyon (Danyon) Mandala Yudha yang pertama" />Letkol Inf M Asmi sebagai Komandan Batalyon (Danyon) Mandala Yudha yang pertama (Foto: Dok. Penkostrad)</div>
</div>
</td>
</tr>
</tbody>
</table>
<p><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Peresmian ini dilakukan di serambi kehormatan, Markas Kostrad, Gambir, Jakarta Pusat, pagi tadi. Dalam sambutannya itu, Edy menyebut pembentukan batalyon baru itu sebagai antisipasi tuntutan profesional dalam setiap penugasan di masa depan.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /></p>
<div id="beacon_9ab51db562" style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img style="vertical-align: middle; max-width: 100%; height: 0px; width: 0px; display: none !important;" src="https://newrevive.detik.com/delivery/lg.php?bannerid=0&amp;campaignid=0&amp;zoneid=642&amp;loc=https%3A%2F%2Fnews.detik.com%2Fberita%2Fd-3758119%2Fjelang-pensiun-dini-pangkostrad-letjen-edy-resmikan-batalyon-baru%3Futm_source%3Dfacebook%26utm_campaign%3Ddetikcomsocmed%26utm_medium%3Dbtn%26utm_content%3Dnews&amp;referer=https%3A%2F%2Fl.facebook.com%2F&amp;cb=9ab51db562" alt="" width="0" height="0" /></div>
<p><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Batalyon Mandala Yudha merupakan satuan tempur dengan penggabungan berbagai unsur kecabangan di dalamnya. Kecepatan manuver dan perlindungan lapis baja adalah keunggulan yang dimiliki Batalyon Mandala Yudha, sehingga diharapkan dalam penyelesaian tugas pokok dapat dilaksanakan secara efektif dan optimal.</span></p>
<p><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Batalyon ini diproyeksikan sebagai embrio dari Brigade Tim Pertempuran Berat. Batalyon Mandala Yudha merupakan komposisi satuan tempur Infanteri, satuan bantuan tempur Artileri Medan (Armed), Artileri Pertahanan Udara (Arhanud), dan satuan helikopter tempur Penerbad.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Batalyon ini juga didukung oleh satuan Zeni, Peralatan, dan Perhubungan dalam penyiapan operasional batalyon tersebut. Dukungan-dukungan ini diharapkan merealisasi konsep \'Modular Brigade\' sehingga satuan setingkat brigade yang lengkap, kuat, dan efektif serta memiliki efek penggentar (</span><em style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">deterrence effect</em><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">) terhadap ancaman kekuatan lawan.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /></p>
<table class="pic_artikel_sisip_table" style="display: table; width: 540px; text-align: center; margin-bottom: 10px; color: #929292; font-size: 12px; line-height: 14.4px; table-layout: fixed; font-family: Helvetica, Arial; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" align="center">
<tbody>
<tr>
<td>
<div class="pic_artikel_sisip" style="margin-bottom: 25px; text-align: center; color: #929292; font-size: 12px; line-height: 14.4px; width: 534px;" align="center">
<div class="pic" style="position: relative; display: inline-block; width: 540px; max-width: 100%; text-align: center; background: #f9f9f9; padding-bottom: 10px;"><img style="vertical-align: middle; max-width: 100%; height: auto !important; overflow: hidden; margin-bottom: 5px; display: block; margin-left: auto; margin-right: auto;" src="https://akcdn.detik.net.id/community/media/visual/2017/12/06/bd079af6-ae24-45cf-b4ee-59dac93269e5_169.jpeg?w=620" alt="Markas ini berada di atas lahan seluas 700 hektare di Ciuyah, Lebak, Banten." />Markas ini berada di atas lahan seluas 700 hektare di Ciuyah, Lebak, Banten (Foto: Dok. Penkostrad)</div>
</div>
</td>
</tr>
</tbody>
</table>
<p><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Markas Batalyon Mandala Yudha Kostrad dilengkapi perumahan dinas personel dan keluarganya, perkantoran, markas komando batalyon, gudang alutsista, serta lahan latihan tempur dan manuver. Markas ini berada di atas lahan seluas 700 hektare di Ciuyah, Lebak, Banten.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /></p>
<p><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Dalam peresmian ini, hadir Kaskostrad, Panglima Divisi Infanteri 1 dan 2 Kostrad, Ir Kostrad, Koorsahli Pangkostrad, Asren Kostrad, para Asisten Kaskostrad, Kabalak Kostrad, serta para Komandan Satuan Jajaran Kostrad.</span></p>
<p><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Rencananya, Pangkostrad Edy Rahmayadi akan mengundurkan diri dalam rangka pensiun dini. Pengunduran diri ini terkait dengan rencananya dalam Pilgub Sumatera Utara.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /></p>
<table class="pic_artikel_sisip_table" style="display: table; width: 540px; text-align: center; margin-bottom: 10px; color: #929292; font-size: 12px; line-height: 14.4px; table-layout: fixed; font-family: Helvetica, Arial; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" align="center">
<tbody>
<tr>
<td>
<div class="pic_artikel_sisip" style="margin-bottom: 25px; text-align: center; color: #929292; font-size: 12px; line-height: 14.4px; width: 534px;" align="center">
<div class="pic" style="position: relative; display: inline-block; width: 540px; max-width: 100%; text-align: center; background: #f9f9f9; padding-bottom: 10px;"><img style="vertical-align: middle; max-width: 100%; height: auto !important; overflow: hidden; margin-bottom: 5px; display: block; margin-left: auto; margin-right: auto;" src="https://akcdn.detik.net.id/community/media/visual/2017/12/06/cf9394bd-311a-4e4f-9cf6-e05acf559ff0_169.jpeg?w=620" alt="Kecepatan manuver dan perlindungan lapis baja adalah keunggulan yang dimiliki Batalyon Mandala Yudha" />Kecepatan manuver dan perlindungan lapis baja adalah keunggulan yang dimiliki Batalyon Mandala Yudha (Dok. Penkostrad)</div>
</div>
</td>
</tr>
</tbody>
</table>
<p><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Hal tersebut diketahui dari surat keputusan Panglima TNI tentang mutasi jabatan 85 perwira tinggi (pati). Surat tersebut bernomor Kep/982/XII/2017 tertanggal 4 Desember 2017.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">"Letjen TNI Edy Rahmayadi dari Pangkostrad menjadi Pati Mabes TNI AD (dalam rangka pensiun dini)," demikian keterangan tertulis Mabes TNI, Selasa (5/12).</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Upacara serah-terima jabatan Edy akan dilakukan pada Kamis (7/12) besok. Jabatan Edy akan diisi Mayjen TNI Sudirman, yang semula menjabat Asops KSAD. </span><strong style="font-weight: normal; font-family: \'Roboto Medium\'; color: #2d2d2d; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">(jbr/elz)<br /><br />https://news.detik.com/berita/d-3758119/jelang-pensiun-dini-pangkostrad-letjen-edy-resmikan-batalyon-baru?utm_source=facebook&amp;utm_campaign=detikcomsocmed&amp;utm_medium=btn&amp;utm_content=news<br /></strong></p>',
                'image' => 'posts/December2017/cf9394bd-311a-4e4f-9cf6-e05acf559ff0_169.jpeg',
            'slug' => 'pangkostrad-letjen-tni-edy-rahmayadi-melantik-letkol-inf-maycel-asmi-(tn-2)-sebagai-komandan-batalyon-(danyon)-mandala-yudha',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-07 16:49:03',
                'updated_at' => '2017-12-07 16:49:03',
                'image_url' => NULL,
            ),
            66 => 
            array (
                'id' => 77,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'AKBP Ade Ary Syam Indradi, S.H., S.IK. MH (TN 3) jabat Wakil Direktur Reserse Kriminal Umum Polda Metro Jaya',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p style="margin: 0px 0px 6px; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Selamat kepada AKBP Ade Ary Syam Indradi, S.H., S.IK. MH (TN 3) yang mendapat kepercayaan menjadi Wakil Direktur Reserse Kriminal Umum Polda Metro Jaya.</p>
<p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Sebelumnya AKBP Ade Ary menjabat sebagai Kapolres Karawang.</p>
<p style="margin: 6px 0px 0px; display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">\'Semoga Tuhan memberkati sumpah dan janjimu\'</p>',
                'image' => 'posts/December2017/24799361_2016062348405686_2412529818775228998_o.jpg',
            'slug' => 'akbp-ade-ary-syam-indradi-s-h-s-ik-mh-(tn-3)-jabat-wakil-direktur-reserse-kriminal-umum-polda-metro-jaya',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-07 19:09:55',
                'updated_at' => '2017-12-07 19:09:55',
                'image_url' => NULL,
            ),
            67 => 
            array (
                'id' => 78,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Kombes. Pol. Hengki Haryadi, S.IK, MH (TN 1) jabat Kapolres Jakarta Barat',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p style="margin: 0px 0px 6px; display: block; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Selamat kepada Kombes. Pol. Hengki Haryadi, S.IK, MH (TN 1) yang mendapat amanah menjadi Kapolres Jakarta Barat, Polda Metro Jaya.</p>
<p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Kombes Pol Hengki Haryadi sebelumnya menjabat Kasubdit I Dittipideksus Bareskrim Polri.</p>
<p style="margin: 6px 0px; display: block; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">\'Semoga Tuhan memberkati sumpah dan janjimu\'</p>',
                'image' => 'posts/December2017/24312686_1123350991101951_245914626948729529_n.jpg',
            'slug' => 'kombes-pol-hengki-haryadi-s-ik-mh-(tn-1)-jabat-kapolres-jakarta-barat',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-07 20:36:48',
                'updated_at' => '2017-12-07 20:36:48',
                'image_url' => NULL,
            ),
            68 => 
            array (
                'id' => 79,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Takwa Fuadi Samad (TN 2) menerima kedatangan Menko Maritim RI Bapak Luhut Panjaitan',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p style="margin: 0px 0px 6px; display: block; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">CEO PT Imani Prima Takwa Fuadi Samad (TN 2) menerima kedatangan Menko Maritim RI Bapak Luhut Panjaitan di booth B1 - B2 Puri Agung, Hotel Sahid Jaya Jakarta, menjelaskan mengenai aplikasi AISSAT yang merupakan buatan lokal dan bukti nyata.</p>
<p style="margin: 6px 0px; display: block; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;"><a class="_58cn" style="color: #365899; cursor: pointer; text-decoration: none; font-family: inherit;" href="https://www.facebook.com/hashtag/indonesianismesummit2017?source=feed_text&amp;story_id=1647143901995647" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx" style="direction: ltr; unicode-bidi: isolate; font-family: inherit;"><span class="_58cl _5afz" style="unicode-bidi: isolate; color: #4267b2; font-family: inherit;">#</span><span class="_58cm" style="font-family: inherit;">IndonesianismeSummit2017</span></span></a><br /><a class="_58cn" style="color: #365899; cursor: pointer; text-decoration: none; font-family: inherit;" href="https://www.facebook.com/hashtag/baktiuntukindonesia?source=feed_text&amp;story_id=1647143901995647" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx" style="direction: ltr; unicode-bidi: isolate; font-family: inherit;"><span class="_58cl _5afz" style="unicode-bidi: isolate; color: #4267b2; font-family: inherit;">#</span><span class="_58cm" style="font-family: inherit;">baktiUntukIndonesia</span></span></a></p>
<div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">
<p style="margin: 0px 0px 6px; font-family: inherit;">\'Semoga Tuhan memberkati sumpah dan janjimu\'</p>
</div>',
                'image' => 'posts/December2017/24831419_1529158723832021_7657314113065330972_o.jpg',
            'slug' => 'takwa-fuadi-samad-(tn-2)-menerima-kedatangan-menko-maritim-ri-bapak-luhut-panjaitan',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-09 12:30:57',
                'updated_at' => '2017-12-09 12:30:57',
                'image_url' => NULL,
            ),
            69 => 
            array (
                'id' => 80,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Laporan Pertanggungjawaban Pengurus OSIS/ MPK masa bakti 2016/2017',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Laporan Pertanggungjawaban Pengurus OSIS/ Majelis Perwakilan Kelas masa bakti 2016/2017.</span><br style="color: #1d2129; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" /><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">di Ruang Baca Perpustakaan SMA TN, 10 Desember 2017.</span></p>',
                'image' => 'posts/December2017/25152226_1648445015198869_1852867437780181964_n.jpg',
                'slug' => 'laporan-pertanggungjawaban-pengurus-osis-mpk-masa-bakti-2016-2017',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-11 09:27:57',
                'updated_at' => '2017-12-13 10:15:29',
                'image_url' => NULL,
            ),
            70 => 
            array (
                'id' => 81,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Kursus Mahir Tingkat Dasar (KMD)',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<h5 id="js_fk" class="_5pbw _5vra" style="color: #1d2129; font-size: 14px; margin: 0px 0px 2px; padding: 0px 22px 0px 0px; font-weight: normal; line-height: 1.38; font-family: Helvetica, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" data-ft="{&quot;tn&quot;:&quot;C&quot;}"><span class="fwn fcg" style="font-weight: normal; color: #90949c; font-family: inherit;"><span class="fcg" style="color: #90949c; font-family: inherit;"><a style="color: #365899; cursor: pointer; text-decoration: underline; font-family: inherit;" href="https://www.facebook.com/media/set/?set=a.1650137831696254.1073742301.200248470018538&amp;type=3" data-ft="{&quot;tn&quot;:&quot;-U&quot;}">Kursus Mahir Tingkat Dasar (KMD)</a></span></span></h5>',
                'image' => 'posts/December2017/25074869_1650140381695999_359874794051125913_o.jpg',
            'slug' => 'kursus-mahir-tingkat-dasar-(kmd)',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-13 10:08:07',
                'updated_at' => '2017-12-13 10:08:07',
                'image_url' => NULL,
            ),
            71 => 
            array (
                'id' => 82,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Sertijab Pengurus OSIS dan MPK masa bakti 2017/2018',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p><a style="color: #365899; cursor: pointer; text-decoration: underline; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;" href="https://www.facebook.com/media/set/?set=a.1650143378362366.1073742302.200248470018538&amp;type=3" data-ft="{">Sertijab Pengurus OSIS dan MPK masa bakti 2017/2018</a><span style="color: #90949c; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"><br /></span></p>',
                'image' => 'posts/December2017/25188679_1650144098362294_4290332912429244134_o.jpg',
                'slug' => 'sertijab-pengurus-osis-dan-mpk-masa-bakti-2017-2018',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-13 10:09:44',
                'updated_at' => '2017-12-13 10:15:11',
                'image_url' => NULL,
            ),
            72 => 
            array (
                'id' => 83,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Malam pengantar Tugas Pengurus OSIS dan MPK masa bakti 2017/2018',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>Malam pengantar Tugas Pengurus OSIS dan MPK masa bakti 2017/2018</p>',
                'image' => 'posts/December2017/25182222_1650147605028610_6462858876890930549_o.jpg',
                'slug' => 'malam-pengantar-tugas-pengurus-osis-dan-mpk-masa-bakti-2017-2018',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-13 10:10:59',
                'updated_at' => '2017-12-13 11:17:54',
                'image_url' => NULL,
            ),
            73 => 
            array (
                'id' => 88,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'LKPL 2017 dan BAKTI NUSANTARA',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="" style="font-family: Helvetica, Arial, sans-serif; color: #7f7f7f; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" data-block="true" data-editor="cmuna" data-offset-key="dgac6-0-0">
<div class="_1mf _1mj" style="position: relative; white-space: pre-wrap; direction: ltr; text-align: left; font-family: inherit;" data-offset-key="dgac6-0-0"><span style="font-family: inherit;" data-offset-key="dgac6-0-0"><span style="font-family: inherit;" data-text="true">Bagi siswa dan alumni SMA TN, ini adalah salah satu wujud nyata dari pelaksanaan janji Tri Prasetya Siswa yang ketiga pada alinea terakhir, yaitu &ldquo;memberikan karya terbaik bagi masyarakat, bangsa, negara dan dunia&rdquo;, yang tentu saja sangat membesarkan hati pendiri dan para pendidik (pamong) SMA Taruna Nusantara Magelang. Lulusan SMA Taruna Nusantara bukan cuma diharapkan baik secara akademis tetapi juga diharapkan memiliki kepedulian dan berani mengambil keputusan untuk memberikan yang terbaik bagi masyarakat, bangsa, negara dan dunia. </span></span></div>
</div>
<div class="" style="font-family: Helvetica, Arial, sans-serif; color: #7f7f7f; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" data-block="true" data-editor="cmuna" data-offset-key="9i5a1-0-0">
<div class="_1mf _1mj" style="position: relative; white-space: pre-wrap; direction: ltr; text-align: left; font-family: inherit;" data-offset-key="9i5a1-0-0"><span style="font-family: inherit;" data-offset-key="9i5a1-0-0">&nbsp;</span></div>
</div>
<div class="" style="font-family: Helvetica, Arial, sans-serif; color: #7f7f7f; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" data-block="true" data-editor="cmuna" data-offset-key="1vb26-0-0">
<div class="_1mf _1mj" style="position: relative; white-space: pre-wrap; direction: ltr; text-align: left; font-family: inherit;" data-offset-key="1vb26-0-0"><span style="font-family: inherit;" data-offset-key="1vb26-0-0"><span style="font-family: inherit;" data-text="true">Alumni-alumni yang secara militan dan semangat untuk memulai melaksanakan proyek2 nyata yang bisa secara langsung dirasakan oleh masyarakat sangat banyak, tetapi peranan Pengurus PP Ikastara seperti Sdr. David Ratadhi, Aryo Saloko, Alvernia Rendra, Anie Febriastati, Bahar Riand Passa, Riski Hapsari, Mauritz Metta, Afriyanti, A.Teguh Dwi N dan banyak lagi alumni lain, didukung oleh tokoh-tokoh The First seperti Tribuana Wetangterah dkk. Mereka tidak kenal lelah menggalang berbagai potensi dan kekuatan untuk mewujudkan cita-cita prasasti pendirian SMA Taruna Nusantara.</span></span></div>
</div>
<div class="" style="font-family: Helvetica, Arial, sans-serif; color: #7f7f7f; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" data-block="true" data-editor="cmuna" data-offset-key="6a3ci-0-0">
<div class="_1mf _1mj" style="position: relative; white-space: pre-wrap; direction: ltr; text-align: left; font-family: inherit;" data-offset-key="6a3ci-0-0"><span style="font-family: inherit;" data-offset-key="6a3ci-0-0"><span style="font-family: inherit;" data-text="true">Photo Credit: Ikastaran dan HumasTN</span></span></div>
</div>',
                'image' => 'posts/December2017/25151972_1652051594838211_1067378555368761464_n4.jpg',
                'slug' => 'lkpl-2017-dan-bakti-nusantara',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-15 08:25:02',
                'updated_at' => '2017-12-15 08:25:02',
                'image_url' => NULL,
            ),
            74 => 
            array (
                'id' => 89,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'LKPL 2017',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="" style="font-family: Helvetica, Arial, sans-serif; color: #7f7f7f; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" data-block="true" data-editor="cmuna" data-offset-key="9urqs-0-0">&nbsp;</div>
<div class="" style="font-family: Helvetica, Arial, sans-serif; color: #7f7f7f; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" data-block="true" data-editor="cmuna" data-offset-key="b3e7n-0-0">
<div class="_1mf _1mj" style="position: relative; white-space: pre-wrap; direction: ltr; text-align: left; font-family: inherit;" data-offset-key="b3e7n-0-0"><span style="font-family: inherit;" data-offset-key="b3e7n-0-0"><span style="font-family: inherit;" data-text="true">Hari-hari ini, tanggal 13, 14, 15 Desember 2017, ada dua kegiatan sosial kemasyarakatan besar yang diselenggarakan oleh Keluarga Besar SMA Taruna Nusantara Magelang. Pertama kegiatan Latihan Kemasyarakatan Peduli Lingkungan (LKPL) bagi siswa SMA Taruna Nusantara kelas XII di Dusun Menayu Desa Menayu Kecamatan Muntilan, dan yang kedua adalah kegiatan besar dan berskala nasional yaitu Bakti Nusantara yang dipusatkan di Desa Sekon Kefamenanu NTT yang diselenggarakan oleh Ikatan Alumni SMA Taruna Nusantara (Ikastara) yang melibatkan dan bekerjasama dengan berbagai lembaga dan pihak melalui berbagai macam rangkaian kegiatan dan proyek pembangunan yang puncaknya adalah peresmian pembangunan SMP Negeri Sekon yang dilaksanakan hari ini tanggal 14 Desember 2017.</span></span></div>
</div>
<div class="" style="font-family: Helvetica, Arial, sans-serif; color: #7f7f7f; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" data-block="true" data-editor="cmuna" data-offset-key="4upbg-0-0">
<div class="_1mf _1mj" style="position: relative; white-space: pre-wrap; direction: ltr; text-align: left; font-family: inherit;" data-offset-key="4upbg-0-0"><span style="font-family: inherit;" data-offset-key="4upbg-0-0">&nbsp;</span></div>
</div>
<div class="" style="font-family: Helvetica, Arial, sans-serif; color: #7f7f7f; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" data-block="true" data-editor="cmuna" data-offset-key="e3h55-0-0">
<div class="_1mf _1mj" style="position: relative; white-space: pre-wrap; direction: ltr; text-align: left; font-family: inherit;" data-offset-key="e3h55-0-0"><span style="font-family: inherit;" data-offset-key="e3h55-0-0"><span style="font-family: inherit;" data-text="true">Kegiatan Bakti Nusantara yang berskala besar ini adalah yang kedua kali diselenggarakan setelah tahun lalu dilaksanakan di Desa Sekotong Lombok Barat yang dipimpin oleh Dandim Lombok Barat Letkol Inf.Ardiansyah. Kali ini kegiatannya dimulai dengan serangkaian kegiatan sosial, mulai dari operasi katarak gratis, khitanan massal, bantuan sosial, bantuan buku-buku dan membangun sebuah SMP Negeri (SMPN Sekon Kefamenanu) bekerja sama dengan Pemda setempat. </span></span></div>
</div>
<div class="" style="font-family: Helvetica, Arial, sans-serif; color: #7f7f7f; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" data-block="true" data-editor="cmuna" data-offset-key="377n3-0-0">
<div class="_1mf _1mj" style="position: relative; white-space: pre-wrap; direction: ltr; text-align: left; font-family: inherit;" data-offset-key="377n3-0-0"><span style="font-family: inherit;" data-offset-key="377n3-0-0">&nbsp;</span></div>
</div>
<div class="" style="font-family: Helvetica, Arial, sans-serif; color: #7f7f7f; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;" data-block="true" data-editor="cmuna" data-offset-key="eh8f2-0-0">
<div class="_1mf _1mj" style="position: relative; white-space: pre-wrap; direction: ltr; text-align: left; font-family: inherit;" data-offset-key="eh8f2-0-0"><span style="font-family: inherit;" data-offset-key="eh8f2-0-0"><span style="font-family: inherit;" data-text="true">Kegiatan Bakti Nusantara yang mengukir sejarah ini melibatkan banyak alumni SMA Taruna Nusantara ini memang sebuah "passion" yang diharapkan dimiliki oleh setiap alumni SMA Taruna Nusantara, setidaknya itulah harapan pendidik (pamong) SMA Taruna Nusantara saat kegiatan Nusantara Bakti Pertiwi dulu digagas pertama kali melalui program Desa Binaan di Dusun Noyoditan pada awal-awal SMA Taruna Nusantara didirikan dan selanjutnya dikembangkan melalui kegiatan Nusantara Bakti Pertiwi dan lalu tradisi ini diteruskan melalui program tahunan terproyek Latihan Kemasyarakatan Peduli Lingkungan (LKPL) seperti saat ini sedang secara paralel dilaksanakan.</span></span></div>
</div>',
                'image' => 'posts/December2017/25151976_1652051628171541_2172295957172767465_n.jpg',
                'slug' => 'lkpl-2017',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-15 08:25:22',
                'updated_at' => '2017-12-15 08:25:22',
                'image_url' => NULL,
            ),
            75 => 
            array (
                'id' => 90,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Uniknya Reuni SMA Taruna Nusantara Adakan Bakti Sosial di Wilayah Terluar Indonesia',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div id="artimg">&nbsp;</div>
<div id="articleright" class="cl2 mr20 fl cright w160 mb20">&nbsp;</div>
<div class="side-article txt-article">
<p><strong>TRIBUNNEWS.COM, KUPANG</strong> - Reuni SMA identik dengan mengenang masa lalu dan bergembira bersama. Berbeda dengan alumni SMA lainnya, Ikastara sebagai wadah alumni SMA Taruna Nusantara justru mengadakan mini reuni dengan aktivitas bakti sosial besar-besaran.</p>
<p>Kegiatan yang bernama Bakti Nusantara pada 2017 ini memilih Desa Sekon di Kabupaten Timor Tengah Utara, Provinsi Nusa Tenggara Timur, salah satu wilayah terluar Indonesia yang dekat dengan perbatasan negara Timor Leste.</p>
<p>Bakti Nusantara adalah kegiatan pengabdian masyarakat yang diinisiasi oleh Ikatan Alumni SMA Taruna Nusantara (Ikastara).</p>
<p>Kegiatan ini menyinergikan kekuatan alumni yang saat ini berada di TNI/Polri maupun sipil, serta berkolaborasi dengan seluruh institusi terkait, di level pusat maupun daerah.</p>
<p>Pelaksanaan Bakti Nusantara meliputi pengembangan 3 (tiga) aspek, yakni kesejahteraan (Bangun Nusantara), kesehatan (Sehat Nusantara), dan pendidikan (Inspirasi Nusantara).</p>
<p>Pada dasarnya, kegiatan bertujuan untuk meningkatkan taraf hidup masyarakat serta menjadi inspirasi dalam membangun sebuah peradaban yang maju dan berkarakter.</p>
<p>Mengambil lokasi di dekat perbatasan Timor Leste, kegiatan Bakti Nusantara di Desa Sekon mengadakan layanan kesehatan dan operasi gratis yang mencakup operasi katarak, bibir sumbing, sunat massal, pemeriksaan kesehatan gratis, hingga kunjungan ke rumah-rumah.</p>
<p>&ldquo;Peserta sunat massal bahkan membludak hingga 86 orang, katarak 49 orang, dan bibir sumbing 14 orang. Lebih dari 40 relawan dan 114 tenaga kesehatan turut hadir untuk kegiatan ini,&rdquo; papar Ketua Pelaksana, Wahyu Andito dalam keterangan pers, Kamis (14/12/2017).</p>
<p>Layanan ditutup karena waktu yang tidak memungkinkan, sehingga tidak semua pasien dapat terlayani.</p>
<p>Dokter-dokter alumni SMA Taruna Nusantara dalam kegiatan ini dibantu oleh tenaga dari RSUD Kefamenanu, Universitas Atmajaya, Rumah Sakit Siloam, serta TNI AD.</p>
<p>Pelatihan guru pun dilakukan bekerja sama dengan tim Persatuan Guru Republik Indonesia (PGRI) Pusat dan diikuti oleh 94 orang dari berbagai penjuru Kabupaten Timor Tengah Utara.</p>
<p>Di luar bidang pendidikan dan kesehatan, dilakukan juga pemeriksaan kepada seluruh ternak Desa Sekon. Selama ini, Desa Sekon hampir tidak pernah disambangi oleh dokter hewan sehingga kesehatan dan kesejahteraan ternak menjadi tidak diperhatikan.</p>
<p><a href="http://www.tribunnews.com/regional/2017/12/14/uniknya-reuni-sma-taruna-nusantara-adakan-bakti-sosial-di-wilayah-terluar-indonesia">http://www.tribunnews.com</a></p>
</div>',
                'image' => 'posts/December2017/latihan-kebidanan_20171214_160820.jpg',
                'slug' => 'uniknya-reuni-sma-taruna-nusantara-adakan-bakti-sosial-di-wilayah-terluar-indonesia',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-15 16:19:55',
                'updated_at' => '2017-12-15 16:19:55',
                'image_url' => NULL,
            ),
            76 => 
            array (
                'id' => 91,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Bakti Nusantara 2017',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p style="margin: 0px 0px 6px; display: block; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Dalam&nbsp;<a id="js_yf" class="profileLink" style="color: #365899; cursor: pointer; text-decoration: none; font-family: inherit;" href="https://www.facebook.com/bn.baktinusantara/?fref=mentions" data-hovercard="/ajax/hovercard/page.php?id=151974935358465&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Bakti Nusantara</a>&nbsp;2017, alumni TN1 hingga TN25 bertemu. Banyak di antara mereka yang baru pertama kali bersua, namun langsung akrab merasa satu keluarga. Dari Jakarta, Medan, Atambua, Singapura mereka datang. Dari Tokyo, Auckland, Yogyakarta mereka memberikan inspirasi. Semuanya tak hanya mengenang, tetapi membuktikan bahwa lirik Hymne Alumni bukan hanya untaian kata tanpa arti. Kata-kata itu sakti, menagih bukti untuk kejayaan negeri. Dan kami bangga memiliki kalian seba<span class="text_exposed_show" style="display: inline; font-family: inherit;">gai alumni SMA Taruna Nusantara.</span></p>
<div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">
<p style="margin: 0px 0px 6px; font-family: inherit;">Hai alumni Taruna Nusantara<br />Pegang teguh janji kita<br />Rela bela bangsa<br />Janji baktimulah yang slalu bercahya<br />Demi tanah airku Indonesia<br />Jiwa ini kusembahkan untuk jaya bangsa<br />Smoga Tuhan memberkati sumpah dan janjimu</p>
</div>
<p style="text-align: center;"><iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fbn.baktinusantara%2Fvideos%2F190458908176734%2F&amp;show_text=0&amp;width=560" width="560" height="316" frameborder="0" scrolling="no" allowfullscreen="true"></iframe></p>',
                'image' => NULL,
                'slug' => 'bakti-nusantara-2017',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-21 08:26:41',
                'updated_at' => '2017-12-21 08:26:41',
                'image_url' => NULL,
            ),
            77 => 
            array (
                'id' => 92,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'LKPL Angkatan 26',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p style="text-align: center;"><iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fmedia%2Fset%2F%3Fset%3Da.1657592087617495.1073742304.200248470018538%26type%3D3&amp;width=500" width="500" height="597" frameborder="0" scrolling="no"></iframe></p>',
                'image' => 'posts/December2017/25626807_1657594994283871_1316310091497615256_o.jpg',
                'slug' => 'lkpl-angkatan-26',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2017-12-21 08:27:55',
                'updated_at' => '2017-12-21 08:30:03',
                'image_url' => NULL,
            ),
            78 => 
            array (
                'id' => 93,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Gubernur Ridho Anggarkan Rp2 Miliar untuk Siswa Taruna Nusantara Berprestasi Asal Lampung',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p style="box-sizing: border-box; margin: 0px 0px 16px; color: #2d2d2d; font-family: \'Open Sans\', serif; font-size: 15px;"><strong style="box-sizing: border-box;">BANDAR LAMPUNG &ndash; Gubernur</strong>&nbsp;Muhammad Ridho Ficardo, melalui Dinas Pendidikan dan Kebudayaan Provinsi Lampung menganggarkan beasiswa Rp2 miliar lebih kepada 11 siswa berprestasi asal Lampung untuk mengikuti pendidikan di SMA Taruna Nusantara, Magelang.</p>
<p style="box-sizing: border-box; margin: 0px 0px 16px; color: #2d2d2d; font-family: \'Open Sans\', serif; font-size: 15px;">Salah satu alasan pemberian beasiswa adalah memprioritaskan pendidikan kepada siswa berpotensi yang ingin masuk SMA Taruna Nusantara, supaya siswa yang memiliki kemampuan namun terkendala biaya dapat meneruskan sekolahnya ke SMA Taruna.</p>
<p style="box-sizing: border-box; margin: 0px 0px 16px; color: #2d2d2d; font-family: \'Open Sans\', serif; font-size: 15px;">Hal tersebut diungkapkan Gubernur Ridho, saat menerima audiensi SMA Taruna Nusantara Angkatan 28, Senin (18/12/2017), di Ruang Rapat Utama Kantor Gubernur Lampung.</p>
<p style="box-sizing: border-box; margin: 0px 0px 16px; color: #2d2d2d; font-family: \'Open Sans\', serif; font-size: 15px;">&ldquo;Kalian adalah yang terpilih dan terbaik untuk masuk di Taruna Nusantara. Tunjukan prestasi kalian, jangan sampai terlibat masalah yang akan mencoreng nama baik Lampung. Tetaplah rendah hati dan berilah suri tauladan kepada lingkungan dan masyarakat,&rdquo;&nbsp; ungkap Gubernur Ridho.</p>
<p style="box-sizing: border-box; margin: 0px 0px 16px; color: #2d2d2d; font-family: \'Open Sans\', serif; font-size: 15px;">Gubernur Ridho berpesan agar para siswa tersebut mampu menjadi kebanggaan Provinsi Lampung dengan memberikan prestasi terbaik, bukan hanya dalam sisi intelektual, namun juga memilki moral.</p>
<p style="box-sizing: border-box; margin: 0px 0px 16px; color: #2d2d2d; font-family: \'Open Sans\', serif; font-size: 15px;">Pada kesempatan tersebut, Gubernur Ridho, juga memotivasi para pelajar agar meningkatkan kemampuan diri dalam menghadapi persaingan yang semakin tinggi di masa mendatang.</p>
<p style="box-sizing: border-box; margin: 0px 0px 16px; color: #2d2d2d; font-family: \'Open Sans\', serif; font-size: 15px;">&ldquo;Masa depan yang kalian hadapi akan jauh lebih sulit, persaingan akan semakin berat. Di era terbuka ini, kita akan bersaing bukan hanya dengan anak bangsa namun juga bersaing secara internasional. Untuk itu, bekalilah diri kalian sebaik mungkin. Pendidikan adalah modal utama, agar bisa memimpin Lampung untuk melanjutkan kepemimpinan dan&nbsp; pembangunan di Lampung,&rdquo; ujar Gubernur Ridho.</p>
<p style="box-sizing: border-box; margin: 0px 0px 16px; color: #2d2d2d; font-family: \'Open Sans\', serif; font-size: 15px;">Sementara itu, Kepala Dinas Pendidikan dan Kebudayaan Provinsi Lampung Sulpakar dalam laporannya mengatakan, konsep kerjasama pemberian beasiswa ini merupakan buah pemikiran Gubernur Muhammad Ridho Ficardo dan baru yang pertama kali dilaksanakan.</p>
<p style="box-sizing: border-box; margin: 0px 0px 16px; color: #2d2d2d; font-family: \'Open Sans\', serif; font-size: 15px;">Ia mejelaskan, berdasarkan Nota Kesepakatan antara Pemerintah Provinsi Lampung dan Lembaga Perguruan Pendidikan Taman Taruna Nusantara (LPPTTN) penyaluran beasiswa dilakukan melalui dua tahap, yakni Tahap Pertama pada APBDP Tahun 2017 sebesar Rp770 juta dan sisanya sebesar Rp1,540 Milyar dibayarkan melalui anggaran APBD Tahun 2018.&nbsp;<strong style="box-sizing: border-box;">(*/JS) <a href="https://fajarsumatera.co.id/gubernur-ridho-anggarkan-rp2-miliar-untuk-siswa-taruna-nusantara-berprestasi-asal-lampung/" target="_blank" rel="noopener noreferrer">https://fajarsumatera.co.id/</a></strong></p>',
                'image' => 'posts/January2018/WhatsApp-Image-2017-12-19-at-09.01-640x365.jpg',
                'slug' => 'gubernur-ridho-anggarkan-rp2-miliar-untuk-siswa-taruna-nusantara-berprestasi-asal-lampung',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-06 08:45:23',
                'updated_at' => '2018-01-06 08:45:23',
                'image_url' => NULL,
            ),
            79 => 
            array (
                'id' => 94,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Nasihat Menteri Siti ke Siswa Taruna Nusantara: Jadilah Pribadi Jujur!',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p><strong style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Jakarta</strong><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">&nbsp;- Di tengah kesibukannya sebagai Menteri Lingkungan Hidup dan Kehutanan (MenLHK), Siti Nurbaya Bakar menyempatkan waktu menerima tiga siswa dari Taruna Nusantara. Mantan Sekjen Depdagri ini bercerita bahwa didikan sang Ibu telah membentuknya menjadi pribadi dengan mental pekerja keras, tanggungjawab dan penuh disiplin.&nbsp;</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Ketiga siswa ini bernama Andira Mega L, Andi Adhyaksa dan Muhammad Daffa Nur. Pertemuan digelar di ruang kerja Menteri Siti di lantai 4 Gedung Manggala Wanabhakti, Jakarta, Rabu (20/12/2017).&nbsp;</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Mereka sangat antusias bertanya tentang kisah hidup Siti Nurbaya, hingga dipercaya sebagai Menteri kabinet kerja Jokowi-JK periode 2014-2019.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /></p>
<div id="beacon_0afdb4886e" style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img style="vertical-align: middle; max-width: 100%; height: 0px; width: 0px;" src="https://newrevive.detik.com/delivery/lg.php?bannerid=0&amp;campaignid=0&amp;zoneid=642&amp;loc=https%3A%2F%2Fnews.detik.com%2Fberita%2F3779510%2Fnasihat-menteri-siti-ke-siswa-taruna-nusantara-jadilah-pribadi-jujur&amp;referer=https%3A%2F%2Fwww.facebook.com%2F&amp;cb=0afdb4886e" alt="" width="0" height="0" /></div>
<p><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Siti mengungkapkan saat kecil, ia pernah menumpahkan tinta pena milik ibunda. Namun, ia tak berani memberitahunya pada sang Ibu.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">"Saya bertanggungjawab dengan mengumpulkan uang jajan untuk mengganti tinta itu," kisah alumni IPB ini.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Siti merasa sangat terpukul ketika Ibunya meninggal dunia, sesaat jelang ia masuk ke Perguruan Tinggi. Namun berkat semangat yang diajarkan sedari kecil, ia berhasil menamatkan pendidikan dan meneruskan pendidikan di International Institute for Aerospace Survey and Earth Science (ITC), Enschede, Belanda, serta S3 IPB dengan Siegen University, Jerman.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Dalam dunia birokrasi, kiprah Siti dimulai dari jenjang karir paling bawah, hingga menduduki beberapa posisi strategis. Sebagai PNS, ia terakhir menjabat sebagai Sekjen DPD RI.&nbsp;</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">"Motto hidup yang terpenting itu adalah kejujuran. Pintar itu penting, tapi mengedepankan kejujuran jauh lebih penting," tegasnya.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Ketiga siswa Taruna Nusantara juga sempat menanyakan pandangan Siti Nurbaya, perihal kebijakan-kebijakan KLHK yang terkadang banyak mendapatkan penolakan.</span></p>
<p><img style="vertical-align: middle; max-width: 100%; overflow: hidden; margin-bottom: 5px; display: block; margin-left: auto; margin-right: auto; color: #929292; font-family: Helvetica, Arial; font-size: 12px; text-align: center; height: auto !important;" src="https://akcdn.detik.net.id/community/media/visual/2017/12/21/904539d9-6706-4733-a8bb-d38619cd26fd.jpg?a=1" alt="Nasihat Menteri Siti ke Siswa Taruna Nusantara: Jadilah Pribadi Jujur!" /><span style="color: #929292; font-family: Helvetica, Arial; font-size: 12px; text-align: center; background-color: #f9f9f9;">Foto: Dok KLHK</span></p>
<p><span style="color: #929292; font-family: Helvetica, Arial; font-size: 12px; text-align: center; background-color: #f9f9f9;">&nbsp;</span></p>
<p><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Menurutnya penolakan yang terjadi itu wajar, karena setiap kebijakan tentu bersinggungan dengan banyak kepentingan. Contohnya saja di KLHK, saat diterapkan kebijakan yang belum pernah ada sebelumnya, atau lebih tegas dari sebelumnya, maka ada saja pihak-pihak yang merasa terganggu.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">"Yang penting pemerintah posisinya harus selalu objektif, tidak boleh berpihak. Karena pemerintah adalah simpul dari segala negosiasi antar pihak berkepentingan," ungkap Siti.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">"Peran pemerintah itu tidak boleh lalai. Kuncinya adalah komunikasi, sehingga kebijakan yang dihasilkan bisa diterima dan ditaati semuanya demi kebaikan bersama," tambahnya.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Diakhir pertemuan, Siti kembali mengingatkan agar para siswa belajar bersungguh-sungguh, dan mempertahankan sikap disiplin.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">"Jadilah pribadi yang jujur. Karena kejujuran itu modal terpenting dalam hidup kita," nasehatnya.&nbsp;</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><strong style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">(ega/nwy) <a href="https://news.detik.com/berita/3779510/nasihat-menteri-siti-ke-siswa-taruna-nusantara-jadilah-pribadi-jujur">https://news.detik.com/</a></strong></p>',
                'image' => 'posts/January2018/e96367d7-6cce-421f-be6d-4fffe96291bc_169.jpg',
                'slug' => 'nasihat-menteri-siti-ke-siswa-taruna-nusantara-jadilah-pribadi-jujur!',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-06 08:46:23',
                'updated_at' => '2018-01-06 08:46:23',
                'image_url' => NULL,
            ),
            80 => 
            array (
                'id' => 95,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Saat Menag Ditanya Suka Dukanya Menjadi Menteri oleh Siswa SMA Taruna Nusantara',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">Jakarta (Kemenag)---Menteri Agama Lukman Hakim Saifuddin, Kamis pagi (21/12) disambangi empat siswa/siswi SMA Taruna Nusantara. Kunjungan siswa terbaik di SMA Taruna Nusantara ini bukan sekadar bersilaturahmi dan berdiskusi dengan Menag, melainkan melakukan wawancara khusus.</p>
<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">Sosok Menag Lukman Hakim menjadi salah satu magnet bagi mereka untuk mengetahui rekam jejak perjalanan sang menteri dari masa kecil hingga dipercaya menjabat Menteri Agama. Begitu juga dengan pandangan Menag terhadap ragam persoalan saat ini tak luput dari perhatian para siswa dengan &lsquo;mencecar&rsquo; sejumlah pertanyaan kepada Menag.&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">Seperti dukungan Indonesia untuk perjuangan kemerdekaan Palestina, perbedaan hukum di Arab Saudi dengan Indonesia, soal toleransi dan kebhinekaan mengawali wawancara para siswa dengan Menag.</p>
<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">Keempat siswa SMA Taruna Nusantara yang bertugas untuk melakukan wawancara khusus dengan Menteri Agama Lukman Hakim Saifuddin yaitu, Amelia Ayu, Rifa Putri Syafiyyah, Hamed Zonas Fadillah dan Muhammad Bintang Fadly Ramadhan.</p>
<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">Wawancara khusus para siswa dengan Menag berjalan santai dan penuh keakraban ini berlangsung di Ruang Kerja Menag &nbsp;Lantai II Kantor Kemenag Jalan Lapangan Banteng, Jakarta.</p>
<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">Menariknya lagi dalam wawancara tersebut siswa SMA Taruna Nusantara ini baru mengetahui bila Menag Lukman Hakim Saifuddin sama sekali tidak pernah bercita-cinta menjadi menteri.</p>
<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">&ldquo;Izin Pak, apakah Bapak termotivasi oleh ayahanda untuk menjadi Menteri Agama? Tanya Amelia Ayu.</p>
<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">&ldquo;Saya menjadi menteri sama sekali tidak pernah saya rencanakan dan tidak saya cita-citakan. Ayah saya memang betul pernah menjadi Menteri Agama pada tahun 1962 hingga 1967. Tapi saya tidak pernah bercita-cita untuk menjadi menteri. Buat saya jabatan menteri itu terlalu jauh dalam pikiran saya. Dulu cita-cita saya itu adalah menjadi guru. Kenapa guru? Saya menilai profesi guru itu adalah pekerjaan yang paling mulia, ya ketika itu saya masih di Pondok Pesantren Gontor, &ldquo;&nbsp; jawab Menag tersenyum.</p>
<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">&ldquo;Apa saja suka duka Bapak selama menjadi Menteri Agama,&rdquo; timpal Hamed Zonas Fadillah.</p>
<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">&ldquo;Saya lebih banyak merasakan sukanya daripada dukanya. Karena saya bersyukur, karena diberikan kepercayaan untuk bisa memberikan sesuatu bagi orang banyak dan melayani umat beragama, Ya bagaimana hidup kita ini bermanfaat bagi orang banyak," ucap Menag.</p>
<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">&ldquo;Semua ajaran agama berbicara, sebaik-baiknya orang adalah yang memberikan kemaslahatan bagi orang banyak. Kalau dukanya relatif tidak begitu banyak yang saya rasakan, &ldquo; ujar Menag menjelaskan.</p>
<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">Kepada para siswa SMA Taruna Nusantara Menag berpesan agar mensyukuri kesempatan ini. Sebab tidak semua anak-anak di Indonesia berkesempatan bisa mengenyam pendidikan di sekolah yang prestisius dan sangat hebat.</p>
<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">Apalagi tambah Menag, siswa yang bersekolah di SMA Taruna Nusantara merupakan hasil seleksi yang ketat dari seluruh Indonesia. &ldquo;Tidak hanya negara yang memilih kalian untuk bisa sekolah di SMA Taruna Nusantara, melainkan juga takdir Tuhan. Jadi kalian tidak hanya mendapatkan kesempatan dan kepercayaan tapi juga mendapat kehormatan untuk menjadi siswa Taruna Nusantara. Ini harus kalian syukuri dengan menjaga kepercayaan dengan sebaik-baiknya dan bagaimana memanfaatkan kesempatan yang ada untuk hal-hal yang positif, &ldquo; kata Menag.</p>
<p style="box-sizing: border-box; margin: 0px 0px 15px; color: #000000; font-family: \'Roboto Slab\', sans-serif; font-size: 15px;">Usai melakukan wawancara selama hampir 1 jam, empat siswa SMA Taruna Nusantara menyerahkan cenderamata kepada Menag dan melakukan foto bersama. <a href="https://kemenag.go.id/berita/read/506486/saat-menag--ditanya-suka-dukanya-menjadi-menteri-oleh-siswa-sma-taruna-nusantara" target="_blank" rel="noopener noreferrer">https://kemenag.go.id/</a></p>',
                'image' => 'posts/January2018/506486.jpg',
                'slug' => 'saat-menag-ditanya-suka-dukanya-menjadi-menteri-oleh-siswa-sma-taruna-nusantara',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-06 08:47:18',
                'updated_at' => '2018-01-06 08:47:18',
                'image_url' => NULL,
            ),
            81 => 
            array (
                'id' => 96,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Kombes Hengki Haryadi (TN1) Bertekad Hapus Stigma \'Kampung Narkoba\' di Jakbar',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p><strong style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Jakarta</strong><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">&nbsp;- Stigma \'kampung narkoba\' seakan melekat di wilayah Jakarta Barat. Bagaimana tidak, beberapa permukiman penduduk hingga tempat hiburan malam di Jakarta Barat menjadi \'sarang\' narkotika.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Kapolres Metro Jakarta Barat Kombes Hengki Haryadi pun bertekad untuk menghapus stigma itu dengan gencar melakukan operasi khusus narkotika.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">"Tekad dari Polres Jakarta Barat, kami sudah sepakat dengan Bapak Wali Kota, Forkominda, untuk Jakarta Barat harus kita berantas narkoba dan tidak ada lagi istilah kampung narkoba," tegas Hengki kepada wartawan di Mapolres Jakarta Barat, Slipi, Jakarta Barat, Kamis (4/1/2018).&nbsp;</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /></p>
<div id="beacon_dd82ef1cb2" style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img style="vertical-align: middle; max-width: 100%; height: 0px; width: 0px;" src="https://newrevive.detik.com/delivery/lg.php?bannerid=0&amp;campaignid=0&amp;zoneid=642&amp;loc=https%3A%2F%2Fnews.detik.com%2Fberita%2Fd-3799255%2Fkombes-hengki-bertekad-hapus-stigma-kampung-narkoba-di-jakbar%3Futm_source%3Dfacebook%26utm_campaign%3Ddetikcomsocmed%26utm_medium%3Dbtn%26utm_content%3Dnews&amp;referer=https%3A%2F%2Fwww.facebook.com%2F&amp;cb=dd82ef1cb2" alt="" width="0" height="0" /></div>
<p><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Beberapa tempat di Jakarta Barat dikenal dengan \'kampung narkoba\' seperti Kampung Boncos dan Kampung Ambon, karena maraknya peredaran narkotika di tempat tersebut. Belum lagi di tempat-tempat hiburan malam.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">"Dari hasil penangkapan, contohnya Boncos sudah jadi korbannya termasuk tempat di daerah lain, termasuk tempat hiburan malam itu kita pantau semua yang potensial terjadinya peredaran narkoba kita pantau semua," lanjutnya.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Hengki mengatakan, pihaknya bersama Forkominda dan juga LSM antinarkoba, termasuk Kajari Jakarta Barat berkomitmen untuk membersihkan narkotika dari Jakarta Barat. "Karena ini merusak generasi bangsa," ucapnya.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Hengki tidak memungkiri apabila di Jakarta Barat, peredaran narkotika masih cukup tinggi. Selama periode 2015-2017, angka peredaran narkotika di Jakarta Barat masih memprihatinkan.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">"Peningkatan kasus narkotika ini dari 2015-2017 itu angkanya sampai 11,8 persen dan korbannya itu banyak anak-anak, remaja, cukup prihatinlah," sambungnya.&nbsp;</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Pihaknya terus melakukan evaluasi di samping menggencarkan operasi terkait narkotika. Hengki juga telah membentuk Satgas Khusus Anti-Narkoba untuk memberantas peredaran barang haram di Jakarta Barat ini.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">"Karena kebijakan dari pimpinan Kapolri dan Kapolda salah satunya kerawanan kamtibmas itu adalah narkoba, di luar terorisme dan konflik sosial. Kita fokus benar dan memang fakta di lapangan Jakarta Barat sangat tinggi, oleh karenanya perlu ada perhatian khusus kita bentuk satgas," tuturnya.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Pihaknya akan terus mengembangkan penangkapan dari sindikat narkotika yang telah dibekuk, dengan memadukan informasi dari masyarakat. Hal ini dilakukan untuk membersihkan Jakarta Barat dari \'kampung narkoba\'.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><strong style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">(idh/idh) <a href="https://news.detik.com/berita/d-3799255/kombes-hengki-bertekad-hapus-stigma-kampung-narkoba-di-jakbar?utm_source=facebook&amp;utm_campaign=detikcomsocmed&amp;utm_medium=btn&amp;utm_content=news" target="_blank" rel="noopener noreferrer">https://news.detik.com/</a></strong></p>',
                'image' => 'posts/January2018/8460500a-6b83-4577-8f94-1cd0918047b8_34.jpeg',
            'slug' => 'kombes-hengki-haryadi-(tn1)-bertekad-hapus-stigma-\'kampung-narkoba\'-di-jakbar',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-06 08:51:09',
                'updated_at' => '2018-01-06 08:51:27',
                'image_url' => NULL,
            ),
            82 => 
            array (
                'id' => 97,
                'author_id' => 2,
                'category_id' => 1,
                'title' => '8 alumni TN 1 naik pangkat 1 menjadi Komisaris Besar Polisi',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p style="margin: 0px 0px 6px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">Kabar membanggakan diawal tahun 2018, kenaikan pangkat 1 tingkat lebih tinggi dari Alumni TN 1 Kepolisian, menjadi Komisaris Besar Polisi! Selamat untuk para Alumni :<br />- Komisaris Besar Polisi Budhi Herdi Susianto<br />- Komisaris Besar Polisi John Weynart Hutagalung<br />- Komisaris Besar Polisi Hando Wibowo<br />- Komisaris Besar Polisi I Ketut Budi Hendrawan<span class="text_exposed_show" style="display: inline; font-family: inherit;"><br />- Komisaris Besar Polisi&nbsp;<a class="profileLink" style="color: #365899; text-decoration-line: none; font-family: inherit;" href="https://www.facebook.com/mas.ariefadi?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=1297855752&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Arief Adiharsa</a><br />- Komisaris Besar Polisi&nbsp;<a class="profileLink" style="color: #365899; text-decoration-line: none; font-family: inherit;" href="https://www.facebook.com/pages/Jeremias-Rontini/212213245603315?fref=mentions" data-hovercard="/ajax/hovercard/page.php?id=212213245603315&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Jeremias Rontini</a><br />- Komisaris Besar Polisi&nbsp;<a class="profileLink" style="color: #365899; text-decoration-line: none; font-family: inherit;" href="https://www.facebook.com/dani.hamdani.393?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=501874184&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Dani Hamdani</a><br />- Komisaris Besar Polisi Erwin Kurniawan</span></p>
<div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">
<p style="margin: 0px 0px 6px; font-family: inherit;">Kami doakan gerbong kedatangan KomBesPol dan Kolonel Alumni TN 1 berikutnya akan segera tiba, amin!</p>
<p style="margin: 6px 0px; font-family: inherit;">Terima kasih utk seluruh Prestasi dan Karya bagi Negeri, karena bocah Pirikan dipersembahkan bagi Bangsa, Negara INDONESIA dan Dunia</p>
<p style="margin: 6px 0px; font-family: inherit;">Semoga tetap amanah dan semakin sukses. Doa kami dan seluruh Bangsa akan selalu menyertai seluruh patriot NKRI! POLRI yang Profesional, Modern dan Terpercaya! KitaSatoe karena kita TN1!</p>
<p style="margin: 6px 0px; font-family: inherit;">\'Semoga Tuhan memberkati sumpah dan janjimu\'</p>
<p style="margin: 6px 0px; font-family: inherit;"><a class="_58cn" style="color: #365899; text-decoration-line: none; font-family: inherit;" href="https://www.facebook.com/hashtag/tn1brotherhood?source=feed_text&amp;story_id=1672729992770371" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx" style="direction: ltr; unicode-bidi: isolate; font-family: inherit;"><span class="_58cl _5afz" style="unicode-bidi: isolate; color: #4267b2; font-family: inherit;" aria-label="tagar">#</span><span class="_58cm" style="font-family: inherit;">TN1brotherhood</span></span></a>&nbsp;<a class="_58cn" style="color: #365899; text-decoration-line: none; font-family: inherit;" href="https://www.facebook.com/hashtag/bocahpirikan?source=feed_text&amp;story_id=1672729992770371" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx" style="direction: ltr; unicode-bidi: isolate; font-family: inherit;"><span class="_58cl _5afz" style="unicode-bidi: isolate; color: #4267b2; font-family: inherit;" aria-label="tagar">#</span><span class="_58cm" style="font-family: inherit;">bocahPirikan</span></span></a>&nbsp;<a class="_58cn" style="color: #365899; text-decoration-line: none; font-family: inherit;" href="https://www.facebook.com/hashtag/polisipromoter?source=feed_text&amp;story_id=1672729992770371" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx" style="direction: ltr; unicode-bidi: isolate; font-family: inherit;"><span class="_58cl _5afz" style="unicode-bidi: isolate; color: #4267b2; font-family: inherit;" aria-label="tagar">#</span><span class="_58cm" style="font-family: inherit;">PolisiPromoter</span></span></a><a class="_58cn" style="color: #365899; text-decoration-line: none; font-family: inherit;" href="https://www.facebook.com/hashtag/creditarungpandangtritiya?source=feed_text&amp;story_id=1672729992770371" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_5afx" style="direction: ltr; unicode-bidi: isolate; font-family: inherit;"><span class="_58cl _5afz" style="unicode-bidi: isolate; color: #4267b2; font-family: inherit;" aria-label="tagar">#</span><span class="_58cm" style="font-family: inherit;">CreditArungPandangTritiya</span></span></a></p>
</div>',
            'image' => 'posts/January2018/collage-2018-01-05 (1).jpg',
                'slug' => '8-alumni-tn-1-naik-pangkat-1-menjadi-komisaris-besar-polisi',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-06 08:59:56',
                'updated_at' => '2018-01-06 08:59:56',
                'image_url' => NULL,
            ),
            83 => 
            array (
                'id' => 98,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Deklarasi Maju Pilgub Sumsel, Ishak Mekki - Yudha Pratomo Mahyuddin (TN 5) Langsung ke KPU',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p><strong style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Palembang</strong><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">&nbsp;- Wakil Gubernur Sumatera Selatan Ishak Mekki diusung Partai Demokrat dan PKB berpasangan dengan Yudha Mahyuddin. Keduanya melakukan deklarasi dan langsung mendaftarkan diri ke KPU Sumsel.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">"Hari ini resmi sudah kami mendeklarasikan diri siap maju di Pilgub Sumsel dan langsung akan mendaftar di KPU Sumatera Selatan. Semua syarat sudah terpenuhi dengan dukungan Demokrat, PBB, dan PKB. Dengan berpasangan dengan Pak Yudha, saya rasa nanti kami dapat saling melengkapi saat memimpin Sumsel," kata Ishak Mekki di rumah perjuangan, Jalan Basuki Rahmat, Selasa (9/1/2018).</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Selain mendeklarasikan diri, Ishak Mekki menyebut berduet dengan Yudha, yang tidak lain adalah putra mantan Gubernur Sumsel, karena memiliki jiwa muda. Jadi diyakini nantinya mereka akan dapat saling melengkapi kekurangan saat terpilih dan memimpin Sumsel.</span></p>
<p><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Ishak Mekki berjanji melanjutkan pembangunan yang selama ini telah dijalankan bersama Gubernur Sumsel Alex Noerdin. Program pendidikan, penarikan investor ke Sumsel, dan proyek infrastruktur akan menjadi prioritas selama 2 tahun pertama jika ia terpilih memimpin Sumsel.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">"Dari jauh hari saya sudah keliling di 17 kabupaten/kota, tapi infrastruktur akan menjadi prioritas utama kita selain pendidikan dan menarik investor untuk masuk ke Sumsel dalam waktu 2 tahun pertama," sambungnya.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">"Pak Yudha ini anak muda, banyak prestasi dan tentu akan melengkapi kekurangan saya sebagai pemimpin dalam memajukan Sumsel. Saya mengistilahkan ini sebagai bentuk kolaborasi pemimpin zaman dulu dan zaman</span><em style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">&nbsp;now</em><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">, jadi apa keinginan masyarakat akan dapat tertampung," tutupnya.</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">Dari pantauan&nbsp;</span><strong style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">detikcom</strong><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">, terlihat ratusan relawan dan&nbsp;</span><em style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">driver</em><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">&nbsp;ojek&nbsp;</span><em style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">online</em><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">&nbsp;ikut mengantar Ishak Mekki-Yudha, yang menggunakan mobil</span><em style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">&nbsp;offroad</em><span style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">&nbsp;iring-iringan untuk mendaftar ke KPU Sumsel.&nbsp;</span><br style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;" /><strong style="color: #2d2d2d; font-family: Helvetica, Arial; font-size: 16px;">(tor/tor) <a href="https://news.detik.com/berita/3805879/deklarasi-maju-pilgub-sumsel-ishak-mekki-yudha-langsung-ke-kpu" target="_blank" rel="noopener noreferrer">https://news.detik.com/</a></strong></p>',
                'image' => 'posts/January2018/69ce5781-d38e-4f56-8b00-a93ae39a21db.jpeg',
            'slug' => 'deklarasi-maju-pilgub-sumsel-ishak-mekki-yudha-pratomo-mahyuddin-(tn-5)-langsung-ke-kpu',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-10 08:23:27',
                'updated_at' => '2018-01-10 08:23:27',
                'image_url' => NULL,
            ),
            84 => 
            array (
                'id' => 99,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Latihan Kemasyarakatan Peduli Lingkungan (LKPL) angkatan 26 SMA Taruna Nusantara di Desa Menayu, Muntilan',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p><iframe src="https://www.youtube.com/embed/4MFw4vf3VJ4?rel=0" width="560" height="315" frameborder="0" allowfullscreen=""></iframe></p>',
                'image' => NULL,
            'slug' => 'latihan-kemasyarakatan-peduli-lingkungan-(lkpl)-angkatan-26-sma-taruna-nusantara-di-desa-menayu-muntilan',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-10 12:08:54',
                'updated_at' => '2018-01-10 12:08:54',
                'image_url' => NULL,
            ),
            85 => 
            array (
                'id' => 100,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Erlangga Ardianza Wibowo, ST (TN 16) - Sekretaris PORSEROSI (Persatuan Olahraga Sepatu Roda Seluruh Indonesia) KONI Jawa Tengah',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p style="margin: 0px 0px 6px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">Puji syukur kehadirat Tuhan Yang Maha Kuasa.<br />Tambah pengalaman, tambah tanggungjawab dan tambah kesempatan untuk belajar lebih lagi.</p>
<p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">Senantiasa mohon doa dan dukungan dari semua pihak, untuk terus memegang janji alumni "bocah pirikan" dengan tetap memberikan karya terbaik bagi Masyarakat, Bangsa, Negara dan Dunia sesuai "passion" di Bidang Keolahragaaan<br />God Bless.</p>
<div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">
<p style="margin: 0px 0px 6px; font-family: inherit;">*Acara Pengukuhan &amp; Pelantikan Pengurus KONI Jawa Tengah 2017-2021, Foto bersama Pengurus dengan Pimpinan Pemerintah Jawa Tengah dan dapat bersalaman langsung dengan Bapak Wakil Gubernur Jawa Tengah Drs. H. Heru Sudjatmoko, M.Si</p>
<p style="margin: 6px 0px; font-family: inherit;">Info Credit :&nbsp;<a class="profileLink" style="color: #365899; text-decoration-line: none; font-family: inherit;" href="https://www.facebook.com/erlangga.ardianzaw?fref=mentions" data-hovercard="/ajax/hovercard/user.php?id=1139720220&amp;extragetparams=%7B%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">Erlangga Ardianza W</a>ibowo, ST (TN 16) - Sekretaris PORSEROSI (Persatuan Olahraga Sepatu Roda Seluruh Indonesia) KONI Jawa Tengah</p>
<p style="margin: 6px 0px; font-family: inherit;">\'Semoga Tuhan memberkati sumpah dan janjimu\'</p>
<p style="margin: 6px 0px; font-family: inherit;">Baca juga :&nbsp;<a class="profileLink" style="color: #365899; text-decoration-line: none; font-family: inherit;" href="https://www.facebook.com/notes/sma-taruna-nusantara/erlangga-ardianza-tn-16-peraih-medali-emas-sea-games-xxvi/861564717220240/?fref=mentions">https://www.facebook.com/notes/sma-taruna-nusantara/erlangga-ardianza-tn-16-peraih-medali-emas-sea-games-xxvi/861564717220240/</a></p>
</div>',
                'image' => 'posts/January2018/26233090_10215145623170830_7312561878371634143_o.jpg',
            'slug' => 'erlangga-ardianza-wibowo-st-(tn-16)-sekretaris-porserosi-(persatuan-olahraga-sepatu-roda-seluruh-indonesia)-koni-jawa-tengah',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-22 10:57:57',
                'updated_at' => '2018-01-22 10:57:57',
                'image_url' => NULL,
            ),
            86 => 
            array (
                'id' => 101,
                'author_id' => 2,
                'category_id' => 1,
            'title' => 'Buku tulisan Alumni SMATN, Khasan Ashari (TN 1) yang berjudul KAMUS HUBUNGAN INTERNASIONAL.',
                'seo_title' => NULL,
                'excerpt' => NULL,
            'body' => '<p>Satu lagi buku tulisan Alumni SMATN, Khasan Ashari​ (TN 1) yang berjudul KAMUS HUBUNGAN INTERNASIONAL.</p>
<p>Silahkan yang berminat bisa langsung inbox FB beliaunya</p>
<p>Berikut review nya :</p>
<p>" Baik sebagai disiplin ilmu maupun dalam konteks kehidupan sehari-hari, hubungan internasional bersifat kompleks dan dinamis. Kompleks karena melibatkan banyak aktor dengan beragam kepentingan, dinamis karena yang menjadi pokok bahasan terus berkembang. Sifat hubungan internasional yang kompleks ini melahirkan beragam konsep dan terminologi yang sebagian hanya digunakan dalam diskusi kelas, dan sebagian besar justru muncul di pemberitaan media dan perbincangan sehari-hari.</p>
<p>Pemahaman mengenai konsep dan terminilogi tersebut berperan penting dalam membantu kita memahami fenomena hubungan internasional. Oleh karena itu, Kamus Hubungan Internasional ini disusun berdasarkan pemikiran tersebut dan keprihatinan akan masih sedikitnya buku penunjang studi hubungan internasional.</p>
<p>Buku ini memuat lebih dari 1.350 konsep dan terminologi di bidang hubungan internasional, diplomasi, dan politik luar negeri. Penulis menjadikan literatur hubungan internasional sebagai rujukan dipadukan dengan pengalaman dalam dunia diplomasi selama kurang lebih sepuluh tahun. Penjelasan setiap entri disusun dengan bahasa sederhana dan mudah dipahami "</p>
<p>Paperback, 488 halaman</p>
<p>Terbitan Februari 2015 oleh Nuansa Cendekia</p>
<p>ISBN 6023500005 (ISBN13: 9786023500000)</p>
<p>Edisi Bahasa Indonesian</p>',
                'image' => 'posts/January2018/26232160_2009745662574810_3881120880889182881_o.jpg',
            'slug' => 'buku-tulisan-alumni-smatn-khasan-ashari-(tn-1)-yang-berjudul-kamus-hubungan-internasional-',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-22 10:59:04',
                'updated_at' => '2018-01-22 10:59:04',
                'image_url' => NULL,
            ),
            87 => 
            array (
                'id' => 102,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Alumni beraudiensi dengan Panglima TNI',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif;">Tak hanya ketika menjadi siswa; para alumni terus bersinergi dengan berbagai komponen bangsa, termasuk TNI, demi kemajuan negeri</span></p>',
                'image' => 'posts/January2018/26172274_2050000408617454_1614342392513487662_o.jpg',
                'slug' => 'alumni-beraudiensi-dengan-panglima-tni',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-22 11:00:36',
                'updated_at' => '2018-01-22 11:00:36',
                'image_url' => NULL,
            ),
            88 => 
            array (
                'id' => 103,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Sertijab OSIS dan MPK',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p style="margin: 0px 0px 6px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">Pergantian Pengurus OSIS adalah hal yang dilakukan oleh setiap sekolah. Di SMA TN pemilihan OSIS memiliki tradisi tersendiri, Dalam konsep yang hendak dikembangkan dan dioptimalkan adalah kebulatan aspek akademis, kepribadian, dan kesamaptaan jasmani. Ketiga aspek itulah yang merupakan modal dasar pembentukan kader pemimpin bangsa Indonesia di masa yang akan datang. Kepemimpinan adalah sebuah ilmu yang dapat dipelajari, dan harus dikuasai oleh seluruh Siswa SMA Taruna Nusanta<span class="text_exposed_show" style="display: inline; font-family: inherit;">ra. Melalui pendidikan, pelatihan dan pengasuhan sebagai sarana pembelajaran akan terwujud kepemimpinan yang berkualitas, unggul, dan kompeten. Kegiatan ini dapat teraktualisasi dalam OSIS sebagai wadah kehidupan siswa berorganisasi.</span></p>
<div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">
<p style="margin: 0px 0px 6px; font-family: inherit;">Fungsi OSIS dan MPK sebagai organisasi siswa diharapkan mampu mengaktualisasi penerapan ilmu kepemimpinan dan melatih tanggung jawab dalam melaksanakan tugas dan fungsinya sebagai siswa SMA Taruna Nusantara. Untuk itu diperlukan pucuk-pucuk pimpinan dari OSIS dan MPK yang diharapkan memiliki integritas dan kualitas serta mampu merangkul rekan-rekannya untuk mewujudkan cita-cita pendirian SMA Taruna Nusantara.</p>
<p style="margin: 6px 0px; font-family: inherit;">Untuk menjadi pengurus OSIS SMA TN harus melalui tes dan wawancara yang biasa dilakukan oleh pengurus OSIS lama, Pamong Pembina OSIS dan Pengurus Sekolah. Tahapan untuk mengikuti sebagai calon pengurus OSIS: seleksi administrasi, tes wawancara abang dan kakak, tes wawancara pamong, kampanye monologis, kampanye dialogis, wawancara Pengurus Sekolah.</p>
<p style="margin: 6px 0px; font-family: inherit;">Dengan kegiatan ini diharapkan menyukseskan misi SMA Taruna Nusantara dalam pembentukan kader pemimpin bangsa, meningkatkan semangat dan tanggung jawab tugas dan prestasi, mengaplikasikan ilmu dan bakat kepemimpinan siswa, menyiapkan calon pengurus OSIS/PK periode 2017/2018 berdasarkan kemampuan minat dan bakat</p>
</div>',
                'image' => 'posts/January2018/26850503_1689921317717905_5156105112055060157_o.jpg',
                'slug' => 'sertijab-osis-dan-mpk',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-30 12:52:22',
                'updated_at' => '2018-01-30 12:52:22',
                'image_url' => NULL,
            ),
            89 => 
            array (
                'id' => 104,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Seleksi CATAR AD',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<p>Seleksi Calon Taruna Angkatan Darat</p>',
                'image' => 'posts/January2018/27023433_1689949401048430_2826506855468303055_o.jpg',
                'slug' => 'seleksi-catar-ad',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-30 12:53:21',
                'updated_at' => '2018-01-30 12:53:21',
                'image_url' => NULL,
            ),
            90 => 
            array (
                'id' => 105,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Daftar Nama Panitia Daerah & Alamat Panda/Sub Panda PENSISRU SMA TN 2018',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">NO PANDA / SUBPANDA - ALAMAT PANDA</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">1 AJENDAM ISKANDAR MUDA - JL. NYA ADAM KAMIL II AD B1 NEUSU BANDA ACEH 23000, NAD</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">2 AJENDAM I / BUKIT BARISAN - JL. BINJAI KM.7,4 MEDAN 20123, SUMATERA UTARA</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">3 AJENREM 023 / KAWAL SAMUDRA - JL. DATUK ITEM NO. 1 SIBOLGA, SUMATERA UTARA</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">4 AJENREM 031 / WIRABIMA - JL. PERWIRA NO.1 PEKANBARU, RIAU</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">5 AJENREM 032 / WIRAPRAJA - JL. SAMUDRA NO. 1 PADANG 25117, SUMATERA BARAT</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">6 AJENREM 033 / WIRA PRATAMA - SENGGARANG SEI TIMUN TANJUNG PINANG, KEPULAUAN RIAU</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">7 AJENDAM II / SRIWIJAYA - JL. URIP SUMOHARJO SEKOJO PALEMBANG 30118, SUMATERA SELATAN</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">8 AJENREM 041 / GARUDA EMAS - JL. PEMBANGUNAN NO.3, PADANG HARAPAN BENGKULU 38225</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">9 AJENREM 042 / GARUDA PUTIH - JL. Dr AK GANI NO. 3 JAMBI 36113</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">10 AJENREM 043 / GARUDA HITAM - JL. SUKARNO HATTA BYPASS, SUKARAME I BANDAR LAMPUNG 35242, LAMPUNG</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">11 KODIM 045 / GARUDA JAYA - BANGKA TENGAH BANGKA BELITUNG</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">12 AJENDAM JAYA - JL. MAYJEN SUTOYO NO.5 JAKARTA TIMUR 13640</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">13 AJENDAM III / SILIWANGI - JL. BOSCHA NO. 4 BANDUNG, JAWA BARAT</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">14 AJENREM 064 / MULANA YUSUF - JL. MAULANA YUSUF NO. 9 SERANG 42111, BANTEN</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">15 AJENDAM IV / DIPONEGORO - JL. PERINTIS KEMERDEKAAN WATUGONG SEMARANG 50269, JAWA TENGAH</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">16 AJENREM 072 / PAMUNGKAS - JL. DEMAK IJO, RING ROAD BARAT YOGYAKARTA</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">17 AJENDAM V / BRAWIJAYA - JL. BELAKANG RSU SYAIFUL ANWAR NO.1 MALANG 65111, JAWA TIMUR</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">18 AJENDAM VI / MULAWARMAN - JL. JENDRAL SUDIRMAN NO.10 BALIKPAPAN, KALIMANTAN TIMUR</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">19 AJENREM 101 / ANTASARI - JL. TENDEAN NO. 60 BANJARMASIN 70231, KALIMANTAN SELATAN</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">20 AJENDAM XII/TANJUNGPURA - JL. ADI SUCIPTO KM 6, SUNGAI RAYA PONTIANAK 78124, KALIMANTAN BARAT</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">21 AJENREM 102 / PANJUNG - JL. ISKANDAR PALANGKARAYA 73111, KALIMANTAN TENGAH</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">22 AJENDAM IV / HASANNUDIN - JL. URIP SUMIHARJO NO.542, KM 7 MAKASSAR 90245, SULAWESI SELATAN</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">23 AJENREM 131 / SANTIAGO - JL. A YANI NO. 19 SARIO MANADO 95114, SULAWESI UTARA</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">24 AJENREM 132 / TADULAKO - JL. PRAMUKA NO.44 PALU 94111, SULAWESI TENGAH</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">25 AJENREM 143 / HALO OLEO - JL. Drs ABD SILANDAI NO.41 KENDARI 93111, SULAWESI TENGGARA</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">26 AJENDAM XVII / CENDRAWASIH - JL. GURABESI UJUNG JAYAPURA 99111, PAPUA</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">27 AJENREM 171/PRAJA VIRA TAMA - JL. PRAMUKA NO.1 SORONG, PAPUA BARAT</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">28 AJENREM 173/PRAJA SIRAPRAJA - JL. MOJOPAHIT NO.9 CENDRAWASIH BIAK, PAPUA</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">29 AJENREM 174 / ATW - JL. RAJAMANDALA MERAUKE, PAPUA</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">30 AJENDAM XVI / PATTIMURA - JL. AJEN NO.1 AMBON, MALUKU</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">31 AJENDAM IX / UDAYANA - JL. PANGLIMA BESAR SUDIRMAN DENPASAR 80114, BALI</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">32 AJENREM 161 / WIRASAKTI - JL. WJ. LALAMENTIK 3 KUPANG 85111, NUSA TENGGARA TIMUR</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">33 AJENREM 162 / WIRA BHAKTI - JL. PEJANGGIK NO.23 MATARAM 83121, NUSA TENGGARA BARAT</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">34 AJENREM 152/BABULAH - JL. AM. KOMARUDDIN NO.1 SEIKO KOTA TERNATE, MALUKU UTARA</div>
<div class="_2cuy _3dgx _2vxa" style="direction: ltr; white-space: pre-wrap; box-sizing: border-box; margin: 0px auto 28px; width: 700px; word-wrap: break-word; font-family: Georgia, serif; color: #1d2129; font-size: 17px;">35 KODIM 1304 - JL JEND. A YANI, GORONTALO</div>',
                'image' => NULL,
                'slug' => 'daftar-nama-panitia-daerah-and-alamat-panda-sub-panda-pensisru-sma-tn-2018',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-30 12:54:46',
                'updated_at' => '2018-01-30 12:54:46',
                'image_url' => NULL,
            ),
            91 => 
            array (
                'id' => 106,
                'author_id' => 2,
                'category_id' => 1,
                'title' => 'Jadwal Rencana PENSISRU 2018',
                'seo_title' => NULL,
                'excerpt' => NULL,
                'body' => '<h1 class="entry-title"><a title="" href="http://taruna-nusantara-mgl.sch.id/2018/01/30/jadwal-rencana-pensisru-2018/" rel="bookmark">Jadwal Rencana PENSISRU 2018</a></h1>',
                'image' => 'posts/January2018/rencana jadwal pensisru 2018 siswa.jpg',
                'slug' => 'jadwal-rencana-pensisru-2018',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2018-01-30 14:05:06',
                'updated_at' => '2018-01-30 14:05:06',
                'image_url' => NULL,
            ),
        ));
        
        
    }
}