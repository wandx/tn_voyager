<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        DB::table('pages')->truncate();
        DB::table('pages')->insert([
            [
                'title' => 'Profil',
                'slug' => 'profile',
                'body' => $faker->paragraphs(10,true)
            ],
            [
                'title' => 'Visi & Misi',
                'slug' => 'visi-misi',
                'body' => $faker->paragraphs(10,true)
            ],
            [
                'title' => 'Kontak Kami',
                'slug' => 'kontak-kami',
                'body' => $faker->paragraphs(10,true)
            ]
        ]);
    }
}
