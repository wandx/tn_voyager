<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'voluptatem expedita',
                'slug' => 'voluptatem-expedita',
                'created_at' => '2017-09-26 20:28:08',
                'updated_at' => '2017-09-26 20:28:08',
            ),
            1 => 
            array (
                'id' => 2,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'esse ut',
                'slug' => 'esse-ut',
                'created_at' => '2017-09-26 20:28:08',
                'updated_at' => '2017-09-26 20:28:08',
            ),
            2 => 
            array (
                'id' => 3,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'dicta ut',
                'slug' => 'dicta-ut',
                'created_at' => '2017-09-26 20:28:08',
                'updated_at' => '2017-09-26 20:28:08',
            ),
            3 => 
            array (
                'id' => 4,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'corporis aut',
                'slug' => 'corporis-aut',
                'created_at' => '2017-09-26 20:28:08',
                'updated_at' => '2017-09-26 20:28:08',
            ),
            4 => 
            array (
                'id' => 5,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'quasi quaerat',
                'slug' => 'quasi-quaerat',
                'created_at' => '2017-09-26 20:28:09',
                'updated_at' => '2017-09-26 20:28:09',
            ),
            5 => 
            array (
                'id' => 6,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'aut laudantium',
                'slug' => 'aut-laudantium',
                'created_at' => '2017-09-26 20:28:09',
                'updated_at' => '2017-09-26 20:28:09',
            ),
            6 => 
            array (
                'id' => 7,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'mollitia est',
                'slug' => 'mollitia-est',
                'created_at' => '2017-09-26 20:28:09',
                'updated_at' => '2017-09-26 20:28:09',
            ),
            7 => 
            array (
                'id' => 8,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'omnis cumque',
                'slug' => 'omnis-cumque',
                'created_at' => '2017-09-26 20:28:09',
                'updated_at' => '2017-09-26 20:28:09',
            ),
            8 => 
            array (
                'id' => 9,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'repellendus voluptas',
                'slug' => 'repellendus-voluptas',
                'created_at' => '2017-09-26 20:28:09',
                'updated_at' => '2017-09-26 20:28:09',
            ),
            9 => 
            array (
                'id' => 10,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'vitae quaerat',
                'slug' => 'vitae-quaerat',
                'created_at' => '2017-09-26 20:28:09',
                'updated_at' => '2017-09-26 20:28:09',
            ),
        ));
        
        
    }
}