<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pages')->delete();
        
        \DB::table('pages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Profile',
                'slug' => 'profile',
            'body' => '<p>LPTTN sebagai unit pengelola SMA TN berkedudukan di Jakarta. Kewajiban dan kewenangan LPTTN menyangkut managemen perencanaan, pelaksanaan, dan pengawasan/pengendalian kegiatan operasional SMA TN. Dalam hal ini LPTTN memiliki kewenangan dalam&nbsp; managemen personil atau sumber daya manusia (SDM), keuangan, dan material.</p>
<p>Dalam bidang SDM LPTTN menyelenggarakan rekruitmen siswa baru, rekruitmen dan pengelolaan/pembinaan personil (pengurus sekolah, pamong/karyawan sekolah), serta menetapkan keputusan-keputusan mendasar mengenai siswa dan pamong SMA TN, seperti penetapan sanksi berat bagi pelanggaran siswa, pengangkatan dan pemberhentian ikatan kerja personil, kenaikan pangkat dan penghasilan, dll.</p>
<p style="text-align: left;">Dalam hal managemen keuangan LPTTN merumuskan rencana anggaran belanja sekolah berdasarkan pengajuan SMA TN dengan pertimbangan Komite Sekolah dan YKPP, merumuskan besaran iuran sekolah dan uang pangkal, mencari dan mengelola sumber-sumber keuangan yang sah lainnya untuk mencapai visi, misi, dan tujuan penyelenggaraan pendidikan di SMA TN.</p>
<p>Dalam bidang material LPTTN memiliki kewenangan dalam merencanakan, mengadakan, memelihara, dan meningkatkan perangkat keras berupa sarana prasarana dan fasilitas pendidikan.<br /> Di samping itu, LPTTN juga memiliki kewenangan dalam konsep dasar dan filosofi pendidikan, strategi pendidikan, sistem pendidikan, kurikulum, serta perangkat lunak pembinaan kesiswaan dan ketenagaan (berupa Tri Prasetya Siswa, Kode Kehormatan Siswa, PUDD, Perdupsis, Pedoman Penyelenggaraan Administrasi, dll).</p>
<p style="text-align: center;"><iframe title="profil SMA TN" src="https://www.youtube.com/embed/esBcjeqs_rQ?wmode=opaque&amp;theme=dark" width="1055" height="593" frameborder="0" allowfullscreen=""></iframe></p>',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Visi & Misi',
                'slug' => 'visi-misi',
                'body' => '<p align="center"><strong>- V I S I -<br /> </strong></p>
<ul>
<li>Sekolah yang membentuk Kader pemimpin bangsa berkualitas dan berkarakter yang berwawasan Kebangsaan, Kejuangan, Kebudayaan, dengan bercirikan kenusantaraan serta memiliki daya saing Nasional maupun Internasional</li>
</ul>
<p align="center"><strong><br /> </strong></p>
<p align="center"><strong>- M I S I -<br /> </strong></p>
<ol>
<li>Menyiapkan kader pemimpin bangsa yang beriman dan bertaqwa kepada Tuhan YME</li>
<li>Menyiapkan kader pemimpin bangsa yang berkualitas, berkarakter dan berbudaya</li>
<li>Menyiapkan lulusan yang memiliki kesetiaan terhadap Bangsa dan Negara Kesatuan Republik Indonesia yang berdasarkan Pancasila dan UUD 1945</li>
<li>Menyiapkan lulusan yang memiliki potensi kepemimpinan yang bewawasan Kebangsaan, Kejuangan, Kebudayaan, bercirikan kenusantaraan</li>
<li>Menyiapkan lulusan yang memiliki keunggulan komparatif, kompetitif, dan distingtif dalam aspek Akademik, Kepribadian dan Kesamaptaan Jasmani serta kamampuan IPTEK sehingga mempunyai daya saing yang tinggi di tingkat Nasional dan Internasional</li>
</ol>',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Kontak Kami',
                'slug' => 'kontak-kami',
                'body' => 'Quia nisi repellat voluptas ab velit illum. Minus maxime animi est ut. Aperiam sit odio quaerat aspernatur sit magni. Aperiam voluptates harum amet soluta sed nobis.

Eum recusandae eveniet et id. Reiciendis ipsa esse et minima magnam. Quia aliquam nam velit dignissimos est consequatur.

Perferendis dolor voluptates illo. Nam perspiciatis quis aut sint. Quia est voluptatem sit optio. Autem consequuntur asperiores ut ratione tenetur sunt modi sapiente.

Maiores architecto accusantium necessitatibus explicabo est debitis. Doloribus aspernatur quae distinctio molestiae fuga eius dicta. Ut sed asperiores architecto enim sunt.

Et consectetur ut ut dicta ipsa sed. Laudantium nostrum iste ipsa consequatur atque quia. Sit ut ut corporis illum iusto. Rerum similique sit voluptatibus eveniet.

Suscipit aut laudantium exercitationem. Impedit sed maxime aut voluptas illo quas. Non ut doloribus quo qui sed voluptatibus non voluptatem. Iure quia accusamus sit amet.

Tenetur blanditiis voluptas necessitatibus totam. Ab incidunt labore qui. Ut tenetur repellendus ipsam pariatur et quae excepturi laboriosam. Voluptas eveniet provident mollitia mollitia.

Est enim magnam dolore nesciunt odio similique ut. Aspernatur voluptatem voluptatem dignissimos pariatur harum quibusdam. Quo culpa labore consequatur at repellendus nam ipsa quisquam. Dolorum ea numquam sed ratione fugit voluptatem cum.

Dignissimos rerum ut repellendus ullam rerum architecto. Autem non magnam consectetur inventore esse. Veritatis harum quas adipisci neque ea laudantium qui.

Aut et aut sed deleniti. Voluptas natus quis sequi qui tempore reiciendis. Numquam consequatur numquam itaque eaque at. Facere autem id excepturi neque dolores.',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Tempat Pendaftaran Penerimaan Siswa SMA Taruna Nusantara',
                'slug' => 'tempat-pendaftaran-penerimaan-siswa-sma-taruna-nusantara',
                'body' => '<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;"><strong>Sumatera :</strong></p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">1. Ajendam Iskandar Muda, Jl. Nyak Adam Kamil II Neusu - Banda Aceh<br />2. Ajendam I/Bukit Barisan, Jl. Binjai KM. 7,4 - Medan<br />3. Ajenrem 023/Kawal Samudera, Jl. Datuk Itam&nbsp; No. 1 - Sibolga&nbsp;&nbsp;<br />4. Ajenrem 031/Wirabima, Jl. Perwira - Pekanbaru<br />5. Ajenrem 032/Wirabraja, Jl. Samudera No. 1 - Padang<br />6. Ajenrem 033/Wira Pratama, Jl. Sei Timun Batu 4 - Sanggrang Tj. Pinang<br />7. Ajendam II/Sriwijaya, Jl. Urip Sumoharjo, Sekojo - Palembang<br />8. Ajenrem 041/Garuda Emas, Jl. Pembangunan No. 3 - Padang Harapan Bengkulu<br />9. Ajenrem 042/Garuda Putih, Jl. Dr.AK. Gani No. 3 - Jambi<br />10. Ajenrem 043/Garuda Hitam, Jl. Ir. Sukarno Hatta - Bandar Lampung<br />11. Kodim 0413/Bangka, Jl. Solikin GP - Pangkal Pinang</p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">&nbsp;</p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;"><strong>Jawa &ndash; Madura :</strong></p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">12. Ajendam Jaya Jl. Mayjen Sutoyo No.5 - Jakarta&nbsp;&nbsp;<br />13. Ajendam III/Siliwangi Jl.Boscha - Bandung<br />14. Ajenrem 064/Maulana Yusuf Jl. Maulana Yusuf - Serang<br />15. Ajendam IV/Diponegoro Jl. P. Kemerdekaan Watugong - Semarang<br />16. Ajenrem 072/Pamungkas, Jl. Ring Road Barat - Demak Ijo Yogyakarta<br />17. Ajendam V/Brawijaya Jl. Belakang Rumkit Saiful Anwar - Malang&nbsp;&nbsp;</p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">&nbsp;</p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;"><strong>Kalimantan :</strong></p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">18. Ajendam VI/ Mulawarman, Jl. Jenderal Sudirman 10 - Balikpapan<br />19. Ajenrem 101/Antasari, Jl. Tendean No. 60 - Banjarmasin<br />20. Ajendam XII/Tanjungpura, Jl. Adi Sucipto KM. 6 - Pontianak<br />21. Ajenrem 102/Panjung, Jl. Imam Bonjol No. 5 - Palangkaraya</p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">&nbsp;</p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;"><strong>Sulawesi :</strong></p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">22. Ajendam VII/Wirabuana, Jl. Garuda No. 3 - Makasar<br />23. Ajenrem 131/Santiago, Jl. A. Yani No. 19 - Sario Manado<br />24. Ajenrem 132/Tadulako, Jl. Jenderal Sudirman 25 - Palu<br />25. Ajenrem 143/Halu Oleo, Jl. Drs. Abdul Silandai 41 - Kendari</p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">26. Kodim 1304/Gorontalo, Jl.Jenderal A.Yani - Gorontalo</p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">&nbsp;</p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;"><strong>Papua :</strong></p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">27. Ajendam XVII/Cendrawasih Jl. Diponegoro Ujung - Jayapura&nbsp;&nbsp;<br />28. Ajenrem 171/Praja Wiratama, Jl. Pramuka No. 1 - Sorong<br />29. Ajenrem 173/Praka Wirabraja, Jl. Mojopahit No. 9 - Trikora I Biak<br />30. Ajenrem 174/Anim Tiwaninggap, Jl. Rajamandala - Merauke</p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">&nbsp;</p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;"><strong>Maluku &ndash; Bali &ndash; Nusa Tenggara :</strong></p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">31. Ajendam XVI/Pattimura, Jl. . Ajen No. 1 - Ambon</p>
<p style="margin: 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129;">32. Ajendam IX/Udayana, Jl. Pang. Besar Sudirman - Denpasar<br />33. Ajenrem 161/Wirasakti, Jl. Tompelo No. 2 - Kupang<br />34. Ajenrem 162/Wira Bhakti, Jl. Malomba Amperan - Mataram<br />35. Ajenrem 152/Babullah, Jl. AM. Komaruddin No.1 - Ternate</p>',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Persyaratan Pendaftaran Calon Siswa Baru TP. 2017/2018',
                'slug' => 'persyaratan-pendaftaran-calon-siswa-baru-tp-20172018',
                'body' => '<figure class="_2cuy _4nuy _2vxa" style="margin: 24px 0px; padding: 0px; white-space: pre-wrap; direction: ltr; line-height: 26px; color: #000000; font-family: Georgia, serif; font-size: 16px;">
<div class="_4a1o" style="margin: 32px 0px; text-align: center;"><img class="_397e img" style="border: 0px; display: inline-block; vertical-align: top; max-width: 100%; max-height: 300px;" src="https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/fr/cp0/e15/q65/16386978_1337289812981059_5838089396527444115_n.jpg?efg=eyJpIjoidCJ9&amp;oh=77b32cb8d896d33b271c06606e2f1f6d&amp;oe=5AEAE8FF" /></div>
</figure>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">Lembaga Perguruan Taman Taruna Nusantara (LPTTN), selaku Pengelola SMA Taruna Nusantara, menerima Pendaftaran Calon Siswa (Casis) Baru Kelas X TP. 2017/2018 dengan ketentuan, sbb :</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">&nbsp;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">1. Persyaratan Umum.</span></div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">a. Warga Negara Indonesia (WNI), laki-laki dan perempuan.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">b. Lulus UN SLTP/setingkat TP. 2016/2017 atau TP. 2015/2016.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">c. Berijazah SLTP/setingkat.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">d. Tidak pernah tinggal kelas selama di SLTP/setingkat.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">e. Nilai rapor semester I s.d. V untuk mata pelajaran Bahasa Indonesia, Bahasa Inggris, Matematika dan IPA masing-masing <span class="_4yxo" style="font-weight: bold;">minimal 75</span> dengan fotokopi rapor SLTP/setingkat semester I s.d. V yang dilegalisir.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">f. Usia maksimal 17 tahun saat masuk pendidikan (Juli 2017).</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">g. Sehat jasmani dan rohani, tinggi dan berat badan proporsional.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">h. Berkelakuan baik dengan surat keterangan dari sekolah.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">i. Persetujuan orang tua/wali, bersedia tinggal di asrama, sanggup mematuhi peraturan sekolah dan memenuhi persyaratan KKM yang ditetapkan SMA Taruna Nusantara.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">j. Daftar konversi nilai dan ijazah yang dilegalisasi oleh Kembuddikdasmen untuk sekolah setingkat SLTP luar negeri atau Dinas Pendidikan setempat bagi Casis yang berasal dari SLTP berlatar belakang International School di dalam negeri.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">k. Membayar uang pangkal, iuran sekolah, iuran komite sekolah dan sumbangan sukarela</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">&nbsp;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">2. Waktu dan Tempat Pendaftaran.</span></div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">a. Waktu : <span class="_4yxo" style="font-weight: bold;">Minggu ke-2 Februari s.d. 31 Maret 2017</span></div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">b. Tempat : <span class="_4yxo" style="font-weight: bold;">Ajen Kodam, Ajen Korem dan Kodim setempat.</span></div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">&nbsp;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">3. Biaya. </span></div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">a. Biaya pendaftaran : <span class="_4yxo" style="font-weight: bold;">Rp.200.000,-</span> ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">b. Biaya Pemeriksaan Kesehatan dan Psikologi ditentukan kemudian. Casis Beasiswa tidak dikenakan biaya Pemeriksaan Kesehatan dan Psikologi (kecuali Beasiswa Prestasi) ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">c. Seleksi Akademik dan Wawancara tidak dipungut biaya ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">d. Transportasi &amp; akomodasi selama seleksi ditanggung Casis ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">e. Biaya uang pangkal <span class="_4yxo" style="font-weight: bold;">Rp. 40.000.000,-</span>, uang Komite Sekolah <span class="_4yxo" style="font-weight: bold;">Rp. 1.000.000,-</span>, uang iuran sekolah <span class="_4yxo" style="font-weight: bold;">Rp. 3.750.000,-/bulan</span>, sumbangan sukarela, kontribusi khusus, dibayarkan setelah mendapat panggilan wawancara.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">&nbsp;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">4. Cara Pendaftaran.</span></div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">a. Kategori Iuran Sekolah (IS)</span>. Mengisi Formulir No. 01-2017 warna biru, sanggup membayar sekaligus uang pangkal, uang Komite Sekolah, uang Iuran Sekolah bulan Juli &amp; sumbangan sukarela.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">b. Kategori Kontribusi Khusus (KK)</span>. Mengisi Formulir No. 01A-2017 warna merah &amp; Formulir No. 03-2017, serta sanggup membayar sekaligus uang pangkal, uang Komite Sekolah, uang Iuran Sekolah bulan Juli &amp; uang KK min Rp. 104.500.000,-.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">c. Kategori Beasiswa (BS)</span>.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">(1) Murni</span>, syarat :</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(a) Nilai rapor SMP/setingkat semester I s.d V, mapel B. Indonesia, B. Inggris, Matematika dan IPA, masing-masing nilai <span class="_4yxo" style="font-weight: bold;">min 85</span>, Mapel lainnya masing-masing nilai <span class="_4yxo" style="font-weight: bold;">min 80. </span></div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(b) Penghasilan Ortu/wali maks <span class="_4yxo" style="font-weight: bold;">Rp. 5.000.000,-/bulan.</span></div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(c) Mengisi Formulir No. 02-2017 warna kuning, Formulir No. 04-2017 dan Formulir No. 05-2017 ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">&nbsp;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">(2) Prestasi</span>, syarat :</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(a) Nilai rapor semester I s.d. V min <span class="_4yxo" style="font-weight: bold;">rata-rata 90</span> untuk semua mapel dengan nilai terendah <span class="_4yxo" style="font-weight: bold;">min 85</span>.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(b) Penghasilan Ortu/wali tidak ditentukan.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(c) Dikenakan biaya Pemeriksaan Kesehatan dan Psikologi.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(d) Mengisi Formulir No. 02-2017 warna kuning dan Formulir No. 04-2016.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">(3) Tidak membayar</span> uang pangkal, uang komite dan iuran sekolah.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">&nbsp;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">d. Kategori Undangan</span>. Mendapat undangan dari LPTTN/SMA Taruna Nusantara, nilai rapor &amp; penghasilan orang tua tidak ditentukan, <span class="_4yxo" style="font-weight: bold;">dibebaskan dari Seleksi Akademik</span>. Terdiri dari :</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">(1) Undangan Beasiswa</span>, syarat :</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(a) Peraih medali Olimpiade Sains tkt. Internasional (IMO, IphO, IBO), medali emas/perak Olimpiade Sains Nasional (OSN), juara 1 s.d. 5 lomba Matematika, Sains dan Bahasa Inggris (MSI), dan LKIP yang diselenggarakan oleh SMA Taruna Nusantara.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(b) Dibebaskan dari biaya Pemeriksaan Kesehatan dan Psikologi.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(c) Mendaftar seperti poin 4.c.(1) ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">&nbsp;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">(2) Undangan Iuran Sekolah</span>, syarat :</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(a) Peraih medali perunggu OSN.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(b) Dikenakan biaya Pemeriksaan Kesehatan dan Psikologi.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(c) Mendaftar seperti poin 4.a.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">&nbsp;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">e. Penyerahan Formulir</span>. Menyerahkan formulir pendaftaran beserta lampirannya masing-masing rangkap tiga :</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(1) SKKB dari Sekolah ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(2) Surat Keterangan Sehat dari Dokter Instansi Pemerintah (Sipil/Militer) ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(3) Fotokopi Kartu Tanda Pelajar ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(4) Fotokopi Akte Kelahiran yang telah dilegalisasi ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(5) Fotokopi nilai rapor SLTP semester I s.d. V yang telah dilegalisasi ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(6) Pas Foto seragam sekolah, latar belakang biru ukuran 3x4 (3 buah) ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(7) Fotokopi piagam prestasi tingkat Lokal, Nasional atau Internasional bidang akademik, olahraga dan kesenian (bila ada) ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(8) Khusus pendaftar jalur beasiswa menyerahkan pula :</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(a) Daftar penghasilan orang tua/wali yang disahkan oleh instansi tempat bekerja bagi karyawan, atau oleh Kades/ Lurah bagi buruh/pedagang/petani/nelayan, dll ;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">(b) Fotokopi Kartu Keluarga &amp; KTP orang tua/wali.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">&nbsp;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">5. Seleksi. </span></div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">Seleksi dilaksanakan dalam dua tahap :</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">a. Awal.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">1) Seleksi Administrasi</span>. Meneliti data Casis pada formulir pendaftaran dihadapkan pada persyaratan penerimaan, dilaksanakan di daerah/Ajen atau pusat/LPTTN.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">2) Seleksi Akademik.</span> Materi : Bhs Indonesia, Bhs Inggris, Matematika dan IPA.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">b. Lanjutan.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">1) Pemeriksaan Psikologi dan Kesehatan.</span> Casis yang dinyatakan lulus Seleksi Akademik akan dipanggil mengikuti Pemeriksaan Psikologi dan Kesehatan, di daerah yang ditunjuk.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">2) Seleksi Wawancara.</span> Casis yang dinyatakan lulus Pemeriksaan Psikologi dan Kesehatan dipanggil mengikuti Seleksi Wawancara, di SMA Taruna Nusantara, Magelang. Bagi Casis yang dinyatakan tidak lulus Seleksi Wawancara, akan dikembalikan ke daerah asal dengan biaya transportasi pulang ditanggung lembaga. Sedangkan uang pangkal, uang komite, uang KK, uang iuran sekolah dan sumbangan sukarela akan dikembalikan 100%.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">&nbsp;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">6. Pengumuman. </span> Casis yang diterima sebagai Siswa Baru SMA Taruna Nusantara, ditetapkan dengan Surat Keputusan Kepala LPTTN dan langsung masuk pendidikan.</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">&nbsp;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">Keterangan lebih lanjut dapat diperoleh di tempat Pendaftaran Panitia Daerah/Sub Panitia Daerah masing-masing</span></div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">NB : TIDAK DIBERLAKUKAN SPONSORSHIP</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">&nbsp;</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;"><span class="_4yxo" style="font-weight: bold;">PANITIA PUSAT PENERIMAAN SISWA BARU SMA TARUNA NUSANTARA TP. 2017/2018</span></div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">LPTTN, Jl. Kwitang Raya No. 21, Jakarta Pusat 10420 ; Telp : 021-31903410 ; Fax : 021-31906707</div>
<div class="_2cuy _3dgx _2vxa" style="white-space: pre-wrap; direction: ltr; line-height: 26px; margin: 24px 0px; color: #000000; font-family: Georgia, serif; font-size: 16px;">SMA TARUNA NUSANTARA, Jl. Raya Purworejo, Km-5, Magelang 56172 ; Telp : 0293-364195 ; Fax : 0293-364047</div>',
),
5 => 
array (
'id' => 6,
'title' => 'Ilustrasi Tahapan lengkap pendaftaran dan seleksi Calon Siswa SMA Taruna Nusantara',
'slug' => 'ilustrasi-tahapan-lengkap-pendaftaran-dan-seleksi-calon-siswa-sma-taruna-nusantara',
'body' => '<p><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif;">Mau daftar SMA TN?</span><br style="color: #1d2129; font-family: Helvetica, Arial, sans-serif;" /><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif;">Simak baik-baik ilustrasi ini ya...</span><br style="color: #1d2129; font-family: Helvetica, Arial, sans-serif;" /><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif;">Semoga sukses!</span><br style="color: #1d2129; font-family: Helvetica, Arial, sans-serif;" /><br style="color: #1d2129; font-family: Helvetica, Arial, sans-serif;" /><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif;">Seleksi masuk SMA TN adalah sebuah proses yang panjang dan ketat. Ketahui dan pahami proses tersebut agar Anda / anak Anda dapat mempersiapkan diri dengan sebaik-baiknya.</span><span class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129;"><br /><br />Proses seleksi semacam ini sudah dijalankan oleh SMA TN sejak tahun 1990 dengan penuh integritas dan sikap profesional, sehingga mampu memberi input berupa siswa-siswi terbaik dari seluruh penjuru nusantara.<br /><br />Pendaftaran akan dibuka selama 2 bulan, mulai Februari s.d. Maret.<br /><br />Tes Akademik akan dilaksanakan pada hari Minggu di bulan April (1 hari selesai).<br /><br />Pemeriksaan Kesehatan akan dilaksanakan pada hari Sabtu di bulan Mei (1 hari selesai) dan keesokan harinya dilaksanakan Pemeriksaan Psikologi yang akan dilaksanakan pada hari Minggu di bulan Mei (1 hari selesai).<br /><br />Wawancara akan dilaksanakan pada bulan Juni / Juli. Pelantikan siswa baru akan dilaksanakan pada bulan Juli.</span></p>
<p><span class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129;"><img title="Ilustrasi Calon Siswa" src="http://203.130.238.189:6080/storage/pages/January2018/15440531_1293406397369401_4515938156431694154_o.jpg" alt="Ilustrasi Calon Siswa" width="1116" height="683" /></span></p>',
),
));
        
        
    }
}