<?php

use Illuminate\Database\Seeder;

class JadwalsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('jadwals')->delete();
        
        \DB::table('jadwals')->insert(array (
            0 => 
            array (
                'id' => 20,
                'kelas' => 'X',
                'hari' => 'SENIN',
                'mapel' => 'MATEMATIKA',
                'guru' => 'GURU 1',
                'waktu' => '10:00 – 12:00',
                'keterangan' => 'MENGERJAKAN BAB 1',
            ),
            1 => 
            array (
                'id' => 21,
                'kelas' => 'X',
                'hari' => 'SELASA',
                'mapel' => 'BAHASA INDO',
                'guru' => 'GURU 2',
                'waktu' => '10:00 – 12:01',
                'keterangan' => 'MENGERJAKAN BAB 2',
            ),
            2 => 
            array (
                'id' => 22,
                'kelas' => 'X',
                'hari' => 'RABU',
                'mapel' => 'BAHASA INGGRIS',
                'guru' => 'GURU 3',
                'waktu' => '10:00 – 12:02',
                'keterangan' => 'MENGERJAKAN BAB 3',
            ),
            3 => 
            array (
                'id' => 23,
                'kelas' => 'X',
                'hari' => 'KAMIS',
                'mapel' => 'FISIKA',
                'guru' => 'GURU 4',
                'waktu' => '10:00 – 12:03',
                'keterangan' => 'MENGERJAKAN BAB 4',
            ),
            4 => 
            array (
                'id' => 24,
                'kelas' => 'X',
                'hari' => 'JUMAT',
                'mapel' => 'KIMIA',
                'guru' => 'GURU 5',
                'waktu' => '10:00 – 12:04',
                'keterangan' => 'MENGERJAKAN BAB 5',
            ),
            5 => 
            array (
                'id' => 25,
                'kelas' => 'X',
                'hari' => 'SABTU',
                'mapel' => 'MATEMATIKA',
                'guru' => 'GURU 6',
                'waktu' => '10:00 – 12:05',
                'keterangan' => 'MENGERJAKAN BAB 6',
            ),
            6 => 
            array (
                'id' => 26,
                'kelas' => 'XI',
                'hari' => 'SENIN',
                'mapel' => 'BAHASA INDO',
                'guru' => 'GURU 7',
                'waktu' => '10:00 – 12:06',
                'keterangan' => 'MENGERJAKAN BAB 7',
            ),
            7 => 
            array (
                'id' => 27,
                'kelas' => 'XI',
                'hari' => 'SELASA',
                'mapel' => 'BAHASA INGGRIS',
                'guru' => 'GURU 8',
                'waktu' => '10:00 – 12:07',
                'keterangan' => 'MENGERJAKAN BAB 8',
            ),
            8 => 
            array (
                'id' => 28,
                'kelas' => 'XI',
                'hari' => 'RABU',
                'mapel' => 'FISIKA',
                'guru' => 'GURU 9',
                'waktu' => '10:00 – 12:08',
                'keterangan' => 'MENGERJAKAN BAB 9',
            ),
            9 => 
            array (
                'id' => 29,
                'kelas' => 'XI',
                'hari' => 'KAMIS',
                'mapel' => 'KIMIA',
                'guru' => 'GURU 10',
                'waktu' => '10:00 – 12:09',
                'keterangan' => 'MENGERJAKAN BAB 10',
            ),
            10 => 
            array (
                'id' => 30,
                'kelas' => 'XI',
                'hari' => 'JUMAT',
                'mapel' => 'MATEMATIKA',
                'guru' => 'GURU 11',
                'waktu' => '10:00 – 12:10',
                'keterangan' => 'MENGERJAKAN BAB 11',
            ),
            11 => 
            array (
                'id' => 31,
                'kelas' => 'XI',
                'hari' => 'SABTU',
                'mapel' => 'BAHASA INDO',
                'guru' => 'GURU 12',
                'waktu' => '10:00 – 12:11',
                'keterangan' => 'MENGERJAKAN BAB 12',
            ),
            12 => 
            array (
                'id' => 32,
                'kelas' => 'XII',
                'hari' => 'SENIN',
                'mapel' => 'BAHASA INGGRIS',
                'guru' => 'GURU 13',
                'waktu' => '10:00 – 12:12',
                'keterangan' => 'MENGERJAKAN BAB 13',
            ),
            13 => 
            array (
                'id' => 33,
                'kelas' => 'XII',
                'hari' => 'SELASA',
                'mapel' => 'FISIKA',
                'guru' => 'GURU 14',
                'waktu' => '10:00 – 12:13',
                'keterangan' => 'MENGERJAKAN BAB 14',
            ),
            14 => 
            array (
                'id' => 34,
                'kelas' => 'XII',
                'hari' => 'RABU',
                'mapel' => 'KIMIA',
                'guru' => 'GURU 15',
                'waktu' => '10:00 – 12:14',
                'keterangan' => 'MENGERJAKAN BAB 15',
            ),
            15 => 
            array (
                'id' => 35,
                'kelas' => 'XII',
                'hari' => 'KAMIS',
                'mapel' => 'MATEMATIKA',
                'guru' => 'GURU 16',
                'waktu' => '10:00 – 12:15',
                'keterangan' => 'MENGERJAKAN BAB 16',
            ),
            16 => 
            array (
                'id' => 36,
                'kelas' => 'XII',
                'hari' => 'JUMAT',
                'mapel' => 'BAHASA INDO',
                'guru' => 'GURU 17',
                'waktu' => '10:00 – 12:16',
                'keterangan' => 'MENGERJAKAN BAB 17',
            ),
            17 => 
            array (
                'id' => 37,
                'kelas' => 'XII',
                'hari' => 'SABTU',
                'mapel' => 'BAHASA INGGRIS',
                'guru' => 'GURU 18',
                'waktu' => '10:00 – 12:17',
                'keterangan' => 'MENGERJAKAN BAB 18',
            ),
        ));
        
        
    }
}