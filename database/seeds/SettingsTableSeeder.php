<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'site.title',
                'display_name' => 'Site Title',
                'value' => 'SMA Taruna Nusantara',
                'details' => '',
                'type' => 'text',
                'order' => 1,
                'group' => 'Site',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'site.description',
                'display_name' => 'Site Description',
                'value' => 'A place to create young qualified generations for the unity of the nation',
                'details' => '',
                'type' => 'text',
                'order' => 2,
                'group' => 'Site',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'site.hotline',
                'display_name' => 'Hotline',
                'value' => '0293 364195',
                'details' => '',
                'type' => 'text',
                'order' => 4,
                'group' => 'Site',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'site.address',
                'display_name' => 'Alamat',
                'value' => 'Jl. Raya Purworejo Km 5 Magelang - 56172',
                'details' => '',
                'type' => 'text',
                'order' => 5,
                'group' => 'Site',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'site.jam_kerja',
                'display_name' => 'Jam Kerja',
                'value' => 'Senin - Sabtu 07:00 - 13:45 , Jumat 11:15',
                'details' => '',
                'type' => 'text',
                'order' => 6,
                'group' => 'Site',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'site.logo',
                'display_name' => 'Site Logo',
                'value' => '',
                'details' => '',
                'type' => 'image',
                'order' => 3,
                'group' => 'Site',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'site.google_analytics_tracking_id',
                'display_name' => 'Google Analytics Tracking ID',
                'value' => '',
                'details' => '',
                'type' => 'text',
                'order' => 4,
                'group' => 'Site',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'admin.bg_image',
                'display_name' => 'Admin Background Image',
                'value' => '',
                'details' => '',
                'type' => 'image',
                'order' => 5,
                'group' => 'Admin',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'admin.title',
                'display_name' => 'Admin Title',
                'value' => 'SMA Taruna Nusantara',
                'details' => '',
                'type' => 'text',
                'order' => 1,
                'group' => 'Admin',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'admin.description',
                'display_name' => 'Admin Description',
                'value' => 'Selamat datang di halaman administrator SMA Taruna Nusantara',
                'details' => '',
                'type' => 'text',
                'order' => 2,
                'group' => 'Admin',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'admin.loader',
                'display_name' => 'Admin Loader',
                'value' => '',
                'details' => '',
                'type' => 'image',
                'order' => 3,
                'group' => 'Admin',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'admin.icon_image',
                'display_name' => 'Admin Icon Image',
                'value' => '',
                'details' => '',
                'type' => 'image',
                'order' => 4,
                'group' => 'Admin',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'admin.google_analytics_client_id',
            'display_name' => 'Google Analytics Client ID (used for admin dashboard)',
                'value' => '',
                'details' => '',
                'type' => 'text',
                'order' => 1,
                'group' => 'Admin',
            ),
            13 => 
            array (
                'id' => 15,
                'key' => 'site.facebook',
                'display_name' => 'Facebook',
                'value' => 'https://www.facebook.com/SMA.Taruna.Nusantara',
                'details' => NULL,
                'type' => 'text',
                'order' => 7,
                'group' => 'Site',
            ),
            14 => 
            array (
                'id' => 16,
                'key' => 'site.gplus',
                'display_name' => 'Google Plus',
                'value' => 'https://plus.google.com/SMA.Taruna.Nusantara',
                'details' => NULL,
                'type' => 'text',
                'order' => 8,
                'group' => 'Site',
            ),
            15 => 
            array (
                'id' => 17,
                'key' => 'site.twitter',
                'display_name' => 'Twitter',
                'value' => 'https://twitter.com/SMATN',
                'details' => NULL,
                'type' => 'text',
                'order' => 9,
                'group' => 'Site',
            ),
            16 => 
            array (
                'id' => 18,
                'key' => 'site.linkedin',
                'display_name' => 'Linkedin',
                'value' => 'https://linkedin.com/SMATN',
                'details' => NULL,
                'type' => 'text',
                'order' => 10,
                'group' => 'Site',
            ),
            17 => 
            array (
                'id' => 19,
                'key' => 'site.lat',
                'display_name' => 'Latitude',
                'value' => '-7.5162432',
                'details' => NULL,
                'type' => 'text',
                'order' => 11,
                'group' => 'Site',
            ),
            18 => 
            array (
                'id' => 20,
                'key' => 'site.lng',
                'display_name' => 'Longitude',
                'value' => '110.1280108',
                'details' => NULL,
                'type' => 'text',
                'order' => 12,
                'group' => 'Site',
            ),
            19 => 
            array (
                'id' => 21,
                'key' => 'site.zoom',
                'display_name' => 'Zoom',
                'value' => '12',
                'details' => NULL,
                'type' => 'text',
                'order' => 13,
                'group' => 'Site',
            ),
            20 => 
            array (
                'id' => 22,
                'key' => 'site.feedback_message',
                'display_name' => 'Feedback email pesan',
                'value' => 'Terimakasih telah mengirim pesan',
                'details' => NULL,
                'type' => 'text_area',
                'order' => 14,
                'group' => 'Site',
            ),
            21 => 
            array (
                'id' => 23,
                'key' => 'site.feedback_msi',
                'display_name' => 'Feedback email lomba MSI',
                'value' => 'Terimakasih telah mengikuti lomba msi, segera lakukan pembayaran sebesar 200rb ke xxx',
                'details' => NULL,
                'type' => 'text_area',
                'order' => 15,
                'group' => 'Site',
            ),
            22 => 
            array (
                'id' => 24,
                'key' => 'site.feedback_lkip',
                'display_name' => 'Feedback email lomba LKIP',
                'value' => 'Terimakasih telah mengikuti lomba LKIP, segera lakukan pembayaran sebesar 200rb ke xxx',
                'details' => NULL,
                'type' => 'text_area',
                'order' => 16,
                'group' => 'Site',
            ),
        ));
        
        
    }
}