<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Models\Post::class,function(Faker $faker){
    return [
        'title'     => $name = $faker->words(4,true),
        'author_id' => 0,
        'seo_title' => null,
        'excerpt'   => 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.',
        'body'      => $faker->paragraphs(10,true),
        'image'            => $faker->randomElement(['posts/post1.jpg','posts/post2.jpg','posts/post3.jpg','posts/post4.jpg']),
        'slug'             => \Illuminate\Support\Str::slug($name),
        'meta_description' => 'this be a meta descript',
        'meta_keywords'    => 'keyword1, keyword2, keyword3',
        'status'           => 'PUBLISHED',
        'featured'         => 0,
        'category_id' => rand(1,10)
    ];
});

$factory->define(\App\Models\Achievement::class,function(Faker $faker){
    return [
        'title' => $tit = $faker->words(4,true),
        'slug' => \Illuminate\Support\Str::slug($tit),
        'image' => $faker->randomElement(['posts/post1.jpg','posts/post2.jpg','posts/post3.jpg','posts/post4.jpg']),
        'body' => $faker->paragraphs(10,true)
    ];
});

$factory->define(\App\Models\Activity::class,function(Faker $faker){
    return [
        'title' => $tit = $faker->words(4,true),
        'slug' => \Illuminate\Support\Str::slug($tit),
        'image' => $faker->randomElement(['posts/post1.jpg','posts/post2.jpg','posts/post3.jpg','posts/post4.jpg']),
        'body' => $faker->paragraphs(10,true)
    ];
});
