@extends('client.master')

@section('content')
    <div class=" ">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white pinside60">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-6 col-sm-12">
                                <div class="error-img mb60">
                                    <img src="{{ Voyager::image(setting('site.error404'),'/client/images/error-img.png') }}" class="" alt="">
                                </div>
                                <div class="error-ctn text-center">
                                    <h2 class="msg">Maaf</h2>
                                    <h1 class="error-title mb40">Halaman tidak ditemukan</h1>
                                    <p class="mb40">Halaman yang anda cari tidak ditemukan. Coba periksa kembali url anda.</p>
                                    <a href="/" class="btn btn-default text-center">Kembali ke Home</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop