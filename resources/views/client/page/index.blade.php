@extends('client.master')

@section('content')
    <div class="">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white pinside40">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-holder">
                                            <div class="post-block mb40">
                                                <div class="bg-white">
                                                    <h1><a href="#" class="title">{{ $page->title }}</a></h1>

                                                    {!! $page->body !!}
                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop