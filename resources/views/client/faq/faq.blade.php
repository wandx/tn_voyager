@extends('client.master')

@section('content')
    <div class="">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white pinside40">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="section-title mb30">
                                    <h1>FAQ</h1>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 st-accordion col-xs-12">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    @forelse($lists as $list)
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading-{{ $loop->index }}">
                                                <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $loop->index }}" aria-expanded="{{ $loop->index == 0 ? 'true':'false' }}" aria-controls="collapse-{{ $loop->index }}">{{ $list->question }}</a> </h4>
                                            </div>
                                            <div id="collapse-{{ $loop->index }}" class="{{ $loop->index == 0 ? 'panel-collapse collapse in':'panel-collapse collapse' }}" role="tabpanel" aria-labelledby="heading-{{ $loop->index }}">
                                                <div class="panel-body">{{ $list->answer }}</div>
                                            </div>
                                        </div>
                                    @empty
                                        <div class="well text-center">Tidak ada data</div>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop