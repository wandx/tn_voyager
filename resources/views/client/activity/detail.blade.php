@extends('client.master')

@section('content')
    <div class="">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white pinside40">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-holder">
                                            <div class="post-block mb40">
                                                @if($post->image != null)
                                                    <div class="post-img mb30">
                                                        <a href="#" class="imghover"><img src="{{ asset('/storage/'.$post->image) }}" alt="{{ $post->title }}" class="img-responsive"></a>
                                                    </div>
                                                @endif
                                            </div>

                                            <div class="bg-white">
                                                <h1><a href="#" class="title">{{ $post->title }}</a></h1>
                                                <p class="meta"><span class="meta-date">{{ $post->created_at->format('M d, Y') }}</span><span class="meta-author">By<a href="#" class="meta-link"> Admin</a></span></p>
                                                {!! $post->body !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop