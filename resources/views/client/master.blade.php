<!DOCTYPE html>
<html lang="en">
    @include('client.partials.head')
    @yield('styles')
<body class="animsition">
    @include('client.partials.topbar')
    @include('client.partials.header')
    @if(!\Illuminate\Support\Facades\Request::is('/'))
    <br>
    <br>
    <br>
    @endif

    @yield('content')

    @include('client.partials.footer')
    @include('client.partials.scripts')
    @yield('scripts')
</body>
</html>