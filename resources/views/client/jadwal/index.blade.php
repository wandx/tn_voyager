@extends('client.master')

@section('content')
    <div class="">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white pinside40">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <table id="tb" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Kelas</th>
                                        <th>Hari</th>
                                        <th>Mata Pelajaran</th>
                                        <th>Jam</th>
                                        <th>Guru</th>
                                        <th>Keterangan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($lists as $list)
                                        <tr>
                                            <td>{{ $list->kelas }}</td>
                                            <td>{{ $list->hari }}</td>
                                            <td>{{ $list->mapel }}</td>
                                            <td>{{ $list->waktu }}</td>
                                            <td>{{ $list->guru }}</td>
                                            <td>{{ $list->keterangan }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@stop

@section('scripts')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(function(){
            $('#tb').DataTable();
        })
    </script>
@stop