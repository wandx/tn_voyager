@extends('client.master')
@section('styles')
    <style>
        .slider-img{
            background: -webkit-linear-gradient(top, rgba(16, 75, 149, 0.1) 0%, rgba(16, 75, 149, 0.1) 100%);
        }
    </style>
@stop
@section('content')
    @include('client.partials.slider')
    @include('client.partials.blog_preview')
    {{--@include('client.partials.testimony')--}}
@stop