@extends('client.master')

@section('content')
    <div class="">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white pinside40">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="section-title mb30">
                                    <div class="map" id="googleMap"></div>
                                    <br>
                                    <br>
                                    <h1>Kirim pesan</h1>
                                </div>
                                @if(count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                @if(session()->has('success'))
                                    <div class="alert alert-success">
                                        <ul>
                                            <li>{{ session('success') }}</li>
                                        </ul>
                                    </div>
                                @endif
                                <form action="" method="post">
                                    {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input class="form-control" type="text" name="nama" required placeholder="Nama" value="{{ old('nama') }}">
                                    </div>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="email" name="email" required placeholder="Email" value="{{ old('email') }}">
                                    </div>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="text" name="telp" required placeholder="No.Telp" value="{{ old('telp') }}">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <textarea name="pesan" id="pesan" cols="30" rows="10" required class="form-control">{{ old('pesan') }}</textarea>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary">
                                                Kirim
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        function initMap() {
            var myLatLng = {
                lat: {{ setting("site.lat") ?? "-7.5162432" }},
                lng: {{ setting("site.lng") ?? "110.1280108" }}
            };

            var map = new google.maps.Map(document.getElementById('googleMap'), {
                zoom: {{ setting("site.zoom") ?? "12" }},
                center: myLatLng,
                scrollwheel: false,

            });
            var image = '/client/images/map-pin.png';
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image,
                title: 'Hello World!'

            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?&key=AIzaSyB5H3ZpkSU5Pe4B0RzkNuNscrr6hgCm0NA&callback=initMap" async defer></script>
@stop