@extends('client.master')

@section('content')
    <div class="">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>{{ $desc->title ?? "LOMBA MSI" }}</h1>
                    {!! $desc->description ?? "Tidak ada deskripsi" !!}
                    <br>
                    <br>
                    <div class="panel">
                        <div class="panel-body">
                            @if(session()->has('success'))
                                <div class="alert alert-success">
                                    <ul>
                                        <li>{{ session('success') }}</li>
                                    </ul>
                                </div>
                            @endif
                            <form action="" method="post" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <div class="form-group {{ $errors->has('name') ? 'has-error':'' }}"}>
                                    <label for="name">Nama Lengkap <span class="glyphicon-asterisk text-danger"></span> </label>
                                    <input required class="form-control" type="text" name="name" id="name" value="{{ old('name') }}">
                                </div>

                                <div class="form-group {{ $errors->has('dob') ? 'has-error':'' }}">
                                    <label for="dob">Tanggal Lahir <span class="glyphicon-asterisk text-danger"></span></label>

                                    <div class="input-group date" id="dob-k">
                                        <input required type="text" name="dob" id="dob" class="form-control" value="{{ old('dob') }}">
                                        <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('address') ? 'has-error':'' }}">
                                    <label for="address">Alamat Peserta <span class="glyphicon-asterisk text-danger"></label>
                                    <textarea required name="address" id="address" cols="30" rows="10" class="form-control"> {{ old('address') }}</textarea>
                                </div>

                                <div class="form-group" {{ $errors->has('telp') ? 'has-error':'' }}>
                                    <label for="telp">Telpon <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required type="text" class="form-control" name="telp" id="telp" value="{{ old('telp') }}">
                                </div>

                                <div class="form-group {{ $errors->has('email') ? 'has-error':'' }}">
                                    <label for="email">Email <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input name="email" type="email" required class="form-control" id="email" value="{{ old('email') }}">
                                </div>

                                <div class="form-group {{ $errors->has('origin') ? 'has-error':'' }}">
                                    <label for="origin">Sekolah Asal <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required type="text" name="origin" class="form-control" id="origin" value="{{ old('origin') }}">
                                </div>

                                <div class="form-group" {{ $errors->has('origin-address') ? 'has-error':'' }}>
                                    <label for="origin-address">Alamat Sekolah Asal <span class="glyphicon-asterisk text-danger"></label>
                                    <textarea required type="text" cols="30" rows="10" name="origin-address" class="form-control" id="origin-address">{{ old('origin-address') }}</textarea>
                                </div>

                                <div class="form-group {{ $errors->has('kelas') ? 'has-error':'' }}">
                                    <label for="kelas">Kelas <span class="glyphicon-asterisk text-danger"></span></label>
                                    <div class="radio-group">
                                        <label for="vii" class="radio-inline">
                                            <input required type="radio" id="vii" value="vii" name="kelas"> VII
                                         </label>

                                        <label for="viii" class="radio-inline">
                                            <input required type="radio" id="viii" value="viii" name="kelas"> VIII
                                         </label>

                                        <label for="ix" class="radio-inline">
                                            <input required type="radio" id="ix" value="ix" name="kelas"> IX
                                         </label>
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('file-scan') ? 'has-error':'' }}">
                                    <label for="file-scan">Scan Kartu Pelajar/Surat Keterangan Sebagai Siswa SMP <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required type="file" name="file-scan" class="form-group" id="file-scan">
                                </div>

                                <div class="form-group {{ $errors->has('captcha') ? 'has-error':'' }}">
                                    {!! captcha_img() !!} <br>
                                    <label for="captcha">Captcha <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required type="text" name="captcha" class="form-control" id="captcha">
                                </div>



                                <div class="form-group">
                                    <div class="text-right">
                                        <button class="btn btn-primary" type="submit">Kirim</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@stop

@section('styles')
    <link rel="stylesheet" href="/client/css/bootstrap-datetimepicker.css">
@stop

@section('scripts')
    <script src="/client/js/moment-with-locales.js"></script>
    <script src="/client/js/bootstrap-datetimepicker.js"></script>
    <script>
        $(function(){
            $('#dob-k').datetimepicker({
                format: "YYYY-MM-DD"
            })
        })
    </script>
@stop