@extends('client.master')

@section('content')
    <div class="">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>{{ $desc->title ?? "LOMBA LKIP" }}</h1>
                    {!! $desc->description ?? "Tidak ada deskripsi" !!}
                    <br>
                    <br>
                    <div class="panel">
                        <div class="panel-body">
                            @if(session()->has('success'))
                                <div class="alert alert-success">
                                    <ul>
                                        <li>{{ session('success') }}</li>
                                    </ul>
                                </div>
                            @endif
                            <form action="" method="post" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <div class="form-group {{ $errors->has('file-scan') ? 'has-error':'' }}">
                                    <label for="judul-makalah">Judul Makalah <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input type="text" class="form-control" name="judul-makalah" id="judul-makalah" required value="{{ old('judul-makalah') }}">
                                </div>

                                <div class="form-group {{ $errors->has('team-name') ? 'has-error':'' }}"}>
                                    <label for="team-name">Nama Tim  <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required class="form-control" type="text" name="team-name" id="team-name" value="{{ old('team-name') }}">
                                </div>

                                <div class="form-group {{ $errors->has('name') ? 'has-error':'' }}"}>
                                    <label for="name">Nama Ketua Tim  <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required class="form-control" type="text" name="name" id="name" value="{{ old('name') }}">
                                </div>

                                <div class="form-group {{ $errors->has('dob') ? 'has-error':'' }}">
                                    <label for="dob">Tanggal Lahir <span class="glyphicon-asterisk text-danger"></span></label>
                                    <div class="input-group date" id="dob-k">
                                        <input required type="text" name="dob" id="dob" class="form-control" value="{{ old('dob') }}">
                                        <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('address') ? 'has-error':'' }}">
                                    <label for="address">Alamat Ketua Tim <span class="glyphicon-asterisk text-danger"></span></label>
                                    <textarea required name="address" id="address" cols="30" rows="10" class="form-control"> {{ old('address') }}</textarea>
                                </div>

                                <div class="form-group" {{ $errors->has('telp') ? 'has-error':'' }}>
                                    <label for="telp">Telpon Ketua Tim <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required type="text" class="form-control" name="telp" id="telp" value="{{ old('telp') }}">
                                </div>

                                <div class="form-group {{ $errors->has('email') ? 'has-error':'' }}">
                                    <label for="email">Email Ketua Tim <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input name="email" type="email" required class="form-control" id="email" value="{{ old('email') }}">
                                </div>

                                <div class="form-group {{ $errors->has('kelas') ? 'has-error':'' }}">
                                    <label for="kelas">Kelas Ketua Tim<span class="glyphicon-asterisk text-danger"></span></label>
                                    <div class="radio-group">
                                        <label for="vii" class="radio-inline">
                                            <input required type="radio" id="vii" value="vii" name="kelas"> VII
                                        </label>

                                        <label for="viii" class="radio-inline">
                                            <input required type="radio" id="viii" value="viii" name="kelas"> VIII
                                        </label>

                                        <label for="ix" class="radio-inline">
                                            <input required type="radio" id="ix" value="ix" name="kelas"> IX
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('average') ? 'has-error':'' }}"}>
                                    <label for="average">Nilai rata - rata  <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required class="form-control" type="text" name="average" id="average" value="{{ old('average') }}">
                                </div>

                                <div class="form-group {{ $errors->has('file-scan') ? 'has-error':'' }}">
                                    <label for="file-scan">Scan Kartu Pelajar/Surat Keterangan Sebagai Siswa SMP Ketua Tim <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required type="file" name="file-scan" class="form-group" id="file-scan">
                                </div>

                                <div class="form-group {{ $errors->has('name-member') ? 'has-error':'' }}"}>
                                    <label for="name-member">Nama Anggota  <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required class="form-control" type="text" name="name-member" id="name-member" value="{{ old('name-member') }}">
                                </div>

                                <div class="form-group {{ $errors->has('dob-member') ? 'has-error':'' }}">
                                    <label for="dob-member">Tanggal Lahir Anggota <span class="glyphicon-asterisk text-danger"></span></label>
                                    <div class="input-group date" id="dob-m">
                                        <input required type="text" name="dob-member" id="dob-member" class="form-control" value="{{ old('dob-member') }}">
                                        <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                                    </div>

                                </div>

                                <div class="form-group {{ $errors->has('address-member') ? 'has-error':'' }}">
                                    <label for="address-member">Alamat Anggota <span class="glyphicon-asterisk text-danger"></span></label>
                                    <textarea required name="address-member" id="address-member" cols="30" rows="10" class="form-control"> {{ old('address-member') }}</textarea>
                                </div>

                                <div class="form-group" {{ $errors->has('telp-member') ? 'has-error':'' }}>
                                    <label for="telp-member">Telpon Anggota <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required type="text" class="form-control" name="telp-member" id="telp-member" value="{{ old('telp-member') }}">
                                </div>

                                <div class="form-group {{ $errors->has('email-member') ? 'has-error':'' }}">
                                    <label for="email-member">Email Anggota <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input name="email-member" type="email-member" required class="form-control" id="email-member" value="{{ old('email-member') }}">
                                </div>

                                <div class="form-group {{ $errors->has('kelas-member') ? 'has-error':'' }}">
                                    <label for="kelas-member">Kelas Anggota<span class="glyphicon-asterisk text-danger"></span></label>
                                    <div class="radio-group">
                                        <label for="vii" class="radio-inline">
                                            <input required type="radio" id="vii" value="vii" name="kelas-member"> VII
                                        </label>

                                        <label for="viii" class="radio-inline">
                                            <input required type="radio" id="viii" value="viii" name="kelas-member"> VIII
                                        </label>

                                        <label for="ix" class="radio-inline">
                                            <input required type="radio" id="ix" value="ix" name="kelas-member"> IX
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('file-scan-member') ? 'has-error':'' }}">
                                    <label for="file-scan-member">Scan Kartu Pelajar/Surat Keterangan Sebagai Siswa SMP Anggota <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required type="file" name="file-scan-member" class="form-group" id="file-scan-member">
                                </div>

                                <div class="form-group {{ $errors->has('origin') ? 'has-error':'' }}">
                                    <label for="origin">Sekolah Asal <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required type="text" name="origin" class="form-control" id="origin" value="{{ old('origin') }}">
                                </div>

                                <div class="form-group" {{ $errors->has('origin-address') ? 'has-error':'' }}>
                                    <label for="origin-address">Alamat Sekolah Asal <span class="glyphicon-asterisk text-danger"></label>
                                    <textarea required type="text" cols="30" rows="10" name="origin-address" class="form-control" id="origin-address">{{ old('origin-address') }}</textarea>
                                </div>

                                <div class="form-group {{ $errors->has('captcha') ? 'has-error':'' }}">
                                    {!! captcha_img() !!} <br>
                                    <label for="captcha">Captcha <span class="glyphicon-asterisk text-danger"></span></label>
                                    <input required type="text" name="captcha" class="form-control" id="captcha">
                                </div>





                                <div class="form-group">
                                    <div class="text-right">
                                        <button class="btn btn-primary" type="submit">Kirim</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@stop

@section('styles')
    <link rel="stylesheet" href="/client/css/bootstrap-datetimepicker.css">
@stop

@section('scripts')
    <script src="/client/js/moment-with-locales.js"></script>
    <script src="/client/js/bootstrap-datetimepicker.js"></script>
    <script>
        $('#dob-k').datetimepicker({
            format: "YYYY-MM-DD"
        });
        $('#dob-m').datetimepicker({
            format: "YYYY-MM-DD"
        });
    </script>
@stop