<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-6">
                <!-- logo -->
                <div class="logo">
                    <a href="/"><img src="{{ Voyager::image(setting('site.logo'),'/client/images/lg.png') }}" alt="SMA Taruna Nusantara"></a>
                </div>
            </div>
            <!-- logo -->
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div id="navigation">
                    <!-- navigation start-->
                    {{--<ul>--}}
                        {{--<li><a href="/page/profile" title="Profile" class="animsition-link">Profil</a></li>--}}
                        {{--<li><a href="/page/visi-misi" title="Visi & Misi" class="animsition-link">VISI & MISI</a></li>--}}
                        {{--<li><a href="/faq" title="FAQ" class="animsition-link">FAQ</a></li>--}}
                        {{--<li><a href="/kegiatan" title="Kegiatan" class="animsition-link">Kegiatan</a></li>--}}
                        {{--<li><a href="/prestasi" title="Prestasi" class="animsition-link">Prestasi</a></li>--}}
                        {{--<li><a href="{{ route('file') }}" title="Publikasi" class="animsition-link">Publikasi</a></li>--}}
                        {{--<li><a href="{{ route('blog') }}" title="Blog" class="animsition-link">Blog</a></li>--}}
                        {{--<li><a href="/events" title="Event" class="animsition-link">Event</a></li>--}}
                        {{--<li><a href="/jadwal" title="Jadwal" class="animsition-link">Jadwal</a></li>--}}
                        {{--<li><a href="{{ route('gallery') }}" title="Gallery" class="animsition-link">Gallery</a></li>--}}
                        {{--<li class="has-sub"><a href="#">Lomba</a></li>--}}
                        {{--@if(getAdditionalMenu()['count'] > 0)--}}
                            {{--<li class="has-sub">--}}
                                {{--<span class="submenu-button"></span>--}}
                                {{--<a href="#" class="animsition-link">Lainya</a>--}}
                                {{--<ul>--}}
                                    {{--<li><a href="/contact-us" title="Kontak Kami" class="animsition-link">Kontak Kami</a></li>--}}
                                    {{--@foreach(getAdditionalMenu()['lists'] as $list)--}}
                                        {{--<li>--}}
                                            {{--<a href="{{ $list->url }}" title="{{ $list->name }}" class="animsition-link">{{ $list->name }}</a>--}}
                                        {{--</li>--}}
                                    {{--@endforeach--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                        {{--@else--}}
                            {{--<li><a href="/contact-us" title="Kontak Kami" class="animsition-link">Kontak Kami</a></li>--}}
                        {{--@endif--}}

                    {{--</ul>--}}
                    {!! menu('front','client.partials.menuapik') !!}
                </div>
                <!-- /.navigation start-->
            </div>
        </div>
    </div>
</div>