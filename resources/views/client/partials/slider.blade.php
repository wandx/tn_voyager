<div class="slider" id="slider">
    @foreach(sliderData() as $slider)
        <div class="slider-img"><img src="{{ asset('/storage/'.$slider->image) }}" alt="{{ $slider->title }}" class="">
            <div class="container">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    @if($slider->title != "" || $slider->subtitle != "")
                    <div class="slider-captions" style="background: rgba(0,0,0,.5);padding: 10px;border-radius:5px">
                        <!-- slider-captions -->
                        @if($slider->title != "")
                        <h1 class="slider-title">{{ $slider->title }}</h1>
                        @endif
                        @if($slider->subtitle != "")
                        <p class="slider-text hidden-xs">{{ $slider->subtitle }}</p>
                        @endif
                        @if($slider->url != "")
                            <a href="{{ $slider->url }}" class="btn btn-primary pull-right">Baca Lagi</a>
                        @endif

                    <!-- /.slider-captions -->
                    </div>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
</div>