<div class="section-space80">
    <div class="container">
        {{--<div class="row">--}}
            {{--<div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">--}}
                {{--<div class="mb60 text-center section-title">--}}
                    {{--<!-- section title start-->--}}
                    {{--<h1>Learn Help &amp; Guide</h1>--}}
                    {{--<p>Our mission is to deliver reliable, latest news and opinions.</p>--}}
                {{--</div>--}}
                {{--<!-- /.section title start-->--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            <?php $i=0; ?>
            @forelse(getRecentPosts() as $post)
                <?php $i++ ?>
                @if(($loop->index+1) % 3 == 1)
                <div class="row">
                @endif

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="post-block mb30">
                        <div class="post-img">
                            <a href="{{ route('blog_detail',['slug'=>$post->slug]) }}" class="imghover">
                                <img style="height: 300px;width: 100%" src="{{ $post->picture  }}" alt="{{ $post->title }}" class="img-responsive">
                            </a>
                        </div>
                        <div class="bg-white pinside40 outline" style="min-height: 200px;">
                            <h3><a href="{{ route('blog_detail',['slug'=>$post->slug]) }}" class="title">{{ $post->title }}</a></h3>
                            <p class="meta"><span class="meta-date">{{ $post->created_at->format('d M, Y') }}</span><span class="meta-author">By<a href="#"> {{ $post->authorId->name ?? "ADMIN" }}</a></span></p>
                        </div>
                    </div>
                </div>

                @if(($loop->index+1) % 3 == 0)
                </div>
                @endif

                @empty

            @endforelse
            @if(($i+1) % 3 == 0)
                </div>
            @endif

            <div class="row">
                <div class="text-center">
                    <a href="/blog" class="btn btn-primary">Lihat semua berita</a>
                </div>
            </div>


        </div>
    </div>
</div>