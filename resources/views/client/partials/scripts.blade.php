<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/client/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/client/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/client/js/menumaker.js"></script>
<!-- animsition -->
<script type="text/javascript" src="/client/js/animsition.js"></script>
<script type="text/javascript" src="/client/js/animsition-script.js"></script>
<!-- sticky header -->
<script type="text/javascript" src="/client/js/jquery.sticky.js"></script>
<script type="text/javascript" src="/client/js/sticky-header.js"></script>
<!-- slider script -->
<script type="text/javascript" src="/client/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="/client/js/slider-carousel.js"></script>
<script type="text/javascript" src="/client/js/service-carousel.js"></script>
<!-- Back to top script -->
<script src="/client/js/back-to-top.js" type="text/javascript"></script>
<script src="/client/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="/client/js/popup-gallery.js" type="text/javascript"></script>