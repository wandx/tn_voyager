<ul>
    @foreach($items as $menu_item)
        <li><a href="{{ $menu_item->url }}" class="animsition-link">{{ $menu_item->title }}</a></li>
    @endforeach

    @if(getAdditionalMenu()['count'] > 0)
        <li class="has-sub">
            <span class="submenu-button"></span>
            <a href="#" class="">Lainya</a>
            <ul>
                {{--<li><a href="/contact-us" title="Kontak Kami" class="animsition-link">Kontak Kami</a></li>--}}
                @foreach(getAdditionalMenu()['lists'] as $list)
                    <li>
                        <a href="{{ $list->url }}" title="{{ $list->name }}" class="animsition-link">{{ $list->name }}</a>
                    </li>
                @endforeach
            </ul>
        </li>
    @endif
</ul>