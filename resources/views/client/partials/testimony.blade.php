<div class="bg-white section-space80">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">
                <div class="mb60 text-center section-title">
                    <!-- section title start-->
                    <h1>Our Client Testimonial</h1>
                    <p>See what our customers have to say about Borrow products, people and services.</p>
                </div>
                <!-- /.section title start-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-4 clearfix col-xs-12">
                <div class="testimonial-block mb30 text-center">
                    <div class=" mb20 testimonial-img-1"> <img src="/client/images/testimonial-img.jpg" alt="Borrow - Loan Company Website Template" class="img-circle"> </div>
                    <div class="mb20">
                        <p class="testimonial-text"> “I loved the customer service you guys provided me. That was very nice and patient with questions I had. I would really like definitely come back here”</p>
                    </div>
                    <div class="testimonial-autor-box">
                        <div class="testimonial-autor">
                            <h4 class="testimonial-name-1">Donny J. Griffin</h4>
                            <span class="testimonial-meta">Personal Loan</span> </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-4 clearfix col-xs-12">
                <div class="testimonial-block mb30 text-center">
                    <div class="mb20 testimonial-img-1"> <img src="/client/images/testimonial-img-1.jpg" alt="Borrow - Loan Company Website Template" class="img-circle"> </div>
                    <div class="mb20">
                        <p class="testimonial-text">Donec arcu lacus, accumsan vel metus laoreet, pulvinar imperdiet nisi lorem sipusmn ras vel sapien imperdiet nisi sagittis ornare sed congue sem.</p>
                    </div>
                    <div class="testimonial-autor-box">
                        <div class="testimonial-autor">
                            <h4 class="testimonial-name-1">Mary O. Randle</h4>
                            <span class="testimonial-meta">Education Loan</span> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>