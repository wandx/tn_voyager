<div class="footer section-space80">
    <!-- footer -->
    <div class="container">

        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="widget-text mt40">
                    <!-- widget text -->
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                            <p class="address-text"><span><i class="icon-placeholder-3 icon-1x"></i> </span>{{ setting('site.address') }} </p>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <p class="call-text">
                                Hotline: {{ setting('site.hotline') }} <br>
                                Fax: {{ setting('site.fax') }} <br>
                                Email: {{ setting('site.email') }} <br>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.widget text -->
            </div>
            {{--<div class="col-md-2 col-sm-4 col-xs-6">--}}
                {{--<div class="widget-footer mt40">--}}
                    {{--<!-- widget footer -->--}}
                    {{--<ul class="listnone">--}}
                        {{--<li><a href="/page/profile">Profil</a></li>--}}
                        {{--<li><a href="/page/visi-misi">Visi & Misi</a></li>--}}
                        {{--<li><a href="/faq">FAQ</a></li>--}}
                        {{--<li><a href="/kegiatan">Kegiatan</a></li>--}}

                    {{--</ul>--}}
                {{--</div>--}}
                {{--<!-- /.widget footer -->--}}
            {{--</div>--}}
            {{--<div class="col-md-2 col-sm-4 col-xs-6">--}}
                {{--<div class="widget-footer mt40">--}}
                    {{--<!-- widget footer -->--}}
                    {{--<ul class="listnone">--}}
                        {{--<li><a href="/blog">Blog</a></li>--}}
                        {{--<li><a href="/gallery">Gallery</a></li>--}}
                        {{--<li><a href="/prestasi">Prestasi</a></li>--}}
                        {{--<li><a href="/file">Publikasi</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<!-- /.widget footer -->--}}
            {{--</div>--}}
            <div class="col-md-2 col-sm-4 col-xs-12 col-sm-offset-4">
                <div class="widget-social mt40">
                    <!-- widget footer -->
                    <ul class="listnone">
                        <li><a href="{{ setting('site.facebook') }}"><i class="fa fa-facebook"></i>Facebook</a></li>
                        <li><a href="{{ setting('site.gplus') }}"><i class="fa fa-google-plus"></i>Google Plus</a></li>
                        <li><a href="{{ setting('site.twitter') }}"><i class="fa fa-twitter"></i>Twitter</a></li>
                        <li><a href="{{ setting('site.linkedin') }}"><i class="fa fa-linkedin"></i>Linked In</a></li>
                    </ul>
                </div>
                <!-- /.widget footer -->
            </div>
        </div>
    </div>
</div>
<!-- /.footer -->
<div class="tiny-footer">
    <!-- tiny footer -->
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <p>© Copyright 2016 | Wandy Purnomo</p>
            </div>
        </div>
    </div>
</div>
<!-- /.tiny footer -->
<!-- /.tiny footer -->
<!-- back to top icon -->
<a href="#0" class="cd-top" title="Go to top">Top</a>