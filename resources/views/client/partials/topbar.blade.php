<div class="top-bar">
    <!-- top-bar -->
    <div class="container">
        <div class="row">
            <div class="col-md-4 hidden-xs hidden-sm">
                <p class="mail-text">{{ setting("site.welcome") }}</p>
            </div>
            <div class="col-md-3 hidden-xs hidden-sm">
                <p class="mail-text text-center">Hotline kami {{ setting('site.hotline') }}</p>
            </div>
            <div class="col-md-5 hidden-xs hidden-sm">
                <p class="mail-text text-center">{{ setting('site.jam_kerja') }}</p>
            </div>
            {{--<div class="col-md-2 hidden-xs hidden-sm">--}}
                {{--<p class="mail-text text-center">Get started today</p>--}}
            {{--</div>--}}
        </div>
    </div>
</div>
<!-- /.top-bar -->