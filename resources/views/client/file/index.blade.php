@extends('client.master')

@section('content')
    <div class="">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white pinside40">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <h1>Files</h1>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Judul</th>
                                            <th>Deskripsi</th>
                                            <th>Tanggal</th>
                                            <th>Uploader</th>
                                            <th class="text-center">Download</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($lists as $list)
                                            <tr>
                                                <td>{{ $list->title }}</td>
                                                <td>{{ $list->description }}</td>
                                                <td>{{ $list->created_at->format('d M Y') }}</td>
                                                <td>{{ $list->user->name ?? '-' }}</td>
                                                <td class="text-center">
                                                    <a target="_blank" href="{{ asset('/storage/'.json_decode($list->file)[0]->download_link) }}" class="btn btn-primary btn-xs">
                                                        <i class="fa fa-download"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td class="text-center" colspan="5">Tidak ada file</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                    <div class="col-md-12 text-center col-xs-12">
                                        <div class="st-pagination">
                                            {!! $lists->render() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop