@extends('client.master')
@section("meta_keyword")
    {{ $post->meta_keywords }}
@stop

@section("meta_description")
    {{ $post->meta_description}}
@stop

@section('content')
    <div class="">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white pinside40">
                        <div class="row">
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-holder">
                                            <div class="post-block mb40">
                                                @if($post->image_url != null || $post->image != null)
                                                    <div class="post-img mb30">
                                                        <a href="#" class="imghover"><img src="{{ asset($post->image_url == "" ? '/storage/'.$post->image:$post->image_url) }}" alt="{{ $post->title }}" class="img-responsive"></a>
                                                    </div>
                                                @endif
                                            </div>

                                            <div class="bg-white">
                                                <h1><a href="#" class="title">{{ $post->title }}</a></h1>
                                                <p class="meta"><span class="meta-date">{{ $post->created_at->format('M d, Y') }}</span><span class="meta-author">By<a href="#" class="meta-link"> {{ $post->authorId->name ?? "ADMIN" }}</a></span><span class="meta-comments"> <a
                                                                href="{{ route('blog',['category'=>$post->category['id']]) }}">{{ $post->category['name'] }}</a> </span></p>
                                                {!! $post->body !!}
                                            </div>
                                        </div>
                                        @include('client.blog.partials.related')
                                        @include('client.blog.partials.comment')
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="sidebar-area">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="bg-primary">
                                                <div class="widget widget-search">
                                                    <h3 class="widget-title">Search Bar</h3>
                                                    <form action="{{ route('blog') }}">
                                                        <div class="input-group">
                                                            <input type="text" name="title" class="form-control" placeholder="Search" value="{{ Request::input('title') }}">
                                                            <span class="input-group-btn"><button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button></span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @include('client.blog.partials.category')
                                        @include('client.blog.partials.recent')
                                        @include('client.blog.partials.tag')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop