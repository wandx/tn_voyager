@extends('client.master')

@section('content')
    <div class="">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white pinside40">
                        <div class="row">
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="row">
                                    @forelse($lists as $list)
                                        <div class="col-md-12 col-xs-12">
                                            <div class="post-holder">
                                                <div class="post-block mb40">
                                                    @if($list->image != null || $list->image_url != null)
                                                        <div class="post-img mb30">
                                                            <a href="{{ route('blog_detail',['slug'=>$list->slug]) }}" class="imghover"><img src="{{ asset($list->image_url == "" ? '/storage/'.$list->image:$list->image_url) }}" alt="{{ $list->title }}" class="img-responsive"></a>
                                                        </div>
                                                    @endif

                                                    <div class="bg-white">
                                                        <h1><a href="{{ route('blog_detail',['slug'=>$list->slug]) }}" class="title">{{ $list->title }}</a></h1>
                                                        <p class="meta"><span class="meta-date">{{ $list->created_at->format('M d, Y') }}</span><span class="meta-author">By<a href="#" class="meta-link"> {{ $list->authorId->name ?? "ADMIN" }}</a></span><span class="meta-comments"> <a
                                                                        href="{{ route('blog',['category'=>$list->category['id']]) }}">{{ $list->category['name'] }}</a> </span></p>
                                                        <p>{{ $list->short_body }}</p>
                                                        <a href="{{ route('blog_detail',['slug'=>$list->slug]) }}" class="btn-link">Read More</a> </div>
                                                </div>
                                            </div>
                                        </div>

                                        @empty
                                        <div class="well text-center">Tidak ada data</div>
                                    @endforelse

                                        <div class="col-md-12 text-center col-xs-12">
                                            <div class="st-pagination">
                                                {!! $lists->render() !!}
                                            </div>
                                        </div>

                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="sidebar-area">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="bg-primary">
                                                <div class="widget widget-search">
                                                    <h3 class="widget-title">Search Bar</h3>
                                                    <form action="{{ route('blog') }}">
                                                        <div class="input-group">
                                                            <input type="text" name="title" class="form-control" placeholder="Search" value="{{ Request::input('title') }}">
                                                            <span class="input-group-btn"><button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button></span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @include('client.blog.partials.category')
                                        @include('client.blog.partials.recent')
                                        @include('client.blog.partials.tag')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop