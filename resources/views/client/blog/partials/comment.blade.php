<div class="comments-area mb-40">
    <h2 class="comments-title">
        {{ $post->comments->count() }} komentar
    </h2>
    @forelse($post->comments as $comment)
    <ul class="comment-list listnone">

        <li class="comment">
            <div class="comment-body mb30">
                <div>
                    <div class="comment-author">
                        <img src="http://placehold.it/120x120" alt="">
                    </div>
                    <div class="comment-info">
                        <div class="comment-header">
                            <h4 class="user-title">{{ $comment->name }}</h4>
                            <div class="comment-meta">
                                <span class="comment-meta-date">{{ \Carbon\Carbon::parse($comment->created_at)->format('d M, Y') }}</span>
                            </div>
                        </div>
                        <div class="comment-content">
                            <p>{{ $comment->body }} </p>
                        </div>

                    </div>
                </div>
            </div>
        </li>


    </ul>
    @empty
        <ul class="comment-list listnone">
            <li class="comment">
                <div class="well">No Comment</div>
            </li>
        </ul>

    @endforelse
</div>
<div class="leave-comments pinside30 bg-primary mb30">
    <h2 class="reply-title">Tinggalkan komentar</h2>
    <form class="reply-form" action="{{ route('leave-comment') }}" method="post">
        {!! csrf_field() !!}
        <input type="hidden" name="post_id" value="{{ $post->id }}">
        <div class="row">
            <!-- Textarea -->
            <div class="form-group">
                <div class="col-md-12">
                    <label class="sr-only control-label" for="textarea"></label>
                    <textarea required class="form-control" id="textarea" name="body" rows="6" placeholder="Comments"></textarea>
                </div>
            </div>
            <div class="col-md-8">
                <!-- Text input-->
                <div class="form-group">
                    <label class="sr-only control-label" for="name"></label>
                    <input required id="name" name="name" type="text" class="form-control" placeholder="Name" required="">
                </div>
            </div>
            <!-- Text input-->
            <div class="col-md-8">
                <div class="form-group">
                    <label class="sr-only control-label" for="email"></label>
                    <input required id="email" name="email" type="text" class="form-control" placeholder="E-mail" required="">
                </div>
            </div>
            <div class="col-md-12">
                <!-- Button -->
                <div class="form-group">
                    <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-default">leave comments</button>
                </div>
            </div>
        </div>
    </form>
</div>