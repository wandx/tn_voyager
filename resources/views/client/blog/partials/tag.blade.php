<div class="col-md-12 ">
    <!--  tags -->
    <div class="bg-primary">
        <div class="widget widget-tags">
            <h3 class="widget-title mb30">Tags</h3>
            <!-- widget title -->
            @forelse(getTags() as $tag)
                <a href="{{ route('blog',['tag'=>$tag->id]) }}" title="{{ $tag->name }}">{{ $tag->name }}</a>
                @empty
                    No tag
            @endforelse
        </div>
        <!-- /.widget well bg -->
    </div>
</div>