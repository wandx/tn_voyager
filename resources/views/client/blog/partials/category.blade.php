<div class="col-md-12">
    <div class="bg-primary">
        <div class="widget widget-category">
            <h3 class="widget-title">Kategori</h3>
            <ul class="listnone bullet bullet-arrow-circle-right">
                @forelse(getCategories() as $category)
                    <li><a href="{{ route('blog',['category'=>$category->id]) }}">{{ $category->name }}</a></li>
                    @empty
                    <li>Tidak ada data</li>
                @endforelse
            </ul>
        </div>
    </div>
</div>