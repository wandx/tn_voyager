<div class="post-related mb40">
    <!-- post related -->
    <div class="row">
        <div class="col-md-12">
            <h2 class="post-related-title mb20">Related post</h2>
        </div>
    </div>
    <div class="row">
        @forelse(getRelated($post->slug) as $related)
            <div class="col-md-6 mb30 col-sm-6 col-xs-6">
                <div class="related-img mb20">
                    @if($related->image != null)
                        <a href="{{ route('blog_detail',['slug'=>$related->slug]) }}">
                            <img src="{{ $related->picture }}" class="img-responsive"
                                         alt="{{ $related->title }}"></a>
                    @endif

                </div>
                <div class="post-related-content">
                    <h4 class="related-title mb10"><a href="{{ route('blog_detail',['slug'=>$related->slug]) }}" class="title">{{ $related->title }}</a></h4>
                    <span> in <a href="{{ route('blog',['category'=>$related->category['id']]) }}" class="title"> “{{ $related->category['name'] }}” </a> </span></div>
            </div>
            @empty
                kosong
        @endforelse
        {{--<div class="col-md-6 mb30 col-sm-6 col-xs-6">--}}
            {{--<div class="related-img mb20">--}}
                {{--<a href="#"><img src="images/related-img-1.jpg" class="img-responsive" alt="Borrow - Loan Company Website Template"></a>--}}
            {{--</div>--}}
            {{--<div class="post-related-content">--}}
                {{--<h4 class="related-title mb10"><a href="#" class="title">Consider Consolidation Loan</a></h4>--}}
                {{--<span> in <a href="#" class="title"> “Home Loan” </a> </span> </div>--}}
        {{--</div>--}}
        {{--<div class="col-md-6 col-sm-6 col-xs-6">--}}
            {{--<div class="related-img mb20">--}}
                {{--<a href="#"><img src="images/related-img-2.jpg" class="img-responsive" alt="Borrow - Loan Company Website Template"></a>--}}
            {{--</div>--}}
            {{--<div class="post-related-content">--}}
                {{--<h4 class="related-title mb10"><a href="#" class="title">5 Essential financial Habits</a></h4>--}}
                {{--<span>in<a href="#" class="title"> “Debt Consolidation” </a> </span> </div>--}}
        {{--</div>--}}
    </div>
</div>