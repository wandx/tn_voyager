<div class="col-md-12">
    <div class="bg-primary">
        <div class="widget widget-recent-post">
            <h3 class="widget-title mb30">Recent post</h3>
            <ul class="listnone">
                @forelse(getRecentPosts() as $post)
                    <li>
                        <div class="recent-block mb20">
                            <div class="row">
{{--                                @if($post->image != null)--}}
                                    <div class="col-md-4 col-sm-2 col-xs-5">
                                        <a href="{{ route('blog_detail',['slug'=>$post->slug]) }}"><img src="{{ $post->picture }}" alt="{{ $post->title }}" class="img-responsive mb10"></a>
                                    </div>
                                {{--@endif--}}

                                <div class="col-md-8 col-sm-10 col-xs-7">
                                    <h4 class="recent-title mb10"><a href="{{ route('blog_detail',['slug'=>$post->slug]) }}" class="title">{{ $post->title }}</a></h4>
                                    <div class="meta">
                                        <!-- post meta -->
                                        <span class="meta-date">{{ $post->created_at->format('M d, Y') }} </span> </div>
                                    <!-- /.post meta -->
                                </div>
                            </div>
                        </div>
                    </li>
                    @empty
                        <li>Tidak ada post</li>
                @endforelse
            </ul>
        </div>
    </div>
</div>