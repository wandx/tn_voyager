@extends('client.master')

@section('content')
    <div class="">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white pinside40">
                        <div class="row">
                            @forelse($lists as $list)
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="">
                                    <div class="post-block mb30">
                                        @if($list->image != null)
                                            <div class="post-img mb30">
                                                <a href="{{ route('prestasi_detail',['slug'=>$list->slug]) }}" class="imghover"><img src="{{ asset('/storage/'.$list->image) }}" alt="{{ $list->title }}" class="img-responsive"></a>
                                            </div>
                                        @endif

                                        <div class="bg-white">
                                            <h1><a href="{{ route('prestasi_detail',['slug'=>$list->slug]) }}" class="title">{{ $list->title }}</a></h1>
                                            <p class="meta"><span class="meta-date">{{ $list->created_at->format('M d, Y') }}</span><span class="meta-author">By<a href="#" class="meta-link"> Admin</a></span><span class="meta-comments"> <a
                                                            href="{{ route('blog',['category'=>$list->category['id']]) }}">{{ $list->category['name'] }}</a> </span></p>
                                            <p>{{ $list->short_body }}</p>
                                            <a href="{{ route('prestasi_detail',['slug'=>$list->slug]) }}" class="btn-link">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @empty
                                    <div class="well text-center">Tidak ada data</div>
                            @endforelse
                                <div class="col-md-12 text-center col-xs-12">
                                    <div class="st-pagination">
                                        {!! $lists->render() !!}
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop