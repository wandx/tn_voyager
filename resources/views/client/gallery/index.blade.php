@extends('client.master')

@section('content')
    <div class=" ">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="wrapper-content clearfix" style="margin-bottom: 50px;">
                    <div class="col-sm-3 col-sm-offset-6">
                        <select name="sort" id="sort" class="form-control" style="height: 44px">
                            <option value="">Pilih Urutan</option>
                            <option value="newest">Terbaru</option>
                            <option value="oldest">Terlama</option>
                        </select>
                    </div>
                <div class="col-sm-3">
                    <form action="" method="get">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" id="search-gallery" style="height: 44px" placeholder="Cari...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    </form>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white pinside40">
                        <div class="row">
                            @forelse($lists as $list)
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="gallery-block text-center mb30">
                                        <div class="gallery-img mb30">
                                            @if($list->video == 1)
                                                <a data-desc="{{ $list->desc }}" href="{{ $list->video_url }}" class="video-link" title="{{ $list->caption }}" data-toggle="tooltip"><img src="{{ $list->image_url == "" ? \TCG\Voyager\Facades\Voyager::image(getImage($list->path,'-cropped')):$list->image_url }}" alt="" class="img-responsive"></a>
                                            @else
                                                <a data-desc="{{ $list->desc }}" href="{{ $list->image_url == "" ? \TCG\Voyager\Facades\Voyager::image(getImage($list->path)) :$list->image_url}}" class="image-link"  title="{{ $list->caption }}" data-toggle="tooltip"><img src="{{ $list->image_url == "" ? \TCG\Voyager\Facades\Voyager::image(getImage($list->path,'-cropped')) :$list->image_url}}" alt="" class="img-responsive"></a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @empty
                            @endforelse


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function(){
            $("[data-toggle=tooltip]").tooltip();

            $('#sort').change(function(){
                window.location = "/gallery?sort="+$(this).val();
            });
        })


    </script>
@stop