<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class VoSeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voseed:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $c2 = 'php '.base_path('artisan').' iseed menu_items,menus,pages,permission_role,permissions,roles,settings,users,data_rows,data_types,translations,categories,menu_items,permission_groups --force';
        exec($c2);
    }
}
