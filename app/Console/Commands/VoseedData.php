<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class VoseedData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voseed:backup-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $c2 = 'php '.base_path('artisan').' iseed categories,events,faqs,files,galleries,jadwals,lkips,m_s_is,menu_histories,pages,post_comments,post_tag,posts,sliders,tags --force';
        exec($c2);
    }
}
