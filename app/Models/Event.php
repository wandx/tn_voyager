<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = ['id'];
    protected $appends = [
        'start','end'
    ];

    public function getStartAttribute(){
        return $this->start_at;
    }

    public function getEndAttribute(){
        return $this->end_at;
    }
}
