<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MSIDesc extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
