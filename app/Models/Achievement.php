<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Achievement extends Model
{

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
        self::creating(function($m){
            $m->slug = Str::slug($m->title);
        });

        self::updating(function($m){
            $m->slug = Str::slug($m->title);
        });
    }
}
