<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdditionalMenu extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
