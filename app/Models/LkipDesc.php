<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LkipDesc extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
