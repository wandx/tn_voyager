<?php

// Build menu



function deleteImage($path){
    try{
        if(file_exists($path) && !is_dir($path)){
            unlink($path);
        }
    }catch (Exception $e){}
}

function oleh(){
    if(auth('admin')->check()){
        return true;
    }
    return false;
}

function sliderData(){
    return (new \App\Models\Slider())->newQuery()->get();
}


function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
}

function isSU(){
    return session('role_id') == 1 ? true:false;
}

function getCategories(){
    return (new \TCG\Voyager\Models\Category())->newQuery()->has('posts','>',0)->get();
}

function getRecentPosts(){
    return (new \App\Models\Post())->newQuery()->whereStatus("PUBLISHED")->latest()->take(9)->get();
}

function getTags(){
    return (new \App\Models\Tag())->newQuery()->has('posts','>',0)->get();
}

function getRelated($slug){
    $post = new \App\Models\Post();

    $data = $post->newQuery()->where('slug',$slug)->first();
//    return $data->tags->pluck('id');

    $q = function($query) use ($slug,$data){
        $query->whereCategoryId($data->category->id)->whereStatus("PUBLISHED")->orWhereHas("tags",function($model) use ($data){
            $model->whereIn('id',$data->tags->pluck('id'));
        });
    };

    return $post->newQuery()->where($q)->take(2)->get();

//    return $post->newQuery()->where('category_id',$data->category['id'])->orWhereHas('tags',function($model) use ($data){
//        $model->whereIn('id',$data->tags->pluck('id'));
//    })->where('status','PUBLISHED')->take(2)->get();
}

function getImage($img,$additional=""){
    try{
        $explode = explode('.',$img);

        $last = $explode[1];
        $name = $explode[0];

        return $name.$additional.'.'.$last;
    }catch (Exception $e){
        return "";
    }


}

function getAdditionalMenu(){
    $x = (new \App\Models\AdditionalMenu())->newQuery();
    $data = [
        'lists' => $x->get(),
        'count' => $x->count()
    ];
    return $data;
}