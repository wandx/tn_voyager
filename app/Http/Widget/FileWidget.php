<?php namespace App\Widget;

use App\Models\File;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Str;

class FileWidget extends AbstractWidget{
    protected $config = [];

    public function run()
    {
        $count = (new File())->newQuery()->count();
        $string = trans_choice('voyager.dimmer.file', $count);

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-folder',
            'title'  => "{$count} {$string}",
            'text'   => __('voyager.dimmer.file_text', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('voyager.dimmer.file_link_text'),
                'link' => '/backoffice/files',
            ],
            'image' => voyager_asset('images/widget-backgrounds/01.jpg'),
        ]));
    }
}