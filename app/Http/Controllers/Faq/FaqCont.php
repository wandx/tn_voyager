<?php

namespace App\Http\Controllers\Faq;

use App\Models\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqCont extends Controller
{
    public function index(){
        $data = [
            'lists' => (new Faq())->newQuery()->get()
        ];

        return view('client.faq.faq',$data);
    }
}
