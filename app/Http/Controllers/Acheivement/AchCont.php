<?php

namespace App\Http\Controllers\Achievement;

use App\Models\Achievement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AchCont extends Controller
{
    public function index(){
        $data = [
            'lists' => (new Achievement())->newQuery()->paginate(12)
        ];

        return view('client.acheivement.index',$data);

    }

    public function detail($slug){
        $data = [
            'post' => (new Achievement())->newQuery()->whereSlug($slug)->first()
        ];

        return view('client.acheivement.detail',$data);
    }
}
