<?php

namespace App\Http\Controllers\File;

use App\Models\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileCont extends Controller
{
    public function index(File $file){
        $data = [
            'lists' => $file->newQuery()->latest()->paginate(15)
        ];
        return view('client.file.index',$data);
    }
}
