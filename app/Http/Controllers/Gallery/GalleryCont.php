<?php

namespace App\Http\Controllers\Gallery;

use Aidantwoods\SecureHeaders\SecureHeaders;
use App\Models\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryCont extends Controller
{
//    public function __construct()
//    {
//        $headers = new SecureHeaders();
//        $headers->apply();
//    }

    public function index(Request $request,Gallery $gallery){
        $data = [
            'lists' => $this->lists($request,$gallery)
        ];

        return view('client.gallery.index',$data);
    }

    public function lists(Request $request,Gallery $gallery){
        $gallery = $gallery->newQuery();

        $query = function($model) use ($request){
            if($request->has('q')){
                $model->where('caption','like','%'.$request->input('q').'%');
            }
        };

        if($request->has('sort')){
            if($request->input('sort') == "oldest"){
                $gallery->orderBy('created_at','asc');
            }else{
                $gallery->orderBy('created_at','desc');
            }
        }

        $gallery->where($query);
        return $gallery->get();
    }
}
