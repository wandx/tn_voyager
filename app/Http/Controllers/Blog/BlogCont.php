<?php

namespace App\Http\Controllers\Blog;

use App\Models\PostComment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;

class BlogCont extends Controller
{
    public function index(Request $request,Post $post){
        $data = [
            'lists' => $this->blogQuery($request,$post)
        ];

        return view('client.blog.index',$data);
    }

    public function detail($slug, Post $post){
        $data = [
            'post' => $post->newQuery()->where('slug',$slug)->first()
        ];

        return view('client.blog.detail',$data);
    }

    public function blogQuery(Request $request,Post $post){
        $post = $post->newQuery();
        $post->orderBy('featured','desc');
        $post->latest();
        $post->whereStatus('published');
        if($request->has('title')){
            $post->where('title','like','%'.$request->input('title').'%');
        }

        if($request->has('category')){
            $post->where('category_id',$request->input('category'));
        }

        if($request->has('tag')){
            $post->whereHas('tags',function($model) use ($request){
                $model->where('id',$request->input('tag'));
            });
        }


        return $post->paginate(3);
    }

    public function store_comment(Request $request,PostComment $comment){
        $data = [
            'body' => $request->input('body'),
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'post_id' => $request->input('post_id')
        ];

        $comment->newQuery()->create($data);
        return redirect()->back();
    }
}
