<?php

namespace App\Http\Controllers\Event;

use App\Models\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventCont extends Controller
{
    public function index(Event $event){
        $data = [
            'lists' => $event->newQuery()->get()
        ];

        return view('client.calendar.index',$data);
    }
}
