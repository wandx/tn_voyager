<?php

namespace App\Http\Controllers\Contact;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactCont extends Controller
{
    public function index(){
        return view('client.contact.index');
    }

    public function sendMessage(Request $request,Contact $contact){
        $validator = app('validator')->make($request->all(),[
            'nama' => 'required',
            'telp' => 'numeric',
            'email' => 'required|email',
            'pesan' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->messages());
        }

        $data = [
            'name' => $request->input('nama'),
            'phone' => $request->input('telp'),
            'email' => $request->input('email'),
            'message' => $request->input('pesan')
        ];

        $contact->newQuery()->create($data);
        return redirect()->back()->with(['success'=>'Pesan telah dikirim, terimakasih']);
    }
}
