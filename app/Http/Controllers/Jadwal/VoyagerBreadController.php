<?php

namespace App\Http\Controllers\Jadwal;

use App\Models\Jadwal;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;

class VoyagerBreadController extends BaseVoyagerBreadController
{
    public function importCSV(Request $request,Jadwal $jadwal){
        if (($handle = fopen ( $request->file('file'), 'r' )) !== FALSE) {
            try{
                while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                    $jadwal->newQuery()->create([
                        'kelas' => $data[0],
                        'hari' => $data[1],
                        'mapel' => $data[2] == "-" ? null:$data[2],
                        'guru' => $data[3] == "-" ? null:$data[3],
                        'waktu' => $data[4],
                        'keterangan' =>$data[5] == "-" ? null:$data[5]
                    ]);
                }
                return redirect()
                    ->back()
                    ->with([
                        'message'    => "Import Berhasil",
                        'alert-type' => 'success',
                    ]);
            }catch(\Exception $e){
                return redirect()
                    ->back()
                    ->with([
                        'message'    => "Import Gagal ".$e->getMessage(),
                        'alert-type' => 'error',
                    ]);
            }

        }

        return redirect()
            ->back()
            ->with([
                'message'    => "Import Gagal",
                'alert-type' => 'error',
            ]);
    }
}
