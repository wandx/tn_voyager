<?php

namespace App\Http\Controllers\Activity;

use App\Models\Activity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivityCont extends Controller
{
    public function index(){
        $data = [
            'lists' => (new Activity())->newQuery()->paginate(12)
        ];

        return view('client.activity.index',$data);

    }

    public function detail($slug){
        $data = [
            'post' => (new Activity())->newQuery()->whereSlug($slug)->first()
        ];

        return view('client.activity.detail',$data);
    }
}
