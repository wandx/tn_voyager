<?php

namespace App\Http\Controllers\Lomba\MSI;

use App\Models\MSI;
use App\Models\MSIDesc;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class MsiCont extends Controller
{
    public function index(){
        $data = [
            'desc' => (new MSIDesc())->newQuery()->first()
        ];
        return view('client.lomba.msi.index',$data);
    }

    public function download(){
        $data = (new MSI())->newQuery()->get();
        $filename = "msi.csv";
        $handle = fopen(storage_path($filename), 'w+');
        fputcsv($handle,[
            'Nama','Email','Tanggal Lahir','Alamat','Telpon','Sekolah Asal',
            'Alamat Sekolah Asal','Kelas'
        ]);

        foreach ($data as $l){
            fputcsv($handle,[
                $l->name,$l->email,$l->dob,$l->address,$l->phone,$l->origin,
                $l->origin_address,$l->class
            ]);
        }

        fclose($handle);

        return response()->download(storage_path($filename),$filename,[
            'Content-Type' => 'text/csv'
        ]);
    }

    public function store(Request $request){
        $validator = app('validator')->make($request->all(),[
            'name' => 'required',
            'dob' => 'required|date',
            'address' => 'required',
            'telp' => 'required',
            'origin' => 'required',
            'origin-address' => 'required',
            'kelas' => 'required',
            'file-scan' => 'required',
            'captcha' => 'required|captcha',
            'email' => 'required|unique:m_s_is',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->messages());
        }

        $data = [
            'name' => $request->input('name'),
            'dob' => Carbon::parse($request->input('dob'))->format('Y-m-d'),
            'address' => $request->input('address'),
            'phone' => $request->input('telp'),
            'email' => $request->input('email'),
            'origin' => $request->input('origin'),
            'origin_address' => $request->input('origin-address'),
            'class' => strtoupper($request->input('kelas')),
        ];

        if($request->hasFile('file-scan')){
            $name = Str::slug($request->input('name')).'_'.str_random(6).'.jpg';
            $path_to_save = 'msi/kp/';
            $actual_path = storage_path('app/public/'.$path_to_save);
            $filename = $actual_path.$name;

            try{
                if(!is_dir($actual_path)){
                    mkdir($actual_path,0777,true);
                }

                Image::make($request->file('file-scan'))->save($filename);
                $data['file'] = $path_to_save.$name;

            }catch (\Exception $e){}

            (new MSI())->newQuery()->create($data);
            return redirect()->back()->with(['success'=>'Berhasil dikirim.']);
        }
    }
}
