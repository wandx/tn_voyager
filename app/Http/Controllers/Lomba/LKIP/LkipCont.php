<?php

namespace App\Http\Controllers\Lomba\LKIP;

use App\Models\Lkip;
use App\Models\LkipDesc;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class LkipCont extends Controller
{
    public function index(){
        $data = [
            'desc' => (new LkipDesc())->newQuery()->first()
        ];
        return view('client.lomba.lkip.index',$data);
    }

    public function download(){
        $data = (new Lkip())->newQuery()->get();
        $filename = "lkip.csv";
        $handle = fopen(storage_path($filename), 'w+');
        fputcsv($handle,[
            'Nama Ketua Tim','Email Ketua Tim','Tanggal Lahir Ketua Tim','Alamat Ketua Tim','Telpon Ketua Tim', 'Nilai Rata2 Ketua Tim','Kelas Ketua Tim',
            'Nama Anggota','Email Anggota','Tanggal Lahir Anggota','Alamat Anggota','Telpon Anggota', 'Nilai Rata2 Anggota', 'Kelas Anggota',
            'Sekolah Asal', 'Alamat Sekolah Asal'
        ]);

        foreach ($data as $l){
            fputcsv($handle,[
                $l->team_leader,$l->team_leader_email,$l->team_leader_dob,$l->team_leader_address,$l->team_leader_phone,$l->average,$l->team_leader_class,
                $l->team_member,$l->team_member_email,$l->team_member_dob,$l->team_member_address,$l->team_member_phone,$l->average,$l->team_member_class,
                $l->origin,$l->origin_address
            ]);
        }

        fclose($handle);

        return response()->download(storage_path($filename),$filename,[
            'Content-Type' => 'text/csv'
        ]);
    }

    public function store(Request $request){
        $validator = app('validator')->make($request->all(),[
            'team-name' => 'required',
            'name' => 'required',
            'dob' => 'required|date',
            'address' => 'required',
            'telp' => 'required',
            'origin' => 'required',
            'origin-address' => 'required',
            'kelas' => 'required',
            'file-scan' => 'required',
            'name-member' => 'required',
            'dob-member' => 'required|date',
            'address-member' => 'required',
            'telp-member' => 'required',
            'kelas-member' => 'required',
            'file-scan-member' => 'required',
            'judul-makalah'=>'required',
            'captcha' => 'required|captcha',
            'email' => 'rquired|unique:lkips'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->messages());
        }

        $data = [
            'team_name' => $request->input('team-name'),
            'average' => $request->input('average',0),
            'team_leader' => $request->input('name'),
            'team_leader_dob' => Carbon::parse($request->input('dob'))->format('Y-m-d'),
            'team_leader_address' => $request->input('address'),
            'team_leader_phone' => $request->input('telp'),
            'team_leader_email' => $request->input('email'),
            'team_leader_class' => strtoupper($request->input('kelas')),
            'team_member' => $request->input('name'),
            'team_member_dob' => Carbon::parse($request->input('dob-member'))->format('Y-m-d'),
            'team_member_address' => $request->input('address-member'),
            'team_member_phone' => $request->input('telp-member'),
            'team_member_email' => $request->input('email-member'),
            'team_member_class' => strtoupper($request->input('kelas-member')),
            'judul_makalah' => $request->input('judul-makalah'),
            'origin' => $request->input('origin'),
            'origin_address' => $request->input('origin-address'),
        ];

        if($request->hasFile('file-scan')){
            $name = Str::slug($request->input('name')).'_'.str_random(6).'.jpg';
            $path_to_save = 'lkip/kp/';
            $actual_path = storage_path('app/public/'.$path_to_save);
            $filename = $actual_path.$name;

            try{
                if(!is_dir($actual_path)){
                    mkdir($actual_path,0777,true);
                }

                Image::make($request->file('file-scan'))->save($filename);
                $data['team_leader_file'] = $path_to_save.$name;

            }catch (\Exception $e){}
        }

        if($request->hasFile('file-scan-member')){
            $name = Str::slug($request->input('name-member')).'_'.str_random(6).'.jpg';
            $path_to_save = 'lkip/kp/';
            $actual_path = storage_path('app/public/'.$path_to_save);
            $filename = $actual_path.$name;

            try{
                if(!is_dir($actual_path)){
                    mkdir($actual_path,0777,true);
                }

                Image::make($request->file('file-scan-member'))->save($filename);
                $data['team_member_file'] = $path_to_save.$name;

            }catch (\Exception $e){}
        }

        (new Lkip())->newQuery()->create($data);
        return redirect()->back()->with(['success'=>'Berhasil dikirim.']);
    }
}
