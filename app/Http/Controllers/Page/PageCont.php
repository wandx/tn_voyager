<?php

namespace App\Http\Controllers\Page;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageCont extends Controller
{
    public function index($slug,Page $page){
        $data = [
            'page' => $page->newQuery()->whereSlug($slug)->first()

        ];

        return view('client.page.index',$data);
    }
}
