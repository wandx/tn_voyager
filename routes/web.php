<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('client.homepage.index');
});

Route::group(['prefix'=>'blog','namespace'=>'Blog'],function(){
    Route::get('/','BlogCont@index')->name('blog');
    Route::get('/{slug}','BlogCont@detail')->name('blog_detail');
    Route::post('leave-comment','BlogCont@store_comment')->name('leave-comment');
});

Route::group(['prefix'=>'file','namespace'=>'File'],function(){
    Route::get('/','FileCont@index')->name('file');
});

Route::group(['prefix'=>'contact-us'],function(){
    Route::get('/','Contact\ContactCont@index')->name('contact');
    Route::post('/','Contact\ContactCont@sendMessage')->name('contact_send');
});

Route::get('page/{slug}','Page\PageCont@index');
Route::get('faq','Faq\FaqCont@index');
Route::group(['prefix'=>'prestasi','namespace'=>'Achievement'],function(){
    Route::get('/','AchCont@index')->name('prestasi');
    Route::get('/{slug}','AchCont@detail')->name('prestasi_detail');
});

Route::group(['prefix'=>'kegiatan','namespace'=>'Activity'],function(){
    Route::get('/','ActivityCont@index')->name('kegiatan');
    Route::get('/{slug}','ActivityCont@detail')->name('kegiatan_detail');
});

Route::group(['prefix'=>'gallery','namespace'=>'Gallery'],function(){
    Route::get('/','GalleryCont@index')->name('gallery');
});

Route::group(['namespace'=>'Lomba','prefix'=>'lomba'],function(){
    Route::group(['namespace'=>'MSI','prefix'=>'msi'],function(){
        Route::get('/','MsiCont@index')->name('lomba.msi');
        Route::post('/','MsiCont@store')->name('lomba.msi.store');
    });

    Route::group(['namespace'=>'LKIP','prefix'=>'lkip'],function(){
        Route::get('/','LkipCont@index')->name('lomba.lkip');
        Route::post('/','LkipCont@store')->name('lomba.lkip.store');
    });
});

Route::group(['prefix' => 'backoffice'], function () {
    Voyager::routes();
});

Route::get('events','Event\EventCont@index');
Route::post('/backoffice/import_csv_event','Calendar\VoyagerBreadController@importCSV')->name('import_csv');
Route::get('jadwal',function(){
    $data = [
        'lists' => (new \App\Models\Jadwal())->newQuery()->get()
    ];
    return view('client.jadwal.index',$data);
});

Route::post('/backoffice/import_csv_jadwal','Jadwal\VoyagerBreadController@importCSV')->name('import_csv_jadwal');
Route::get('/backoffice/download_msi','Lomba\MSI\MsiCont@download');
Route::get('/backoffice/download_lkip','Lomba\LKIP\LkipCont@download');
